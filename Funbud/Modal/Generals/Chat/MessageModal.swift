//
//  MessageModal.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 10/8/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation
import Firebase
import MessageKit

class MessageModal : MessageType {
    
    var pMessageId: String!
    var pDate : Timestamp!
    var pIsread : Bool!
    var pMessage : String!
    var pMessageuser : MessageuserModal!
    var pOwner : String!
    
    /// The sender of the message.
    var sender: SenderType {
        return pMessageuser
    }

    /// The unique identifier for the message.
    var messageId: String {
        return pMessageId
    }

    /// The date the message was sent.
    var sentDate: Date {
        if pDate != nil {
            return DateTimeManager.shared.dateFrom(unix: Int(pDate.seconds))
        }
        return Date()
    }
    
    /// The kind of message and its underlying kind.
    var kind: MessageKind {
        return MessageKind.text(pMessage)
    }
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        pDate = dictionary["date"] as? Timestamp
        pIsread = dictionary["isread"] as? Bool
        pMessage = dictionary["message"] as? String
        pMessageId = dictionary["messageid"] as? String
        if let messageuserData = dictionary["messageuser"] as? [String:Any]{
            pMessageuser = MessageuserModal(fromDictionary: messageuserData)
        }
        pOwner = dictionary["owner"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if pDate != nil{
            dictionary["date"] = pDate
        }
        if pIsread != nil{
            dictionary["isread"] = pIsread
        }
        if pMessage != nil{
            dictionary["message"] = pMessage
        }
        if pMessageId != nil{
            dictionary["messageid"] = pMessageId
        }
        if pMessageuser != nil{
            dictionary["messageuser"] = pMessageuser.toDictionary()
        }
        if pOwner != nil{
            dictionary["owner"] = pOwner
        }
        return dictionary
    }
}
