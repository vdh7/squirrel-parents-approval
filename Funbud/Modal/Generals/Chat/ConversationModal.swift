//
//  ConversationModal.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 10/5/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation
import Firebase

class ConversationModal: NSObject, NSCoding{
    
    var conversationId: String? {
        didSet {
            updatePlaydateName()
        }
    }
    var lastMsg : String?
    var name : String? {
        didSet {
            didUpdateBlock?()
        }
    }
    var owner : String?
    var kidsList : [String] = [] {
        didSet {
            updateKidsName()
        }
    }
    var createdAt : Timestamp!
    var updatedAt : Timestamp!
    var avatar : String?
    var access: [String]?
    var deviceTokens: [String] = []
    var isForBabysitter : String = "0"
    
    var kidsName: String?
    var username: String?
    
    private var kidsNames: [String] = []
    
    var isSubscribe: Bool = false
    var groupEvent: String = "0"
    
    var didUpdateBlock:(()->Void)?
        
    var playDatesModal: PlayDatesModal?
    
    func getLastMessageTime() -> String {
        if updatedAt != nil {
            let updatedDate = DateTimeManager.shared.dateFrom(unix: Int(updatedAt.seconds))
            if DateTimeManager.shared.isToday(updatedDate) {
                return DateTimeManager.shared.stringFrom(date: updatedDate, inFormate: DateFormate.HH_MM)
            } else {
                return DateTimeManager.shared.stringFrom(date: updatedDate, inFormate: DateFormate.MMM_DD_COM_yyyy_HH_MM)
            }
        }
        if createdAt != nil {
            let createdDate = DateTimeManager.shared.dateFrom(unix: Int(createdAt.seconds))
            if DateTimeManager.shared.isToday(createdDate) {
                return DateTimeManager.shared.stringFrom(date: createdDate, inFormate: DateFormate.HH_MM)
            } else {
                return DateTimeManager.shared.stringFrom(date: createdDate, inFormate: DateFormate.MMM_DD_COM_yyyy_HH_MM)
            }
        }
        return String()
    }
    
    func updateKidsName() {
        kidsNames.removeAll()
        for kid in kidsList {
            FirebaseManager.shared.findKid(fromID: kid) { kidsModal in
                self.kidsNames.append(kidsModal?.name ?? String())
                self.kidsName = self.kidsNames.joined(separator: ", ")
                if self.isForBabysitter == "1" {
                    self.name = self.kidsName
                } else if self.groupEvent == "1" {
                    self.name = self.playDatesModal?.title
                } else if self.name?.isEmpty == true || self.name == nil {
                    self.name = self.kidsName
                }
            }
        }
    }
    
    func updatePlaydateName() {
        if let conversationId = conversationId {
            FirebaseManager.shared.findPlaydateModal(byID: conversationId) { (playdateModal: PlayDatesModal?) in
                self.playDatesModal = playdateModal
                if self.isForBabysitter == "1" {
                    self.name = self.kidsName
                } else if playdateModal?.groupEvent == true {
                    self.name = playdateModal?.title
                } else {
                    self.name = self.kidsName
                }
            }
        }
    }
    
    func displayKidsCount() -> (Bool, String) {
        /*if kidsList.count > 2 {
            let stringToReturn = String(format: "+%d", 2)
            return (false, stringToReturn)
        } else if kidsList.count == 2 {
            let stringToReturn = String(format: "%d", kidsList.count)
            return (false, stringToReturn)
        } else {
            return (true, String())
        }*/
        
        if kidsList.count > 1 {
            let stringToReturn = String(format: "+%d", kidsList.count - 2)
            return (false, stringToReturn)
        } else {
            return (true, String())
        }
    }
    
    func displayChatDetailsKidsName() -> String {
        
        let fullname = self.kidsName ?? String()
        var aNames = fullname.components(separatedBy: ", ")
        let fName = aNames.first ?? String()
        aNames.removeFirst()
        let lName = aNames.first ?? String()
        
        if kidsList.count > 2 {
            let stringToReturn = String(format: "%@, %@ +%d", fName, lName, kidsList.count - 2)
            return stringToReturn
        } else if kidsList.count == 2 {
            let stringToReturn = String(format: "%@, %@", fName, lName)
            return stringToReturn
        } else {
            return String()
        }
    }
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]) {
        
        conversationId = dictionary["conversationId"] as? String
        lastMsg = dictionary["last_msg"] as? String
        //name = dictionary["name"] as? String
        owner = dictionary["owner"] as? String
        kidsList = dictionary["kidsList"] as? [String] ?? []
        createdAt = dictionary["createdAt"] as? Timestamp
        updatedAt = dictionary["updatedAt"] as? Timestamp
        avatar = dictionary["avatar"] as? String
        access = dictionary["access"] as? [String] ?? []
        isForBabysitter = dictionary["isForBabysitter"] as? String ?? "0"
        deviceTokens = dictionary["deviceTokens"] as? [String] ?? []
        groupEvent = dictionary["groupEvent"] as? String ?? "0"
        
        super.init()
        
        self.updateKidsName()
        self.updatePlaydateName()
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if conversationId != nil{
            dictionary["conversationId"] = conversationId
        }
        if lastMsg != nil{
            dictionary["last_msg"] = lastMsg
        }
        /*if name != nil{
            dictionary["name"] = name
        }*/
        if owner != nil{
            dictionary["owner"] = owner
        }
        
        dictionary["kidsList"] = kidsList
        
        /*if createdAt != nil{
            dictionary["createdAt"] = createdAt
        }
        if updatedAt != nil{
            dictionary["updatedAt"] = updatedAt
        }*/
        if avatar != nil{
            dictionary["avatar"] = avatar
        }
        if access != nil{
            dictionary["access"] = access
        }
        dictionary["isForBabysitter"] = isForBabysitter
        dictionary["deviceTokens"] = deviceTokens
        dictionary["groupEvent"] = groupEvent
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        conversationId = aDecoder.decodeObject(forKey: "conversationId") as? String
        lastMsg = aDecoder.decodeObject(forKey: "last_msg") as? String
        //name = aDecoder.decodeObject(forKey: "name") as? String
        owner = aDecoder.decodeObject(forKey: "owner") as? String
        kidsList = aDecoder.decodeObject(forKey: "kidsList") as? [String] ?? []
        createdAt = aDecoder.decodeObject(forKey: "createdAt") as? Timestamp
        updatedAt = aDecoder.decodeObject(forKey: "updatedAt") as? Timestamp
        avatar = aDecoder.decodeObject(forKey: "avatar") as? String
        access = aDecoder.decodeObject(forKey: "access") as? [String] ?? []
        isForBabysitter = aDecoder.decodeObject(forKey: "isForBabysitter") as? String ?? "0"
        deviceTokens = aDecoder.decodeObject(forKey: "deviceTokens") as? [String] ?? []
        groupEvent = aDecoder.decodeObject(forKey: "groupEvent") as? String ?? "0"
        
        super.init()
        
        self.updateKidsName()
        self.updatePlaydateName()
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if conversationId != nil {
            aCoder.encode(conversationId, forKey: "conversationId")
        }
        if lastMsg != nil{
            aCoder.encode(lastMsg, forKey: "last_msg")
        }
        /*if name != nil{
            aCoder.encode(name, forKey: "name")
        }*/
        if owner != nil{
            aCoder.encode(owner, forKey: "owner")
        }
        
        aCoder.encode(kidsList, forKey: "kidsList")
        
        /*if createdAt != nil{
            aCoder.encode(createdAt, forKey: "createdAt")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updatedAt")
        }*/
        if avatar != nil{
            aCoder.encode(avatar, forKey: "avatar")
        }
        if access != nil{
            aCoder.encode(access, forKey: "access")
        }
        aCoder.encode(isForBabysitter, forKey: "isForBabysitter")
        aCoder.encode(deviceTokens, forKey: "deviceTokens")
        aCoder.encode(groupEvent, forKey: "groupEvent")
    }
}
