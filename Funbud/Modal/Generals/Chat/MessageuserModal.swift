//
//  MessageuserModal.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 10/8/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation
import MessageKit

class MessageuserModal : SenderType {
    
    /// Note: This value must be unique across all senders.
    var senderId: String {
        return id
    }

    /// The display name of a sender.
    var displayName: String {
        return name
    }

    var avatar : String!
    var id : String!
    var name : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        avatar = dictionary["avatar"] as? String
        id = dictionary["id"] as? String
        name = dictionary["name"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if avatar != nil{
            dictionary["avatar"] = avatar
        }
        if id != nil{
            dictionary["id"] = id
        }
        if name != nil{
            dictionary["name"] = name
        }
        return dictionary
    }
}
