//
//	RootClass.swift
//
//	Create by Dharmesh Avaiya on 8/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ParentsModal : BaseModal {
		
	var kidsList : [String]!
	var number : String!
       
    func getGender() -> String {
        if gender == "2" {
            return genderText ?? String()
        } else {
            return gender == "0" ? ParentsType.mother.rawValue : ParentsType.father.rawValue
        }
    }
    
    func findKid(completion: @escaping(_ kids: [KidsModal])->Void) {
        FirebaseManager.shared.find(kidsFrom: userid ?? String()) { kids in
            completion(kids)
        }
    }
    
	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
    override init(fromDictionary dictionary: [String:Any]){
        super.init(fromDictionary: dictionary)
        
		/*kidsList = [KidsModal]()
		if let kidsListArray = dictionary["kidsList"] as? [[String:Any]]{
			for dic in kidsListArray{
				let value = KidsModal(fromDictionary: dic)
				kidsList.append(value)
			}
		}*/
        kidsList = dictionary["kidsList"] as? [String]
		number = dictionary["number"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
    override func toDictionary() -> [String:Any]
	{
        var dictionary = super.toDictionary()
		/*if kidsList != nil{
			var dictionaryElements = [[String:Any]]()
			for kidsListElement in kidsList {
				dictionaryElements.append(kidsListElement.toDictionary())
			}
			dictionary["kidsList"] = dictionaryElements
		}*/
        if kidsList != nil {
            dictionary["kidsList"] = kidsList
        }
		if number != nil {
			dictionary["number"] = number
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        super.init(coder: aDecoder)
        
         //kidsList = aDecoder.decodeObject(forKey :"kidsList") as? [KidsModal]
        kidsList = aDecoder.decodeObject(forKey :"kidsList") as? [String]
        number = aDecoder.decodeObject(forKey: "number") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc override func encode(with aCoder: NSCoder)
	{
        super.encode(with: aCoder)
        		
		if kidsList != nil{
			aCoder.encode(kidsList, forKey: "kidsList")
		}
		if number != nil{
			aCoder.encode(number, forKey: "number")
		}
	}
}
