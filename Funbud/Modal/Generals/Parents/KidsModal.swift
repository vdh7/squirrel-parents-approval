//
//	KidsList.swift
//
//	Create by Dharmesh Avaiya on 8/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class KidsModal : NSObject, NSCoding{
    
    var parentID : String!
    var firebaseID : String!
    var name : String!
    var dob: String!
    var gender : String!
	var avatar : String!
	var hobbies : String!
    var genderText: String!
    
    func getDOB() -> Date {
        if dob != nil {
            if let date = DateTimeManager.shared.dateFrom(stringDate: dob, inFormate: DateFormate.UTC) {
                return date
            }
        }
        return Date()
    }
    
    func displayDOB() -> String {
        if dob != nil {
            if let date = DateTimeManager.shared.dateFrom(stringDate: dob, inFormate: DateFormate.UTC) {
                return DateTimeManager.shared.stringFrom(date: date, inFormate: DateFormate.MMM_DD_COM_yyyy)
            }
        }
        return String()
    }
    
    func getAge() -> String  {
        if dob != nil {
            let year = calculateAge(dob: dob, format: DateFormate.UTC).year
            if year <= 0 {
                let months = calculateAge(dob: dob, format: DateFormate.UTC).month
                return String(format: "%dm", months)
            } else {
                return String(format: "%dy", year)
            }
        } else {
            return String()
        }
    }
    
    func getNameWithAge() -> String {
        return name.appending(", \(getAge())")
    }
    
	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		avatar = dictionary["avatar"] as? String
		gender = dictionary["gender"] as? String
		hobbies = dictionary["hobbies"] as? String
        firebaseID = dictionary["firebaseID"] as? String
		name = dictionary["name"] as? String
		parentID = dictionary["parentID"] as? String
        dob = dictionary["dob"] as? String
        genderText = dictionary["genderText"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if avatar != nil{
			dictionary["avatar"] = avatar
		}
		if gender != nil{
			dictionary["gender"] = gender
		}
		if hobbies != nil{
			dictionary["hobbies"] = hobbies
		}
		if firebaseID != nil{
			dictionary["firebaseID"] = firebaseID
		}
		if name != nil{
			dictionary["name"] = name
		}
		if parentID != nil{
			dictionary["parentID"] = parentID
		}
        if dob != nil{
            dictionary["dob"] = dob
        }
        if genderText != nil{
            dictionary["genderText"] = genderText
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        avatar = aDecoder.decodeObject(forKey: "avatar") as? String
        gender = aDecoder.decodeObject(forKey: "gender") as? String
        hobbies = aDecoder.decodeObject(forKey: "hobbies") as? String
        firebaseID = aDecoder.decodeObject(forKey: "firebaseID") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        parentID = aDecoder.decodeObject(forKey: "parentID") as? String
        dob = aDecoder.decodeObject(forKey: "dob") as? String
        genderText = aDecoder.decodeObject(forKey: "genderText") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if avatar != nil{
			aCoder.encode(avatar, forKey: "avatar")
		}
		if gender != nil{
			aCoder.encode(gender, forKey: "gender")
		}
		if hobbies != nil{
			aCoder.encode(hobbies, forKey: "hobbies")
		}
		if firebaseID != nil{
			aCoder.encode(firebaseID, forKey: "firebaseID")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if parentID != nil{
			aCoder.encode(parentID, forKey: "parentID")
		}
        if dob != nil{
            aCoder.encode(dob, forKey: "dob")
        }
        if genderText != nil{
            aCoder.encode(genderText, forKey: "genderText")
        }
	}
}
