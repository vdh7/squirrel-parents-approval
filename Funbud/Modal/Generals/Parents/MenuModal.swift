//
//  ParentsMenuModal.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 9/16/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation

class MenuModal : NSObject, NSCoding {

    var imgName : String!
    var menuId : String!
    var name : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        imgName = dictionary["img_name"] as? String
        menuId = dictionary["menu_id"] as? String
        name = dictionary["name"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if imgName != nil{
            dictionary["img_name"] = imgName
        }
        if menuId != nil{
            dictionary["menu_id"] = menuId
        }
        if name != nil{
            dictionary["name"] = name
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         imgName = aDecoder.decodeObject(forKey: "img_name") as? String
         menuId = aDecoder.decodeObject(forKey: "menu_id") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if imgName != nil{
            aCoder.encode(imgName, forKey: "img_name")
        }
        if menuId != nil{
            aCoder.encode(menuId, forKey: "menu_id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
    }
    
    static func getParentsProfileMenu() -> [MenuModal] {
        
        var menus: [MenuModal] = []
        
        let home = MenuModal(fromDictionary: [:])
        home.menuId = "8"
        home.name = "Home"
        home.imgName = "Home"
        menus.append(home)
        
        let myKids = MenuModal(fromDictionary: [:])
        myKids.menuId = "1"
        myKids.name = "My Kids"
        myKids.imgName = "Kid"
        menus.append(myKids)
        
        let parentProfile = MenuModal(fromDictionary: [:])
        parentProfile.menuId = "2"
        parentProfile.name = "Parent Profile"
        parentProfile.imgName = "Kid"
        menus.append(parentProfile)
        
        let myPlayDate = MenuModal(fromDictionary: [:])
        myPlayDate.menuId = "3"
        myPlayDate.name = "My Playdates"
        myPlayDate.imgName = "Home"
        menus.append(myPlayDate)
        
        let settings = MenuModal(fromDictionary: [:])
        settings.menuId = "4"
        settings.name = "Settings"
        settings.imgName = "Setting"
        menus.append(settings)
        
        let empty = MenuModal(fromDictionary: [:])
        empty.menuId = "5"
        empty.name = nil
        empty.imgName = nil
        menus.append(empty)
        
        let support = MenuModal(fromDictionary: [:])
        support.menuId = "6"
        support.name = "Support"
        support.imgName = "Support"
        menus.append(support)
        
        let privacy = MenuModal(fromDictionary: [:])
        privacy.menuId = "7"
        privacy.name = "Privacy Policy"
        privacy.imgName = "Privacy"
        menus.append(privacy)
        
        return menus
    }
    
    static func getBabysitterProfileMenu() -> [MenuModal] {
        
        var menus: [MenuModal] = []
        
        let home = MenuModal(fromDictionary: [:])
        home.menuId = "7"
        home.name = "Home"
        home.imgName = "Home"
        menus.append(home)
        
        let myKids = MenuModal(fromDictionary: [:])
        myKids.menuId = "1"
        myKids.name = "Profile"
        myKids.imgName = "Kid"
        menus.append(myKids)
        
        let parentProfile = MenuModal(fromDictionary: [:])
        parentProfile.menuId = "2"
        parentProfile.name = "My Availability"
        parentProfile.imgName = "Avaliaility"
        menus.append(parentProfile)
        
        let myPlayDate = MenuModal(fromDictionary: [:])
        myPlayDate.menuId = "3"
        myPlayDate.name = "Settings"
        myPlayDate.imgName = "Setting"
        menus.append(myPlayDate)
        
        let empty = MenuModal(fromDictionary: [:])
        empty.menuId = "4"
        empty.name = nil
        empty.imgName = nil
        menus.append(empty)
        
        let support = MenuModal(fromDictionary: [:])
        support.menuId = "5"
        support.name = "Support"
        support.imgName = "Support"
        menus.append(support)
        
        let privacy = MenuModal(fromDictionary: [:])
        privacy.menuId = "6"
        privacy.name = "Privacy Policy"
        privacy.imgName = "Privacy"
        menus.append(privacy)
        
        return menus
    }
}
