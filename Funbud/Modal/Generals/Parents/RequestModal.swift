//
//  RequestModal.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 10/4/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation

class RequestModal : NSObject, NSCoding{
    
    var id : String!
    var status : String!
    var kids : [String]! {
        didSet {
            updateKidsName()
        }
    }
    var playdateId : String!
    var requestParentId : String!
    var playdateCreatedParentId : String!
    var playdateName : String!
    var isForBabysitter: String = "0"
    
    var kidsName: String?
    private var kidsNames: [String] = []
    
    var kidsModal : [KidsModal] = [] {
        didSet {
            didUpdateBlock?()
        }
    }
    var didUpdateBlock:(()->Void)?
        
    func updateKidsName() {
        if kids != nil {
            self.kidsModal.removeAll()
            for kid in kids {
                FirebaseManager.shared.findKid(fromID: kid) { pModal in
                    if let stringToAdd = pModal?.getNameWithAge() {
                        self.kidsNames.removeAll()
                        self.kidsNames.append(stringToAdd)
                        self.kidsName = self.kidsNames.joined(separator: " ")
                        if pModal != nil {
                            self.kidsModal.append(pModal!)
                        }
                    }
                }
            }
        }
    }
    
    func getChildNames() -> String {
        /*var names: [String] = []
        _ = kids.filter { arg0 in
            let stringToAdd = arg0.getNameWithAge()
            names.append(stringToAdd)
            return false
        }
        return names.joined(separator: " ")*/
        return self.kidsName ?? String()
    }
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
                
        id = dictionary["id"] as? String
        status = dictionary["status"] as? String
        /*kids = [KidsModal]()
        if let kidsArray = dictionary["kids"] as? [[String:Any]]{
            for dic in kidsArray{
                let value = KidsModal(fromDictionary: dic)
                kids.append(value)
            }
        }*/
        kids = dictionary["kids"] as? [String]
        playdateId = dictionary["playdate_id"] as? String
        requestParentId = dictionary["request_parent_id"] as? String
        playdateCreatedParentId = dictionary["playdate_parent_id"] as? String
        playdateName = dictionary["playdate_name"] as? String
        isForBabysitter = dictionary["isForBabysitter"] as? String ?? "0"
        
        super.init()
        self.updateKidsName()
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if status != nil{
            dictionary["status"] = status
        }
        /*if kids != nil{
            var dictionaryElements = [[String:Any]]()
            for kidsElement in kids {
                dictionaryElements.append(kidsElement.toDictionary())
            }
            dictionary["kids"] = dictionaryElements
        }*/
        if kids != nil{
            dictionary["kids"] = kids
        }
        if playdateId != nil{
            dictionary["playdate_id"] = playdateId
        }
        if requestParentId != nil{
            dictionary["request_parent_id"] = requestParentId
        }
        if playdateCreatedParentId != nil{
            dictionary["playdate_parent_id"] = playdateCreatedParentId
        }
        if playdateName != nil{
            dictionary["playdate_name"] = playdateName
        }
        dictionary["isForBabysitter"] = isForBabysitter
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        //kids = aDecoder.decodeObject(forKey :"kids") as? [KidsModal]
        kids = aDecoder.decodeObject(forKey :"kids") as? [String]
        playdateId = aDecoder.decodeObject(forKey: "playdate_id") as? String
        requestParentId = aDecoder.decodeObject(forKey: "request_parent_id") as? String
        playdateCreatedParentId = aDecoder.decodeObject(forKey: "playdate_parent_id") as? String
        playdateName = aDecoder.decodeObject(forKey: "playdate_name") as? String
        isForBabysitter = aDecoder.decodeObject(forKey: "isForBabysitter") as? String ?? "0"
        
        super.init()
        
        self.updateKidsName()
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if kids != nil{
            aCoder.encode(kids, forKey: "kids")
        }
        if playdateId != nil{
            aCoder.encode(playdateId, forKey: "playdate_id")
        }
        if requestParentId != nil{
            aCoder.encode(requestParentId, forKey: "request_parent_id")
        }
        if playdateCreatedParentId != nil{
            aCoder.encode(playdateCreatedParentId, forKey: "playdate_parent_id")
        }
        if playdateName != nil{
            aCoder.encode(playdateName, forKey: "playdate_name")
        }
        aCoder.encode(isForBabysitter, forKey: "isForBabysitter")
    }
}
