//
//  UserModal.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 25/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation

// MARK: - UserModal
struct UserModal: Codable {
    
    let userID, firstname, lastname, email: String?
    let password, image, creationDate, emailVerification: String?
    let emailAccessToken, userAccessToken, disable: String?
    
    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case firstname, lastname, email, password, image
        case creationDate = "creation_date"
        case emailVerification = "email_verification"
        case emailAccessToken = "email_access_token"
        case userAccessToken = "user_access_token"
        case disable
    }
}

// MARK: UserModal convenience initializers and mutators

extension UserModal {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(UserModal.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
