//
//  ResponseModal.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 25/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation

struct ResponseModal<T: Codable>: Codable {
    
    let code, status: Int?
    let message: String?
    var data : T?

    enum CodingKeys: String, CodingKey {
        case code
        case status
        case message
        case data
    }
}
