//
//  BaseModal.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 9/14/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation

class BaseModal : NSObject, NSCoding{
    
    var gender : String!
    var imgsource : String!
    var location : String!
    var name : String!
    var userid : String!
    var genderText : String!
    var lattitude : String!
    var longitude : String!
    var role : String!
    var isNotificationEnabled: String!
    var isLocationEnable: String!
    var badge: String!
    var deviceToken: String!
    var phone: String?
    let isBlocked: String?
    var imageId: String!
    var isAdminApproved: String!
    
    func displayGender() -> String? {
        if gender == "0" {
            return GenderType.male.rawValue
        } else if gender == "1" {
            return GenderType.female.rawValue
        } else if gender == "2" {
            //return genderText ?? GenderType.other.rawValue
            return GenderType.other.rawValue
        }
        return nil
    }
    
    func getGenderValueToSet(from stringGender: String?) -> String {
        
        if stringGender == GenderType.male.rawValue {
            return "0"
        } else if stringGender == GenderType.female.rawValue {
            return "1"
        } else {
            return "2"
        }
    }
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        gender = dictionary["gender"] as? String
        genderText = dictionary["genderText"] as? String
        imgsource = dictionary["imgsource"] as? String
        location = dictionary["location"] as? String
        name = dictionary["name"] as? String
        userid = dictionary["userid"] as? String
        lattitude = dictionary["lattitude"] as? String
        longitude = dictionary["longitude"] as? String
        role = dictionary["role"] as? String
        isNotificationEnabled = dictionary["isNotificationEnabled"] as? String
        isLocationEnable = dictionary["isLocationEnable"] as? String
        badge = dictionary["badge"] as? String
        deviceToken = dictionary["deviceToken"] as? String ?? String()
        phone = dictionary["phone"] as? String ?? String()
        isBlocked = dictionary["isBlocked"] as? String ?? "0"
        imageId = dictionary["imageId"] as? String ?? String()
        isAdminApproved = dictionary["isAdminApproved"] as? String ?? "0"
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if gender != nil{
            dictionary["gender"] = gender
        }
        if genderText != nil{
            dictionary["genderText"] = genderText
        }
        if imgsource != nil{
            dictionary["imgsource"] = imgsource
        }
        if location != nil{
            dictionary["location"] = location
        }
        if name != nil{
            dictionary["name"] = name
        }
        if userid != nil{
            dictionary["userid"] = userid
        }
        if lattitude != nil{
            dictionary["lattitude"] = lattitude
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        if role != nil{
            dictionary["role"] = role
        }
        if isNotificationEnabled != nil{
            dictionary["isNotificationEnabled"] = isNotificationEnabled
        } else {
            dictionary["isNotificationEnabled"] = "0"
        }
        if isLocationEnable != nil{
            dictionary["isLocationEnable"] = isLocationEnable
        } else {
            dictionary["isLocationEnable"] = "0"
        }
        if badge != nil{
            dictionary["badge"] = badge
        } else {
            dictionary["badge"] = "0"
        }
        dictionary["deviceToken"] = deviceToken ?? String()
        if phone != nil{
            dictionary["phone"] = phone
        }
        if isBlocked != nil{
            dictionary["isBlocked"] = isBlocked
        }
        if imageId != nil {
            dictionary["imageId"] = imageId
        }
        if isAdminApproved != nil{
            dictionary["isAdminApproved"] = isAdminApproved
        } else {
            dictionary["isAdminApproved"] = "0"
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        gender = aDecoder.decodeObject(forKey: "gender") as? String
        genderText = aDecoder.decodeObject(forKey: "genderText") as? String
        imgsource = aDecoder.decodeObject(forKey: "imgsource") as? String
        location = aDecoder.decodeObject(forKey: "location") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        userid = aDecoder.decodeObject(forKey: "userid") as? String
        lattitude = aDecoder.decodeObject(forKey: "lattitude") as? String
        longitude = aDecoder.decodeObject(forKey: "longitude") as? String
        role = aDecoder.decodeObject(forKey: "role") as? String
        isNotificationEnabled = aDecoder.decodeObject(forKey: "isNotificationEnabled") as? String
        isLocationEnable = aDecoder.decodeObject(forKey: "isLocationEnable") as? String
        badge = aDecoder.decodeObject(forKey: "badge") as? String
        deviceToken = aDecoder.decodeObject(forKey: "deviceToken") as? String
        phone = aDecoder.decodeObject(forKey: "phone") as? String ?? String()
        isBlocked = aDecoder.decodeObject(forKey: "isBlocked") as? String ?? "0"
        imageId = aDecoder.decodeObject(forKey: "imageId") as? String ?? String()
        isAdminApproved = aDecoder.decodeObject(forKey :"isAdminApproved") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if gender != nil{
            aCoder.encode(gender, forKey: "gender")
        }
        if genderText != nil{
            aCoder.encode(genderText, forKey: "genderText")
        }
        if imgsource != nil{
            aCoder.encode(imgsource, forKey: "imgsource")
        }
        if location != nil{
            aCoder.encode(location, forKey: "location")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if userid != nil{
            aCoder.encode(userid, forKey: "userid")
        }
        if lattitude != nil{
            aCoder.encode(lattitude, forKey: "lattitude")
        }
        if longitude != nil{
            aCoder.encode(longitude, forKey: "longitude")
        }
        if role != nil{
            aCoder.encode(role, forKey: "role")
        }
        if isNotificationEnabled != nil{
            aCoder.encode(isNotificationEnabled, forKey: "isNotificationEnabled")
        }
        if isLocationEnable != nil{
            aCoder.encode(isLocationEnable, forKey: "isLocationEnable")
        }
        if badge != nil{
            aCoder.encode(badge, forKey: "badge")
        }
        
        aCoder.encode(deviceToken ?? String(), forKey: "deviceToken")
        
        if phone != nil{
            aCoder.encode(phone, forKey: "phone")
        }
        if isBlocked != nil{
            aCoder.encode(isBlocked, forKey: "isBlocked")
        }
        if imageId != nil{
            aCoder.encode(imageId, forKey: "imageId")
        }
        if isAdminApproved != nil{
            aCoder.encode(isAdminApproved, forKey: "isAdminApproved")
        }
    }
}
