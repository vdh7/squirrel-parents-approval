//
//  PlayDatesModal.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 9/18/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation
import Firebase

class PlayDatesModal : NSObject, NSCoding{
    
    var id : String!
    var childList : [String]!
    var date : String!
    var groupEvent : Bool!
    var parentID : String!
    var postalCode : String!
    var title : String!
        
    var lattitude : String!
    var longitude : String!
    var location : String!
    
    var createdAt: Timestamp!
    var ownerImage: String!
    
    var kidsNames: [String] = []
    var displayName: String? {
        didSet {
            didUpdateBlock?()
        }
    }
    var isAutoGenerate : String!
    var createdOnChildId : String!
    var isActive : String!
    
    var didUpdateBlock:(()->Void)?
    
    func updateKidsName() {
        //kidsNames.removeAll()
        if childList != nil {
            for kid in childList {
                FirebaseManager.shared.findKid(fromID: kid) { kidsModal in
                    if let stringToAdd = kidsModal?.getNameWithAge() {
                        self.kidsNames.removeAll()
                        self.kidsNames.append(stringToAdd)
                        self.displayName = self.kidsNames.joined(separator: " ")
                    }
                }
            }
        }
    }
    func getChildNames() -> String {
        /*var names: [String] = []
        _ = childList.filter { arg0 in
            let stringToAdd = arg0.getNameWithAge()
            names.append(stringToAdd)
            return false
        }
        return names.joined(separator: " ")
        */
        return displayName ?? String("No kids were selected")
    }
    
    func getDay() -> String {
        if let date = DateTimeManager.shared.dateFrom(stringDate: date, inFormate: DateFormate.UTC) {
            return DateTimeManager.shared.stringFrom(date: date, inFormate: DateFormate.DAY)
        }
        return String()
    }
    
    func getMonth() -> String {
        if let date = DateTimeManager.shared.dateFrom(stringDate: date, inFormate: DateFormate.UTC) {
            return DateTimeManager.shared.stringFrom(date: date, inFormate: DateFormate.MONTH)
        }
        return String()
    }
    
    func getTime() -> String {
        if let date = DateTimeManager.shared.dateFrom(stringDate: date, inFormate: DateFormate.UTC) {
            return DateTimeManager.shared.stringFrom(date: date, inFormate: DateFormate.HH_MM_24)
        }
        return String()
    }
    
    func getDistnaceInKm() -> String {
        
        let doubleLattitude = Double( lattitude ?? String() ) ?? .zero
        let doubleLongitude = Double( longitude ?? String() ) ?? .zero
        let distanceInMeter = LocationManager.shared.getDistanceFrom(lattitude: doubleLattitude, longitude: doubleLongitude)
        return covert(distanceInMeter: distanceInMeter, to: .kilometers)
    }
    
    func getPlaydateInTimestamp() -> TimeInterval? {
        if date != nil {
            if let date = DateTimeManager.shared.dateFrom(stringDate: date, inFormate: DateFormate.UTC) {
                return date.timeIntervalSince1970
            }
        }        
        return nil
    }
    
    func displayKidsCount() -> (Bool, String) {
        /*if childList.count > 2 {
            let stringToReturn = String(format: "+%d", 2)
            return (false, stringToReturn)
        } else if childList.count == 2 {
            let stringToReturn = String(format: "%d", childList.count)
            return (false, stringToReturn)
        } else {
            return (true, String())
        }*/
        
        if childList.count > 1 {
            let stringToReturn = String(format: "+%d", childList.count - 1)
            return (false, stringToReturn)
        } else {
            return (true, String())
        }
    }
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        /*childList = [KidsModal]()
        if let kidsListArray = dictionary["childList"] as? [[String:Any]]{
            for dic in kidsListArray{
                let value = KidsModal(fromDictionary: dic)
                childList.append(value)
            }
        }*/
        
        childList = dictionary["childList"] as? [String]
        id = dictionary["id"] as? String
        date = dictionary["date"] as? String
        groupEvent = dictionary["groupEvent"] as? Bool
        parentID = dictionary["parentID"] as? String
        postalCode = dictionary["postalCode"] as? String
        title = dictionary["title"] as? String
        location = dictionary["location"] as? String
        lattitude = dictionary["lattitude"] as? String
        longitude = dictionary["longitude"] as? String
        createdAt = dictionary["createdAt"] as? Timestamp
        ownerImage = dictionary["ownerImage"] as? String
        isAutoGenerate = dictionary["isAutoGenerate"] as? String ?? "false"
        createdOnChildId = dictionary["createdOnChildId"] as? String
        isActive = dictionary["isActive"] as? String ?? "true"
        
        super.init()
        
        self.updateKidsName()
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        /*if childList != nil{
            var dictionaryElements = [[String:Any]]()
            for kidsListElement in childList {
                dictionaryElements.append(kidsListElement.toDictionary())
            }
            dictionary["childList"] = dictionaryElements
        }*/
        if childList != nil{
            dictionary["childList"] = childList
        }
        if id != nil{
            dictionary["id"] = id
        }
        if date != nil{
            dictionary["date"] = date
        }
        if groupEvent != nil{
            dictionary["groupEvent"] = groupEvent
        }
        if parentID != nil{
            dictionary["parentID"] = parentID
        }
        if postalCode != nil{
            dictionary["postalCode"] = postalCode
        }
        if title != nil{
            dictionary["title"] = title
        }
        if location != nil{
            dictionary["location"] = location
        }
        if lattitude != nil{
            dictionary["lattitude"] = lattitude
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        /*if createdAt != nil{
            dictionary["createdAt"] = createdAt
        }*/
        if ownerImage != nil{
            dictionary["ownerImage"] = ownerImage
        }
        if isAutoGenerate != nil{
            dictionary["isAutoGenerate"] = isAutoGenerate
        }
        if createdOnChildId != nil {
            dictionary["createdOnChildId"] = createdOnChildId
        }
        if isActive != nil {
            dictionary["isActive"] = isActive
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {         
        //childList = aDecoder.decodeObject(forKey :"childList") as? [KidsModal]
        childList = aDecoder.decodeObject(forKey :"childList") as? [String]
        id = aDecoder.decodeObject(forKey: "id") as? String
         date = aDecoder.decodeObject(forKey: "date") as? String
         groupEvent = aDecoder.decodeObject(forKey: "groupEvent") as? Bool
         parentID = aDecoder.decodeObject(forKey: "parentID") as? String
         postalCode = aDecoder.decodeObject(forKey: "postalCode") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
        location = aDecoder.decodeObject(forKey: "location") as? String
        lattitude = aDecoder.decodeObject(forKey: "lattitude") as? String
        longitude = aDecoder.decodeObject(forKey: "longitude") as? String
        createdAt = aDecoder.decodeObject(forKey: "createdAt") as? Timestamp
        ownerImage = aDecoder.decodeObject(forKey: "ownerImage") as? String
        isAutoGenerate = aDecoder.decodeObject(forKey: "isAutoGenerate") as? String ?? "false"
        createdOnChildId = aDecoder.decodeObject(forKey: "createdOnChildId") as? String
        isActive = aDecoder.decodeObject(forKey: "isActive") as? String
        
        super.init()
        
        self.updateKidsName()
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if childList != nil{
            aCoder.encode(childList, forKey: "childList")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if date != nil{
            aCoder.encode(date, forKey: "date")
        }
        if groupEvent != nil{
            aCoder.encode(groupEvent, forKey: "groupEvent")
        }
        if parentID != nil{
            aCoder.encode(parentID, forKey: "parentID")
        }
        if postalCode != nil{
            aCoder.encode(postalCode, forKey: "postalCode")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if location != nil{
            aCoder.encode(location, forKey: "location")
        }
        if lattitude != nil{
            aCoder.encode(lattitude, forKey: "lattitude")
        }
        if longitude != nil{
            aCoder.encode(longitude, forKey: "longitude")
        }
        /*if createdAt != nil{
            aCoder.encode(createdAt, forKey: "createdAt")
        }*/
        if ownerImage != nil {
            aCoder.encode(ownerImage, forKey: "ownerImage")
        }
        if isAutoGenerate != nil {
            aCoder.encode(isAutoGenerate, forKey: "isAutoGenerate")
        }
        if createdOnChildId != nil {
            aCoder.encode(createdOnChildId, forKey: "createdOnChildId")
        }
        if isActive != nil {
            aCoder.encode(isActive, forKey: "isActive")
        }
    }
}

