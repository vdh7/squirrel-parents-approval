//
//  BabysitterModal.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 9/14/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation

class BabysitterModal : BaseModal {
    
    var dob: String!
    var rate: String!
    var isAcceptUrgentRequest: String!
    var isAlwaysAvailable: String!
    var days : [DaysModal]!
    var userRating: String!
    
    func getDOB() -> Date {
        if dob != nil {
            if let date = DateTimeManager.shared.dateFrom(stringDate: dob, inFormate: DateFormate.UTC) {
                return date
            }
        }
        return Date()
    }
    
    func displayDOB() -> String {
        if dob != nil {
            if let date = DateTimeManager.shared.dateFrom(stringDate: dob, inFormate: DateFormate.UTC) {
                return DateTimeManager.shared.stringFrom(date: date, inFormate: DateFormate.MMM_DD_COM_yyyy)
            }
        }
        return String()
    }
    
    func getAge() -> String  {
        if dob != nil {
            let year = calculateAge(dob: dob, format: DateFormate.UTC).year
            if year <= 0 {
                let months = calculateAge(dob: dob, format: DateFormate.UTC).month
                return String(format: "%dm", months)
            } else {
                return String(format: "%dy", year)
            }            
        } else {
            return String()
        }
    }
    
    func getNameWithAge() -> String {
        return (name ?? String()).appending(", \(getAge())")
    }
    
    func getDistnaceInKm() -> String {
        
        let doubleLattitude = Double( lattitude ?? String() ) ?? .zero
        let doubleLongitude = Double( longitude ?? String() ) ?? .zero
        let distanceInMeter = LocationManager.shared.getDistanceFrom(lattitude: doubleLattitude, longitude: doubleLongitude)
        return covert(distanceInMeter: distanceInMeter, to: .kilometers)
    }
    
    func displayHours() -> String {        
        return String("\(rate ?? String())$/hr")
    }
    
    func displayUserRating() -> String {
        return String(format: "%.1f", Double(userRating ?? "0") ?? .zero)
    }
        
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    override init(fromDictionary dictionary: [String:Any]){
        super.init(fromDictionary: dictionary)
        
        dob = dictionary["dob"] as? String
        imageId = dictionary["imageId"] as? String
        isAcceptUrgentRequest = dictionary["isAcceptUrgentRequest"] as? String
        isAlwaysAvailable = dictionary["isAlwaysAvailable"] as? String
        rate = dictionary["rate"] as? String
        
        days = [DaysModal]()
        if let daysArray = dictionary["days"] as? [[String:Any]]{
            for dic in daysArray{
                let value = DaysModal(fromDictionary: dic)
                days.append(value)
            }
        }                
        userRating = dictionary["userRating"] as? String ?? "0"
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    override func toDictionary() -> [String:Any]
    {
        var dictionary = super.toDictionary()
        
        if dob != nil{
            dictionary["dob"] = dob
        }        
        if isAcceptUrgentRequest != nil{
            dictionary["isAcceptUrgentRequest"] = isAcceptUrgentRequest
        }
        if isAlwaysAvailable != nil{
            dictionary["isAlwaysAvailable"] = isAlwaysAvailable
        }
        if rate != nil{
            dictionary["rate"] = rate
        }
        if days != nil{
            var dictionaryElements = [[String:Any]]()
            for daysElement in days {
                dictionaryElements.append(daysElement.toDictionary())
            }
            dictionary["days"] = dictionaryElements
        }    
        if userRating != nil{
            dictionary["userRating"] = userRating
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        dob = aDecoder.decodeObject(forKey: "dob") as? String        
        
        isAcceptUrgentRequest = aDecoder.decodeObject(forKey: "isAcceptUrgentRequest") as? String
        isAlwaysAvailable = aDecoder.decodeObject(forKey: "isAlwaysAvailable") as? String
        rate = aDecoder.decodeObject(forKey: "rate") as? String
        days = aDecoder.decodeObject(forKey :"days") as? [DaysModal]                
        userRating = aDecoder.decodeObject(forKey :"userRating") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc override func encode(with aCoder: NSCoder)
    {
        super.encode(with: aCoder)
        
        if dob != nil{
            aCoder.encode(dob, forKey: "dob")
        }        
        if isAcceptUrgentRequest != nil{
            aCoder.encode(isAcceptUrgentRequest, forKey: "isAcceptUrgentRequest")
        }
        if isAlwaysAvailable != nil{
            aCoder.encode(isAlwaysAvailable, forKey: "isAlwaysAvailable")
        }
        if rate != nil{
            aCoder.encode(rate, forKey: "rate")
        }
        if days != nil{
            aCoder.encode(days, forKey: "days")
        }       
        if userRating != nil{
            aCoder.encode(userRating, forKey: "userRating")
        }
    }
}
