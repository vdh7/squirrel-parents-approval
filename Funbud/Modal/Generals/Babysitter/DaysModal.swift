//
//	DaysModal.swift
//
//	Create by Dharmesh Avaiya on 14/9/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import AVFAudio

class DaysModal : NSObject, NSCoding {

	var day : String!
	var from : String!
	var isAvailable : String!
	var to : String!

    func displayFrom() -> String {
        if from != nil {
            if let date = DateTimeManager.shared.dateFrom(stringDate: from, inFormate: DateFormate.UTC) {
                return DateTimeManager.shared.stringFrom(date: date, inFormate: DateFormate.HH_MM)
            }
        }
        return String()
    }
    
    func displayTo() -> String {
        if to != nil {
            if let date = DateTimeManager.shared.dateFrom(stringDate: to, inFormate: DateFormate.UTC) {
                return DateTimeManager.shared.stringFrom(date: date, inFormate: DateFormate.HH_MM)
            }
        }
        return String()
    }
    
    func actualFromDate() -> Date {
        
        let localFrom = DateTimeManager.shared.utcToLocal(dateStr: from, formate: DateFormate.UTC) ?? String()
        
        let fmDate = DateTimeManager.shared.dateFrom(stringDate: localFrom, inFormate: DateFormate.UTC) ?? Date()
        debugLog(object: fmDate)
        let strFrom = DateTimeManager.shared.stringFrom(date: fmDate, inFormate: DateFormate.UTC)
        
        let stringUTC = DateTimeManager.shared.stringFrom(date: Date(), inFormate: DateFormate.UTC)
        
        let sDate = stringUTC.components(separatedBy: "T").first ?? String()
        let sTime = strFrom.components(separatedBy: "T").last ?? String()
        let nDate = "\(sDate) \(sTime)"
        debugLog(object: nDate)
        if let nnDate = DateTimeManager.shared.dateFrom(stringDate: nDate, inFormate: DateFormate.UTC_SPACE) {
            debugLog(object: nnDate)
            return nnDate
        }
        return Date()
    }
    
    func actualToDate() -> Date {
        
        let localTo = DateTimeManager.shared.utcToLocal(dateStr: to, formate: DateFormate.UTC) ?? String()
        
        let toDate = DateTimeManager.shared.dateFrom(stringDate: localTo, inFormate: DateFormate.UTC) ?? Date()
        debugLog(object: toDate)
        let strTO = DateTimeManager.shared.stringFrom(date: toDate, inFormate: DateFormate.UTC)
        
        let stringUTC = DateTimeManager.shared.stringFrom(date: Date(), inFormate: DateFormate.UTC)
        
        let sDate = stringUTC.components(separatedBy: "T").first ?? String()
        let sTime = strTO.components(separatedBy: "T").last ?? String()
        let nDate = "\(sDate) \(sTime)"
        debugLog(object: nDate)
        if let nnDate = DateTimeManager.shared.dateFrom(stringDate: nDate, inFormate: DateFormate.UTC_SPACE) {
            debugLog(object: nnDate)
            return nnDate
        }
        return Date()
    }
    
    func fromDate(_ fromTimeSlot: String?) -> Date {
            
        let utcDate = DateTimeManager.shared.dateFrom(stringDate: fromTimeSlot ?? String(), inFormate: DateFormate.HH_MM) ?? Date()
        let stringDate = DateTimeManager.shared.stringFrom(date: utcDate, inFormate: DateFormate.UTC)
        let strFrom = DateTimeManager.shared.utcToLocal(dateStr: stringDate, formate: DateFormate.UTC) ?? String()
        debugLog(object: strFrom)
        //let strFrom = DateTimeManager.shared.stringFrom(date: fmDate, inFormate: DateFormate.UTC)
        
        let stringUTC = DateTimeManager.shared.stringFrom(date: Date(), inFormate: DateFormate.UTC)
        
        let sDate = stringUTC.components(separatedBy: "T").first ?? String()
        let sTime = strFrom.components(separatedBy: "T").last ?? String()
        let nDate = "\(sDate) \(sTime)"
        debugLog(object: nDate)
        if let nnDate = DateTimeManager.shared.dateFrom(stringDate: nDate, inFormate: DateFormate.UTC_SPACE) {
            debugLog(object: nnDate)
            return nnDate
        }
        return Date()
    }
    
    func toDate(_ toTimeSlot: String?) -> Date {
        
        let utcDate = DateTimeManager.shared.dateFrom(stringDate: toTimeSlot ?? String(), inFormate: DateFormate.HH_MM) ?? Date()
        let stringDate = DateTimeManager.shared.stringFrom(date: utcDate, inFormate: DateFormate.UTC)
        let strTO = DateTimeManager.shared.utcToLocal(dateStr: stringDate, formate: DateFormate.UTC) ?? String()
        debugLog(object: strTO)
        //let strTO = DateTimeManager.shared.stringFrom(date: toDate, inFormate: DateFormate.UTC)
        
        let stringUTC = DateTimeManager.shared.stringFrom(date: Date(), inFormate: DateFormate.UTC)
        
        let sDate = stringUTC.components(separatedBy: "T").first ?? String()
        let sTime = strTO.components(separatedBy: "T").last ?? String()
        let nDate = "\(sDate) \(sTime)"
        debugLog(object: nDate)
        if let nnDate = DateTimeManager.shared.dateFrom(stringDate: nDate, inFormate: DateFormate.UTC_SPACE) {
            debugLog(object: nnDate)
            return nnDate
        }
        return Date()
    }
    
	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		day = dictionary["day"] as? String
		from = dictionary["from"] as? String
		isAvailable = dictionary["isAvailable"] as? String
		to = dictionary["to"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if day != nil{
			dictionary["day"] = day
		}
		if from != nil{
			dictionary["from"] = from
		}
		if isAvailable != nil{
			dictionary["isAvailable"] = isAvailable
		}
		if to != nil{
			dictionary["to"] = to
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         day = aDecoder.decodeObject(forKey: "day") as? String
         from = aDecoder.decodeObject(forKey: "from") as? String
         isAvailable = aDecoder.decodeObject(forKey: "isAvailable") as? String
         to = aDecoder.decodeObject(forKey: "to") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if day != nil{
			aCoder.encode(day, forKey: "day")
		}
		if from != nil{
			aCoder.encode(from, forKey: "from")
		}
		if isAvailable != nil{
			aCoder.encode(isAvailable, forKey: "isAvailable")
		}
		if to != nil{
			aCoder.encode(to, forKey: "to")
		}

	}

}
