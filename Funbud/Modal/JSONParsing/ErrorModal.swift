//
//  ErrorModal.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 25/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation

enum PodmanyError: Error {
    
    case InvalidURL
}

struct ErrorModal: Error {
   
    var code: Int
    var errorDescription: String
    
    init(code: Int, errorDescription: String) {
        self.code = code
        self.errorDescription = errorDescription
    }
}
