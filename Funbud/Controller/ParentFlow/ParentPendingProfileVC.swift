//
//  ParentPendingProfileVC.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 21/08/22.
//  Copyright © 2022 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import SwiftGifOrigin
import MessageUI

class ParentPendingProfileVC : BaseVC, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var gifImage: UIImageView!
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.pop()
    }
    
    @IBAction func btnSupportTap(_ sender: Any) {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([""])
            mail.setCcRecipients([""])
            mail.setSubject("Funbud Support")
            present(mail, animated: true, completion: nil)
            
        } else {
            debugPrint("Cannot send mail")
        }
    }
    
    //------------------------------------------------------
    
    //MARK: MFMailComposeViewControllerDelegate
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            debugPrint("Cancelled")
        case MFMailComposeResult.saved.rawValue:
            debugPrint("Saved")
        case MFMailComposeResult.sent.rawValue:
            debugPrint("Sent")
        case MFMailComposeResult.failed.rawValue:
            debugPrint("Error: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        gifImage.image = UIImage.gif(name: "scooter")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if FirebaseManager.shared.isSignedIn() {
            
            /*FirebaseManager.shared.find(parentFrom: PreferenceManager.shared.userId ?? String()) { dict in
                let baseModal = BaseModal(fromDictionary: dict)
                if baseModal.role == RoleType.parent.rawValue {
                    let parentModal = ParentsModal(fromDictionary: dict)
                    if parentModal.isAdminApproved == "2" {
                        DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Error.admin_reject_parent_request) {
                            try? FirebaseManager.shared.signOut()
                            PreferenceManager.shared.reset()
                            NavigationManager.shared.setupSignInOption()
                        }
                    } else if parentModal.isAdminApproved == "1" {
                        
                        NavigationManager.shared.setupSigninUserRootController()
                    }
                }
            }*/
            
            let userId = isParentProfile ? (parentModal?.userid ?? String()) : (babysitterModal?.userid ?? String())
            let node = isParentProfile ? FirebaseManager.shared.nodeParents : FirebaseManager.shared.nodeBabysitters
            FirebaseManager.shared.listenMe(userId, ofNode: node) { result in
                if result?.isAdminApproved == "2" {
                    DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Error.admin_reject_parent_request) {
                        try? FirebaseManager.shared.signOut()
                        PreferenceManager.shared.reset()
                        NavigationManager.shared.setupSignInOption()
                    }
                } else if result?.isAdminApproved == "1" {
                    NavigationManager.shared.setupSigninUserRootController()
                }
            }
        }
    }
        
    //------------------------------------------------------
}
