//
//  KidsProfileVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 20/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage
import FittedSheets

class KidsProfileVC : BaseVC,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    
    @IBOutlet weak var lblParentName: UILabel!
    @IBOutlet weak var lblCategory: PoppinsRegularLabel!
    @IBOutlet weak var userProfileImg: UIImageView!
    @IBOutlet weak var avatarcollection: UICollectionView!
    @IBOutlet weak var lblKidName: FredokaOneRegularLabel!
    @IBOutlet weak var lblDesp: PoppinsRegularLabel!
    
    var profileModal: ParentsModal?
    var kids: [KidsModal] = []
    var selectedKid: KidsModal? {
        didSet {
            if selectedKid != nil {
                lblKidName.text = selectedKid?.getNameWithAge()
                lblDesp.text = selectedKid?.hobbies
            } else {
                lblKidName.text = nil
                lblDesp.text = nil
            }
        }
    }
    
    var img : [String] {
        return PMImageName.avatars
    }
    
    var selectedAvatarIndex: Int? = nil
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    static func instantiate() -> KidsProfileVC {
        let storyBoard = UIStoryboard(name: PMStoryboard.main, bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: "KidsProfileVC") as! KidsProfileVC
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        lblParentName.text = profileModal?.name
        lblCategory.text = profileModal?.getGender()
        
        if let imageURL = profileModal?.imgsource {
            userProfileImg.sd_setIndicatorStyle(UIActivityIndicatorView.Style.gray)
            userProfileImg.sd_addActivityIndicator()
            userProfileImg.sd_showActivityIndicatorView()
            userProfileImg.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                self.userProfileImg.image = img
                self.userProfileImg.sd_removeActivityIndicator()
            }
        }
        
        avatarcollection.delegate = self
        avatarcollection.dataSource = self
        avatarcollection.register(UINib(nibName: String(describing: AvatarCVC.self), bundle: nil), forCellWithReuseIdentifier: String(describing: AvatarCVC.self))
    }
    
    func loadKids() {
        
        profileModal?.findKid(completion: { kids in
            self.kids = kids
            debugPrint(kids)
            self.avatarcollection.reloadData()
            if self.selectedKid == nil {
                self.selectedKid = kids.first
            }
            self.avatarcollection.layoutIfNeeded()
            //self.avatarcollection.flashScrollIndicators()
        })
    }
    
    //------------------------------------------------------
    
    //MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return kids.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AvatarCVC.self), for: indexPath) as? AvatarCVC {
            
            let kid = kids[indexPath.row]
            let avatarIndex = Int(kid.avatar) ?? .zero
            cell.set(imageName: img[avatarIndex])
            if selectedKid?.firebaseID == kid.firebaseID {
                cell.set(selected: true)
                cell.avatarImg.borderColor = PMColor.green
                cell.avatarImg.borderWidth = 2
            } else {
                cell.set(selected: false)
                cell.avatarImg.borderColor = .clear
            }
            cell.layoutIfNeeded()
            return cell
        }
        return UICollectionViewCell()
    }
    
    //------------------------------------------------------
    
    //MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedKid = kids.first
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        loadKids()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    
    //------------------------------------------------------
}
