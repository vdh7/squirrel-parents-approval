//
//  PDRequestGroupVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 27/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class PDRequestGroupVC : BaseVC {
    
    //------------------------------------------------------
    
    @IBOutlet weak var lblKid2: FredokaOneRegularLabel!
    @IBOutlet weak var lblKid1: FredokaOneRegularLabel!
    @IBOutlet weak var imgKid2: UIImageView!
    @IBOutlet weak var imgKid1: UIImageView!
    
    @IBOutlet weak var txtMessage: UITextView!
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    @IBAction func btnCrossTap(_ sender: Any) {
    }
    
    @IBAction func btnDismissTap(_ sender: Any) {
    }
    @IBAction func btnSendTap(_ sender: Any) {
    }
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
