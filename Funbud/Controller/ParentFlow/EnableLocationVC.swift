//
//  EnableLocationVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 07/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import MapKit
import GooglePlaces

class EnableLocationVC : BaseVC, LocationManagerDelegate, UITextFieldDelegate, LocationSearchDelegate {
    
    @IBOutlet weak var txtplace: ATCTextField!
    @IBOutlet weak var locationSwitch: UISwitch!
    
    override var parentModal: ParentsModal? {
        didSet {
            guard txtplace != nil else {
                return
            }
            txtplace.text = parentModal?.location
        }
    }
    
    var isVicinity: Bool = false
   
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func validate() -> Bool {
        
        /*if ValidationManager.shared.isEmpty(text: self.parentModal?.lattitude) || ValidationManager.shared.isEmpty(text: self.parentModal?.longitude) {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.location) {
            }
            return false
        }*/
        return true
    }
    
    func performSave(parent parentModal: ParentsModal) {
             
        LoadingManager.shared.showLoading()
        
        delay {
            
            let firebaseId: String = PreferenceManager.shared.userId ?? String()
            parentModal.isLocationEnable = self.locationSwitch.isOn ? "1" : "0"
            FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
            
            if self.isVicinity && parentModal.kidsList != nil {
                
                parentModal.kidsList.forEach { arg0 in
                    
                    let playDatesModal = PlayDatesModal(fromDictionary: [:])
                    playDatesModal.parentID = PreferenceManager.shared.userId ?? String()
                    playDatesModal.title = getDynamicTitles().random() ?? String()
                    playDatesModal.childList = [arg0]
                    playDatesModal.groupEvent = false
                    playDatesModal.ownerImage = parentModal.imgsource
                    playDatesModal.location = parentModal.location
                    
                    let playDate = DateTimeManager.shared.date(byAddingHours: 336, toDate: Date()) ?? Date()
                    let stringDate = DateTimeManager.shared.stringFrom(date: playDate, inFormate: DateFormate.UTC)
                    playDatesModal.date = stringDate
                    playDatesModal.createdOnChildId = arg0
                    playDatesModal.isAutoGenerate = "true"
                    playDatesModal.isActive = "false"
                    
                    FirebaseManager.shared.save(playDatesOf: playDatesModal)
                }
            }
            
            LoadingManager.shared.hideLoading()
            
            delay {
                
                //NavigationManager.shared.setupLanding()
                NavigationManager.shared.setupParentPendingForApproval()
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func locationSwitchChanged(_ sender: UISwitch) {
        
        if (sender.isOn == true) {
            //self.txtplace.isHidden = true
            self.txtplace.isEnabled = false
            if LocationManager.shared.isRequiredToShowPermissionDialogue() {
                LocationManager.shared.requestForLocationPermissionWhenInUse()
                locationSwitch.setOn(false, animated: true)
            } else {
                LocationManager.shared.delegate = self
                LocationManager.shared.startMonitoring()
            }
        } else {
            //self.txtplace.isHidden = false
            self.txtplace.isEnabled = true
        }
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.pop()
    }
    
    @IBAction func finishBtnClicked(_ sender: UIButton) {
        
        if validate() == false {
            return
        } else {
            self.parentModal?.location = txtplace.text
            self.parentModal?.isNotificationEnabled = "1"
            if let parentModal = self.parentModal {
                performSave(parent: parentModal)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        /*let controller = NavigationManager.shared.locationSearchNC
        if let vc = controller.viewControllers.first as? LocationSearchVC {
            vc.delegate = self
        }
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true, completion: nil)*/
        
        PlacesSearchManager.shared.showAutoComplete(self.navigationController, didAutoCompleteWith: { (ac: GMSAutocompleteViewController, place: GMSPlace) in
            
            debugLog(object: place.description)
            let formattedAddress = place.formattedAddress
            self.txtplace.text = formattedAddress
            self.parentModal?.lattitude = "\(place.coordinate.latitude)"
            self.parentModal?.longitude = "\(place.coordinate.longitude)"
            self.parentModal?.location = formattedAddress
            ac.dismiss(animated: true, completion: nil)
            
        }, failureBlock:  { (ac: GMSAutocompleteViewController, error: Error?) in
            
            ac.dismiss(animated: true, completion: nil)
            
            if let error = error {
                DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
            }
        })
        
        return false
    }
        
    //------------------------------------------------------
    
    //MARK: LocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let lat = locations.first?.coordinate.latitude ?? .zero
        let lng = locations.first?.coordinate.longitude ?? .zero
                
        self.parentModal?.lattitude = String( lat )
        self.parentModal?.longitude = String( lng )
        
        LocationManager.shared.lat = lat
        LocationManager.shared.lng = lng
        
        if lat != .zero && lng != .zero {
        
            LocationManager.shared.stopMonitoring()
            
            PlacesSearchManager.shared.getAddressFrom(latitude: LocationManager.shared.lat, longitude: LocationManager.shared.lng) { address, error in
                self.txtplace.text = address
                self.parentModal?.location = address
                
                if self.locationSwitch.isOn == false {
                    self.locationSwitch.isOn = true
                }
            }
        }                
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        #if targetEnvironment(simulator)
        DisplayAlertManager.shared.displayAlertWithNoYes(target: self, animated: true, message: error.localizedDescription) {
        } handlerYes: {
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
        #else
        DisplayAlertManager.shared.displayAlertWithNoYes(target: self, animated: true, message: LocalizableConstants.Error.locationServiceDisable.localized()) {
        } handlerYes: {
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
        #endif
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        //Handle heading
    }
    
    //------------------------------------------------------
    
    //MARK: LocationSearchDelegate
    
    func locationSearch(controller: LocationSearchVC, didSelect location: MKPlacemark) {
        
        let fullAddress = String(format: "%@ %@", location.name ?? String(), location.formattedAddress ?? location.name ?? String())
        txtplace.text = fullAddress
        self.parentModal?.lattitude = "\(location.coordinate.latitude)"
        self.parentModal?.longitude = "\(location.coordinate.longitude)"
        self.parentModal?.location = fullAddress
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtplace.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        locationSwitch.isOn = true
        //self.txtplace.isHidden = true
        //self.txtplace.isEnabled = false
        if LocationManager.shared.isRequiredToShowPermissionDialogue() {
            LocationManager.shared.requestForLocationPermissionWhenInUse()
            locationSwitch.setOn(false, animated: true)
        } else {
            LocationManager.shared.delegate = self
            LocationManager.shared.startMonitoring()
        }
        PreferenceManager.shared.isAtLocationEnable  = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if parentModal == nil {
            FirebaseManager.shared.find(parentFrom: PreferenceManager.shared.userId ?? String()) { dict in
                self.parentModal = ParentsModal(fromDictionary: dict)
            }
        } else if let parentModal = self.parentModal {
            self.parentModal = parentModal
        }
    }
        
    //------------------------------------------------------
}
