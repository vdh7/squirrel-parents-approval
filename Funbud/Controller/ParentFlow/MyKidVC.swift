//
//  MyKidVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 18/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import FittedSheets

class MyKidVC : BaseVC, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var myKidTableVw: UITableView!
    
    var kids: [KidsModal] = []
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(PMNotification.refreshMyKids), object: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        myKidTableVw.delegate = self
        myKidTableVw.dataSource = self
        myKidTableVw.register(UINib(nibName: String(describing: MyKidsTVC.self), bundle: nil), forCellReuseIdentifier: String(describing: MyKidsTVC.self))
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshMyKids), name: NSNotification.Name(rawValue: PMNotification.refreshMyKids), object: nil)
    }
    
    func bottomSheetView(kid: KidsModal?) {
        
        let controller = KidsProfileVC.instantiate()
        //controller.kid = kid
        var sizes = [SheetSize]()
        sizes.append(.fixed(UIScreen.main.bounds.size.height * 0.5))
        sizes.append(.marginFromTop(60))
        let sheetController = SheetViewController(controller: controller, sizes: sizes)
        self.present(sheetController, animated: true, completion: nil)
    }
    
    func loadKids() {
        
        //LoadingManager.shared.showLoading()
        
        parentModal?.findKid(completion: { kids in
        
            //LoadingManager.shared.hideLoading()
            
            delay {
                self.kids = kids
                debugPrint(kids)
                self.myKidTableVw.reloadData()
            }
        })
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func btnBackTap(_ sender: Any) {
        
        //self.pop()
        NavigationManager.shared.sideMenuController.showLeftView()
    }
    
    @IBAction func btnEditTap(_ sender: Any) {
        
        let controller = NavigationManager.shared.editKidVC
        controller.kidModal = nil
        push(controller: controller)
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return kids.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = myKidTableVw.dequeueReusableCell(withIdentifier: String(describing: MyKidsTVC.self), for: indexPath) as? MyKidsTVC {
            let kid = kids[indexPath.row]
            cell.setup(kidsModal: kid)
            cell.kidDetails = { kid in
                self.bottomSheetView(kid: kid)
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let kid = kids[indexPath.row]
        let controller = NavigationManager.shared.editKidVC
        controller.kidModal = kid
        push(controller: controller)
    }
    
    //------------------------------------------------------
    
    //MARK: Selector
    
    @objc func refreshMyKids(_ notification: Notification) {
        
        FirebaseManager.shared.find(parentFrom: FirebaseManager.shared.currentUser()?.uid ?? String()) { dict in
            let parentModal = ParentsModal(fromDictionary: dict)
            FirebaseManager.shared.parentModal = parentModal
            self.parentModal = FirebaseManager.shared.parentModal
            self.myKidTableVw.reloadData()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if FirebaseManager.shared.isSignedIn() {
            FirebaseManager.shared.find(parentFrom: FirebaseManager.shared.currentUser()?.uid ?? String()) { dict in
                let parentModal = ParentsModal(fromDictionary: dict)
                FirebaseManager.shared.parentModal = parentModal
                self.parentModal = FirebaseManager.shared.parentModal
                self.loadKids()                
            }
        }
    }
    
    //------------------------------------------------------
}
