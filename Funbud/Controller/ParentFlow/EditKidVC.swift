//
//  EditKidVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 18/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class EditKidVC : BaseVC, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var lblTitle: PoppinsRegularLabel!
    @IBOutlet weak var btnDeleteKid: FredokaOneRegularButton!
    @IBOutlet weak var editKidTableVw: UITableView!
    @IBOutlet weak var viewVictinity: UIView!
    @IBOutlet weak var btnVicinity: UIButton!
    
    var kidModal: KidsModal?
    
    override var parentModal: ParentsModal? {
        
        didSet {
            
            guard editKidTableVw != nil else {
                return
            }
            editKidTableVw.reloadData()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
        
    deinit { //same like dealloc in ObjectiveC
        
    }

    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func validate() -> Bool {
        
        let indexPath = IndexPath(row: .zero, section: .zero)
        
        if let cell = editKidTableVw.cellForRow(at: indexPath) as? AddKidTVC {
            
            if cell.isEmptyName() {
                DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.emptyKidName) {
                }
                return false
            }
            
            if cell.isEmptyDOB() {
                DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.emptyKidDOB) {
                }
                return false
            }
            
            if cell.isEmptySpecifyGender() {
                DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.emptyKidSpecifyGender) {
                }
                return false
            }
            
            if cell.isAvatarSelected() == false {
                DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.emptyKidAvatar) {
                }
                return false
            }
            
            if cell.isEmptyDescription() {
                DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.emptyKidDescription) {
                }
                return false
            }
        }
        
        return true
    }
      
    func getKidsInfo() -> [KidsModal] {
        
        //var kids: [KidsModal] = parentModal?.kidsList ?? []
        var kids: [KidsModal] = []
        
        let indexPath = IndexPath(row: .zero, section: .zero)
        
        if let cell = editKidTableVw.cellForRow(at: indexPath) as? AddKidTVC {
            
            let name = cell.getName()
            let dob = cell.getDOB()
            let genderIndex = cell.getGenderIndex()
            let specifyGender = cell.getSpecifyGender()
            let selectedAvatarIndex = cell.getSelectedAvtarIndex()
            let hobbies = cell.getDescription()
            
            let kid = self.kidModal ?? KidsModal(fromDictionary: [:])
            kid.parentID = parentModal?.userid ?? PreferenceManager.shared.userId ?? String()
            //kid.id = kidModal?.id ?? String( (parentModal?.kidsList.count ?? .zero) + 1)
            kid.name = name
            kid.dob = dob
            kid.gender = genderIndex
            kid.genderText = specifyGender
            kid.avatar = selectedAvatarIndex
            kid.hobbies = hobbies
            
            if self.kidModal == nil {
                kids.append(kid)
            } else {
                if let index = kids.firstIndex(where: { arg0 in
                    return arg0.firebaseID == kid.firebaseID && kid.parentID == arg0.parentID
                }) {
                    kids[index] = kid
                } else {
                    kids.append(kid)
                }
            }
        }
        return kids
    }
    
    /*func getKidsInfoAfterDelete() -> [KidsModal] {
        
        //var kids: [KidsModal] = parentModal?.kidsList ?? []
        var kids: [KidsModal] = []
        
        if let index = kids.firstIndex(where: { arg0 in
            return arg0.firebaseID == kidModal?.firebaseID && kidModal?.parentID == arg0.parentID
        }) {
            kids.remove(at: index)
        }
        return kids
    }*/
    
    func performSave(parent parentModal: ParentsModal) {
       
        LoadingManager.shared.showLoading()
        
        delay {
            
            if let kidModal = self.getKidsInfo().first {
                //_ = FirebaseManager.shared.save(kid: kidModal)
                var kidIds: [String] = parentModal.kidsList ?? []
                for kid in self.getKidsInfo() {
                    let kidID = FirebaseManager.shared.save(kid: kid)
                    if !kidIds.contains(kidID) {
                        kidIds.append(kidID)
                        
                        if self.btnVicinity.isSelected {
                            
                            let playDatesModal = PlayDatesModal(fromDictionary: [:])
                            playDatesModal.parentID = PreferenceManager.shared.userId ?? String()
                            playDatesModal.title = getDynamicTitles().random() ?? String()
                            playDatesModal.childList = [kidID]
                            playDatesModal.groupEvent = true
                            playDatesModal.ownerImage = parentModal.imgsource
                            playDatesModal.location = parentModal.location
                            
                            let playDate = DateTimeManager.shared.date(byAddingHours: 336, toDate: Date()) ?? Date()
                            let stringDate = DateTimeManager.shared.stringFrom(date: playDate, inFormate: DateFormate.UTC)
                            playDatesModal.date = stringDate
                            playDatesModal.createdOnChildId = kidID
                            playDatesModal.isAutoGenerate = "true"
                            
                            FirebaseManager.shared.save(playDatesOf: playDatesModal)
                        }
                    }
                }
                FirebaseManager.shared.update(kidIDs: kidIds, to: kidModal.parentID)
                //FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
                NotificationCenter.default.post(name: NSNotification.Name(PMNotification.refreshMyKids), object: nil)
                LoadingManager.shared.hideLoading()
                delay {
                    self.pop()
                }
            }
        }
    }
    
    /*func performDeleteKid(parent parentModal: ParentsModal) {
             
        LoadingManager.shared.showLoading()
        
        delay {
            
            let firebaseId: String = self.currentUser?.uid ?? String()
            //parentModal.kidsList = self.getKidsInfoAfterDelete()
            FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
            LoadingManager.shared.hideLoading()
            
            delay {
                
                self.pop()
            }
        }
    }*/
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func btnDeleteKidTap(_ sender: Any) {
        
        DisplayAlertManager.shared.displayAlertWithNoYes(target: self, animated: true, message: LocalizableConstants.Controller.confirmationKidDelete) {

            //Handle no block

        } handlerYes: {

            self.view.endEditing(true)

            /*if let parentModal = self.parentModal {
                self.performDeleteKid(parent: parentModal)
            }*/
            
            if let kid = self.kidModal {
                
                LoadingManager.shared.showLoading()
                            
                FirebaseManager.shared.delete(kid: kid) { error in
                    if let error = error {
                        LoadingManager.shared.hideLoading()
                        delay {
                            DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
                        }
                    } else {
                        LoadingManager.shared.hideLoading()
                        delay {
                            self.pop()
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func btnBackTap(_ sender: Any) {
        self.pop()
    }
    
    @IBAction func btnSaveTap(_ sender: PoppinsBoldButton) {
        
        if validate() == false {
            return
        }
        self.view.endEditing(true)
        
        if let parentModal = self.parentModal {
            performSave(parent: parentModal)
        }
    }
    
    @IBAction func btnVictinityTap(_ sender: UIButton) {
        if sender.isSelected == false {
            sender.isSelected = true
        } else {
            sender.isSelected = false
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = editKidTableVw.dequeueReusableCell(withIdentifier: String(describing: AddKidTVC.self), for: indexPath) as? AddKidTVC {
            cell.segmentVW.addTarget(self, action: #selector(EditKidVC.segmentValueChanged(_:)), for: .valueChanged)
            cell.segmentVW.tag = indexPath.row
            cell.deleteStackVw.isHidden = true
            if let kid = kidModal {
                cell.setup(kidsModal: kid)
            } else {
                cell.reset()
            }
            return cell
        }
        return UITableViewCell()
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    //------------------------------------------------------
    
    //MARK: Selector
    
    @objc func segmentValueChanged(_ sender: HBSegmentedControl) {
        
        let indexs = IndexPath.init(row:sender.tag, section: 0)
        let cell = editKidTableVw.cellForRow(at: indexs) as! AddKidTVC
        if sender.selectedIndex == 0{
            cell.txtSpecifyGender.isHidden = true
        }else if sender.selectedIndex == 1{
            cell.txtSpecifyGender.isHidden = true
        }else{
            cell.txtSpecifyGender.isHidden = false
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnDeleteKid.isHidden = kidModal == nil
        
        if self.kidModal == nil { //add
            lblTitle.text = LocalizableConstants.Controller.addKid.localized()
            viewVictinity.isHidden = false
        } else {
            lblTitle.text = LocalizableConstants.Controller.editKid.localized()
            viewVictinity.isHidden = true
        }
        
        editKidTableVw.delegate = self
        editKidTableVw.dataSource = self
        editKidTableVw.register(UINib(nibName: String(describing: AddKidTVC.self), bundle: nil), forCellReuseIdentifier: String(describing: AddKidTVC.self))
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
   
}
