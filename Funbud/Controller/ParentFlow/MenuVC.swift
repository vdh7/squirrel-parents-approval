//
//  MenuVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 08/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Firebase

class MenuVC: BaseVC, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var menuTableVw: UITableView!
    @IBOutlet weak var gestureVw: UIView!
    
    var menus: [MenuModal] = []
    
    override var parentModal: ParentsModal? {
        didSet {
            imgLogo.image = UIImage(named: PMImageName.menu_logo_parents)
        }
    }
    override var babysitterModal: BabysitterModal? {
        didSet {
            imgLogo.image = UIImage(named: PMImageName.menu_logo_babysitter)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
   
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
             
        if isParentProfile {
            menus = MenuModal.getParentsProfileMenu()
        } else {
            menus = MenuModal.getBabysitterProfileMenu()
        }
                      
        menuTableVw.delegate = self
        menuTableVw.dataSource = self
        menuTableVw.register(UINib(nibName: String(describing: MenuListTVC.self), bundle: nil), forCellReuseIdentifier: String(describing: MenuListTVC.self))
    }
    
    func dismissDetail() {
        
        NavigationManager.shared.sideMenuController.hideLeftView()
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        dismissDetail()
    }
    
    @IBAction func logOutBtnClicked(_ sender: PoppinsBoldButton) {
        
        DisplayAlertManager.shared.displayAlertWithNoYes(target: self, animated: true, message: LocalizableConstants.Controller.confirmationSignout) {
            
            //Handle no block
            
        } handlerYes: {

            delay {
                
                //LoadingManager.shared.showLoading()
                
                do {
                    
                    LoadingManager.shared.showLoading()
                    
                    try FirebaseManager.shared.signOut() 
                    PreferenceManager.shared.reset()
                    
                    delay {
                        delay {
                            delay {
                                LoadingManager.shared.hideLoading()
                                NavigationManager.shared.setupSignInOption()
                            }
                        }
                    }                    
                    
                    /*FirebaseManager.shared.resetUser { isReset in
                        
                        LoadingManager.shared.hideLoading()
                        
                        if isReset {
                            
                            delay {
                                PreferenceManager.shared.reset()
                                NavigationManager.shared.setupSignInOption()
                            }
                            
                        } else {
                            
                            delay {
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Error.failed_to_signout, handlerOK: nil)
                            }
                        }
                    }*/
                    
                } catch {
                    
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
                    }
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MenuListTVC.self), for: indexPath) as? MenuListTVC {
            
            let menu = menus[indexPath.row]
            if menu.name == nil {
                cell.lineVw.isHidden = true
            } else {
                cell.lineVw.isHidden = false
            }
            cell.lblMenu.text = menus[indexPath.row].name
            cell.menuImg.image = UIImage(named: menus[indexPath.row].imgName ?? String())
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let menu = menus[indexPath.row]
        if isParentProfile {
            if menu.menuId == "1" {//parent profile
                NavigationManager.shared.sideMenuController.hideLeftView()
                let controller = NavigationManager.shared.myKidVC
                //NavigationManager.shared.sideMenuController.navigationController?.pushViewController(controller, animated: true)
                NavigationManager.shared.sideMenuController.rootViewController = controller
            } else if menu.menuId == "2" {//parent profile
                NavigationManager.shared.sideMenuController.hideLeftView()
                if let parentModal = self.parentModal, parentModal.role == RoleType.parent.rawValue {
                    let controller = NavigationManager.shared.editParentProfileVC
                    //NavigationManager.shared.sideMenuController.navigationController?.pushViewController(controller, animated: true)
                    NavigationManager.shared.sideMenuController.rootViewController = controller
                } else if let babysitterModal = self.babysitterModal, babysitterModal.role == RoleType.babysitter.rawValue {
                    let controller = NavigationManager.shared.editBabysitterProfileVC
                    //NavigationManager.shared.sideMenuController.navigationController?.pushViewController(controller, animated: true)
                    NavigationManager.shared.sideMenuController.rootViewController = controller
                }
            } else if menu.menuId == "3" {
                NavigationManager.shared.sideMenuController.hideLeftView()
                let controller = NavigationManager.shared.myPlaydatesListVC
                //NavigationManager.shared.sideMenuController.navigationController?.pushViewController(controller, animated: true)
                NavigationManager.shared.sideMenuController.rootViewController = controller
            } else if menu.menuId == "4" {
                NavigationManager.shared.sideMenuController.hideLeftView()
                let controller = NavigationManager.shared.settingsVC
                //NavigationManager.shared.sideMenuController.navigationController?.pushViewController(controller, animated: true)
                NavigationManager.shared.sideMenuController.rootViewController = controller
            } else if menu.menuId == "6" {
                //NavigationManager.shared.sideMenuController.hideLeftView()
                openSafariViewController(urlString: "https://www.apple.com")
            } else if menu.menuId == "7" {
                //NavigationManager.shared.sideMenuController.hideLeftView()
                openSafariViewController(urlString: "https://www.apple.com")
            } else if menu.menuId == "8" {
                NavigationManager.shared.sideMenuController.hideLeftView()
                let controller = NavigationManager.shared.tabbarController
                controller?.selectedIndex = 0
                NavigationManager.shared.sideMenuController.rootViewController = controller
            }
            
        } else {
            
            if menu.menuId == "1" {//babysitter profile
                
                NavigationManager.shared.sideMenuController.hideLeftView()
                let controller = NavigationManager.shared.editBabysitterProfileVC
                //NavigationManager.shared.sideMenuController.navigationController?.pushViewController(controller, animated: true)
                NavigationManager.shared.sideMenuController.rootViewController = controller
                
            } else if menu.menuId == "2" {
                
                NavigationManager.shared.sideMenuController.hideLeftView()
                let controller = NavigationManager.shared.bSAvailabilityVC
                controller.isFromMenu = true
                //NavigationManager.shared.sideMenuController.navigationController?.pushViewController(controller, animated: true)
                NavigationManager.shared.sideMenuController.rootViewController = controller
                
            } else if menu.menuId == "3" {
                
                NavigationManager.shared.sideMenuController.hideLeftView()
                let controller = NavigationManager.shared.settingsVC
                //NavigationManager.shared.sideMenuController.navigationController?.pushViewController(controller, animated: true)
                NavigationManager.shared.sideMenuController.rootViewController = controller
                
            } else if menu.menuId == "5" {
                //NavigationManager.shared.sideMenuController.hideLeftView()
                openSafariViewController(urlString: "https://www.apple.com")
            } else if menu.menuId == "6" {
                //NavigationManager.shared.sideMenuController.hideLeftView()
                openSafariViewController(urlString: "https://www.apple.com")
            } else if menu.menuId == "7" {
                NavigationManager.shared.sideMenuController.hideLeftView()
                let landingsNC = NavigationManager.shared.landingNC
                let bSChatVC = NavigationManager.shared.bSChatVC
                landingsNC.setViewControllers([bSChatVC], animated: false)
                NavigationManager.shared.sideMenuController.rootViewController = landingsNC
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Selector
        
    @objc func handleSwipes(_ sender: UISwipeGestureRecognizer)
    {
        if sender.direction == .left
        {
           print("Swipe left")
           // show the view from the right side
            dismissDetail()
        }

        if sender.direction == .right
        {
           print("Swipe right")
           
           // self.navigationController?.popViewController(animated: true)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
               
        setup()
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        FirebaseManager.shared.find(parentFrom: PreferenceManager.shared.userId ?? String()) { dict in
            let baseModal = BaseModal(fromDictionary: dict)
            if baseModal.role == RoleType.parent.rawValue {
                self.parentModal = ParentsModal(fromDictionary: dict)
            } else if baseModal.role == RoleType.babysitter.rawValue {
                self.babysitterModal = BabysitterModal(fromDictionary: dict)
            }
            
            if self.isParentProfile {
                self.menus = MenuModal.getParentsProfileMenu()
            } else {
                self.menus = MenuModal.getBabysitterProfileMenu()
            }
            
            self.menuTableVw.reloadData()
        }
    }
    
    //------------------------------------------------------
}
