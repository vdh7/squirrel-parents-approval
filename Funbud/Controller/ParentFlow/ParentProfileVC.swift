//
//  ParentProfileVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 01/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Toucan
import HMSegmentedControl
import SDWebImage

class ParentProfileVC : BaseVC,ImagePickerDelegate {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var imgPlaceholder: SLBaseImageView!
    @IBOutlet weak var imgSelectedView: SLBaseImageView!
    @IBOutlet weak var txtName: ATCTextField!
    @IBOutlet weak var segmentVW: HBSegmentedControl!
    @IBOutlet weak var txtSpecificRelation: ATCTextField!
    @IBOutlet weak var nextButton: PoppinsBoldButton!
    @IBOutlet weak var specificGenderTxtHeight: NSLayoutConstraint!
    
    @IBOutlet weak var fileDetailVw: UIView!
    @IBOutlet weak var lblFilesize: PoppinsRegularLabel!
    @IBOutlet weak var lblFileName: PoppinsRegularLabel!
    @IBOutlet weak var uploadfileBtn: UIButton!
    @IBOutlet weak var uploadFIleVw: UIView!
    
    var imagePickerVC: SLImagePickerVC?
    var isImagePickerForProfile: Bool = false
    
    var selectedImage: UIImage? {
        didSet {
            if selectedImage != nil {
                imgSelectedView.image = selectedImage
            }
        }
    }
    
    var selectedImageId: UIImage? {
        
        didSet {
            
            if selectedImageId != nil {
                self.fileDetailVw.isHidden = false
                uploadFIleVw.isHidden = !self.fileDetailVw.isHidden
                
                let fileName = String(format: "%@.jpeg", "ID")
                lblFileName.text = fileName
                let displaySize = countBytes(ofData: selectedImageId?.pngData())
                lblFilesize.text = displaySize
                
            } else {
                
                self.fileDetailVw.isHidden = true
                uploadFIleVw.isHidden = !self.fileDetailVw.isHidden
            }
        }
    }
    
    override var parentModal: ParentsModal? {
        
        didSet {
            
            guard parentModal != nil else {
                return
            }
            
            txtName.text = parentModal?.name
            segmentVW.selectedIndex = Int(parentModal?.gender ?? "0") ?? .zero
            txtSpecificRelation.text = parentModal?.genderText
            segmentValueChanged(segmentVW)
            
            if let imageURL = parentModal?.imgsource {
                imgSelectedView.sd_setIndicatorStyle(UIActivityIndicatorView.Style.gray)
                imgSelectedView.sd_addActivityIndicator()
                imgSelectedView.sd_showActivityIndicatorView()
                imgSelectedView.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                    self.imgSelectedView.image = img
                    self.imgSelectedView.sd_removeActivityIndicator()
                }
            }
            
            if let imageURL = parentModal?.imageId {
                let imgView = UIImageView()
                imgView.sd_setIndicatorStyle(UIActivityIndicatorView.Style.gray)
                imgView.sd_addActivityIndicator()
                imgView.sd_showActivityIndicatorView()
                imgView.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                    self.selectedImageId = img
                    imgView.sd_removeActivityIndicator()
                }
            }
        }
    }
    
    var role: RoleType = .parent
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //MARK: Customs
    
    func setup() {
        imagePickerVC = SLImagePickerVC(presentationController: self, delegate: self)
    }
    
    func validate() -> Bool {
        
        if ValidationManager.shared.isEmpty(text: parentModal?.imgsource) && selectedImage == nil {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.selectParentProfileImage) {
            }
            return false
        }
        
        if ValidationManager.shared.isEmpty(text: txtName.text) == true {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.enterParentName) {
            }
            return false
        }
        
        if ValidationManager.shared.isEmpty(text: txtSpecificRelation.text) == true && txtSpecificRelation.isHidden == false {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.specficRelation) {
            }
            return false
        }
        
        if  ValidationManager.shared.isEmpty(text: parentModal?.imageId) && selectedImageId == nil {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.selectBabysitterIdImage) {
            }
            return false
        }
        
        return true
    }
    
    func performSave(parent parentModal: ParentsModal) {
        
        LoadingManager.shared.showLoading()
        
        delay {
            
            let firebaseId: String = PreferenceManager.shared.userId ?? String()
            let fileName = String(format: "%@.jpeg", firebaseId)
            
            if self.selectedImage != nil {
                
                parentModal.userid = firebaseId
                parentModal.role = self.role.rawValue
                
                FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
                                
                FirebaseManager.shared.save(image: firebaseId, fileName: fileName, imageData: self.selectedImage?.pngData() ?? Data(), completion: { (imageURL: URL?, error: Error?) in
                    
                    if let error = error {
                        LoadingManager.shared.hideLoading()
                        delay {
                            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message:
                                                                        error.localizedDescription) {
                            }
                        }
                    } else if self.selectedImageId != nil {
                        
                        FirebaseManager.shared.save(imageId: firebaseId, fileName: fileName, imageData: self.selectedImageId?.pngData() ?? Data(), completion: { (imageIdURL: URL?, error: Error?) in
                            
                            if let error = error {
                                LoadingManager.shared.hideLoading()
                                delay {
                                    DisplayAlertManager.shared.displayAlert(target: self, animated: true, message:
                                                                                error.localizedDescription) {
                                    }
                                }
                            } else {
                                                                                                
                                parentModal.imgsource = imageURL?.absoluteString ?? String()
                                parentModal.imageId = imageIdURL?.absoluteString ?? String()
                                FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
                                LoadingManager.shared.hideLoading()
                                
                                delay {
                                    
                                    /*let controller = NavigationManager.shared.bSAvailabilityVC
                                    controller.babysitterModal = self.babysitterModal
                                    self.push(controller: controller)
                                    */
                                    
                                    let controller = NavigationManager.shared.addKidVC
                                    controller.parentModal = parentModal
                                    self.push(controller: controller)
                                }
                            }
                        })
                        
                    } else if let stringImageURL = imageURL?.absoluteString {
                        
                        parentModal.imgsource = stringImageURL
                        FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
                        LoadingManager.shared.hideLoading()
                        
                        delay {
                            let controller = NavigationManager.shared.addKidVC
                            controller.parentModal = parentModal
                            self.push(controller: controller)
                        }
                    }
                })
                
            } else if self.selectedImageId != nil {
                
                FirebaseManager.shared.save(imageId: firebaseId, fileName: fileName, imageData: self.selectedImageId?.pngData() ?? Data(), completion: { (imageIdURL: URL?, error: Error?) in
                    
                    if let error = error {
                        LoadingManager.shared.hideLoading()
                        delay {
                            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message:
                                                                        error.localizedDescription) {
                            }
                        }
                    } else {
                                                                                                                
                        parentModal.imageId = imageIdURL?.absoluteString ?? String()
                        FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
                        LoadingManager.shared.hideLoading()
                        
                        delay {
                            let controller = NavigationManager.shared.addKidVC
                            controller.parentModal = parentModal
                            self.push(controller: controller)
                        }
                    }
                })
                
            } else {
                
                FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
                LoadingManager.shared.hideLoading()
                
                delay {
                    
                    parentModal.userid = firebaseId
                    parentModal.role = self.role.rawValue
                    
                    FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        
                        let controller = NavigationManager.shared.addKidVC
                        controller.parentModal = parentModal
                        self.push(controller: controller)
                    }
                }
            }
        }
    }
    
    //-----------------------------
    
    // MARK: Actions
    
    @IBAction func nextButtonClicked(_ sender: PoppinsBoldButton) {
     
        if validate() == false {
            return
        }
        self.view.endEditing(true)
        let parentModals = parentModal ?? ParentsModal(fromDictionary: [:])
        parentModals.name = txtName.text
        parentModals.gender = String(segmentVW.selectedIndex)
        parentModals.genderText = txtSpecificRelation.text
        performSave(parent: parentModals)
    }
    
    @IBAction func cameraButtonClicked(_ sender: UIButton) {
        
        isImagePickerForProfile = true
        self.imagePickerVC?.present(from: sender)
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.pop()        
    }
    
    @IBAction func uploadFileBtnClicked(_ sender: UIButton) {
        
        /*self.fileDetailVw.isHidden = false
        self.uploadFIleVw.isHidden = true*/
        
        isImagePickerForProfile = false
        self.imagePickerVC?.present(from: sender)
    }
    
    
    @IBAction func fileCrossBtnClicked(_ sender: UIButton) {
        
        /*self.fileDetailVw.isHidden = true
        self.uploadFIleVw.isHidden = false*/
        selectedImageId = nil
    }
    
    //------------------------------------------------------
    
    //MARK: SlImagePickerDelegate
    
    func didSelect(image: UIImage?) {
        
        /*if let imageData = image?.jpegData(compressionQuality: 0), let compressImage = UIImage(data: imageData) {
            self.selectedImage = Toucan.init(image: compressImage).resizeByCropping(PMSettings.profileImageSize).maskWithRoundedRect(cornerRadius: PMSettings.profileImageSize.width/2, borderWidth: PMSettings.profileBorderWidth, borderColor: PMColor.clear).image
        }*/
        
        if let imageData = image?.jpegData(compressionQuality: 0), let compressImage = UIImage(data: imageData) {            
            if isImagePickerForProfile {
                self.selectedImage = Toucan.init(image: compressImage).resizeByCropping(PMSettings.profileImageSize).maskWithRoundedRect(cornerRadius: PMSettings.profileImageSize.width/2, borderWidth: PMSettings.profileBorderWidth, borderColor: PMColor.clear).image
            } else {
                self.selectedImageId = Toucan.init(image: compressImage).image
            }
        }
    }
    
    //------------------------------------------------------
    
    // MARK:- Selector
    
    @objc func segmentValueChanged(_ sender: AnyObject?) {
        if segmentVW.selectedIndex == 0 {
            txtSpecificRelation.isHidden = true
            
        }else if segmentVW.selectedIndex == 1{
            txtSpecificRelation.isHidden = true
        }else{
            txtSpecificRelation.isHidden = false
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentVW.items = ParentsType.allCases.map({ $0.rawValue })
        segmentVW.font = PMFont.sfpoppinsRegular(size: 14)
       
        segmentVW.selectedIndex = 0
        segmentVW.padding = 4
        txtSpecificRelation.isHidden = true
        segmentVW.addTarget(self, action: #selector(ParentProfileVC.segmentValueChanged(_:)), for: .valueChanged)
        setup()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PreferenceManager.shared.isAtParentProfileSelection = true
        
        if self.parentModal == nil {
            FirebaseManager.shared.find(parentFrom: PreferenceManager.shared.userId ?? String()) { dict in
                self.parentModal = ParentsModal(fromDictionary: dict)
            }
        }
    }
    
    //------------------------------------------------------
}

