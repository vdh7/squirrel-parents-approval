//
//  DeleteVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 22/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class DeleteVC : BaseVC {
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    @IBAction func btnCancelTap(_ sender: Any) {
    }
    @IBAction func btnDeleteTap(_ sender: Any) {
    }
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
