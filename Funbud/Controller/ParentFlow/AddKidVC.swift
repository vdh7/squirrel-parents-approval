//
//  AddKidVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 02/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

var keyAssociateObjKids: Int = 0

class AddKidVC : BaseVC, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var addKidBtn: PoppinsBoldButton!
    @IBOutlet weak var nextBtn: PoppinsBoldButton!
    @IBOutlet weak var addkidScollView: UIScrollView!
    @IBOutlet weak var addkidTableVw: UITableView!
    @IBOutlet weak var addKidTWHeightConst: NSLayoutConstraint!
    @IBOutlet weak var btnVicinity: UIButton!
    
    override var parentModal: ParentsModal? {
        
        didSet {
           
            guard addkidTableVw != nil else {
                return
            }
            numberOfCell = .zero
            addkidTableVw.reloadData()
        }
    }
    
    var numberOfCell = Int() {
        didSet {
            if numberOfCell > .zero {                
                guard nextBtn != nil else { return }
                nextBtn.isEnabled = true
            }
        }
    }
    
    let kid = ["First Kid", "Second Kid", "Third Kid", "Fourth Kid", "Fifth Kid"]
    var kidsModal: [KidsModal] = []        
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit { //same like dealloc in ObjectiveC
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func validate() -> Bool {
        
        if numberOfCell > .zero {
            
            for index in 0..<numberOfCell {
                
                let indexPath = IndexPath(row: index, section: .zero)
                
                if let cell = addkidTableVw.cellForRow(at: indexPath) as? AddKidTVC {
                    
                    if cell.isEmptyName() {
                        DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.emptyKidName) {
                        }
                        return false
                    }
                    
                    if cell.isEmptyDOB() {
                        DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.emptyKidDOB) {
                        }
                        return false
                    }
                    
                    if cell.isEmptySpecifyGender() {
                        DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.emptyKidSpecifyGender) {
                        }
                        return false
                    }
                    
                    if cell.isAvatarSelected() == false {
                        DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.emptyKidAvatar) {
                        }
                        return false
                    }
                    
                    if cell.isEmptyDescription() {
                        DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.emptyKidDescription) {
                        }
                        return false
                    }
                }
            }
        }
        
        return true
    }
    
    func performSave(parent parentModal: ParentsModal) {
        
        LoadingManager.shared.showLoading()
        
        delay {
            
            let firebaseId: String = PreferenceManager.shared.userId ?? String()
            let kidsModal = self.getKidsInfo()
            
            var kidIds: [String] = []
            for kid in self.getKidsInfo() {
                let kidID = FirebaseManager.shared.save(kid: kid)
                kidIds.append(kidID)
                
                /*let playDatesModal = PlayDatesModal(fromDictionary: [:])
                playDatesModal.parentID = PreferenceManager.shared.userId ?? String()
                playDatesModal.title = getDynamicTitles().random() ?? String()
                playDatesModal.childList = [kidID]
                playDatesModal.groupEvent = true
                playDatesModal.ownerImage = parentModal.imgsource
                playDatesModal.location = parentModal.location
                
                let playDate = DateTimeManager.shared.date(byAddingHours: 336, toDate: Date()) ?? Date()
                let stringDate = DateTimeManager.shared.stringFrom(date: playDate, inFormate: DateFormate.UTC)
                playDatesModal.date = stringDate
                
                FirebaseManager.shared.save(playDatesOf: playDatesModal)*/
            }
            
            parentModal.kidsList = kidIds
            FirebaseManager.shared.update(kidIDs: kidIds, to: firebaseId)
            //FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
            LoadingManager.shared.hideLoading()
            
            delay {
                
                let controller = NavigationManager.shared.enableLocationVC
                controller.isVicinity = self.btnVicinity.isSelected
                self.push(controller: controller)
            }
        }
    }
    
    func getKidsInfo() -> [KidsModal] {
        
        var kids: [KidsModal] = []
        
        for index in 0..<numberOfCell {
            
            let indexPath = IndexPath(row: index, section: .zero)
            
            if let cell = addkidTableVw.cellForRow(at: indexPath) as? AddKidTVC {
                
                let name = cell.getName()
                let dob = cell.getDOB()
                let genderIndex = cell.getGenderIndex()
                let specifyGender = cell.getSpecifyGender()
                let selectedAvatarIndex = cell.getSelectedAvtarIndex()
                let hobbies = cell.getDescription()
                
                let kid = cell.kidsModal ?? KidsModal(fromDictionary: [:])
                kid.parentID = parentModal?.userid ?? PreferenceManager.shared.userId ?? String()
                //kid.id = String(index + 1)                
                kid.name = name
                kid.dob = dob
                kid.gender = genderIndex
                kid.genderText = specifyGender
                kid.avatar = selectedAvatarIndex
                kid.hobbies = hobbies
                
                kids.append(kid)
            }
        }
        return kids
    }
    
    func loadKids() {
        
        //LoadingManager.shared.showLoading()
        
        parentModal?.findKid(completion: { kids in
        
            //LoadingManager.shared.hideLoading()
            
            delay {
                self.kidsModal = kids
                self.numberOfCell = kids.count
                /*var indexPaths: [IndexPath] = []
                for index in 0..<self.numberOfCell {
                    let indexPath = IndexPath(row: index, section: .zero)
                    indexPaths.append(indexPath)
                }
                self.addkidTableVw.beginUpdates()
                self.addkidTableVw.insertRows(at: indexPaths, with: .automatic)
                self.addkidTableVw.endUpdates()*/
                debugPrint(kids)
                self.addkidTableVw.reloadData()
            }
        })
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.pop()
    }
    
    @IBAction func nextBtnClicked(_ sender: PoppinsBoldButton) {
        
        if validate() == false {
            return
        }
        self.view.endEditing(true)
        
        if let parentModal = self.parentModal {
            performSave(parent: parentModal)
        }
    }
    
    @IBAction func addKidBtnClicked(_ sender: PoppinsBoldButton) {
        
        view.endEditing(true)
        
        numberOfCell += 1
        self.nextBtn.isHidden = false
        
        let indexPath = IndexPath(row: numberOfCell - 1, section: 0)
        addkidTableVw.beginUpdates()
        addkidTableVw.insertRows(at: [indexPath], with: .automatic)
        addkidTableVw.endUpdates()
        let baseHeight = UIScreen.main.bounds.width * 0.8
        if baseHeight > self.addkidTableVw.contentSize.height {
            self.addKidTWHeightConst.constant = baseHeight
        } else {
            self.addKidTWHeightConst.constant = self.addkidTableVw.contentSize.height
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.2) {
            let bottomOffset = CGPoint(x: self.addkidScollView.contentOffset.x, y: self.addkidScollView.contentSize.height - self.addkidScollView.bounds.height)
            self.addkidScollView.setContentOffset(bottomOffset, animated: true)
        }
    }
    
    @IBAction func btnVicinityTap(_ sender: UIButton) {
        if sender.isSelected == false {
            sender.isSelected = true
        } else {
            sender.isSelected = false
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDataSource
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return numberOfCell
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AddKidTVC.self), for: indexPath) as? AddKidTVC {
            
            if numberOfCell == 5 {
                self.addKidBtn.isHidden = true
            } else {
                self.addKidBtn.isHidden = false
            }
            
            cell.deleteBtn.addTarget(self, action: #selector(nHapusTap(_:)), for: .touchUpInside)
            /*DispatchQueue.main.async {
                self.addKidTWHeightConst.constant = self.addkidTableVw.contentSize.height
            }*/
            cell.segmentVW.addTarget(self, action: #selector(AddKidVC.segmentValueChanged(_:)), for: .valueChanged)
            cell.segmentVW.tag = indexPath.row
            cell.lblKidCount.text = kid[indexPath.row]
            
            if kidsModal.indices.contains(indexPath.row) {
                let kid = kidsModal[indexPath.row]
                objc_setAssociatedObject(cell.deleteBtn!, &keyAssociateObjKids, kid, objc_AssociationPolicy.OBJC_ASSOCIATION_ASSIGN)
                cell.setup(kidsModal: kid)
            } else {
                objc_setAssociatedObject(cell.deleteBtn!, &keyAssociateObjKids, indexPath, objc_AssociationPolicy.OBJC_ASSOCIATION_ASSIGN)
                cell.kidsModal = nil
                cell.reset()
            }
            cell.avatarCollectionVw.reloadData()
            return cell
        }
        return UITableViewCell()
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    //------------------------------------------------------
    
    //MARK: Selector
    
    @objc func segmentValueChanged(_ sender: HBSegmentedControl) {
        
        let indexs = IndexPath.init(row:sender.tag, section: 0)
        let cell = addkidTableVw.cellForRow(at: indexs) as! AddKidTVC
        if sender.selectedIndex == 0 {
            cell.txtSpecifyGender.isHidden = true
        } else if sender.selectedIndex == 1{
            cell.txtSpecifyGender.isHidden = true
        } else {
            cell.txtSpecifyGender.isHidden = false
        }
    }
    
    @objc func nHapusTap(_ sender: UIButton) {
        
        if numberOfCell == 1 {
            self.nextBtn.isHidden = true
            //self.addKidTWHeightConst.constant = 0
            numberOfCell -= 1
            addkidTableVw.reloadData()
            self.addKidTWHeightConst.constant = UIScreen.main.bounds.width * 0.8
        } else {
            DisplayAlertManager.shared.displayAlertWithNoYes(target: self, animated: true, message: LocalizableConstants.Controller.confirmationKidDelete) {
                //Nothing to handle
            } handlerYes: {
                
                if let kid = objc_getAssociatedObject(sender, &keyAssociateObjKids) as? KidsModal {
                    debugPrint(kid.toDictionary())
                    
                    LoadingManager.shared.showLoading()
                    
                    FirebaseManager.shared.delete(kid: kid) { error in
                        if let error = error {
                            LoadingManager.shared.hideLoading()
                            delay {
                                DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
                            }
                        } else {
                            LoadingManager.shared.hideLoading()
                            delay {
                                self.loadKids()
                            }
                        }
                    }
                } else if let indexPath = objc_getAssociatedObject(sender, &keyAssociateObjKids) as? IndexPath {
                    if self.numberOfCell > 0 {
                        self.numberOfCell -= 1
                        self.addkidTableVw.beginUpdates()
                        self.addkidTableVw.deleteRows(at: [indexPath], with: .automatic)
                        self.addkidTableVw.endUpdates()
                        self.view.endEditing(true)
                        self.addKidTWHeightConst.constant = self.addkidTableVw.contentSize.height
                    }
                } else {
                    if self.numberOfCell > 0 {
                        self.numberOfCell -= 1
                        self.addkidTableVw.reloadData()
                    }
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addkidTableVw.register(UINib(nibName: String(describing: AddKidTVC.self), bundle: nil), forCellReuseIdentifier: String(describing: AddKidTVC.self))
        //self.addKidTWHeightConst.constant = 0
        self.addKidTWHeightConst.constant = UIScreen.main.bounds.width * 0.8
        self.nextBtn.isEnabled = false
        
        addkidTableVw.dataSource = self
        addkidTableVw.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PreferenceManager.shared.isAtAddKid = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if parentModal == nil {
            FirebaseManager.shared.find(parentFrom: PreferenceManager.shared.userId ?? String()) { dict in
                self.parentModal = ParentsModal(fromDictionary: dict)
                self.loadKids()
            }
        } else if let parentModal = self.parentModal {
            self.parentModal = parentModal
            self.loadKids()
        }
    }
    
    //------------------------------------------------------
}

