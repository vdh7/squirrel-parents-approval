//
//  CreatePlaydatesVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 20/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import Contacts
import GooglePlaces

class CreatePlaydatesVC : BaseVC, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, LocationSearchDelegate, LocationManagerDelegate {
    
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblHeaderTitle: PoppinsRegularLabel!
    
    @IBOutlet weak var lblCreatePlaydate: FredokaOneRegularLabel!
    @IBOutlet weak var lblEditPlaydate: FredokaOneRegularLabel!
    
    @IBOutlet weak var txtTitle: ATCTextField!
    @IBOutlet weak var txtLocation: ATCLocationTextField!
    @IBOutlet weak var txtDates: FGDateTextField!
    @IBOutlet weak var txtTime: FGTimeTextField!
    @IBOutlet weak var avatarCollection: UICollectionView!
    @IBOutlet weak var eventGroupSwitch: UISwitch!
    
    @IBOutlet weak var btnPost: PoppinsBoldButton!
    
    lazy var btnCurrentLocation: UIButton = {
        let btnLocation = UIButton(type: UIButton.ButtonType.custom)
        btnLocation.frame = CGRect(origin: .zero, size: CGSize(width: 40, height: 25))
        btnLocation.imageView?.contentMode = .scaleAspectFit
        btnLocation.setImage(UIImage(named: "gps"), for: .normal)
        btnLocation.contentHorizontalAlignment = .center
        btnLocation.contentEdgeInsets = UIEdgeInsets(top: .zero, left: .zero, bottom: .zero, right: 10)
        btnLocation.addTarget(self, action: #selector(btnLocationTap), for: .touchUpInside)
        return btnLocation
    }()
    
    var commingFrom:String?
    
    // var kids: [KidsModal] = []
    var selectedAvatarIndex: Int? = nil
    var img: [String] {
        return PMImageName.avatars
    }
    
    var kids: [KidsModal] = []
    var selectedKids: [String] = []
    var playDatesModal = PlayDatesModal(fromDictionary: [:])
    var isEdit: Bool = false
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    func validate() -> Bool {
        
        if ValidationManager.shared.isEmpty(text: txtTitle.text) == true {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.title) {
            }
            return false
        }
        
        if ValidationManager.shared.isEmpty(text: txtLocation.text) == true {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.location_pd) {
            }
            return false
        }
        
        if ValidationManager.shared.isEmpty(text: txtDates.text) == true {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.date) {
            }
            return false
        }
        
        if ValidationManager.shared.isEmpty(text: txtTime.text) == true{
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.time) {
            }
            
            return false
        }
        
        if selectedKids.count == .zero {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.selectKidToPlaydate) {
            }
            return false
        }
        return true
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        self.backImg.isHidden = false
        self.btnBack.isHidden = false
        
        if isEdit {
            lblHeaderTitle.text = LocalizableConstants.Controller.titleEditPlaydate.localized()
            lblCreatePlaydate.isHidden = true
        } else {
            lblHeaderTitle.text = LocalizableConstants.Controller.titleCreatePlaydate.localized()
            lblEditPlaydate.isHidden = true
        }
        
        if commingFrom == "Menu" {
            self.backImg.image = UIImage(named: "Back")
        } else {
            self.backImg.image = UIImage(named: "cross")
        }
        
        txtLocation.delegate = self
        
        txtLocation.rightView = btnCurrentLocation
        txtLocation.rightViewMode = .always
        
        avatarCollection.delegate = self
        avatarCollection.dataSource = self
        avatarCollection.register(UINib(nibName: String(describing: AvatarCVC.self), bundle: nil), forCellWithReuseIdentifier: String(describing: AvatarCVC.self))
        
        txtDates.dpDate.minimumDate = DateTimeManager.shared.date(byAddingHours: 2, toDate: Date())
        txtDates.dpDate.maximumDate = DateTimeManager.shared.date(byAddingHours: 744, toDate: Date())
        txtTime.dpDate.minimumDate = txtDates.dpDate.minimumDate
        txtTime.dpDate.maximumDate = txtDates.dpDate.maximumDate
        
        if isEdit {
            
            txtTitle.text = playDatesModal.title
            txtLocation.text = playDatesModal.location
            if let date = DateTimeManager.shared.dateFrom(stringDate: playDatesModal.date, inFormate: DateFormate.UTC) {
                let stringPlayDate = DateTimeManager.shared.stringFrom(date: date, inFormate: DateFormate.yyyy_MM_dd)
                let stringPlayTime = DateTimeManager.shared.stringFrom(date: date, inFormate: DateFormate.HH_mm_ss)
                txtDates.dpDate.date = date
                txtDates.text = stringPlayDate
                txtTime.text = stringPlayTime
            }
            selectedKids = playDatesModal.childList
            eventGroupSwitch.isOn = playDatesModal.groupEvent
            btnPost.setTitle(LocalizableConstants.Controller.save.localized(), for: .normal)
            
        } else {
            btnPost.setTitle(LocalizableConstants.Controller.post.localized(), for: .normal)
        }
        
        delay {
            self.loadKids()
        }
    }
    
    func performSave() {
        
        LoadingManager.shared.showLoading()
        
        FirebaseManager.shared.save(playDatesOf: playDatesModal)
        
        if let title = playDatesModal.title, let location = playDatesModal.location {
            let data = playDatesModal.toDictionary()
            RequestManager.shared.fcmSendPlaydateMassPush(withTitle: title, withSubTitle: location, withData: data)
        }
        
        delay {
            
            LoadingManager.shared.hideLoading()
            
            NotificationCenter.default.post(name: NSNotification.Name(PMNotification.refreshPlayDates), object: nil)
            
            if self.commingFrom == "Menu" {
                self.pop()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func loadKids() {
        
        //LoadingManager.shared.showLoading()
        
        parentModal?.findKid(completion: { kids in
        
            //LoadingManager.shared.hideLoading()
            
            delay {
                self.kids = kids
                debugPrint(kids)
                self.avatarCollection.reloadData()
                //self.avatarCollection.flashScrollIndicators()
            }
        })
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func eventSwitchChanged(_ sender: Any) {
    }
    
    @IBAction func btnBackTap(_ sender: Any) {
        
        if commingFrom == "Menu" {
            self.pop()
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnPostTap(_ sender: Any) {
        
        if validate() == false {
            return
        }
        self.view.endEditing(true)
        
        self.playDatesModal.parentID = PreferenceManager.shared.userId ?? String()
        self.playDatesModal.title = txtTitle.text
        self.playDatesModal.childList = selectedKids
        self.playDatesModal.groupEvent = eventGroupSwitch.isOn
        self.playDatesModal.ownerImage = parentModal?.imgsource
        self.playDatesModal.isAutoGenerate = "false"
        
        let stringPlayDate = DateTimeManager.shared.stringFrom(date: txtDates.dpDate.date, inFormate: DateFormate.yyyy_MM_dd)
        let stringPlayTime = DateTimeManager.shared.stringFrom(date: txtTime.dpDate.date, inFormate: DateFormate.HH_mm_ss)
        let finalDate = stringPlayDate.appending(" ").appending(stringPlayTime)
        let formate = DateFormate.yyyy_MM_dd.appending(" ").appending(DateFormate.HH_mm_ss)
        if let playDate = DateTimeManager.shared.dateFrom(stringDate: finalDate, inFormate: formate) {
            let stringDate = DateTimeManager.shared.stringFrom(date: playDate, inFormate: DateFormate.UTC)
            self.playDatesModal.date = stringDate
            performSave()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return kids.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = avatarCollection.dequeueReusableCell(withReuseIdentifier: String(describing: AvatarCVC.self), for: indexPath) as? AvatarCVC {
            
            let kid = kids[indexPath.row]
            let avatarIndex = Int(kid.avatar) ?? .zero
            cell.set(imageName: img[avatarIndex])
            if selectedKids.contains(where: { arg0 in
                return arg0 == kid.firebaseID
            }) {
                cell.set(selected: true)
                cell.avatarImg.borderColor = PMColor.green
                cell.avatarImg.borderWidth = 2
            } else {
                cell.set(selected: false)
                cell.avatarImg.borderColor = .clear
            }
            cell.layoutIfNeeded()
            return cell
        }
        return UICollectionViewCell()
    }
    
    //------------------------------------------------------
    
    //MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedAvatarIndex = nil
        
        let kid = kids[indexPath.row]
        if selectedKids.contains(where: { arg0 in
            return arg0 == kid.firebaseID
        }) {
            selectedKids.removeAll { arg0 in
                return arg0 == kid.firebaseID
            }
        } else {
            selectedKids.append(kid.firebaseID)
        }
        self.avatarCollection.reloadData()
        //self.avatarCollection.flashScrollIndicators()
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtLocation {
            /*let controller = NavigationManager.shared.locationSearchNC
            if let vc = controller.viewControllers.first as? LocationSearchVC {
                vc.delegate = self
            }
            controller.modalPresentationStyle = .fullScreen
            present(controller, animated: true, completion: nil)
            return false
            */
            
            PlacesSearchManager.shared.showAutoComplete(self.navigationController, didAutoCompleteWith: { (ac: GMSAutocompleteViewController, place: GMSPlace) in
                
                debugLog(object: place.description)
                let formattedAddress = place.formattedAddress
                self.txtLocation.text = formattedAddress
                self.playDatesModal.lattitude = "\(place.coordinate.latitude)"
                self.playDatesModal.longitude = "\(place.coordinate.longitude)"
                self.playDatesModal.location = formattedAddress
                self.playDatesModal.postalCode = PlacesSearchManager.shared.getPostalCode(fromPlace: place)
                ac.dismiss(animated: true, completion: nil)
                
            }, failureBlock:  { (ac: GMSAutocompleteViewController, error: Error?) in
                
                if let error = error {
                    debugLog(object: error)
                    DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
                } else {
                    ac.dismiss(animated: true, completion: nil)
                }
            })
        }
        return true
    }
    
    //------------------------------------------------------
    
    //MARK: LocationSearchDelegate
    
    func locationSearch(controller: LocationSearchVC, didSelect location: MKPlacemark) {
        
        let fullAddress = String(format: "%@ %@", location.name ?? String(), location.formattedAddress ?? location.name ?? String())
        txtLocation.text = fullAddress
        self.playDatesModal.lattitude = "\(location.coordinate.latitude)"
        self.playDatesModal.longitude = "\(location.coordinate.longitude)"
        self.playDatesModal.location = fullAddress
        self.playDatesModal.postalCode = location.postalCode ?? String()
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: Selector
    
    @objc func btnLocationTap(_ sender: Any) {
        
        if LocationManager.shared.isRequiredToShowPermissionDialogue() {
            LocationManager.shared.requestForLocationPermissionWhenInUse()
        } else {
            LocationManager.shared.delegate = self
            LocationManager.shared.startMonitoring()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: LocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let lat = locations.first?.coordinate.latitude ?? .zero
        let lng = locations.first?.coordinate.longitude ?? .zero
                
        self.parentModal?.lattitude = String( lat )
        self.parentModal?.longitude = String( lng )
        
        LocationManager.shared.lat = lat
        LocationManager.shared.lng = lng
        
        if lat != .zero && lng != .zero {
            
            LocationManager.shared.stopMonitoring()
            
            PlacesSearchManager.shared.getAddressFrom(latitude: LocationManager.shared.lat, longitude: LocationManager.shared.lng) { address, error in
                self.txtLocation.text = address
                self.playDatesModal.location = address
            }
        }
        
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        #if targetEnvironment(simulator)
        DisplayAlertManager.shared.displayAlertWithNoYes(target: self, animated: true, message: error.localizedDescription) {
        } handlerYes: {
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
        #else
        DisplayAlertManager.shared.displayAlertWithNoYes(target: self, animated: true, message: LocalizableConstants.Error.locationServiceDisable.localized()) {
        } handlerYes: {
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
        #endif
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        //Handle heading
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        txtDates.rightViewMode = UITextField.ViewMode.always
        txtTime.rightViewMode = UITextField.ViewMode.always
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.avatarCollection.reloadData()
        //self.avatarCollection.flashScrollIndicators()
    }
    
    //------------------------------------------------------
}
