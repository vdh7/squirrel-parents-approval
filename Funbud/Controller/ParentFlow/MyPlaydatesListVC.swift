//
//  MyPlaydatesListVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 20/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

class MyPlaydatesListVC : BaseVC, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var createPlayDateVw: UIView!
    @IBOutlet weak var imgPlaceHold: UIImageView!
    @IBOutlet weak var lblDesp: PoppinsRegularLabel!
    @IBOutlet weak var playdateListTable: UITableView!
    
    var playDates: [PlayDatesModal] = [] {
        
        didSet{
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
                self.playdateListTable.reloadData()
            }
            if playDates.count == .zero {
                createPlayDateVw.isHidden = false
            } else {
                createPlayDateVw.isHidden = true
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    @IBAction func btnCreatePlayDatesTap(_ sender: Any) {
        
        let controller = NavigationManager.shared.createPlaydatesVC
        controller.commingFrom = "Menu"
        push(controller: controller)
    }
    
    @IBAction func btnBackTap(_ sender: Any) {
        //self.pop()
        NavigationManager.shared.sideMenuController.showLeftView()
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return playDates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LandingKidTVC.self), for: indexPath) as? LandingKidTVC {
            let playDate = playDates[indexPath.row]
            cell.setupMy(playdateModal: playDate)
            cell.parentVC = self
            return cell
        }
        return UITableViewCell()
    }
       
    //------------------------------------------------------
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createPlayDateVw.isHidden = true
        
        playdateListTable.register(UINib(nibName: String(describing: LandingKidTVC.self), bundle: nil), forCellReuseIdentifier: String(describing: LandingKidTVC.self))
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        FirebaseManager.shared.findMyPlaydates { (result: [PlayDatesModal]) in
            self.playDates = result
        }
    }
    
    //------------------------------------------------------
}
