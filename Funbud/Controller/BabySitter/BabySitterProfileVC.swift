//
//  BabySitterProfileVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 21/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage
import GSImageViewerController

class BabySitterProfileVC : BaseVC,UITableViewDelegate, UITableViewDataSource {
        
    @IBOutlet weak var bsProfileTable: UITableView!
    @IBOutlet weak var bsProfileImg: UIImageView!
    
    @IBOutlet weak var lbl_Bs_Name: PoppinsBoldLabel!
    @IBOutlet weak var lbl_Bs_Hours: PoppinsSemiBoldLabel!
    @IBOutlet weak var lbl_Bs_Rating: PoppinsExtraBoldLabel!
    @IBOutlet weak var lbl_Bs_Desp: PoppinsRegularLabel!
    
    @IBOutlet weak var imgAcceptUrgentRequest: UIImageView!
    @IBOutlet weak var lblAcceptUrgentRequest: PoppinsRegularLabel!
    
    @IBOutlet weak var lbl_always_available: PoppinsRegularLabel!
    
    var profileModal: BabysitterModal?
    var visibleDays : [DaysModal]! {
        return profileModal?.days.filter({ arg0 in
            return arg0.isAvailable == "1"
        }) ?? []
    }
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setupData() {
        
        bsProfileImg.clipsToBounds = true
        
        if let imageURL = profileModal?.imgsource {
            bsProfileImg.sd_setIndicatorStyle(UIActivityIndicatorView.Style.medium)
            bsProfileImg.sd_addActivityIndicator()
            bsProfileImg.sd_showActivityIndicatorView()
            bsProfileImg.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                self.bsProfileImg.image = img
                self.bsProfileImg.sd_removeActivityIndicator()
            }
        } else {
            bsProfileImg.image = UIImage(named: PMImageName.placeholder)
        }
        
        lbl_Bs_Name.text = profileModal?.getNameWithAge()
        lbl_Bs_Desp.text = profileModal?.location
        lbl_Bs_Hours.text = profileModal?.displayHours()
        lbl_Bs_Rating.text = profileModal?.displayUserRating()
        //lbl_bs_distance.text = profileModal.getDistnaceInKm()
        
        if profileModal?.isAcceptUrgentRequest == "1" {
            imgAcceptUrgentRequest.isHidden = false
        } else {
            imgAcceptUrgentRequest.isHidden = true
        }
        lblAcceptUrgentRequest.isHidden = imgAcceptUrgentRequest.isHidden
        
        if visibleDays.count == .zero {
            if profileModal?.isAlwaysAvailable == nil {
                lbl_always_available.text = "No any availability set yet"
                lbl_always_available.isHidden = false
            } else {
                lbl_always_available.isHidden = profileModal?.isAlwaysAvailable != "1"
                lbl_always_available.text = "Always available"
            }
        } else {
            lbl_always_available.isHidden = true
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func btnRatingTap(_ sender: Any) {
        
        let bbsitterId = profileModal?.userid ?? String()
        let parentId = PreferenceManager.shared.userId ?? String()
        
        LoadingManager.shared.showLoading()
        
        FirebaseManager.shared.isBabysitterConnect(withUser: bbsitterId) { latestConversationObj in
            
            if latestConversationObj != nil {
                
                FirebaseManager.shared.checkIfRatingAlreadyGiven(with: bbsitterId, parentId: parentId) { isExist in
                    
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        
                        if isExist {
                            
                            DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.ValidationMessage.alredyGivenRating) {
                            }
                            
                        } else {
                            
                            let controller = NavigationManager.shared.bSRatingVC
                            controller.profileModal = self.profileModal
                            self.present(controller, animated: true, completion: nil)
                        }
                    }
                }
                
            } else {
                
                LoadingManager.shared.hideLoading()
                
                delay {
                    DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.ValidationMessage.not_allow_to_rate_babysitter) {
                    }
                }
            }
        }
    }
    
    @IBAction func btnProfileTap(_ sender: UIButton) {
        
        if let image = bsProfileImg.image {
            let imageInfo   = GSImageInfo(image: image, imageMode: .aspectFit)
            let transitionInfo = GSTransitionInfo(fromView: sender)
            let imageViewer = GSImageViewerController(imageInfo: imageInfo, transitionInfo: transitionInfo)
            present(imageViewer, animated: true, completion: nil)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return visibleDays.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BabySitterProfileTVC.self), for: indexPath) as? BabySitterProfileTVC else{
            return BabySitterProfileTVC()
        }
        cell.selectionStyle = .none
        if profileModal != nil {
            //cell.setup(profileModal!, indexPath: indexPath)
            let dayModal = visibleDays[indexPath.row]
            cell.setup(dayModal)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    //------------------------------------------------------
    
    //MARK: Selector
    
    @objc func refreshProfileData(_ sender: Any) {
        
        self.refreshUserDataIfNeeded()
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let reuseIdentifier = String(describing: BabySitterProfileTVC.self)
        bsProfileTable.register(UINib(nibName: reuseIdentifier, bundle: Bundle.main), forCellReuseIdentifier: reuseIdentifier)
        
        setupData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshProfileData), name: NSNotification.Name(PMNotification.refreshPlayDates), object: nil)
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //------------------------------------------------------
}
