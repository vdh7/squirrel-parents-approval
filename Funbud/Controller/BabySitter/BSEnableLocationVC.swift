//
//  BSEnableLocationVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 13/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import MapKit
import GooglePlaces

class BSEnableLocationVC : BaseVC, LocationManagerDelegate, UITextFieldDelegate, LocationSearchDelegate {
    
    //------------------------------------------------------
    
    @IBOutlet weak var locationEnableSwitch: UISwitch!
    @IBOutlet weak var txtSelectPlace: ATCTextField!
    
    override var babysitterModal: BabysitterModal? {
        didSet {
            guard txtSelectPlace != nil else {
                return
            }
            txtSelectPlace.text = babysitterModal?.location
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func validate() -> Bool {
        
        /*if ValidationManager.shared.isEmpty(text: self.babysitterModal?.lattitude) || ValidationManager.shared.isEmpty(text: self.babysitterModal?.longitude) {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.location) {
            }
            return false
        }*/
        return true
    }
    
    func performSave(babysitter babysitterModal: BabysitterModal) {
             
        LoadingManager.shared.showLoading()
        
        delay {
            
            let firebaseId: String = PreferenceManager.shared.userId ?? String()
                                    
            FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
            LoadingManager.shared.hideLoading()
            
            delay {
                
                //NavigationManager.shared.setupSigninUserRootController()
                NavigationManager.shared.setupBabysitterPendingForApproval()
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func locationSwitchChanged(_ sender: UISwitch) {
        
        if (sender.isOn == true) {
            //self.txtSelectPlace.isHidden = true
            self.txtSelectPlace.isEnabled = false
            if LocationManager.shared.isRequiredToShowPermissionDialogue() {
                LocationManager.shared.requestForLocationPermissionWhenInUse()
                locationEnableSwitch.setOn(false, animated: true)
            } else {
                LocationManager.shared.delegate = self
                LocationManager.shared.startMonitoring()
            }
        } else {
            //self.txtSelectPlace.isHidden = false
            self.txtSelectPlace.isEnabled = true
        }
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.pop()
    }
    
    @IBAction func finishBtnClicked(_ sender: UIButton) {
        
        if validate() == false {
            return
        } else {
            self.babysitterModal?.location = txtSelectPlace.text
            self.babysitterModal?.isNotificationEnabled = "1"
            if let babysitterModal = self.babysitterModal {
                performSave(babysitter: babysitterModal)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        /*let controller = NavigationManager.shared.locationSearchNC
        if let vc = controller.viewControllers.first as? LocationSearchVC {
            vc.delegate = self
            
        }
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true, completion: nil)*/
        
        PlacesSearchManager.shared.showAutoComplete(self.navigationController, didAutoCompleteWith: { (ac: GMSAutocompleteViewController, place: GMSPlace) in
            
            debugLog(object: place.description)
            let formattedAddress = place.formattedAddress
            self.txtSelectPlace.text = formattedAddress
            self.babysitterModal?.lattitude = "\(place.coordinate.latitude)"
            self.babysitterModal?.longitude = "\(place.coordinate.longitude)"
            self.babysitterModal?.location = formattedAddress
            ac.dismiss(animated: true, completion: nil)
            
        }, failureBlock:  { (ac: GMSAutocompleteViewController, error: Error?) in
            
            ac.dismiss(animated: true, completion: nil)
            
            if let error = error {
                DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
            }
        })
        
        return false
    }
        
    //------------------------------------------------------
    
    //MARK: LocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let lat = locations.first?.coordinate.latitude ?? .zero
        let lng = locations.first?.coordinate.longitude ?? .zero
                
        self.babysitterModal?.lattitude = String( lat )
        self.babysitterModal?.longitude = String( lng )
        
        LocationManager.shared.lat = lat
        LocationManager.shared.lng = lng
                
        if lat != .zero && lng != .zero {
            
            LocationManager.shared.stopMonitoring()
            
            PlacesSearchManager.shared.getAddressFrom(latitude: LocationManager.shared.lat, longitude: LocationManager.shared.lng) { address, error in
                self.txtSelectPlace.text = address
                self.babysitterModal?.location = address
                
                if self.locationEnableSwitch.isOn == false {
                    self.locationEnableSwitch.isOn = true
                }
            }
        }        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        locationEnableSwitch.isOn = false
        
        DisplayAlertManager.shared.displayAlertWithNoYes(target: self, animated: true, message: LocalizableConstants.Error.locationServiceDisable.localized()) {
        } handlerYes: {
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        //Handle heading
    }
    
    //------------------------------------------------------
    
    //MARK: LocationSearchDelegate
    
    func locationSearch(controller: LocationSearchVC, didSelect location: MKPlacemark) {
        
        let fullAddress = String(format: "%@ %@", location.name ?? String(), location.formattedAddress ?? location.name ?? String())
        txtSelectPlace.text = fullAddress
        self.babysitterModal?.lattitude = "\(location.coordinate.latitude)"
        self.babysitterModal?.longitude = "\(location.coordinate.longitude)"
        self.babysitterModal?.location = fullAddress
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtSelectPlace.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        locationEnableSwitch.isOn = true
        //self.txtSelectPlace.isHidden = true
        //self.txtSelectPlace.isEnabled = false
        if LocationManager.shared.isRequiredToShowPermissionDialogue() {
            LocationManager.shared.requestForLocationPermissionWhenInUse()
            locationEnableSwitch.setOn(false, animated: true)
        } else {
            LocationManager.shared.delegate = self
            LocationManager.shared.startMonitoring()
        }
        PreferenceManager.shared.isAtBabysitterLocationEnabled  = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if babysitterModal == nil {
            FirebaseManager.shared.find(parentFrom: PreferenceManager.shared.userId ?? String()) { dict in
                self.babysitterModal = BabysitterModal(fromDictionary: dict)
            }
        } else if let babysitterModal = self.babysitterModal {
            self.babysitterModal = babysitterModal
        }
    }
        
    //------------------------------------------------------
}
