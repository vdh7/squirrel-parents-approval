//
//  BSPickDateVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 13/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

protocol BSPickDateDelegate {
    
    func pickDate(controller: BSPickDateVC, didSelect dateUTC: String)
    func pickDate(controller: BSPickDateVC, didCancel sender: Any)
}

class BSPickDateVC : BaseVC {
    
    @IBOutlet weak var pickVw: UIView!
    @IBOutlet weak var datePickerVw: UIDatePicker!
    @IBOutlet weak var hideVW: UIView!
    
    var isSelected:Bool?
    var delegate: BSPickDateDelegate?
    var sourceTextField: UITextField?
    var previousDate: Date?
    var currentDate: Date?
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        datePickerVw.datePickerMode = .time
        if let prevDate = previousDate {
            let newDate = DateTimeManager.shared.date(byAddingHours: 2, toDate: prevDate)
            datePickerVw.date = newDate ?? Date()
            currentDate = newDate
        }
        datePickerVw.setValue(UIColor.init(cgColor: #colorLiteral(red: 0.04210930318, green: 0.0574330166, blue: 0.2074161172, alpha: 1)), forKeyPath: "textColor")
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func ConfirmBtnClicked(_ sender: UIButton) {
        
        currentDate = datePickerVw.date
        previousDate = currentDate
        let utc = DateTimeManager.shared.stringFrom(date: datePickerVw.date, inFormate: DateFormate.UTC)
        delegate?.pickDate(controller: self, didSelect: utc)
    }
    
    @IBAction func cancelBtnClicked(_ sender: UIButton) {
        
        delegate?.pickDate(controller: self, didCancel: sender)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
         
    //------------------------------------------------------
}
