//
//  BSEditProfileVC.swift
//  Squirrel
//
//  Created by Vivek Dharmani on 20/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class BSEditProfileVC : BaseVC {
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
