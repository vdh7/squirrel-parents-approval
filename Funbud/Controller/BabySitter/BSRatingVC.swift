//
//  BSRatingVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 21/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage

class BSRatingVC : BaseVC, FloatRatingViewDelegate, UITextViewDelegate {
    
    @IBOutlet weak var bsScrollView: UIScrollView!
    @IBOutlet weak var bsProfileImg: UIImageView!
    @IBOutlet weak var lbl_Bs_Name: PoppinsBoldLabel!
    @IBOutlet weak var ratingVw: FloatRatingView!
    @IBOutlet weak var txtDesp: UITextView!
    
    @IBOutlet weak var btnPostReview: PoppinsBoldButton!
    
    var profileModal: BabysitterModal?
    var starsRating = 0
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    func setup() {
        
        txtDesp.delegate = self
        txtDesp.textContainerInset = UIEdgeInsets(top: 25, left: 25, bottom: 11, right: 25)
        
        setupData()
    }
    
    func setupData() {
        
        if let imageURL = profileModal?.imgsource {
            bsProfileImg.sd_setIndicatorStyle(UIActivityIndicatorView.Style.medium)
            bsProfileImg.sd_addActivityIndicator()
            bsProfileImg.sd_showActivityIndicatorView()
            bsProfileImg.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                self.bsProfileImg.image = img
                self.bsProfileImg.sd_removeActivityIndicator()
            }
        } else {
            bsProfileImg.image = UIImage(named: PMImageName.placeholder)
        }
        
        lbl_Bs_Name.text = profileModal?.getNameWithAge()
    }
    
    //------------------------------------------------------
    
    // MARK: Actions
    
    @IBAction func btnBackTap(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPostSaveTap(_ sender: Any) {
        
        self.view.endEditing(true)
        
        guard ratingVw.rating > .zero else {
            DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.ValidationMessage.emptyUserRating, handlerOK: nil)
            return
        }
        
        if FirebaseManager.shared.isSignedIn() {
            
            LoadingManager.shared.showLoading()
            
            let givenRating: Double = ratingVw.rating
            let comment = txtDesp.text
            let bbsitterId = profileModal?.userid ?? String()
            let parentId = PreferenceManager.shared.userId ?? String()
            
            FirebaseManager.shared.find(parentFrom: bbsitterId) { dict in
                let babysitterModal = BabysitterModal(fromDictionary: dict)
                let existingRating = (Double(babysitterModal.userRating ?? "0") ?? .zero)
                let isFirsttime = existingRating == .zero
                let divider: Double = isFirsttime ? 1 : 2
                babysitterModal.userRating = String(( existingRating + givenRating) / divider)
                FirebaseManager.shared.save(babysitter: babysitterModal, of: self.profileModal?.userid ?? String())
                
                FirebaseManager.shared.saveUser(rating: bbsitterId, parentId: parentId, parentRating: String( givenRating ), comment: comment)
                LoadingManager.shared.hideLoading()
                
                NotificationCenter.default.post(name: NSNotification.Name(PMNotification.refreshPlayDates), object: nil)
                
                delay {
                    DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.SuccessMessage.user_rating_success) {
                        self.dismiss(animated: false, completion: nil)
                    }
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        // liveLabel.text = String(format: "%.2f", self.floatRatingView.rating)
        print(ratingView.rating)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        //  updatedLabel.text = String(format: "%.2f", self.floatRatingView.rating)
        print(ratingView.rating)
    }
    
    //------------------------------------------------------
    
    //MARK: UITextViewDelegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        bsScrollView.setContentOffset(CGPoint(x: bsScrollView.contentOffset.x, y: ratingVw.frame.origin.y), animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        ratingVw.backgroundColor = UIColor.clear
        ratingVw.delegate = self
        ratingVw.contentMode = UIView.ContentMode.scaleAspectFit
        ratingVw.type = .halfRatings
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //------------------------------------------------------
}
