//
//  BSAvailabilityVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 13/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class BSAvailabilityVC : BaseVC, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
      
    @IBOutlet weak var heightStepImg: NSLayoutConstraint!
    @IBOutlet weak var stepImg: UIImageView!
    @IBOutlet weak var lblChangeLater: PoppinsRegularLabel!
    @IBOutlet weak var lblActive: PoppinsRegularLabel!
    @IBOutlet weak var availabilitytableHeight: NSLayoutConstraint!
    @IBOutlet weak var availabilityTableVw: UITableView!
    @IBOutlet weak var availabilitySwitch: UISwitch!
    @IBOutlet weak var acceptUrgentRequestSwitch: UISwitch!
    @IBOutlet weak var txtHourlyRated: ATCTextField!
    @IBOutlet weak var btnNext: PoppinsBoldButton!
    
    var weeks = [DaysType.monday.rawValue, DaysType.tuesday.rawValue, DaysType.wednesday.rawValue, DaysType.thursday.rawValue, DaysType.friday.rawValue, DaysType.saturday.rawValue, DaysType.sunday.rawValue]
    var isFromMenu: Bool = false
    
    override var babysitterModal: BabysitterModal? {
        didSet {
            guard txtHourlyRated != nil else {
                return
            }
            setupData()
        }
    }
    
    var daysModal: [DaysModal] = [] {
        didSet {
            guard availabilityTableVw != nil else {
                return
            }
            availabilityTableVw.reloadData()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    //MARK: Customs
    
    func setupData() {
        
        txtHourlyRated.text = babysitterModal?.rate
        acceptUrgentRequestSwitch.isOn = babysitterModal?.isAcceptUrgentRequest == "1" ? true : false
        if let value = babysitterModal?.isAlwaysAvailable {
            availabilitySwitch.isOn = value == "1" ? true : false
        } else {
            availabilitySwitch.isOn = true
        }
        
        if babysitterModal?.days != nil && babysitterModal?.days.count ?? .zero > .zero {
            daysModal = babysitterModal?.days ?? []
        } else {
            daysModal = createDaysInfoIfNeeded()
        }
        availabilityTableVw.reloadData()
        
        if isFromMenu {
            btnNext.setTitle(LocalizableConstants.Controller.save.localized(), for: .normal)
        } else {
            btnNext.setTitle(LocalizableConstants.Controller.next.localized(), for: .normal)
        }
    }
    
    func validate() -> Bool {
        
        if ValidationManager.shared.isEmpty(text: txtHourlyRated.text) {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.emptyHourlyRate) {
            }
            return false
        }
        
        for index in 0...weeks.count {
            
            let indexPath = IndexPath(row: index, section: .zero)
            
            if let cell = availabilityTableVw.cellForRow(at: indexPath) as? AvailabilityTVC {
                
                if cell.isEmptyFrom() && cell.isAvailable() {
                    DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.emptyBabysitterFrom) {
                    }
                    return false
                }
                
                if cell.isEmptyTo() && cell.isAvailable() {
                    DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.emptyBabysitterTo) {
                    }
                    return false
                }
            }
        }
        return true
    }
    
    func performSave(babysitter babysitterModal: BabysitterModal) {
        
        LoadingManager.shared.showLoading()
        
        delay {
            
            let firebaseId: String = PreferenceManager.shared.userId ?? String()
            babysitterModal.days = self.getDaysInfo()            
            FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
            LoadingManager.shared.hideLoading()
            
            delay {
                if self.isFromMenu {
                    self.pop()
                } else {
                    let controller = NavigationManager.shared.bSEnableLocationVC
                    self.push(controller: controller)
                }
            }
        }
    }
    
    func createDaysInfoIfNeeded() -> [DaysModal] {
        
        var modals: [DaysModal] = []
        for index in 0...weeks.count {
            let indexPath = IndexPath(row: index, section: .zero)
            let dayModal = DaysModal(fromDictionary: [:])
            dayModal.day = String(indexPath.row + 1)
            dayModal.from = String()
            dayModal.to = String()
            dayModal.isAvailable = "0"
            modals.append(dayModal)
        }
        return modals
    }
    
    func getDaysInfo() -> [DaysModal] {
        
        var modals: [DaysModal] = []
        
        for index in 0...weeks.count {
            
            let indexPath = IndexPath(row: index, section: .zero)
            
            if let cell = availabilityTableVw.cellForRow(at: indexPath) as? AvailabilityTVC {
                
                let from = cell.getFrom()
                let to = cell.getTo()
                let isAvailabile = cell.isAvailable()
                
                let dayModal = DaysModal(fromDictionary: [:])
                dayModal.day = String(indexPath.row + 1)
                dayModal.from = from
                dayModal.to = to
                dayModal.isAvailable = isAvailabile ? "1" : "0"
                
                if availabilitySwitch.isOn {
                    dayModal.isAvailable = "0"
                }
                modals.append(dayModal)
            }
        }
        return modals
    }
    
    func refreshAvailability() {
        self.availabilityTableVw.reloadData()
        self.availabilitytableHeight.constant = self.availabilityTableVw.contentSize.height
    }
    
    //------------------------------------------------------
    
    // MARK:-- Actions
    
    @IBAction func nextBtnClicked(_ sender: PoppinsBoldButton) {
        
        if validate() == false {
            return
        }
        
        self.view.endEditing(true)
        
        self.babysitterModal?.rate = txtHourlyRated.text
        self.babysitterModal?.isAlwaysAvailable = availabilitySwitch.isOn ? "1" : "0"
        self.babysitterModal?.isAcceptUrgentRequest = acceptUrgentRequestSwitch.isOn ? "1" : "0"
        
        if let parentModal = self.babysitterModal {
            performSave(babysitter: parentModal)
        }
    }
    
    @IBAction func availabilitySwitchChanged(_ sender: UISwitch) {
        
        if sender.isOn {
            lblChangeLater.isHidden = false
            availabilityTableVw.isUserInteractionEnabled = false
        } else {
            lblChangeLater.isHidden = true
            availabilityTableVw.isUserInteractionEnabled = true
        }
        availabilityTableVw.reloadData()
    }
    
    @IBAction func acceptUrgentSwitchChanged(_ sender: UISwitch) {
        if (sender.isOn == true) {
            lblActive.isHidden = true
        } else {
            lblActive.isHidden = false
        }
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        //self.pop()
        NavigationManager.shared.sideMenuController.showLeftView()
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weeks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = availabilityTableVw.dequeueReusableCell(withIdentifier: String(describing: AvailabilityTVC.self), for: indexPath) as? AvailabilityTVC {
            
            cell.parentController = self
            cell.isAlwaysAvailable = availabilitySwitch.isOn
            cell.setup(week: weeks[indexPath.row])
            
            if daysModal.indices.contains(indexPath.row) {
                
                let dayModal = daysModal[indexPath.row]
                cell.setup(daysModal: dayModal)
                
                /*cell.switchAction = { (bool) in
                    if self.daysModal.indices.contains(indexPath.row) {
                        let dayModal = self.daysModal[indexPath.row]
                        dayModal.isAvailable = (bool == true) ? "1" : "0"
                    }
                    cell.setCell(isEnabled: bool)
                    self.availabilityTableVw.reloadData()
                }*/
                
                DispatchQueue.main.async {
                    self.availabilitytableHeight.constant = self.availabilityTableVw.contentSize.height
                }
            }
                       
            return cell
        }
        
        return UITableViewCell()
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        heightStepImg.constant = 0
        txtHourlyRated.keyboardType = .numberPad
        
        availabilityTableVw.register(UINib(nibName: String(describing: AvailabilityTVC.self), bundle: nil), forCellReuseIdentifier: String(describing: AvailabilityTVC.self))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PreferenceManager.shared.isAtBabysitterAvailabilityProfile = true
        
        if self.babysitterModal == nil {
            FirebaseManager.shared.find(babysitterFrom: PreferenceManager.shared.userId ?? String()) { dict in
                self.babysitterModal = BabysitterModal(fromDictionary: dict)
            }
        } else if let babysitterModal = self.babysitterModal {
            self.babysitterModal = babysitterModal
        }
    }
    
    //------------------------------------------------------
}


