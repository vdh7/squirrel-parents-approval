//
//  BSChatVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 27/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class BSChatVC: BaseVC, SegmentViewDelegate, UITableViewDataSource, UITableViewDelegate,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var VwCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var newRequestCollection: UICollectionView!
    @IBOutlet weak var lblNoRecordsFound: PoppinsSemiBoldLabel!
    @IBOutlet weak var chatListingTable: UITableView!
    @IBOutlet weak var lblNoConversationFound: PoppinsSemiBoldLabel!
    @IBOutlet weak var bsSegment: SegmentView!
    @IBOutlet weak var pdSegment: SegmentView!
    
    var isSelected = "0"
    
    var requests: [RequestModal] = [] {
        didSet {
            //lblNoRecordsFound.isHidden = requests.count > .zero
            lblNoRecordsFound.isHidden = true
            if requests.count > .zero {
                VwCollectionHeight.constant = 100
            } else {
                VwCollectionHeight.constant = .zero
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
                self.newRequestCollection.reloadData()
            }
        }
    }
    
    var conversations: [ConversationModal] = [] {
        didSet {
            lblNoConversationFound.isHidden = conversations.count > .zero
            updateUI()
        }
    }
  
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    
    //MARK: SegmentViewDelegate
    
    func segment(view: SegmentView, didChange flag: Bool) {
        if view == pdSegment {
            isSelected = "0"
            bsSegment.isSelected = false
            if requests.count > .zero {
                lblNoRecordsFound.isHidden = true
                VwCollectionHeight.constant = 100
            } else {
                lblNoRecordsFound.isHidden = true
                VwCollectionHeight.constant = .zero
            }
            //lblNoRecordsFound.isHidden = requests.count > .zero
            updateUI()
        } else if view == bsSegment {
            isSelected = "1"
            pdSegment.isSelected = false
            VwCollectionHeight.constant = 0
            lblNoRecordsFound.isHidden = true
            updateUI()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Custom
    
    func setup() {
        
        lblNoRecordsFound.isHidden = true
        lblNoConversationFound.isHidden = true
        
        pdSegment.btn.setTitle("Playdates", for: .normal)
        bsSegment.btn.setTitle("Babysitters", for: .normal)
        pdSegment.delegate = self
        bsSegment.delegate = self
        pdSegment.isSelected = true
        bsSegment.isSelected = !pdSegment.isSelected
        
        chatListingTable.delegate = self
        chatListingTable.dataSource = self
        chatListingTable.register(UINib(nibName: String(describing: ChatPlaydatesTVC.self), bundle: nil), forCellReuseIdentifier: String(describing: ChatPlaydatesTVC.self))
        chatListingTable.register(UINib(nibName: String(describing: ChatBabySitterTVC.self), bundle: nil), forCellReuseIdentifier: String(describing: ChatBabySitterTVC.self))
        
        newRequestCollection.delegate = self
        newRequestCollection.dataSource = self
        newRequestCollection.register(UINib(nibName: String(describing: ChatRequestCVC.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ChatRequestCVC.self))
    }
    
    func updateUI() {
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            self.chatListingTable.reloadData()
        }        
    }
    
    @IBAction func btnMenuTap(_ sender: UIButton) {
        
        /*let controller = NavigationManager.shared.menuVC
        push(controller: controller)*/
        
        NavigationManager.shared.sideMenuController.showLeftView()
    }
    
    //------------------------------------------------------
    
    // MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return requests.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = newRequestCollection.dequeueReusableCell(withReuseIdentifier: String(describing: ChatRequestCVC.self), for: indexPath) as? ChatRequestCVC {
            let req = requests[indexPath.row]
            cell.setup(request: req)
            return cell
        }
        return UICollectionViewCell()
    }
    
    //------------------------------------------------------
    
    //MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 55, height: 55)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        //1. check if pending for request.
        let req = requests[indexPath.row]
        if req.status == "0" || req.status.isEmpty == true {
            let controller = NavigationManager.shared.admitVC
            controller.requestModal = req
            controller.modalPresentationStyle = .fullScreen
            present(controller, animated: false, completion: nil)
        }
        
        //2. if already in - make it available for chat
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if pdSegment.isSelected == true {
            return conversations.count
        } else if bsSegment.isSelected == true {
            return .zero
        }
        return Int()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if pdSegment.isSelected == true {
            if let cell = chatListingTable.dequeueReusableCell(withIdentifier: String(describing: ChatPlaydatesTVC.self), for: indexPath) as? ChatPlaydatesTVC {
                let conversation = conversations[indexPath.row]
                cell.isParentProfile = isParentProfile
                cell.setup(conversation: conversation)
                return cell
            }
        } else if bsSegment.isSelected == true {
            if let cell = chatListingTable.dequeueReusableCell(withIdentifier: String(describing: ChatBabySitterTVC.self), for: indexPath) as? ChatPlaydatesTVC {
                cell.selectionStyle = .none
                let conversation = conversations[indexPath.row]
                cell.isParentProfile = isParentProfile
                cell.setup(conversation: conversation)
                return cell
            }
        }
        return UITableViewCell()
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if pdSegment.isSelected == true {
            let controller = NavigationManager.shared.chatVC
            let conversation = conversations[indexPath.row]
            controller.conversationModal = conversation
            controller.isParentProfile = isParentProfile
            if let cell = tableView.cellForRow(at: indexPath) as? ChatPlaydatesTVC {
                controller.imgAvatar = cell.avatarImg.image
            }
            push(controller: controller)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Selector
    
    @objc func refreshPlayDates(_ notification: Notification) {
        
        FirebaseManager.shared.getAllwithBabysitter(conversations: PreferenceManager.shared.userId ?? String()) { (result: [ConversationModal]) in
            self.conversations = result
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        updateUI()
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshPlayDates), name: NSNotification.Name(PMNotification.refreshPlayDates), object: nil)
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        newRequestCollection.reloadData()
        updateUI()
            
        /*FirebaseManager.shared.getAllMy(request: currentUser?.uid ?? String()) { (requestModals: [RequestModal]) in
            self.requests = requestModals
        }*/
        
        FirebaseManager.shared.listenMyRequests(PreferenceManager.shared.userId ?? String()) { result in
            self.requests = result
        }
        
        FirebaseManager.shared.getAllwithBabysitter(conversations: PreferenceManager.shared.userId ?? String()) { (result: [ConversationModal]) in
            self.conversations = result
        }
        
        AppDelegate.shared.registerRemoteNotificaton(UIApplication.shared)        
    }
    
    //------------------------------------------------------
}
