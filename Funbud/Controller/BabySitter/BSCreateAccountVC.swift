//
//  BSCreateAccountVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 13/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Toucan
import HMSegmentedControl
import SDWebImage

class BSCreateAccountVC : BaseVC ,ImagePickerDelegate {
        
    @IBOutlet weak var baseImage: SLBaseImageView!
    @IBOutlet weak var profileImg: SLBaseImageView!
    @IBOutlet weak var fileDetailVw: UIView!
    @IBOutlet weak var lblFilesize: PoppinsRegularLabel!
    @IBOutlet weak var txtName: ATCTextField!
    @IBOutlet weak var lblFileName: PoppinsRegularLabel!
    @IBOutlet weak var uploadfileBtn: UIButton!
    @IBOutlet weak var uploadFIleVw: UIView!
    @IBOutlet weak var txtDOB: FGBirthDateTextField!
    @IBOutlet weak var txtSpecifyGender: SLGenderTextField!
    @IBOutlet weak var segmentVw: HBSegmentedControl!
    
    var imagePickerVC: SLImagePickerVC?
    var isImagePickerForProfile: Bool = false
    
    var selectedImage: UIImage? {
        didSet {
            if selectedImage != nil {
                baseImage.image = selectedImage
            }
        }
    }
    
    var selectedImageId: UIImage? {
      
        didSet {
            
            if selectedImageId != nil {
                self.fileDetailVw.isHidden = false
                uploadFIleVw.isHidden = !self.fileDetailVw.isHidden
                
                let fileName = String(format: "%@.jpeg", "ID")
                lblFileName.text = fileName
                let displaySize = countBytes(ofData: selectedImageId?.pngData())
                lblFilesize.text = displaySize
                
            } else {
                
                self.fileDetailVw.isHidden = true
                uploadFIleVw.isHidden = !self.fileDetailVw.isHidden
            }
        }
    }
    
    override var babysitterModal: BabysitterModal? {
        
        didSet {
            
            guard babysitterModal != nil else {
                return
            }
            
            txtName.text = babysitterModal?.name
            segmentVw.selectedIndex = Int(babysitterModal?.gender ?? "0") ?? .zero
            txtSpecifyGender.text = babysitterModal?.genderText
            segmentValueChanged(segmentVw)
            
            if let imageURL = babysitterModal?.imgsource {
                baseImage.sd_setIndicatorStyle(UIActivityIndicatorView.Style.gray)
                baseImage.sd_addActivityIndicator()
                baseImage.sd_showActivityIndicatorView()
                baseImage.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                    self.baseImage.image = img
                    self.baseImage.sd_removeActivityIndicator()
                }
            }
            
            if let imageIdURL = babysitterModal?.imageId {
                
                self.fileDetailVw.isHidden = false
                uploadFIleVw.isHidden = !self.fileDetailVw.isHidden
                
                let fileName = String(format: "%@.jpeg", "ID")
                lblFileName.text = fileName
                
                SDWebImageDownloader.shared().downloadImage(with: URL(string: imageIdURL)) { (img: UIImage?, imgData: Data?, error:Error?, flag: Bool) in
                                                            
                    let displaySize = countBytes(ofData: imgData)
                    self.lblFilesize.text = displaySize
                }
                
            } else {
                
                self.fileDetailVw.isHidden = true
                uploadFIleVw.isHidden = !self.fileDetailVw.isHidden
            }
            
            if let dateToSet = babysitterModal?.getDOB() {
                txtDOB.dpDate.date = dateToSet
            }
            txtDOB.text = babysitterModal?.displayDOB()
        }
    }
    
    var role: RoleType = .babysitter
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //MARK: Customs
    
    func setup() {
        
        imagePickerVC = SLImagePickerVC(presentationController: self, delegate: self)
        
        self.lblFilesize.text = String()
    }
    
    func validate() -> Bool {
        
        if babysitterModal?.imgsource == nil && selectedImage == nil {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.selectParentProfileImage) {
            }
            return false
        }
        
        if ValidationManager.shared.isEmpty(text: txtName.text) == true {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.BS_name) {
            }
            return false
        }
        
        if ValidationManager.shared.isEmpty(text: txtDOB.text) == true {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.BS_DOB) {
            }
            return false
        }
        
        if ValidationManager.shared.isEmpty(text: txtSpecifyGender.text) == true && txtSpecifyGender.isHidden == false {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.specficRelation) {
            }
            return false
        }
        
        if babysitterModal?.imageId == nil && selectedImageId == nil {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.selectBabysitterIdImage) {
            }
            return false
        }
        return true
    }
    
    func performSave(babysitter babysitterModal: BabysitterModal) {
        
        LoadingManager.shared.showLoading()
        
        delay {
            
            let firebaseId: String = PreferenceManager.shared.userId ?? String()
            let fileName = String(format: "%@.jpeg", firebaseId)
            
            if self.selectedImage != nil {
                
                babysitterModal.userid = firebaseId
                babysitterModal.role = self.role.rawValue
                
                FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
                
                FirebaseManager.shared.save(image: firebaseId, fileName: fileName, imageData: self.selectedImage?.pngData() ?? Data(), completion: { (profileImageURL: URL?, error: Error?) in
                    
                    if let error = error {
                        LoadingManager.shared.hideLoading()
                        delay {
                            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message:
                                                                        error.localizedDescription) {
                            }
                        }
                    } else if self.selectedImageId != nil {
                        
                        FirebaseManager.shared.save(imageId: firebaseId, fileName: fileName, imageData: self.selectedImageId?.pngData() ?? Data(), completion: { (imageIdURL: URL?, error: Error?) in
                            
                            if let error = error {
                                LoadingManager.shared.hideLoading()
                                delay {
                                    DisplayAlertManager.shared.displayAlert(target: self, animated: true, message:
                                                                                error.localizedDescription) {
                                    }
                                }
                            } else {
                                 
                                babysitterModal.imgsource = profileImageURL?.absoluteString ?? String()
                                babysitterModal.imageId = imageIdURL?.absoluteString ?? String()
                               
                                FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
                                LoadingManager.shared.hideLoading()
                                
                                delay {
                                    
                                    let controller = NavigationManager.shared.bSAvailabilityVC
                                    controller.babysitterModal = self.babysitterModal
                                    self.push(controller: controller)
                                }
                            }
                        })
                        
                    } else if let stringImageURL = profileImageURL?.absoluteString {
                        
                        babysitterModal.imgsource = stringImageURL
                        FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
                        LoadingManager.shared.hideLoading()
                        
                        delay {
                            
                            let controller = NavigationManager.shared.bSAvailabilityVC
                            controller.babysitterModal = self.babysitterModal
                            self.push(controller: controller)
                        }
                    }
                })
                
            } else if self.selectedImageId != nil {
                
                FirebaseManager.shared.save(imageId: firebaseId, fileName: fileName, imageData: self.selectedImageId?.pngData() ?? Data(), completion: { (imageIdURL: URL?, error: Error?) in
                    
                    if let error = error {
                        LoadingManager.shared.hideLoading()
                        delay {
                            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message:
                                                                        error.localizedDescription) {
                            }
                        }
                    } else {
                                                
                        babysitterModal.imageId = imageIdURL?.absoluteString ?? String()
                       
                        FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
                        LoadingManager.shared.hideLoading()
                        
                        delay {
                            
                            let controller = NavigationManager.shared.bSAvailabilityVC
                            controller.babysitterModal = self.babysitterModal
                            self.push(controller: controller)
                        }
                    }
                })
                
            } else {
                
                FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
                LoadingManager.shared.hideLoading()
                
                delay {
                    
                    babysitterModal.userid = firebaseId
                    babysitterModal.role = self.role.rawValue
                    
                    FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        
                        let controller = NavigationManager.shared.bSAvailabilityVC
                        controller.babysitterModal = self.babysitterModal
                        self.push(controller: controller)
                    }
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    // MARK:- Actions
   
    @IBAction func fileCrossBtnClicked(_ sender: UIButton) {
        
        /*self.fileDetailVw.isHidden = true
        self.uploadFIleVw.isHidden = false*/
        selectedImageId = nil
    }
    
    @IBAction func uploadFileBtnClicked(_ sender: UIButton) {
        
        /*self.fileDetailVw.isHidden = false
        self.uploadFIleVw.isHidden = true*/
        
        isImagePickerForProfile = false
        self.imagePickerVC?.present(from: sender)
    }
    
    @IBAction func uploadProfileBtnClicked(_ sender: UIButton) {
        
        isImagePickerForProfile = true
        self.imagePickerVC?.present(from: sender)
    }
    
    @IBAction func nextBtnClicked(_ sender: PoppinsBoldButton) {
        
        if validate() == false {
            return
        }
        
        self.view.endEditing(true)
        
        let babysitterModals = babysitterModal ?? BabysitterModal(fromDictionary: [:])
        babysitterModals.name = txtName.text
        babysitterModals.gender = String(segmentVw.selectedIndex)
        babysitterModals.genderText = txtSpecifyGender.text
        
        let dob = DateTimeManager.shared.stringFrom(date: txtDOB.dpDate.date, inFormate: DateFormate.UTC)
        babysitterModals.dob = dob
        
        performSave(babysitter: babysitterModals)
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.pop()
    }
    
    //------------------------------------------------------
    
    //MARK: SlImagePickerDelegate
    
    func didSelect(image: UIImage?) {
        
        if let imageData = image?.jpegData(compressionQuality: 0), let compressImage = UIImage(data: imageData) {
            
            if isImagePickerForProfile {
                self.selectedImage = Toucan.init(image: compressImage).resizeByCropping(PMSettings.profileImageSize).maskWithRoundedRect(cornerRadius: PMSettings.profileImageSize.width/2, borderWidth: PMSettings.profileBorderWidth, borderColor: PMColor.clear).image
            } else {
                self.selectedImageId = Toucan.init(image: compressImage).image
            }
        }
    }
    
    //------------------------------------------------------
    
    // MARK:- Selector
    
    @objc func segmentValueChanged(_ sender: AnyObject?) {
        if segmentVw.selectedIndex == 0 {
            txtSpecifyGender.isHidden = true
        }else if segmentVw.selectedIndex == 1{
            txtSpecifyGender.isHidden = true
        }else{
            txtSpecifyGender.isHidden = false
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        segmentVw.items = GenderType.allCases.map({ $0.rawValue })
        segmentVw.font = PMFont.sfpoppinsRegular(size: 14)
        segmentVw.selectedIndex = 0
        segmentVw.padding = 4
        txtSpecifyGender.isHidden = true
        fileDetailVw.isHidden = true
        segmentVw.addTarget(self, action: #selector(ParentProfileVC.segmentValueChanged(_:)), for: .valueChanged)
        setup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        PreferenceManager.shared.isAtBabysitterProfile = true
            
        if self.babysitterModal == nil {
            FirebaseManager.shared.find(babysitterFrom: PreferenceManager.shared.userId ?? String()) { dict in
                self.babysitterModal = BabysitterModal(fromDictionary: dict)
            }
        }
    }
        
    //------------------------------------------------------
}
