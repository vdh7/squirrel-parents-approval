//
//  BSMenuVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 15/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Firebase

class BSMenuVC : BaseVC {
    
    @IBOutlet weak var BsMenuTableVw: UITableView!
    //------------------------------------------------------
    var menulist = ["Profile","My Availability","Settings","","Support","Privacy Policy"]
    var imgs = ["Kid","Avaliaility","Setting","","Support","Privacy"]
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BsMenuTableVw.register(UINib(nibName: "MenuListTVC", bundle: nil), forCellReuseIdentifier: "MenuListTVC")
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
    @IBAction func logOutBtnClicked(_ sender: PoppinsBoldButton) {
        
        DisplayAlertManager.shared.displayAlertWithNoYes(target: self, animated: true, message: LocalizableConstants.Controller.confirmationSignout) {
            
            //Handle no block
            
        } handlerYes: {
                         
            try? FirebaseManager.shared.signOut()
            self.parentModal = nil
            self.babysitterModal = nil
            
            FirebaseManager.shared.resetUser { isReset in
                if isReset {
                    PreferenceManager.shared.reset()
                    NavigationManager.shared.setupSignInOption()
                } else {
                    delay {
                        DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Error.failed_to_signout, handlerOK: nil)
                    }
                }
            }
        }
    }
    
    @IBAction func crossBtnClicked(_ sender: UIButton) {
        self.pop()
    }
}
extension BSMenuVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menulist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuListTVC", for: indexPath) as! MenuListTVC
        if indexPath.row == 3 {
            cell.lineVw.isHidden = true
        }else {
            cell.lineVw.isHidden = false
        }
        cell.lblMenu.text = menulist[indexPath.row]
        cell.menuImg.image = UIImage(named: imgs[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let controller = NavigationManager.shared.editParentProfileVC
            push(controller: controller)
            
        }else if indexPath.row == 1 {
            let controller = NavigationManager.shared.bSAvailabilityVC
            push(controller: controller)
           
        } else if indexPath.row == 2 {
            let controller = NavigationManager.shared.bSSettingVC
            push(controller: controller)
            
        }else if indexPath.row == 4 {
            let controller = NavigationManager.shared.bSRatingVC
            push(controller: controller)
        }else if indexPath.row == 5 {
            let controller = NavigationManager.shared.requestGroupChatVC
            push(controller: controller)
        }
    }
}
