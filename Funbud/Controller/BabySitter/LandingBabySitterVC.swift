//
//  LandingBabySitterVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 20/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import FittedSheets
import CoreLocation
import DatePicker

class LandingBabySitterVC : BaseVC, UITableViewDataSource, UITableViewDelegate, LocationManagerDelegate, UITextFieldDelegate, FGCalendarTextFieldDelegate, FGTimeSlotTextFieldDelegate {
    
    @IBOutlet weak var bs_LandingTable: UITableView!
    @IBOutlet weak var btn_switch_to_Playdates: PoppinsRegularButton!
    @IBOutlet weak var btn_Menu: UIButton!
    //@IBOutlet weak var txtAnyDay: FGDayTextField!
    @IBOutlet weak var txtAnyDay: FGCalendarTextField!
    @IBOutlet weak var txtAnyTime: FGTimeSlotTextField!
    @IBOutlet weak var lblNoRecordsFound: PoppinsSemiBoldLabel!
    
    var babySitters: [BabysitterModal] = [] {
        didSet {
            if babySitters.count > .zero {
                lblNoRecordsFound.isHidden = true
            } else {
                lblNoRecordsFound.isHidden = false
            }
            bs_LandingTable.reloadData()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func refresh() {
        
        func loadAll() {
            FirebaseManager.shared.findBabysitters { (babysitterModals: [BabysitterModal]) in
                self.babySitters = babysitterModals
            }
        }
        
        func applyDateFilter() {
            let index: Int = DaysType.allCases.firstIndex(of: txtAnyDay.selectedDay!) ?? 1
            FirebaseManager.shared.findBabysitters(String( index )) { (babysitterModals: [BabysitterModal]) in
                self.babySitters = babysitterModals
            }
        }
        
        func applyTimeFilter() {
            FirebaseManager.shared.findBabysitters(nil, fromTimeSlot: txtAnyTime.selectedFrom?.rawValue, toTimeSlot: txtAnyTime.selectedTo?.rawValue) { (babysitterModals: [BabysitterModal]) in
                self.babySitters = babysitterModals
            }
        }
        
        func applyDateAndTimeFilter() {
            let index: Int = DaysType.allCases.firstIndex(of: txtAnyDay.selectedDay!) ?? 1
            FirebaseManager.shared.findBabysitters(String( index ), fromTimeSlot: txtAnyTime.selectedFrom?.rawValue, toTimeSlot: txtAnyTime.selectedTo?.rawValue) { (babysitterModals: [BabysitterModal]) in
                self.babySitters = babysitterModals
            }
        }
        
        if (txtAnyDay.selectedDay?.rawValue != DaysType.all.rawValue && txtAnyDay.selectedDay != nil) && (txtAnyTime.text == nil || txtAnyTime.selectedFrom?.rawValue == TimeSlotTypes.all.rawValue) {
            //applyDateFilter()
            applyDateAndTimeFilter()
        } else if (txtAnyDay.selectedDay?.rawValue == DaysType.all.rawValue || txtAnyDay.selectedDay == nil) && (txtAnyTime.text != nil && txtAnyTime.selectedFrom?.rawValue != TimeSlotTypes.all.rawValue) {
            applyTimeFilter()
        } else if (txtAnyDay.selectedDay?.rawValue != DaysType.all.rawValue && txtAnyDay.selectedDay != nil) && (txtAnyTime.text != nil && txtAnyTime.selectedFrom?.rawValue != TimeSlotTypes.all.rawValue)  {
            applyDateAndTimeFilter()
        } else {
            loadAll()
        }
    }
    
    //------------------------------------------------------
    
    // Actions
    
    @IBAction func btn_Menu_Tap(_ sender: Any) {
        
        /*let controller = NavigationManager.shared.bSMenuVC
         push(controller: controller)*/
        
        NavigationManager.shared.sideMenuController.showLeftView()
    }
    
    @IBAction func btn_Playdates_tap(_ sender: Any) {
        
        /*let controller = NavigationManager.shared.landingVC
         push(controller: controller)*/
        
        NavigationManager.shared.parentProfileSwitchToPlayDates()
    }
    
    //MARK: Custom
    
    func bottomSheetView(_ bsModal: BabysitterModal) {
        let controller = NavigationManager.shared.babySitterProfileVC
        controller.profileModal = bsModal
        var sizes = [SheetSize]()
        sizes.append(.fixed(UIScreen.main.bounds.size.height * 0.5))
        sizes.append(.marginFromTop(60))
        let sheetController = SheetViewController(controller: controller, sizes: sizes)
        self.present(sheetController, animated: true, completion: nil)
    }
    
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtAnyDay {
            let today = Date()
            //let todaysHour = DateTimeManager.shared.getTodaysHour()
            let minDate = DateTimeManager.shared.date(byAddingHours: -24, toDate: today)!
            let maxDate = DateTimeManager.shared.date(byAddingHours: 744, toDate: today)!
            let datePicker = DatePicker()
            datePicker.setColors(main: PMColor.themeOrange, background: PMColor.white, inactive: UIColor.lightGray)
            datePicker.setup(beginWith: today, min: minDate, max: maxDate) { (selected, date) in
                if selected, let selectedDate = date {
                    debugLog(object: selectedDate)
                    let weekDay = DateTimeManager.shared.getWeekDayOf(date: selectedDate)
                    debugLog(object: weekDay)
                    self.txtAnyDay.selectedDay = self.txtAnyDay.days[weekDay]
                    self.txtAnyDay.text = self.txtAnyDay.days[weekDay].rawValue
                    self.refresh()
                }
            }
            // Display
            self.view.endEditing(true)
            datePicker.show(in: NavigationManager.shared.sideMenuController)
            return false
        }
        return true
    }
    
    /*func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == txtAnyDay || textField == txtAnyTime {
            
            var stringFilterDate = String()
            var stringFilterTime = String()
            if !ValidationManager.shared.isEmpty(text: txtAnyDay.text) {
                stringFilterDate = DateTimeManager.shared.stringFrom(date: txtAnyDay.dpDate.date, inFormate: DateFormate.yyyy_MM_dd)
            }
            if !ValidationManager.shared.isEmpty(text: txtAnyTime.text) {
                stringFilterTime = DateTimeManager.shared.stringFrom(date: txtAnyTime.dpDate.date, inFormate: DateFormate.HH_mm_ss)
            }
            
            var finalDate = String()
            if !stringFilterDate.isEmpty && !stringFilterTime.isEmpty {
                finalDate = stringFilterDate.appending(" ").appending(stringFilterTime)
            } else if !stringFilterDate.isEmpty {
                finalDate = stringFilterDate.appending(" ").appending("00:00:00")
            } else if !stringFilterTime.isEmpty {
                finalDate = "1970-01-01".appending(" ").appending("00:00:00")
            }
            let formate = DateFormate.yyyy_MM_dd.appending(" ").appending(DateFormate.HH_mm_ss)
            if let playDate = DateTimeManager.shared.dateFrom(stringDate: finalDate, inFormate: formate) {
                FirebaseManager.shared.findBabysitters { (babysitterModals: [BabysitterModal]) in
                    self.babySitters = babysitterModals
                }
            }
        }
    }*/
    
    //------------------------------------------------------
    
    //MARK: FGCalendarTextFieldDelegate
    
    func fgDay(textField: FGCalendarTextField, didEndEditing value: DaysType?) {
        
        refresh()
    }
    
    func fgDay(textField: FGCalendarTextField, didRightViewTap value: DaysType?) {
        
        txtAnyDay.selectedDay = nil
        txtAnyDay.text = nil
        refresh()        
    }
    
    //------------------------------------------------------
    
    //MARK: FGTimeSlotTextFieldDelegate
    
    func fgTimeslot(textField: FGTimeSlotTextField, didEndEditing from: TimeSlotTypes?, to: TimeSlotTypes?) {
        
        refresh()
    }
    
    func fgTimeslot(textField: FGTimeSlotTextField, didRightViewTap from: TimeSlotTypes?, to: TimeSlotTypes?) {
        
        textField.text = nil
        textField.selectedFrom = nil
        textField.selectedTo = nil
        refresh()
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return babySitters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = bs_LandingTable.dequeueReusableCell(withIdentifier: String(describing: LandingBabySitterTVC.self), for: indexPath) as? LandingBabySitterTVC {
            let bsModal = babySitters[indexPath.row]
            cell.setup(babysitterModal: bsModal)
            return cell
        }
        
        return UITableViewCell()
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let bsModal = babySitters[indexPath.row]
        bottomSheetView(bsModal)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
           
        return true
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if let parentModal = self.parentModal {
            let bbsitter = babySitters[indexPath.row]
            let playdateParentId = parentModal.userid ?? String()
                    
            let actionLike = UIContextualAction(style: .normal, title: String(), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                
                LoadingManager.shared.showLoading()
                
                delay {
                    
                    FirebaseManager.shared.findBabysitter(requestModalFrom: bbsitter.userid, userId: playdateParentId) { (result: RequestModal?) in
                        
                        LoadingManager.shared.hideLoading()
                        
                        if result != nil && (result?.status == "0" || result?.status.isEmpty == true) {
                        
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestAlreadySent) {
                                }
                            }
                            
                        } else if result?.status == "1" {
                            
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestAlreadyApproved) {
                                }
                            }
                            
                        } else if result?.status == "2" {
                            
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestRejected) {
                                }
                            }
                        } else if result?.status == "3" {
                            
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestOwnCancel) {
                                }
                            }
                            
                        } else {
                            
                            delay {
                                
                                let controller = NavigationManager.shared.sendBabysitterRequestVC
                                controller.parentId = parentModal.userid                                 
                                controller.bbsitterUserId = bbsitter.userid
                                controller.requesterName = parentModal.name
                                controller.modalPresentationStyle = .fullScreen
                                self.present(controller, animated: false, completion: nil)
                            }
                        }
                    }
                }
                success(false)
            })
            actionLike.image = UIImage(named: "icon_playdate_like")
            actionLike.backgroundColor = tableView.backgroundColor
            return UISwipeActionsConfiguration(actions: [actionLike])
            
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if let parentModal = self.parentModal {
                        
            let bbsitter = babySitters[indexPath.row]
            let playdateParentId = parentModal.userid ?? String()
            
            let actionDislike = UIContextualAction(style: .normal, title: String(), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                
                LoadingManager.shared.showLoading()
                
                delay {
                    
                    FirebaseManager.shared.findBabysitter(requestModalFrom: bbsitter.userid, userId: playdateParentId) { (result: RequestModal?) in
                                          
                        LoadingManager.shared.hideLoading()
                                            
                        if result != nil && result?.status == "0" {
                        
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestAlreadySent) {
                                }
                            }
                            
                        } else if result?.status == "1" {
                            
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestAlreadyApproved) {
                                }
                            }
                            
                        } else if result?.status == "2" {
                            
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestRejected) {
                                }
                            }
                            
                        } else if result?.status == "3" {
                            
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestOwnCancel) {
                                }
                            }
                            
                        } else {
                            
                            PreferenceManager.shared.blockedBabysitter.append(bbsitter.userid)
                            self.refresh()
                            
                            delay {
                                FirebaseManager.shared.sendRequest(playdateId: String(), playdateParentId: playdateParentId, playdateName: parentModal.name, userId: bbsitter.userid, kids: [], status: "3")
                            }
                        }
                    }
                }
                success(false)
            })
            
            actionDislike.image = UIImage(named: "icon_playdate_dislike")
            actionDislike.backgroundColor = tableView.backgroundColor
            return UISwipeActionsConfiguration(actions: [actionDislike])
        }
        return nil
    }
    //------------------------------------------------------
    
    //MARK: LocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let lat = locations.first?.coordinate.latitude ?? .zero
        let lng = locations.first?.coordinate.longitude ?? .zero
        
        self.parentModal?.lattitude = String( lat )
        self.parentModal?.longitude = String( lng )
        self.babysitterModal?.lattitude = String( lat )
        self.babysitterModal?.longitude = String( lng )
        
        refresh()
        LocationManager.shared.stopMonitoring()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        DisplayAlertManager.shared.displayAlertWithNoYes(target: self, animated: true, message: LocalizableConstants.Error.locationServiceDisable.localized()) {
        } handlerYes: {
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        //Handle heading
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtAnyDay.selectedDay = nil
        txtAnyTime.selectedFrom = nil
        txtAnyTime.selectedTo = nil
        
        bs_LandingTable.delegate = self
        bs_LandingTable.dataSource = self
        bs_LandingTable.register(UINib(nibName: String(describing: LandingBabySitterTVC.self), bundle: nil), forCellReuseIdentifier: String(describing: LandingBabySitterTVC.self))
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        
        //txtAnyDay.rightViewMode = UITextField.ViewMode.always
        //txtAnyTime.rightViewMode = UITextField.ViewMode.always
        
        txtAnyDay.delegate = self
        txtAnyDay.daysDelegate = self
        txtAnyTime.timeslotDelegate = self
        
        if LocationManager.shared.isRequiredToShowPermissionDialogue() {
            LocationManager.shared.requestForLocationPermissionWhenInUse()
        } else {
            LocationManager.shared.delegate = self
            LocationManager.shared.startMonitoring()
        }        
                
        refresh()
    }
    
    //------------------------------------------------------
}

