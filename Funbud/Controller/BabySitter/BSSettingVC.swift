//
//  BSSettingVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 20/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class BSSettingVC : BaseVC {
    
    @IBOutlet weak var hideVw: NSLayoutConstraint!
    @IBOutlet weak var lineVw: UIView!
    @IBOutlet weak var txtSelectCity: ATCTextField!
    @IBOutlet weak var enablenotificationSwitch: UISwitch!
    @IBOutlet weak var enableLocationSwitch: UISwitch!
    @IBOutlet weak var lblDesp: PoppinsRegularLabel!
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func performSave(parentModal: ParentsModal) {
        
        LoadingManager.shared.showLoading()
        
        delay {
                        
            FirebaseManager.shared.save(parent: parentModal, of: parentModal.userid ?? String())
            LoadingManager.shared.hideLoading()
            
            delay {
                
                self.pop()
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        //self.pop()
        NavigationManager.shared.sideMenuController.showLeftView()
    }
    
    @IBAction func locationSwitchChanged(_ sender: UISwitch) {
        
        if (sender.isOn == true) {
            self.txtSelectCity.isHidden = true
            self.lblDesp.isHidden = false
            self.lineVw.isHidden = false
            self.hideVw.constant = 0
           
        } else {
            self.txtSelectCity.isHidden = false
            self.lblDesp.isHidden = true
            self.lineVw.isHidden = true
            self.hideVw.constant = 120
        }
        
        parentModal?.isLocationEnable = sender.isOn ? "1" : "0"
        if let parentModal = parentModal {
            performSave(parentModal: parentModal)
        }
    }
    
    @IBAction func notificationswitchChanged(_ sender: UISwitch) {
        
        parentModal?.isNotificationEnabled = sender.isOn ? "1" : "0"
        if let parentModal = parentModal {
            performSave(parentModal: parentModal)
        }
    }
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtSelectCity.isHidden = true
        self.hideVw.constant = 0
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        enableLocationSwitch.isOn = parentModal?.isLocationEnable == "1" ? true : false
        enablenotificationSwitch.isOn = parentModal?.isNotificationEnabled == "1" ? true : false
    }
    
    //------------------------------------------------------
}
