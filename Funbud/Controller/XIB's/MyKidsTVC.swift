//
//  MyKidsTVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 18/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

class MyKidsTVC: UITableViewCell {

    @IBOutlet weak var btnkidDetails: UIButton!
    @IBOutlet weak var lblKidName: PoppinsRegularLabel!
    @IBOutlet weak var avatarImg: UIImageView!
    
    var kidDetails:((_ kid: KidsModal?)->Void)?
    
    var img: [String] {
        return PMImageName.avatars
    }
    var kid: KidsModal?
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup(kidsModal: KidsModal) {
        
        self.kid = kidsModal
        
        lblKidName.text = kidsModal.getNameWithAge()
        
        if let index = Int(kidsModal.avatar), img.indices.contains(index) {
            avatarImg.image = UIImage(named: img[index])
        } else {
            avatarImg.image = nil
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }

    //------------------------------------------------------
    @IBAction func btnKidDetailTapped(_ sender: Any) {
        
        kidDetails?(kid)
    }
}
