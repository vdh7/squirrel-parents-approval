//
//  AvailabilityTVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 13/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

class AvailabilityTVC: UITableViewCell, UITextFieldDelegate, BSPickDateDelegate {
          
    @IBOutlet weak var fieldsStackVw: UIStackView!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var weekSwitchBtn: UISwitch!
    @IBOutlet weak var lblWeek: PoppinsRegularLabel!
    
    @IBOutlet weak var heightStackVw: NSLayoutConstraint!
        
    var parentController: BSAvailabilityVC?
    var dayModal: DaysModal?
    var isAlwaysAvailable: Bool = false
    
    var dateFrom: Date?
    var dateTo: Date?
    
    //var switchAction:((Bool)->())?
    var isEnable: Bool = true
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func getFrom() -> String {
        
        let dateFrom = DateTimeManager.shared.dateFrom(stringDate: dayModal?.from ?? String(), inFormate: DateFormate.UTC)
        return DateTimeManager.shared.stringFrom(date: dateFrom ?? Date(), inFormate: DateFormate.UTC)
    }
    
    func isEmptyFrom() -> Bool {
        return ValidationManager.shared.isEmpty(text: txtFrom.text)
    }
    
    func getTo() -> String {
        
        let dateTo = DateTimeManager.shared.dateFrom(stringDate: dayModal?.to ?? String(), inFormate: DateFormate.UTC)
        return DateTimeManager.shared.stringFrom(date: dateTo ?? Date(), inFormate: DateFormate.UTC)
    }
    
    func isEmptyTo() -> Bool {
        return ValidationManager.shared.isEmpty(text: txtTo.text)
    }
    
    func isAvailable() -> Bool {
        return weekSwitchBtn.isOn
    }
    
    func defaultsetup() {
        
        txtFrom.delegate = self
        txtTo.delegate = self
        
        txtFrom.text = String()
        txtTo.text = String()
        weekSwitchBtn.isOn = false
        
        selectionStyle = .none
    }
    
    func setup(week: String?) {
        
        lblWeek.text = week
        weekSwitchBtn.isHidden = isAlwaysAvailable
        isEnable = !isAlwaysAvailable
    }
    
    func setup(daysModal: DaysModal) {
        
        self.dayModal = daysModal
        self.weekSwitchBtn.tag = Int(daysModal.day) ?? .zero
        
        txtFrom.text = daysModal.displayFrom()
        txtTo.text = daysModal.displayTo()
        isEnable = daysModal.isAvailable == "1" ? true : false
        weekSwitchBtn.isOn = isEnable
        
        setCell(isEnabled: isEnable)
    }
    
    func setCell(isEnabled: Bool) {
        
        if isAlwaysAvailable == true {
            heightStackVw.constant = .zero
            txtFrom.isHidden = true
            txtTo.isHidden = true
        } else {
            if isEnabled {
                heightStackVw.constant = 60
                txtFrom.isHidden = false
                txtTo.isHidden = false
            } else {
                heightStackVw.constant = .zero
                txtFrom.isHidden = true
                txtTo.isHidden = true
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func switchWeekBtnChanged(_ sender: UISwitch) {
        if isAlwaysAvailable == true {
            setCell(isEnabled: false)
        } else {
            self.dayModal?.isAvailable = isEnable == true ? "0" : "1"
            setCell(isEnabled: !isEnable)
        }
        parentController?.refreshAvailability()
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        parentController?.view.endEditing(true)
        
        let controller = NavigationManager.shared.bSPickDateVC
        controller.delegate = self
        controller.previousDate = dateFrom
        controller.sourceTextField = textField
        controller.modalPresentationStyle = .overFullScreen
        parentController?.present(controller, animated: false, completion: nil)
        
        return false
    }
    
    //------------------------------------------------------
    
    //MARK: BSPickDateDelegate
    
    func pickDate(controller: BSPickDateVC, didSelect date: String) {
        
        if controller.sourceTextField == txtFrom {
            self.dayModal?.from = date
            self.dateFrom = controller.previousDate
        } else if controller.sourceTextField == txtTo {
            self.dayModal?.to = date
            self.dateTo = controller.previousDate
        }
        if let dayModal = self.dayModal {
            setup(daysModal: dayModal)
        }
        controller.dismiss(animated: false, completion: nil)
    }
    
    func pickDate(controller: BSPickDateVC, didCancel sender: Any) {
        
        controller.dismiss(animated: false, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        defaultsetup()
    }
    
    //------------------------------------------------------
}
