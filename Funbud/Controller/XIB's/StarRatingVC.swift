//
//  StarRatingVC.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 01/02/22.
//  Copyright © 2022 dharmesh. All rights reserved.
//

import UIKit

class StarRatingVC: BaseVC {
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func btnSaveTap(_ sender: PoppinsBoldButton) {
        
        LoadingManager.shared.showLoading()
        
        delay {
        }
    }
    
    @IBAction func btnCloseTap(_ sender: PoppinsBoldButton) {
        
        dismiss(animated: false, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
