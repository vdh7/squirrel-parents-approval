//
//  AddKid.swift
//  Funbud
//
//  Created by Vivek Dharmani on 02/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

class AddKidTVC: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UITextViewDelegate {
    
    @IBOutlet weak var deleteStackVw: UIStackView!
    @IBOutlet weak var lblKidCount: UILabel!
    @IBOutlet weak var avatarCollectionVw: UICollectionView!
    @IBOutlet weak var txtVwDep: UITextView!
    @IBOutlet weak var txtSpecifyGender: SLGenderTextField!
    @IBOutlet weak var segmentVW: HBSegmentedControl!
    @IBOutlet weak var deleteStackHeight: NSLayoutConstraint!
    @IBOutlet weak var txtDob: FGBirthDateTextField!
    @IBOutlet weak var txtName: ATCTextField!
    @IBOutlet weak var deleteBtn: UIButton!

    var img: [String] {
        return PMImageName.avatars
    }
    
    var selectedAvatarIndex: Int? = nil
    var kidsModal: KidsModal?
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func getName() -> String {
        return txtName.text ?? String()
    }
    
    func isEmptyName() -> Bool {
        return ValidationManager.shared.isEmpty(text: txtName.text)
    }
    
    func getDOB() -> String {
        return DateTimeManager.shared.stringFrom(date: txtDob.dpDate.date, inFormate: DateFormate.UTC)
    }
    
    func isEmptyDOB() -> Bool {
        return ValidationManager.shared.isEmpty(text: txtDob.text)
    }
    
    func getGenderIndex() -> String {
        return String(segmentVW.selectedIndex)
    }
    
    func getSpecifyGender() -> String {
        return txtSpecifyGender.text ?? String()
    }
    
    func isEmptySpecifyGender() -> Bool {
        return segmentVW.selectedIndex == 2 && ValidationManager.shared.isEmpty(text: txtSpecifyGender.text)
    }
    
    func getSelectedAvtarIndex() -> String {
        return String(selectedAvatarIndex ?? .zero)
    }
    
    func isAvatarSelected() -> Bool {
        return selectedAvatarIndex != nil
    }
    
    func getDescription() -> String {
        return txtVwDep.text ?? String()
    }
    
    func isEmptyDescription() -> Bool {
        return ValidationManager.shared.isEmpty(text: txtVwDep.text)
    }
    
    func setup(kidsModal: KidsModal) {
        self.kidsModal = kidsModal
        txtName.text = kidsModal.name
        txtDob.dpDate.date = kidsModal.getDOB()
        txtDob.text = kidsModal.displayDOB()
        segmentVW.selectedIndex = Int( kidsModal.gender ) ?? .zero
        txtSpecifyGender.text = kidsModal.genderText
        selectedAvatarIndex = Int( kidsModal.avatar ) ?? .zero
        txtVwDep.text = kidsModal.hobbies        
        if segmentVW.selectedIndex == .zero ||  segmentVW.selectedIndex == 1 {
            txtSpecifyGender.isHidden = true
        }
        avatarCollectionVw.reloadData()
    }
    
    func reset() {
        
        txtName.text = nil
        txtDob.text = nil
        segmentVW.selectedIndex = .zero
        txtVwDep.text = nil
        if segmentVW.selectedIndex == .zero ||  segmentVW.selectedIndex == 1 {
            txtSpecifyGender.isHidden = true
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func deleteBtnClicked(_ sender: Any) {
        
    }
    
    //------------------------------------------------------
    
    //MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return img.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AvatarCVC.self), for: indexPath) as? AvatarCVC {
            cell.set(imageName: img[indexPath.row])
            if selectedAvatarIndex == indexPath.row {
                cell.set(selected: true)
                cell.avatarImg.borderColor = PMColor.green
                cell.avatarImg.borderWidth = 2
            } else {
                cell.set(selected: false)
                cell.avatarImg.borderColor = .clear
                
            }
            cell.layoutIfNeeded()
            return cell
        }
        return UICollectionViewCell()
    }
    
    //------------------------------------------------------
    
    //MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 80, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedAvatarIndex = indexPath.row
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        selectedAvatarIndex = nil
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        txtVwDep.delegate = self
        txtVwDep.textContainerInset = UIEdgeInsets(top: 25, left: 25, bottom: 11, right: 25)
        
        segmentVW.items = GenderType.allCases.map({ $0.rawValue })
        segmentVW.font = PMFont.sfpoppinsRegular(size: 14)
        segmentVW.selectedFont = PMFont.sfpoppinsExtraBold(size: 14)
        segmentVW.selectedIndex = 2
        segmentVW.padding = 4
        selectionStyle = .none
        
        //Initialization code
        avatarCollectionVw.delegate = self
        avatarCollectionVw.dataSource = self
        avatarCollectionVw.register(UINib(nibName: String(describing: AvatarCVC.self), bundle: nil), forCellWithReuseIdentifier: String(describing: AvatarCVC.self))
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let str = (txtVwDep.text + text)
        if str.count <= 120 {
            return true
        }
        textView.text = str.substring(to: str.index(str.startIndex, offsetBy: 120))
        return false
    }
    
    //------------------------------------------------------
}
