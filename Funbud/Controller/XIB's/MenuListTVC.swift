//
//  MenuListTVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 09/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

class MenuListTVC: UITableViewCell {

    @IBOutlet weak var lineVw: UIView!
    @IBOutlet weak var lblMenu: UILabel!
    @IBOutlet weak var menuImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }    
}
