//
//  SendRequestVC.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 10/4/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class SendRequestVC : BaseVC, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var lblTitle: PoppinsBoldLabel!
    @IBOutlet weak var lblWhoWantsToJoin: PoppinsRegularLabel!
    @IBOutlet weak var collectionViewKids: UICollectionView!
    @IBOutlet weak var lblPlaydatesName: PoppinsBoldLabel!
    @IBOutlet weak var btnSendRequst: PoppinsBoldButton!
    
    var selectedAvatarIndex: Int? = nil
    var img: [String] {
        return PMImageName.avatars
    }
    
    var kids: [KidsModal] = []
    var selectedKids: [KidsModal] = [] {
        didSet {
            //btnSendRequst?.isEnabled = selectedKids.count > .zero ? true : false
        }
    }
    var playdateId: String = String()
    var playdateName: String = String()
    var playdateParentId: String = String()
    var userId: String = String()
    var status: String = "0"
    var playdateModal: PlayDatesModal?
        
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        collectionViewKids.delegate = self
        collectionViewKids.dataSource = self
        collectionViewKids.register(UINib(nibName: String(describing: AvatarCVC.self), bundle: nil), forCellWithReuseIdentifier: String(describing: AvatarCVC.self))
        
        lblPlaydatesName.text = playdateModal?.title
        
        //btnSendRequst.isEnabled = false
    }
    
    func loadKids() {
        
        //LoadingManager.shared.showLoading()
        
        parentModal?.findKid(completion: { kids in
        
            //LoadingManager.shared.hideLoading()
            
            delay {
                self.kids = kids
                debugPrint(kids)
                if kids.count == 1, let first = kids.first {
                    self.selectedKids = [first]
                }
                self.collectionViewKids.reloadData()
                //self.collectionViewKids.flashScrollIndicators()
            }
        })
    }
    
    //------------------------------------------------------
    
    func validate() -> Bool {
        
        if selectedKids.count == .zero {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.selectKidForRequest) {
            }
            return false
        }
               
        return true
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func btnSendRequestTap(_ sender: PoppinsBoldButton) {
        
        if validate() == false {
            return
        }
        
        LoadingManager.shared.showLoading()
        
        delay {
        
            let selectedKidsIds = self.selectedKids.compactMap { arg0 in
                return arg0.firebaseID
            }
            
            if self.playdateModal?.groupEvent == true {
                FirebaseManager.shared.sendRequest(playdateId: self.playdateId, playdateParentId: self.playdateParentId, playdateName: self.playdateName, userId: self.userId, kids: selectedKidsIds, status: self.status)
            } else {
                FirebaseManager.shared.sendNonGroupRequest(playdateId: String(), playdateParentId: self.playdateParentId, playdateName: self.playdateName, userId: self.userId, kids: selectedKidsIds, status: self.status)
            }
            
            PreferenceManager.shared.blockedPlaydates.append(self.playdateModal?.id ?? String())
            
            LoadingManager.shared.hideLoading()
            
            delay {
                
                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestSent, handlerOK: {
                    self.dismiss(animated: false, completion: nil)
                })
            }
        }
    }
    
    @IBAction func btnCloseTap(_ sender: PoppinsBoldButton) {
        
        dismiss(animated: false, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return kids.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AvatarCVC.self), for: indexPath) as? AvatarCVC {
            
            let kid = kids[indexPath.row]
            let avatarIndex = Int(kid.avatar) ?? .zero
            cell.set(imageName: img[avatarIndex])
            if selectedKids.contains(where: { arg0 in
                return arg0.firebaseID == kid.firebaseID
            }) {
                cell.set(selected: true)
                cell.avatarImg.borderColor = PMColor.white
                cell.avatarImg.borderWidth = 2
            } else {
                cell.set(selected: false)
                cell.avatarImg.borderColor = .clear
            }
            cell.layoutIfNeeded()
            return cell
        }
        return UICollectionViewCell()
    }
    
    //------------------------------------------------------
    
    //MARK: UICollectionViewDelegateFlowLayout
    
    var cellSize = CGSize(width: 150, height: 150)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if kids.count < 2 {
            let totalCellWidth: CGFloat = cellSize.width * CGFloat(kids.count)
            let totalSpacingWidth: CGFloat = 10 * CGFloat(kids.count - 1)

            let leftInset = (collectionView.bounds.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset

            return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        }
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedAvatarIndex = nil
        
        let kid = kids[indexPath.row]
        if selectedKids.contains(where: { arg0 in
            return arg0.firebaseID == kid.firebaseID
        }) {
            selectedKids.removeAll { arg0 in
                return arg0.firebaseID == kid.firebaseID
            }
        } else {
            selectedKids.append(kid)
        }
        collectionView.reloadData()
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        loadKids()
        
        btnSendRequst.isEnabled = true
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
