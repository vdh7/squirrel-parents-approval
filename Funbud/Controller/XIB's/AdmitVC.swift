//
//  AdmitVC.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 10/5/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

class AdmitVC: BaseVC, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var lblTitle: PoppinsBoldLabel!
    @IBOutlet weak var lblWhoWantsToJoin: PoppinsRegularLabel!
    @IBOutlet weak var collectionViewKids: UICollectionView!
    @IBOutlet weak var lblKidsName: PoppinsBoldLabel!
    @IBOutlet weak var lblPlaydatesName: PoppinsBoldLabel!
    @IBOutlet weak var btnAdmit: PoppinsBoldButton!
    
    private var kids: [KidsModal] = []
    var requestModal: RequestModal? = nil
        
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        //kids = requestModal?.kids ?? []
        requestModal?.updateKidsName()
        requestModal?.didUpdateBlock = {
            DispatchQueue.main.async {
                self.kids = self.requestModal?.kidsModal ?? []
                self.collectionViewKids.reloadData()
            }
        }
        //kids = requestModal?.kidsModal ?? []
        if requestModal?.isForBabysitter == "1" {
            lblWhoWantsToJoin.text = nil
            lblPlaydatesName.text = LocalizableConstants.Controller.isRequestingForBabysitter.localized()
        } else {
            lblPlaydatesName.text = requestModal?.playdateName
        }
        lblKidsName.text = requestModal?.getChildNames()
        
        collectionViewKids.delegate = self
        collectionViewKids.dataSource = self
        collectionViewKids.register(UINib(nibName: String(describing: AdmitAvatarCVC.self), bundle: nil), forCellWithReuseIdentifier: String(describing: AdmitAvatarCVC.self))
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func btnAdmitTap(_ sender: PoppinsBoldButton) {
        
        LoadingManager.shared.showLoading()
        
        delay {
        
            if let requestModal = self.requestModal {
                
                FirebaseManager.shared.admit(requestModal: requestModal) {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(PMNotification.refreshPlayDates), object: nil)
                    
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.admitedSuccessfully, handlerOK: {
                            self.dismiss(animated: false, completion: nil)
                        })
                    }
                }
            }
        }
    }
    
    @IBAction func btnCloseTap(_ sender: PoppinsBoldButton) {
        
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnCancelTap(_ sender: PoppinsBoldButton) {
        
        LoadingManager.shared.showLoading()
        
        delay {
        
            if let requestModal = self.requestModal {
                
                FirebaseManager.shared.reject(requestModal: requestModal) {
                    
                    NotificationCenter.default.post(name: NSNotification.Name(PMNotification.refreshPlayDates), object: nil)
                    
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestRejectedSuccessfully, handlerOK: {
                            self.dismiss(animated: false, completion: nil)
                        })
                    }
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        debugLog(object: kids.count)
        return kids.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AdmitAvatarCVC.self), for: indexPath) as? AdmitAvatarCVC {
            let kid = kids[indexPath.row]            
            cell.set(kid: kid)
            return cell
        }
        return UICollectionViewCell()
    }
    
    //------------------------------------------------------
    
    //MARK: UICollectionViewDelegateFlowLayout
    
    var cellSize = CGSize(width: 100, height: 150)
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        let totalCellWidth: CGFloat = cellSize.width * CGFloat(kids.count)
        let totalSpacingWidth: CGFloat = 10 * CGFloat(kids.count - 1)

        let leftInset = (collectionView.bounds.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset

        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
