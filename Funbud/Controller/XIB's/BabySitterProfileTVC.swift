//
//  BabySitterProfileTVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 20/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.

import UIKit

class BabySitterProfileTVC: UITableViewCell {

    @IBOutlet weak var lblDay: PoppinsRegularLabel!
    @IBOutlet weak var lblTime: PoppinsRegularLabel!
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup(_ bsModal: BabysitterModal, indexPath: IndexPath) {
        
        if bsModal.days.indices.contains(indexPath.row) {
            let dayObj = bsModal.days[indexPath.row]
            lblDay.text = getDayName(fromDay: Int( dayObj.day ?? String("0")) ?? .zero )
            lblTime.text = String(format: "%@ - %@", dayObj.displayFrom(), dayObj.displayTo())
        } else {
            lblDay.text = nil
            lblTime.text = nil
        }
    }
    
    func setup(_ dayModal: DaysModal) {
        
        if dayModal.isAvailable == "1" {
            lblDay.text = getDayName(fromDay: Int( dayModal.day ?? String("0")) ?? .zero )
            lblTime.text = String(format: "%@ - %@", dayModal.displayFrom(), dayModal.displayTo())
        }
    }
    
    //------------------------------------------------------
}
