//
//  ChatDetailsTVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 22/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

class ChatDetailsTVC: UITableViewCell {
    
    @IBOutlet weak var lblKidsName: UILabel!
    @IBOutlet weak var avatarImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
