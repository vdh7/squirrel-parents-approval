//
//  AdmitAvatarCVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 02/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

class AdmitAvatarCVC: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerSubView: UIView!
    @IBOutlet weak var selectedCheck: UIImageView!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var lblName: FredokaOneRegularLabel!
    
    var avtarImgs: [String] {
        return PMImageName.avatars
    }
    
    var isThisSelected: Bool = false {
        didSet {
            if isThisSelected {
                selectedCheck.isHidden = false
            } else {
                selectedCheck.isHidden = true
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func set(kid: KidsModal) {
       
        let index = Int( kid.avatar ?? String("0") ) ?? .zero
        set(imageName: avtarImgs[index])
        set(selected: isThisSelected)
        set(kidName: kid.getNameWithAge())
    }
    
    private func set(imageName: String) {
        self.avatarImg.image = UIImage(named: imageName)
    }
    
    func set(selected: Bool) {
        self.isThisSelected = selected
    }
    
    private func set(kidName: String?) {
        self.lblName.text = kidName
        self.lblName.setNeedsDisplay()
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        containerView.cornerRadius = containerView.bounds.height/2
        containerSubView.cornerRadius = containerSubView.bounds.height/2
    }
    
    //------------------------------------------------------
}
