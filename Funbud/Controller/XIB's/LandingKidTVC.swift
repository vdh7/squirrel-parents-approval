//
//  LandingKidTVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 06/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import SDWebImage

class LandingKidTVC: UITableViewCell {
    
    @IBOutlet weak var containerSubView: UIView!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var imgParentAvatar: UIImageView!
    @IBOutlet weak var lblCount: PoppinsSemiBoldLabel!
    @IBOutlet weak var threeDotsImg: UIImageView!
    @IBOutlet weak var btnThreeDots: UIButton!
    @IBOutlet weak var groupBtn: PoppinsSemiBoldButton!
    @IBOutlet weak var lbldesp: PoppinsRegularLabel!
    @IBOutlet weak var lblMonth: PoppinsLightLabel!
    @IBOutlet weak var lblTIme: PoppinsBoldLabel!
    @IBOutlet weak var lblDate: PoppinsBoldLabel!
    @IBOutlet weak var lblKidName: FredokaOneRegularLabel!
    @IBOutlet weak var lblKm: PoppinsSemiBoldLabel!
    @IBOutlet weak var lblNewHere: PoppinsBoldLabel!
    @IBOutlet weak var stackTime: UIStackView!
    
    var parentVC: UIViewController?
    var playdateModal: PlayDatesModal?
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup(playdateModal: PlayDatesModal, isOwnPlaydate: Bool = false) {
        
        self.playdateModal = playdateModal
        
        if isOwnPlaydate == false {
            if playdateModal.isAutoGenerate == "true" {
                lblNewHere.isHidden = false
                lblDate.isHidden = true
                stackTime.isHidden = true
            } else {
                lblNewHere.isHidden = true
                lblDate.isHidden = false
                stackTime.isHidden = false
            }
        } else {
            
            lblNewHere.isHidden = true
            lblDate.isHidden = false
            stackTime.isHidden = false
        }
        
        if let firstChild = playdateModal.childList.first {
            FirebaseManager.shared.findKid(fromID: firstChild) { kidsModal in
                //self.imgAvatar.image = nil
                if let kidsModal = kidsModal {
                    let images = PMImageName.avatars
                    let index = Int( kidsModal.avatar ?? "0") ?? .zero
                    if images.indices.contains(index) {
                        self.imgAvatar.image = UIImage(named:  images[index])
                    } else {
                        self.imgAvatar.image = UIImage(named:  PMImageName.placeholder)
                    }
                } else {
                    self.imgAvatar.image = UIImage(named:  PMImageName.placeholder)
                }
            }
        } else {
            imgAvatar.image = UIImage(named:  PMImageName.placeholder)
        }
    
        let (isHidden, stringToDisplayKidsCount) = playdateModal.displayKidsCount()
        lblCount.isHidden = isHidden
        lblCount.text = stringToDisplayKidsCount
    
        lblKidName.text = playdateModal.getChildNames()
        playdateModal.updateKidsName()
        playdateModal.didUpdateBlock = {
            self.lblKidName.text = playdateModal.getChildNames()
        }
        lblDate.text = playdateModal.getDay()
        lblTIme.text = playdateModal.getTime()
        lblMonth.text = playdateModal.getMonth()
        lbldesp.text = playdateModal.title
        lblKm.text = playdateModal.getDistnaceInKm()
        
        if playdateModal.groupEvent == true && playdateModal.isAutoGenerate == "false" {
            groupBtn.isHidden = false
        } else {
            groupBtn.isHidden = true
        }
        
        if let ownerImage = playdateModal.ownerImage {
            self.imgParentAvatar.sd_setIndicatorStyle(UIActivityIndicatorView.Style.medium)
            self.imgParentAvatar.sd_addActivityIndicator()
            self.imgParentAvatar.sd_showActivityIndicatorView()
            self.imgParentAvatar.sd_setImage(with: URL(string: ownerImage), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                self.imgParentAvatar.image = img
                self.imgParentAvatar.sd_removeActivityIndicator()
            }
        } else {
            imgParentAvatar.image = UIImage(named:  PMImageName.placeholder)
        }
    }
    
    func setupMy(playdateModal: PlayDatesModal) {
        
        setup(playdateModal: playdateModal, isOwnPlaydate: true)
        
        lblKm.isHidden = true
        threeDotsImg.isHidden = false
        btnThreeDots.isUserInteractionEnabled = true
    }
    
    private func performDelete() {
        
        if let playdateModal = playdateModal {
            
            LoadingManager.shared.showLoading()
            
            delay {
                
                FirebaseManager.shared.delete(playdateModal: playdateModal) { error in
                    
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        if let error = error {
                            DisplayAlertManager.shared.displayAlert(message: error.localizedDescription) {
                            }
                        } else {
                            self.parentVC?.viewWillAppear(true)
                        }
                    }
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func groupBtnClicked(_ sender: PoppinsSemiBoldButton) {
        
    }
    
    @IBAction func btnthreeDotsTap(_ sender: Any) {
        
        let edit = LocalizableConstants.Controller.editPlayDate.localized()
        let delete = LocalizableConstants.Controller.deletePlayDate.localized()
        
        if let parentViewController = self.parentVC {
         
            DisplayAlertManager.shared.displayActionSheet(target: parentViewController, titles: [edit, delete], animated: true, message: String()) { (selectedOption: String?) in
                
                if edit == selectedOption {
                    
                    let controller = NavigationManager.shared.createPlaydatesVC
                    controller.commingFrom = "Menu"
                    if let playdateModal = self.playdateModal {
                        controller.playDatesModal = playdateModal
                    }
                    controller.isEdit = true
                    parentViewController.navigationController?.pushViewController(controller, animated: true)
                    
                } else if delete == selectedOption {
                     
                    if let parentViewController = self.parentVC {
                        DisplayAlertManager.shared.displayAlertWithNoYes(target: parentViewController, animated: true, message: LocalizableConstants.Controller.confirmationDeletePlaydate) {
                            
                        } handlerYes: {
                            
                            self.performDelete()
                        }
                    }
                }
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        containerSubView.cornerRadius = containerSubView.bounds.width/2
        imgAvatar.cornerRadius = imgAvatar.bounds.width/2
    }
    
    //------------------------------------------------------
    
    //MARK: Init        
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgAvatar.image = nil
        selectionStyle = .none
    }

    //------------------------------------------------------
}
