//
//  ChatPlaydatesTVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 21/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import SDWebImage

class ChatPlaydatesTVC: UITableViewCell {

    @IBOutlet weak var lblActiveOnline: UILabel!
    @IBOutlet weak var lblTIme: UILabel!
    @IBOutlet weak var lblDesp: UILabel!
    @IBOutlet weak var lblKidName: UILabel!
    @IBOutlet weak var lblCount: PoppinsSemiBoldLabel!
    @IBOutlet weak var containerSubView: UIView!
    @IBOutlet weak var avatarImg: UIImageView!
    
    var conversation: ConversationModal?
    var isParentProfile: Bool = false
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setupDisplayName(_ conversation: ConversationModal) {
        
        if conversation.isForBabysitter == "1" {
            if self.isParentProfile {                
                self.lblKidName.text = conversation.username
            } else {
                /*if ValidationManager.shared.isEmpty(text: conversation.name) {
                    self.lblKidName.text = conversation.username
                } else {
                    self.lblKidName.text = conversation.name
                }*/
                self.lblKidName.text = conversation.username
            }
        } else {
            if conversation.groupEvent == "1" {
                self.lblKidName.text = conversation.name
            } else {
                if ValidationManager.shared.isEmpty(text: conversation.name) {
                    self.lblKidName.text = conversation.username
                } else {
                    self.lblKidName.text = conversation.name
                }
            }
        }
    }
    
    fileprivate func setAvatar(_ conversation: ConversationModal) {
        
        //if conversation.isForBabysitter == "1" && isParentProfile == true {
        if conversation.isForBabysitter == "1" {
            let bbsitterId = conversation.access?.first(where: { userId in
                return conversation.owner != userId
            })
            if let bbsitterId = bbsitterId, isParentProfile == true {
                FirebaseManager.shared.findDeviceToken(of: bbsitterId, roleType: RoleType.babysitter) { baseModal in
                    conversation.username = baseModal.name
                    self.setupDisplayName(conversation)
                    if let imageURL = baseModal.imgsource {
                        self.avatarImg.sd_setIndicatorStyle(UIActivityIndicatorView.Style.medium)
                        self.avatarImg.sd_addActivityIndicator()
                        self.avatarImg.sd_showActivityIndicatorView()
                        self.avatarImg.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                            self.avatarImg.image = img
                            self.avatarImg.sd_removeActivityIndicator()
                        }
                    } else {
                        self.avatarImg.image = nil
                    }
                }
                
            } else if isParentProfile == false, let parentId = conversation.owner {
                
                FirebaseManager.shared.findDeviceToken(of: parentId, roleType: RoleType.parent) { parentModal in
                    conversation.username = parentModal.name
                    self.setupDisplayName(conversation)
                    if let imageURL = parentModal.imgsource {
                        self.avatarImg.sd_setIndicatorStyle(UIActivityIndicatorView.Style.medium)
                        self.avatarImg.sd_addActivityIndicator()
                        self.avatarImg.sd_showActivityIndicatorView()
                        self.avatarImg.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                            self.avatarImg.image = img
                            self.avatarImg.sd_removeActivityIndicator()
                        }
                    } else {
                        self.avatarImg.image = nil
                    }
                }
            } else {
                avatarImg.image = UIImage(named: PMImageName.placeholder)
            }
        } else {
            if let first = conversation.kidsList.first {
                FirebaseManager.shared.findKid(fromID: first) { kidsModal in
                    let avatarIndex = Int(kidsModal?.avatar ?? String()) ?? .zero
                    let img = PMImageName.avatars
                    self.avatarImg.image = UIImage(named: img[avatarIndex])
                }
            } else {
                avatarImg.image = nil
            }
        }
    }
    
    func setup(conversation: ConversationModal) {
        
        self.conversation = conversation
        refresh()
        
        FirebaseManager.shared.listenEvent(ofConversationId: conversation.conversationId ?? String()) { (updatedObj: ConversationModal) in
            
            if updatedObj.conversationId == self.conversation?.conversationId {
                updatedObj.isSubscribe = self.conversation?.isSubscribe ?? false
                self.conversation = updatedObj
                self.refreshLastMessageOnly()
            }
        }
        
        if self.conversation?.isSubscribe == false, let conversationId = conversation.conversationId {
            FirebaseManager.shared.subscribe(conversation: conversationId)
        }
    }
    
    private func refresh() {
        
        if let conversation = conversation {
            //if conversation.isForBabysitter == "1" && isParentProfile == true {
            setupDisplayName(conversation)
            lblDesp.text = conversation.lastMsg
            lblTIme.text = conversation.getLastMessageTime()
            let count =  conversation.kidsList.count
            lblCount.text = String( count )
            if count > 1 {
                lblCount.isHidden = false
            } else {
                lblCount.isHidden = true
            }
            setAvatar(conversation)
            
            conversation.updatePlaydateName()
            conversation.didUpdateBlock = {
                //if conversation.isForBabysitter == "1" && self.isParentProfile == true {
                self.setupDisplayName(conversation)
                self.lblDesp.text = conversation.lastMsg
                self.setAvatar(conversation)
                self.lblKidName.setNeedsDisplay()
                self.lblDesp.setNeedsDisplay()
                self.layoutIfNeeded()
            }
        }
    }
    
    private func refreshLastMessageOnly() {
        
        self.lblDesp.text = conversation?.lastMsg
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        avatarImg.cornerRadius = avatarImg.bounds.width / 2
        containerSubView.cornerRadius = containerSubView.bounds.width / 2
    }
    //------------------------------------------------------
    
    //MARK: Init
        
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        lblActiveOnline.isHidden = true
    }
    
    //------------------------------------------------------
}
