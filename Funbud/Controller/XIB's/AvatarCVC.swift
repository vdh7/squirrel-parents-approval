//
//  AvatarCVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 02/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

class AvatarCVC: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var selectedCheck: UIImageView!
    @IBOutlet weak var avatarImg: UIImageView!
    
    var isThisSelected: Bool = false {
        didSet {
            if isThisSelected {
                selectedCheck.isHidden = false
            } else {
                selectedCheck.isHidden = true
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func set(imageName: String) {
        self.avatarImg.image = UIImage(named: imageName)
    }
    
    func set(selected: Bool) {
        self.isThisSelected = selected
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatarImg.clipsToBounds = true
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        avatarImg.cornerRadius = containerView.bounds.height/2
        containerView.cornerRadius = containerView.bounds.height/2
    }
    
    //------------------------------------------------------
}
