//
//  ChatBabySitterTVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 21/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

class ChatBabySitterTVC: UITableViewCell {

    @IBOutlet weak var bs_lbl_Active: UILabel!
    @IBOutlet weak var bs_lbl_time: UILabel!
    @IBOutlet weak var bs_lbl_desp: UILabel!
    @IBOutlet weak var bs_lbl_Name: UILabel!
    @IBOutlet weak var bs_profileImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
