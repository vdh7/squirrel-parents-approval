//
//  ChatRequestCVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 21/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

class ChatRequestCVC: UICollectionViewCell {
    
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var containerSubView: UIView!
    @IBOutlet weak var lblCount: UILabel!

    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup(request: RequestModal) {
        
        if let firstChild = request.kidsModal.first {
            let images = PMImageName.avatars
            let index = Int( firstChild.avatar ) ?? .zero
            if images.indices.contains(index) {
                avatarImg.image = UIImage(named:  images[index])
            } else {
                avatarImg.image = UIImage(named:  PMImageName.placeholder)
            }
        } else {
            //avavatarImg.image = UIImage(named:  PMImageName.placeholder)
        }
        avatarImg.layoutIfNeeded()
        
        lblCount.text = String( request.kids.count )
        if request.kids.count > 1 {
            lblCount.isHidden = false
        } else {
            lblCount.isHidden = true
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        containerSubView.cornerRadius = containerSubView.bounds.width/2
        avatarImg.cornerRadius = avatarImg.bounds.width/2
    }
    
    //------------------------------------------------------
    
    //MARK: Init

    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    //------------------------------------------------------
}
