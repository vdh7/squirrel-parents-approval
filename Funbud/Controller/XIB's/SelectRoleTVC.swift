//
//  SelectRoleTVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 27/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
class SelectRoleTVC: UITableViewCell {
    @IBOutlet weak var despProfile: PoppinsRegularLabel!
    @IBOutlet weak var titleProfile: PoppinsBoldLabel!
    @IBOutlet weak var profileSelectImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
