//
//  AlertPopupVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 01/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class AlertPopupVC : BaseVC {
    
    @IBOutlet weak var alertPopUpTableVw: UITableView!
    
    //------------------------------------------------------
    
    
    var titleProfiles = ["Edit Playdate","Delete Playdate"]
    var img = ["parent","babysitter"]
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
extension AlertPopupVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleProfiles.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = alertPopUpTableVw.dequeueReusableCell(withIdentifier: "AlertPopUpTVC", for: indexPath) as? AlertPopUpTVC
       // let cell = selectRoleTableVw.dequeueReusableCell(withIdentifier: "SelectRoleTVC", for: indexPath) as! SelectRoleTVC
        cell?.selectionStyle = .none
        cell?.lblTitle.text = titleProfiles[indexPath.row]
        cell?.itemImg.image = UIImage(named: img[indexPath.row])
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
