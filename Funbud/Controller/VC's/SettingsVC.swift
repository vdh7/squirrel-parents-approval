//
//  SettingsVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 16/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import MapKit
import GooglePlaces

class SettingsVC : BaseVC, LocationManagerDelegate, UITextFieldDelegate, LocationSearchDelegate {
    
    @IBOutlet weak var lblGeneral: FredokaOneRegularLabel!
    @IBOutlet weak var lblSettings: FredokaOneRegularLabel!
    @IBOutlet weak var hideVw: NSLayoutConstraint!
    @IBOutlet weak var lineVw: UIView!
    @IBOutlet weak var txtSelectCity: ATCTextField!
    @IBOutlet weak var enablenotificationSwitch: UISwitch!
    @IBOutlet weak var enableLocationSwitch: UISwitch!
    @IBOutlet weak var lblDesp: PoppinsRegularLabel!
    @IBOutlet weak var btnSave: PoppinsBoldButton!
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        if isParentProfile {
            lblSettings.textColor = PMColor.themeOrange
            enableLocationSwitch.tintColor = PMColor.themeOrange
            enablenotificationSwitch.tintColor = PMColor.themeOrange
            btnSave.backgroundColor = PMColor.themeOrange
            enableLocationSwitch.isOn = parentModal?.isLocationEnable == "1" ? true : false
            enablenotificationSwitch.isOn = parentModal?.isNotificationEnabled == "1" ? true : false
            txtSelectCity.text = parentModal?.location
            self.lblDesp.text = parentModal?.location
        } else {
            lblSettings.textColor = PMColor.themeGreen
            enableLocationSwitch.tintColor = PMColor.themeGreen
            enablenotificationSwitch.tintColor = PMColor.themeGreen
            btnSave.backgroundColor = PMColor.themeGreen
            enableLocationSwitch.isOn = babysitterModal?.isLocationEnable == "1" ? true : false
            enablenotificationSwitch.isOn = babysitterModal?.isNotificationEnabled == "1" ? true : false
            txtSelectCity.text = babysitterModal?.location
            self.lblDesp.text = babysitterModal?.location
        }
        
        isLocationEnabled(flag: enableLocationSwitch.isOn)
    }
    
    func performSave(parentModal: ParentsModal) {
        
        LoadingManager.shared.showLoading()
        
        delay {
                   
            LocationManager.shared.lat = Double( parentModal.lattitude ?? "0") ?? .zero
            LocationManager.shared.lng = Double( parentModal.longitude ?? "0") ?? .zero
            FirebaseManager.shared.save(parent: parentModal, of: parentModal.userid ?? String())
            
            LoadingManager.shared.hideLoading()
            
            delay {
                
                //self.pop()
                DisplayAlertManager.shared.displayAlert(message: "You have save your changes successfully.") {
                    NavigationManager.shared.sideMenuController.showLeftView()
                }
            }
        }
    }
    
    func performSave(babysitter: BabysitterModal) {
        
        LoadingManager.shared.showLoading()
        
        delay {
                        
            LocationManager.shared.lat = Double( babysitter.lattitude ?? "0") ?? .zero
            LocationManager.shared.lng = Double( babysitter.longitude ?? "0") ?? .zero
            FirebaseManager.shared.save(babysitter: babysitter, of: babysitter.userid ?? String())
            LoadingManager.shared.hideLoading()
            
            delay {
                
                //self.pop()
                DisplayAlertManager.shared.displayAlert(message: "You have save your changes successfully.") {
                    NavigationManager.shared.sideMenuController.showLeftView()
                }
            }
        }
    }
    
    func isLocationEnabled(flag: Bool) {
        if flag {
            self.txtSelectCity.isHidden = true
            self.lblDesp.isHidden = false
            self.lineVw.isHidden = false
            self.hideVw.constant = 0
        } else {
            self.txtSelectCity.isHidden = false
            self.txtSelectCity.isEnabled = true
            self.lblDesp.isHidden = true
            self.lineVw.isHidden = true
            self.hideVw.constant = 120
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        //self.pop()
        NavigationManager.shared.sideMenuController.showLeftView()
    }
    
    @IBAction func locationSwitchChanged(_ sender: UISwitch) {
                        
        if (sender.isOn == true) {
            if LocationManager.shared.isRequiredToShowPermissionDialogue() {
                LocationManager.shared.requestForLocationPermissionWhenInUse()
                isLocationEnabled(flag: false)
                self.parentModal?.isLocationEnable = "0"
                self.babysitterModal?.isLocationEnable = "0"
            } else {
                LocationManager.shared.delegate = self
                LocationManager.shared.startMonitoring()
                self.parentModal?.isLocationEnable = "1"
                self.babysitterModal?.isLocationEnable = "1"
            }
        } else {
            isLocationEnabled(flag: false)
            self.parentModal?.isLocationEnable = "0"
            self.babysitterModal?.isLocationEnable = "0"
        }
    }
    
    @IBAction func notificationswitchChanged(_ sender: UISwitch) {
    }
    
    @IBAction func btnSaveTap(_ sender: UIButton) {
        
        if enableLocationSwitch.isOn {
            FirebaseManager.shared.subscribeEvents()
            AppDelegate.shared.registerRemoteNotificaton(UIApplication.shared)
        } else {
            FirebaseManager.shared.unsubscribeEvents()
            FirebaseManager.shared.unsubscribeParents()
            FirebaseManager.shared.unsubscribeBabysitter()
            UIApplication.shared.unregisterForRemoteNotifications()
        }
        
        if isParentProfile {
            if let parentModal = parentModal {
                parentModal.isLocationEnable = enableLocationSwitch.isOn ? "1" : "0"
                parentModal.isNotificationEnabled = enablenotificationSwitch.isOn ? "1" : "0"
                parentModal.location = txtSelectCity.text
                performSave(parentModal: parentModal)
            }
        } else {
            if let babysitterModal = babysitterModal {
                babysitterModal.isLocationEnable = enableLocationSwitch.isOn ? "1" : "0"
                babysitterModal.isNotificationEnabled = enablenotificationSwitch.isOn ? "1" : "0"
                babysitterModal.location = txtSelectCity.text
                performSave(babysitter: babysitterModal)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        /*let controller = NavigationManager.shared.locationSearchNC
        if let vc = controller.viewControllers.first as? LocationSearchVC {
            vc.delegate = self
        }
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true, completion: nil)*/
        
        PlacesSearchManager.shared.showAutoComplete(self.navigationController, didAutoCompleteWith: { (ac: GMSAutocompleteViewController, place: GMSPlace) in
            
            debugLog(object: place.description)
            let formattedAddress = place.formattedAddress
            self.txtSelectCity.text = formattedAddress
            if self.isParentProfile {
                self.parentModal?.lattitude = "\(place.coordinate.latitude)"
                self.parentModal?.longitude = "\(place.coordinate.longitude)"
                self.parentModal?.location = formattedAddress
            } else {
                self.babysitterModal?.lattitude = "\(place.coordinate.latitude)"
                self.babysitterModal?.longitude = "\(place.coordinate.longitude)"
                self.babysitterModal?.location = formattedAddress
            }
            ac.dismiss(animated: true, completion: nil)
            
        }, failureBlock:  { (ac: GMSAutocompleteViewController, error: Error?) in
            
            ac.dismiss(animated: true, completion: nil)
            
            if let error = error {
                DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
            }
        })
        
        return false
    }
    
    //------------------------------------------------------
    
    //MARK: LocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let lat = locations.first?.coordinate.latitude ?? .zero
        let lng = locations.first?.coordinate.longitude ?? .zero
        
        if self.parentModal?.isLocationEnable == "1" {
            
            isLocationEnabled(flag: true)
            
            if isParentProfile {
                self.parentModal?.lattitude = String( lat )
                self.parentModal?.longitude = String( lng )
            } else {
                self.babysitterModal?.lattitude = String( lat )
                self.babysitterModal?.longitude = String( lng )
            }
            
            if lat != .zero && lng != .zero {
                
                LocationManager.shared.stopMonitoring()
                
                PlacesSearchManager.shared.getAddressFrom(latitude: LocationManager.shared.lat, longitude: LocationManager.shared.lng) { address, error in
                    self.lblDesp.text = address
                    self.txtSelectCity.text = address
                    if self.isParentProfile {
                        self.parentModal?.location = address
                    } else {
                        self.babysitterModal?.location = address
                    }
                }
            }            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        enableLocationSwitch.isOn = false
        isLocationEnabled(flag: false)
        
        #if targetEnvironment(simulator)
        DisplayAlertManager.shared.displayAlertWithNoYes(target: self, animated: true, message: error.localizedDescription) {
        } handlerYes: {
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
        #else
        DisplayAlertManager.shared.displayAlertWithNoYes(target: self, animated: true, message: LocalizableConstants.Error.locationServiceDisable.localized()) {
        } handlerYes: {
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
        #endif
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        //Handle heading
    }
    
    //------------------------------------------------------
    
    //MARK: LocationSearchDelegate
    
    func locationSearch(controller: LocationSearchVC, didSelect location: MKPlacemark) {
        
        let fullAddress = String(format: "%@ %@", location.name ?? String(), location.formattedAddress ?? location.name ?? String())
        txtSelectCity.text = fullAddress
        if isParentProfile {
            self.parentModal?.lattitude = "\(location.coordinate.latitude)"
            self.parentModal?.longitude = "\(location.coordinate.longitude)"
            self.parentModal?.location = fullAddress
        } else {
            self.babysitterModal?.lattitude = "\(location.coordinate.latitude)"
            self.babysitterModal?.longitude = "\(location.coordinate.longitude)"
            self.babysitterModal?.location = fullAddress
        }
        
        LocationManager.shared.lat = location.coordinate.latitude
        LocationManager.shared.lng = location.coordinate.longitude
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtSelectCity.isHidden = true
        txtSelectCity.delegate = self
        self.hideVw.constant = 0
        
        setup()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //enableLocationSwitch.isOn = true
        //self.txtplace.isHidden = true
        //self.txtSelectCity.isEnabled = false
        
        /*if LocationManager.shared.isRequiredToShowPermissionDialogue() {
            //LocationManager.shared.requestForLocationPermissionWhenInUse()
            //enableLocationSwitch.setOn(false, animated: true)
        } else {
            LocationManager.shared.delegate = self
            LocationManager.shared.startMonitoring()
        }
        PreferenceManager.shared.isAtLocationEnable  = true
        */
        
        /*if CLLocationManager.locationServicesEnabled() {
             switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                 if enableLocationSwitch.isOn {
                     enableLocationSwitch.isOn = false
                    isLocationEnabled(flag: false)
                 }
                case .authorizedAlways, .authorizedWhenInUse:
                 if enableLocationSwitch.isOn {
                     LocationManager.shared.delegate = self
                     LocationManager.shared.startMonitoring()
                 }
                 break
             @unknown default:
                 break
             }
        } else {
            if enableLocationSwitch.isOn {
                enableLocationSwitch.isOn = false
                isLocationEnabled(flag:  false)
            }
        }*/
    }
    
    //------------------------------------------------------
}
