//
//  SelectRoleVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 27/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class SelectRoleVC : BaseVC {
   
    @IBOutlet weak var selectRoleTableVw: UITableView!
   
    var titleProfiles = ["Continue as a parent","Continue as a babysitter"]
    var desp = ["Find activities for your kids","Find babysitting jobs around you"]
    var img = ["parent","babysitter"]
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //-----------------------------
    
    // MARK:-- Actions
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        pop()
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectRoleTableVw.register(UINib(nibName: "SelectRoleTVC", bundle: nil), forCellReuseIdentifier: "SelectRoleTVC")
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        PreferenceManager.shared.isAtRoleSelection = true
    }
    
    //------------------------------------------------------
}

extension SelectRoleVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleProfiles.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = selectRoleTableVw.dequeueReusableCell(withIdentifier: "SelectRoleTVC", for: indexPath) as? SelectRoleTVC
       // let cell = selectRoleTableVw.dequeueReusableCell(withIdentifier: "SelectRoleTVC", for: indexPath) as! SelectRoleTVC
        cell?.selectionStyle = .none
        cell?.titleProfile.text = titleProfiles[indexPath.row]
        cell?.despProfile.text = desp[indexPath.row]
        cell?.profileSelectImg.image = UIImage(named: img[indexPath.row])
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let controller = NavigationManager.shared.parentProfileVC
            push(controller: controller)
        }else {
            let controller = NavigationManager.shared.bSCreateAccountVC
            push(controller: controller)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 280
    }

}
