//
//  LogInVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 30/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import CryptoKit
import AuthenticationServices

class LogInVC : BaseVC, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    //------------------------------------------------------
    
    //MARK: IBOutlets
    
    @IBOutlet weak var fbButton: PoppinsBoldButton!
    @IBOutlet weak var googleLoginButton: PoppinsBoldButton!
    @IBOutlet weak var phoneButton: PoppinsBoldButton!
    @IBOutlet weak var appleButton: PoppinsBoldButton!
    @IBOutlet weak var lblOR: PoppinsSemiBoldLabel!
    
    // Unhashed nonce.
    fileprivate var currentNonce: String?
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs

    func setup() {
        
        fbButton.isHidden = true
        googleLoginButton.isHidden = fbButton.isHidden
        appleButton.isHidden = googleLoginButton.isHidden
        lblOR.isHidden = appleButton.isHidden
    }
    
    private func randomNonceString(length: Int = 32) -> String {
      precondition(length > 0)
      let charset: Array<Character> =
          Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
      var result = ""
      var remainingLength = length

      while remainingLength > 0 {
        let randoms: [UInt8] = (0 ..< 16).map { _ in
          var random: UInt8 = 0
          let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
          if errorCode != errSecSuccess {
            fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
          }
          return random
        }
        randoms.forEach { random in
          if remainingLength == 0 {
            return
          }
          if random < charset.count {
            result.append(charset[Int(random)])
            remainingLength -= 1
          }
        }
      }
      return result
    }
    
    @available(iOS 13, *)
    func performAppleSignIn() {
      let nonce = randomNonceString()
      currentNonce = nonce
      let appleIDProvider = ASAuthorizationAppleIDProvider()
      let request = appleIDProvider.createRequest()
      request.requestedScopes = [.fullName, .email]
      request.nonce = sha256(nonce)

      let authorizationController = ASAuthorizationController(authorizationRequests: [request])
      authorizationController.delegate = self
      authorizationController.presentationContextProvider = self
      authorizationController.performRequests()
    }

    @available(iOS 13, *)
    private func sha256(_ input: String) -> String {
      let inputData = Data(input.utf8)
      let hashedData = SHA256.hash(data: inputData)
      let hashString = hashedData.compactMap {
        return String(format: "%02x", $0)
      }.joined()

      return hashString
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func fbButtonClicked(_ sender: PoppinsBoldButton) {
       
        let permissions = ["public_profile", "email"]
        LoginManager().logOut()
        LoginManager().logIn(permissions: permissions, from: self) { (result: LoginManagerLoginResult?, error: Error?) in
            
            if let result = result, let tokenString = result.token?.tokenString {
                
                delay {
                    
                    LoadingManager.shared.showLoading()
                    
                    let credential = FacebookAuthProvider.credential(withAccessToken: tokenString)
                    Auth.auth().signIn(with: credential) { (authResult, error) in
                        
                        LoadingManager.shared.hideLoading()
                        
                        delay {
                            
                            if let error = error {
                                DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
                                return
                            }
                            
                            NavigationManager.shared.setupSigninUserRootController()
                        }
                    }
                }
                
            } else if let error = error {
                
                DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
            }
        }
    }
    
    @IBAction func googleButtonClicked(_ sender: PoppinsBoldButton) {
        
        guard let clientID = FirebaseApp.app()?.options.clientID else { return }
        
        let config = GIDConfiguration(clientID: clientID)
        
        GIDSignIn.sharedInstance.signIn(with: config, presenting: self) { [unowned self] user, error in
            
            if let error = error {
                DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
                return
            }
            
            guard
                let authentication = user?.authentication,
                let idToken = authentication.idToken
            else {
                return
            }
            
            delay {
                
                LoadingManager.shared.showLoading()
                
                let credential = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: authentication.accessToken)
                
                FirebaseManager.shared.signIn(withCredential: credential) { error in
                    
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        
                        if let error = error {
                            DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
                            return
                        }
                        
                        NavigationManager.shared.setupSigninUserRootController()
                    }
                }
            }
        }
    }
    
    @IBAction func appleBtnClicked(_ sender: PoppinsBoldButton) {
        
        if #available(iOS 13, *) {
            performAppleSignIn()
        }
    }
    
    @IBAction func continueWithPhoneBtnClicked(_ sender: PoppinsBoldButton) {
        
        phoneButton.isEnabled = false
        
        try? FirebaseManager.shared.signOut()
        self.parentModal = nil
        self.babysitterModal = nil
        
        let controller = NavigationManager.shared.phoneNumerVC
        push(controller: controller)
        
        delay {
            self.phoneButton.isEnabled = true
        }
    }
    
    //------------------------------------------------------
    
    //MARK: ASAuthorizationControllerDelegate
    
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        
        return self.view.window!
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            
            guard let nonce = currentNonce else {
                return
            }
            
            guard let appleIDToken = appleIDCredential.identityToken else {
                return
            }
            
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            
            delay {
                
                LoadingManager.shared.showLoading()
             
                // Initialize a Firebase credential.
                let credential = OAuthProvider.credential(withProviderID: "apple.com", idToken: idTokenString, rawNonce: nonce)
                
                // Sign in with Firebase.
                FirebaseManager.shared.signIn(withCredential: credential) { error in
                    
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                    
                        if let error = error {
                            DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
                            return
                        }
                        
                        NavigationManager.shared.setupSigninUserRootController()
                    }
                }
            }
        }
    }

    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        
        DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
           
    //------------------------------------------------------
}
