//
//  SplashVC.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 9/15/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class SplashVC: BaseVC {
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func performRedirection() {
        
        if PreferenceManager.shared.isInstalled == false {
            try? FirebaseManager.shared.signOut()
            PreferenceManager.shared.isInstalled = true
        }
        
        //if user signed in / registered with firebase.
        
        if FirebaseManager.shared.isSignedIn() {
            NavigationManager.shared.setupSigninUserRootController()
        } else {
            NavigationManager.shared.setupSignInOption()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delay {
            self.performRedirection()
        }
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
