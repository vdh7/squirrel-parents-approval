//
//  PhoneNumerVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 31/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//
import UIKit
import Foundation
import FlagPhoneNumber

class PhoneNumerVC : BaseVC, UITextViewDelegate, FPNTextFieldDelegate {
    
    @IBOutlet weak var termsTxtVw: UITextView!
    @IBOutlet weak var txtPhoneNumber: SQPhoneTextField!
    @IBOutlet weak var continueButton: PoppinsBoldButton!
    
    var phoneNumber = String()
    let listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func validate() -> Bool {
        
        if RequestManager.shared.isReachable == false {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.noNetworkConnection) {
            }
            return false
        }
        
        if ValidationManager.shared.isEmpty(text: txtPhoneNumber.text) == true {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.enterPhoneNumber) {
            }
            return false
        }
        
        if ValidationManager.shared.isEmpty(text: phoneNumber) == true {
            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.enterValidPhoneNumber) {
            }
            return false
        }
        
        return true
    }
    
    func performSignIn(with phoneNumber: String) {
        
        FirebaseManager.shared.signIn(withPhone: phoneNumber) { (verificationID: String?, error: Error?) in
            
            LoadingManager.shared.hideLoading()
        
            delay {
                
                self.continueButton.isEnabled = true
                
                if let error = error {
                    DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
                    return
                }
                
                PreferenceManager.shared.verificationId = verificationID
                let controller = NavigationManager.shared.otpVC
                controller.phoneNumber = phoneNumber
                self.push(controller: controller)
            }
        }
    }
    
    func assignAttributedtextToTextView() {
        termsTxtVw.isEditable = false
        self.termsTxtVw.attributedText = setLoginTextView()
        self.termsTxtVw.delegate = self
        self.termsTxtVw.textAlignment = .center
        self.termsTxtVw.linkTextAttributes = [NSMutableAttributedString.Key.font:PMFont.sfpoppinsRegular(size: 14),NSMutableAttributedString.Key.foregroundColor:PMColor.green]
    }
    
    func setLoginTextView()->NSMutableAttributedString{
        let str = LocalizableConstants.Controller.termsAndConditions.localized()
        let attributedString = NSMutableAttributedString(string: str)
        var foundRange = attributedString.mutableString.range(of: LocalizableConstants.Controller.terms.localized())
        attributedString.addAttributes([NSMutableAttributedString.Key.font:PMFont.sfpoppinsRegular(size: 14),NSMutableAttributedString.Key.foregroundColor:PMColor.green], range: foundRange)
        attributedString.addAttribute(NSAttributedString.Key.link, value: Request.Method.terms, range: foundRange)
        foundRange = attributedString.mutableString.range(of: LocalizableConstants.Controller.privacy.localized())
        attributedString.addAttributes([NSMutableAttributedString.Key.font:PMFont.sfpoppinsRegular(size: 14),NSMutableAttributedString.Key.foregroundColor:PMColor.green], range: foundRange)
        attributedString.addAttribute(NSAttributedString.Key.link, value: Request.Method.privacy, range: foundRange)
        return attributedString
    }
    
    //------------------------------------------------------
    
    // MARK:- Actions
    
    @IBAction func continueBtnClicked(_ sender: PoppinsBoldButton) {
        
        if validate() == false {
            return
        }
        self.view.endEditing(true)
        
        continueButton.isEnabled = false
        
        LoadingManager.shared.showLoading()
        
        delay {
            self.performSignIn(with: self.phoneNumber)
        }
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.pop()
    }
    
    //------------------------------------------------------
    
    //MARK: UITextViewDelegate
    
    private func textView(textView: UITextView, shouldInteractWithURL URL: NSURL, inRange characterRange: NSRange) -> Bool {
        if (URL.absoluteString == Request.Method.terms) {
            let myAlert = UIAlertController(title: "Terms", message: nil, preferredStyle: UIAlertController.Style.alert)
            myAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(myAlert, animated: true, completion: nil)
        } else if (URL.absoluteString == Request.Method.privacy) {
            let myAlert = UIAlertController(title: "Conditions", message: nil, preferredStyle: UIAlertController.Style.alert)
            myAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(myAlert, animated: true, completion: nil)
        }
        return false
    }
    
    //------------------------------------------------------
    
    //MARK: FPNTextFieldDelegate
    
    /// The place to present/push the listController if you choosen displayMode = .list
    func fpnDisplayCountryList() {
                
        let navigationViewController = UINavigationController(rootViewController: listController)
        navigationViewController.modalPresentationStyle = .fullScreen
        present(navigationViewController, animated: true, completion: nil)
    }
    
    /// Lets you know when a country is selected
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code) // Output "France", "+33", "FR"
    }
    
    /// Lets you know when the phone number is valid or not. Once a phone number is valid, you can get it in severals formats (E164, International, National, RFC3966)
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            phoneNumber = textField.getFormattedPhoneNumber(format: .E164) ?? String()
        } else {
            phoneNumber = String()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtPhoneNumber.delegate = self
        txtPhoneNumber.displayMode = .list
        txtPhoneNumber.placeholder = LocalizableConstants.Controller.placeholderPhoneNumber.localized()
        listController.setup(repository: txtPhoneNumber.countryRepository)
        listController.didSelect = { [weak self] country in
            self?.txtPhoneNumber.setFlag(countryCode: country.code)
            self?.txtPhoneNumber.placeholder = LocalizableConstants.Controller.placeholderPhoneNumber.localized()
        }
        
        self.assignAttributedtextToTextView()
        self.termsTxtVw.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
