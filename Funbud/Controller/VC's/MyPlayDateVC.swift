//
//  MyPlayDateVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 17/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class MyPlayDateVC : BaseVC {
    
    //------------------------------------------------------
    
    @IBOutlet weak var btnCreatePlayDate: PoppinsBoldButton!
    @IBOutlet weak var lblDesp: PoppinsRegularLabel!
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    @IBAction func createPlayDateBtnClicked(_ sender: PoppinsBoldButton) {
        let controller = NavigationManager.shared.createPlaydatesVC
        controller.commingFrom = "Menu"
        push(controller: controller)
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        //self.pop()
        NavigationManager.shared.sideMenuController.showLeftView()
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
