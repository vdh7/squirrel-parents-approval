//
//  LandingVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 06/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import FittedSheets
import CoreLocation
import FirebaseMessaging

class LandingVC : BaseVC, UITableViewDataSource, UITableViewDelegate, LocationManagerDelegate, UIAlertViewDelegate {
       
    @IBOutlet weak var landingTableVw: UITableView!
    @IBOutlet weak var lblNoRecordsFound: PoppinsSemiBoldLabel!
    
    var isLocationFilterApplied: Bool = false
    var playDates: [PlayDatesModal] = []
    
    var isRequesting: Bool = true {
        didSet {
            if isRequesting == false {
                updateUI()
            }
        }
    }
    
    private(set) lazy var refreshControl: UIRefreshControl = {
        let control = UIRefreshControl()
        control.addTarget(self, action: #selector(refreshPlayDates), for: .valueChanged)
        control.tintColor = .black
        return control
    }()
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(PMNotification.refreshPlayDates), object: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func bottomSheetView() {
        let controller = KidsProfileVC.instantiate()
        var sizes = [SheetSize]()
        sizes.append(.fixed(UIScreen.main.bounds.size.height * 0.5))
        sizes.append(.marginFromTop(60))
        let sheetController = SheetViewController(controller: controller, sizes: sizes)
        self.present(sheetController, animated: true, completion: nil)
    }
   
    func updateUI() {
        
        lblNoRecordsFound.isHidden = playDates.count > .zero
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1) {
            self.landingTableVw.reloadData()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func changeSwitchBtnClicked(_ sender: PoppinsRegularButton) {
        
        //let controller = NavigationManager.shared.landingBabySitterVC
        //push(controller: controller)
        
        NavigationManager.shared.parentProfileSwitchToBabysitter()
    }
    
    @IBAction func menuBtnClicked(_ sender: UIButton) {
        NavigationManager.shared.sideMenuController.showLeftView()
    }
    
    //------------------------------------------------------
    
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return playDates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = landingTableVw.dequeueReusableCell(withIdentifier: String(describing: LandingKidTVC.self), for: indexPath) as? LandingKidTVC {
            let playDate = playDates[indexPath.row]
            cell.setup(playdateModal: playDate)
            cell.threeDotsImg.isHidden = true
            cell.btnThreeDots.isUserInteractionEnabled = false
            return cell
        }
     
        return UITableViewCell()
    }
       
    //------------------------------------------------------
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                
        /*LoadingManager.shared.showLoading()
        
        let playDate = self.playDates[indexPath.row]
        FirebaseManager.shared.findGotoConversation(forConversationId: playDate.id, userId: PreferenceManager.shared.userId ?? String()) { latestConversationObj in
        
            LoadingManager.shared.hideLoading()
            
            delay {
                
                if latestConversationObj != nil {
                    
                    let goToConversation = LocalizableConstants.Controller.goToConversation.localized()
                    DisplayAlertManager.shared.displayActionSheet(target: self, titles: [goToConversation], animated: true, message: String()) { selectedTitle in
                        
                        if selectedTitle == goToConversation {
                            
                            let controller = NavigationManager.shared.chatVC
                            controller.isParentProfile = self.isParentProfile
                            controller.conversationModal = latestConversationObj
                            self.push(controller: controller)
                        }
                    }
                    
                } else {
                    
                    if playDate.parentID == PreferenceManager.shared.userId {
                        
                        FirebaseManager.shared.create(conversationFromDic: [:], conversationId: playDate.id, title: playDate.title, ownerId: playDate.parentID, kidsList: playDate.childList) { conversationObj in
                            
                            let controller = NavigationManager.shared.chatVC
                            controller.isParentProfile = self.isParentProfile
                            controller.conversationModal = conversationObj
                            self.push(controller: controller)
                        }
                        
                    } else {
                        DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Error.conversation_not_available, handlerOK: nil)
                    }
                }
            }
        }*/
        
        let playDate = self.playDates[indexPath.row]
        FirebaseManager.shared.findDeviceToken(of: playDate.parentID, roleType: RoleType.parent) { baseModal in
            //conversation.username = parentModal.name
            let controller = NavigationManager.shared.kidsProfileVC
            controller.profileModal = baseModal as? ParentsModal
            var sizes = [SheetSize]()
            sizes.append(.fixed(UIScreen.main.bounds.size.height * 0.5))
            sizes.append(.marginFromTop(60))
            let sheetController = SheetViewController(controller: controller, sizes: sizes)
            self.present(sheetController, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     
        if let parentModal = self.parentModal {
            let playDate = playDates[indexPath.row]
            let playdateParentId = playDate.parentID ?? String()
            if playdateParentId == parentModal.userid {
                return false
            }
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if let parentModal = self.parentModal {
            let playDate = playDates[indexPath.row]
            let playdateParentId = playDate.parentID ?? String()
                    
            let actionLike = UIContextualAction(style: .normal, title: String(), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                
                LoadingManager.shared.showLoading()
                
                delay {
                    
                    FirebaseManager.shared.find(requestModalFrom: playDate.id, playdateParentId: playdateParentId, userId: parentModal.userid) { (result: RequestModal?) in
                                          
                        LoadingManager.shared.hideLoading()
                        
                        if result != nil && (result?.status == "0" || result?.status.isEmpty == true) {
                        
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestAlreadySent) {
                                }
                            }
                            
                        } else if result?.status == "1" {
                            
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestAlreadyApproved) {
                                }
                            }
                            
                        } else if result?.status == "2" {
                            
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestRejected) {
                                }
                            }
                        } else if result?.status == "3" {
                            
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestOwnCancel) {
                                }
                            }
                            
                        } else {
                            
                            delay {
                                
                                let controller = NavigationManager.shared.sendRequestVC
                                controller.playdateId = playDate.id
                                controller.playdateName = playDate.title
                                controller.playdateParentId = playdateParentId
                                controller.userId = parentModal.userid
                                controller.playdateModal = playDate
                                controller.modalPresentationStyle = .fullScreen
                                self.present(controller, animated: false, completion: nil)
                            }
                        }
                    }
                }
                success(false)
            })
            actionLike.image = UIImage(named: "icon_playdate_like")
            actionLike.backgroundColor = tableView.backgroundColor
            return UISwipeActionsConfiguration(actions: [actionLike])
            
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if let parentModal = self.parentModal {
            
            let playDate = self.playDates[indexPath.row]
            let playdateParentId = playDate.parentID ?? String()
                    
            let actionDislike = UIContextualAction(style: .normal, title: String(), handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                
                LoadingManager.shared.showLoading()
                
                delay {
                    
                    FirebaseManager.shared.find(requestModalFrom: playDate.id, playdateParentId: playdateParentId, userId: parentModal.userid) { (result: RequestModal?) in
                                          
                        LoadingManager.shared.hideLoading()
                                            
                        if result != nil && result?.status == "0" {
                        
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestAlreadySent) {
                                }
                            }
                            
                        } else if result?.status == "1" {
                            
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestAlreadyApproved) {
                                }
                            }
                            
                        } else if result?.status == "2" {
                            
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestRejected) {
                                }
                            }
                            
                        } else if result?.status == "3" {
                            
                            delay {
                                
                                DisplayAlertManager.shared.displayAlert(message: LocalizableConstants.Controller.requestOwnCancel) {
                                }
                            }
                            
                        } else {
                            
                            PreferenceManager.shared.blockedPlaydates.append(playDate.id)
                            NotificationCenter.default.post(name: NSNotification.Name(PMNotification.refreshPlayDates), object: nil)
                            
                            delay {
                                
                                FirebaseManager.shared.sendRequest(playdateId: playDate.id, playdateParentId: playdateParentId, playdateName: playDate.title, userId: parentModal.userid, kids: [], status: "3")
                            }
                        }
                    }
                }
                success(false)
            })
            
            actionDislike.image = UIImage(named: "icon_playdate_dislike")
            actionDislike.backgroundColor = tableView.backgroundColor
            return UISwipeActionsConfiguration(actions: [actionDislike])
        }
        return nil
    }
    
    //------------------------------------------------------
    
    //MARK: LocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let lat = locations.first?.coordinate.latitude ?? .zero
        let lng = locations.first?.coordinate.longitude ?? .zero
        
        self.parentModal?.lattitude = String( lat )
        self.parentModal?.longitude = String( lng )
        self.babysitterModal?.lattitude = String( lat )
        self.babysitterModal?.longitude = String( lng )
           
        if isLocationFilterApplied == false {
            isLocationFilterApplied = true            
            isRequesting = true
            FirebaseManager.shared.findPlaydates { (result: [PlayDatesModal]) in
                self.playDates = result
                self.isRequesting = false
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        DisplayAlertManager.shared.displayAlertWithNoYes(target: self, animated: true, message: LocalizableConstants.Error.locationServiceDisable.localized()) {
        } handlerYes: {
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        //Handle heading
    }
    
    //------------------------------------------------------
    
    //MARK: Selector
    
    @objc func refreshPlayDates(_ sender: Any) {
        
        isRequesting = true
        FirebaseManager.shared.findPlaydates { (result: [PlayDatesModal]) in
            self.playDates = result
            self.isRequesting = false
            self.refreshControl.endRefreshing()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        landingTableVw.register(UINib(nibName: String(describing: LandingKidTVC.self), bundle: nil), forCellReuseIdentifier: String(describing: LandingKidTVC.self))
        
        landingTableVw.refreshControl = refreshControl
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshPlayDates), name: NSNotification.Name(PMNotification.refreshPlayDates), object: nil)
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        
        if LocationManager.shared.isRequiredToShowPermissionDialogue() {
            LocationManager.shared.requestForLocationPermissionWhenInUse()
        } else {
            LocationManager.shared.delegate = self
            LocationManager.shared.startMonitoring()
        }
        
        self.isRequesting = true
        FirebaseManager.shared.findPlaydates { (result: [PlayDatesModal]) in
            self.playDates = result
            self.isRequesting = false
        }
        
        if self.isNotificationEnabled {
            AppDelegate.shared.registerRemoteNotificaton(UIApplication.shared)
            FirebaseManager.shared.subscribeEvents()
            FirebaseManager.shared.subscribeParents()
            FirebaseManager.shared.subscribeBabysitter()
        }
        
        //Here in the landing page below method used in background to update device token if token expires
        FirebaseManager.shared.getAllMy(conversations: PreferenceManager.shared.userId ?? String()) { (result: [ConversationModal]) in
            //nothing to use response of this method
        }            
    }
    
    //------------------------------------------------------
}
