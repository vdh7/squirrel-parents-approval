//
//  EditBabysitterProfileVC.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 9/17/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage
import Toucan

class EditBabysitterProfileVC : BaseVC, ImagePickerDelegate, EditNameDelegate {
    
    @IBOutlet weak var imgPlaceholder: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnEdit: PoppinsRegularButton!
    @IBOutlet weak var btnName: PoppinsRegularButton!
    //@IBOutlet weak var txtDOB: FGBirthDateTextField!
    //@IBOutlet weak var txtSpecifyGender: SLGenderTextField!
    @IBOutlet weak var btnDob: PoppinsRegularButton!
    @IBOutlet weak var btnGender: PoppinsRegularButton!
    @IBOutlet weak var btnID: PoppinsRegularButton!
    @IBOutlet weak var btnSave: PoppinsBoldButton!
        
    var imagePickerVC: SLImagePickerVC?
    var isImagePickerForProfile: Bool = false
    var dateDOB: Date?
    var selectedImage: UIImage? {
        didSet {
            if selectedImage != nil {
                imgProfile.image = selectedImage
            }
        }
    }
    
    var selectedImageId: UIImage? {
      
        didSet {
            
            if selectedImageId != nil {
                
                let fileName = String(format: "%@.jpeg", "ID")
                btnID.setTitle(fileName, for: .normal)
                
            } else {
                
                btnID.setTitle(nil, for: .normal)
            }
        }
    }
        
    override var babysitterModal: BabysitterModal? {
        
        didSet {
            
            guard babysitterModal != nil else {
                return
            }
                        
            if let imageURL = babysitterModal?.imgsource {
                imgProfile.sd_setIndicatorStyle(UIActivityIndicatorView.Style.medium)
                imgProfile.sd_addActivityIndicator()
                imgProfile.sd_showActivityIndicatorView()
                imgProfile.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                    self.imgProfile.image = img
                    self.imgProfile.sd_removeActivityIndicator()
                }
            }
            
            btnName.setTitle(babysitterModal?.name, for: .normal)
            
            if let dateToSet = babysitterModal?.getDOB() {
                //txtDOB.dpDate.date = dateToSet
                dateDOB = dateToSet
            }
            //txtDOB.text = babysitterModal?.displayDOB()
            btnDob.setTitle(babysitterModal?.displayDOB(), for: .normal)
            
            if babysitterModal?.gender == "2" {
                //txtSpecifyGender.text = babysitterModal?.genderText
                btnGender.setTitle(babysitterModal?.genderText, for: .normal)
            } else {
                //txtSpecifyGender.text = babysitterModal?.gender == "0" ? GenderType.male.rawValue : GenderType.female.rawValue
                let gender = babysitterModal?.gender == "0" ? GenderType.male.rawValue : GenderType.female.rawValue
                btnGender.setTitle(gender, for: .normal)
            }
            
            if babysitterModal?.imageId != nil {
                
                let fileName = String(format: "%@.jpeg", "ID")
                btnID.setTitle(fileName, for: .normal)
                
            } else {
                
                btnID.setTitle(nil, for: .normal)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        imgPlaceholder.layoutIfNeeded()
        
        imgPlaceholder.contentMode = .scaleToFill
        imgPlaceholder.clipsToBounds = true
        imgProfile.layoutIfNeeded()
        imgProfile.contentMode = .scaleToFill
        imgProfile.clipsToBounds = true
        
        imagePickerVC = SLImagePickerVC(presentationController: self, delegate: self)
        
        btnName.titleLabel?.numberOfLines = 2
        btnName.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    func performSave(babysitter babysitterModal: BabysitterModal) {
        
        LoadingManager.shared.showLoading()
        
        delay {
            
            let firebaseId: String = PreferenceManager.shared.userId ?? String()
            let fileName = String(format: "%@.jpeg", firebaseId)
            
            if self.selectedImage != nil {
                
                babysitterModal.userid = firebaseId
                babysitterModal.role = RoleType.babysitter.rawValue
                
                FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
                
                FirebaseManager.shared.save(image: firebaseId, fileName: fileName, imageData: self.selectedImage?.pngData() ?? Data(), completion: { (profileImageURL: URL?, error: Error?) in
                    
                    if let error = error {
                        LoadingManager.shared.hideLoading()
                        delay {
                            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message:
                                                                        error.localizedDescription) {
                            }
                        }
                    } else if self.selectedImageId != nil {
                        
                        FirebaseManager.shared.save(imageId: firebaseId, fileName: fileName, imageData: self.selectedImageId?.pngData() ?? Data(), completion: { (imageIdURL: URL?, error: Error?) in
                            
                            if let error = error {
                                LoadingManager.shared.hideLoading()
                                delay {
                                    DisplayAlertManager.shared.displayAlert(target: self, animated: true, message:
                                                                                error.localizedDescription) {
                                    }
                                }
                            } else {
                                 
                                babysitterModal.imgsource = profileImageURL?.absoluteString ?? String()
                                babysitterModal.imageId = imageIdURL?.absoluteString ?? String()
                               
                                FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
                                LoadingManager.shared.hideLoading()
                                
                                /*delay {
                                    let controller = NavigationManager.shared.bSAvailabilityVC
                                    controller.babysitterModal = self.babysitterModal
                                    self.push(controller: controller)
                                }*/
                                
                                delay {
                                    self.pop()
                                }
                            }
                        })
                        
                    } else if let stringImageURL = profileImageURL?.absoluteString {
                        
                        babysitterModal.imgsource = stringImageURL
                        FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
                        LoadingManager.shared.hideLoading()
                        
                        /*delay {
                            
                            let controller = NavigationManager.shared.bSAvailabilityVC
                            controller.babysitterModal = self.babysitterModal
                            self.push(controller: controller)
                        }*/
                        
                        delay {
                            self.pop()
                        }
                    }
                })
                
            } else if self.selectedImageId != nil {
                
                FirebaseManager.shared.save(imageId: firebaseId, fileName: fileName, imageData: self.selectedImageId?.pngData() ?? Data(), completion: { (imageIdURL: URL?, error: Error?) in
                    
                    if let error = error {
                        LoadingManager.shared.hideLoading()
                        delay {
                            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message:
                                                                        error.localizedDescription) {
                            }
                        }
                    } else {
                                                
                        babysitterModal.imageId = imageIdURL?.absoluteString ?? String()
                       
                        FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
                        LoadingManager.shared.hideLoading()
                        
                        delay {
                            
                            self.pop()
                        }
                    }
                })
                
            } else {
                
                FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
                LoadingManager.shared.hideLoading()
                
                delay {
                    
                    babysitterModal.userid = firebaseId
                    babysitterModal.role = RoleType.babysitter.rawValue
                    
                    FirebaseManager.shared.save(babysitter: babysitterModal, of: firebaseId)
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        self.pop()
                    }
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func btnBackTap(_ sender: Any) {
        //self.pop()
        NavigationManager.shared.sideMenuController.showLeftView()
    }
    
    @IBAction func btnEditTap(_ sender: PoppinsRegularButton) {
        
        isImagePickerForProfile = true
        self.imagePickerVC?.present(from: sender)
    }
    
    @IBAction func btnNameTap(_ sender: Any) {
        
        let controller = NavigationManager.shared.editNameVC
        controller.comingfrom = .name
        controller.role = self.babysitterModal?.role
        controller.delegate = self
        push(controller: controller)
    }
    
    @IBAction func btnDobTap(_ sender: Any) {
        
        let controller = NavigationManager.shared.editNameVC
        controller.delegate = self
        controller.comingfrom = .dob
        controller.role = self.babysitterModal?.role
        controller.bbModal = self.babysitterModal
        push(controller: controller)
    }
    
    @IBAction func btnGenderTap(_ sender: Any) {
        
        let controller = NavigationManager.shared.editNameVC
        controller.delegate = self
        controller.comingfrom = .gender
        controller.role = self.babysitterModal?.role
        controller.bbModal = self.babysitterModal
        push(controller: controller)
    }
    
    @IBAction func btnIDTap(_ sender: UIButton) {
        
        isImagePickerForProfile = false
        self.imagePickerVC?.present(from: sender)
    }
    
    @IBAction func btnSaveTap(_ sender: Any) {
        
        self.view.endEditing(true)            
        
        let babysitterModals = babysitterModal ?? BabysitterModal(fromDictionary: [:])
        babysitterModals.name = btnName.title(for: .normal)
        
        babysitterModals.gender = babysitterModals.getGenderValueToSet(from: btnGender.title(for: .normal))
        babysitterModals.genderText = btnGender.title(for: .normal)
        
        //let dob = DateTimeManager.shared.stringFrom(date: txtDOB.dpDate.date, inFormate: DateFormate.UTC)
        let dob = DateTimeManager.shared.stringFrom(date: dateDOB ?? Date(), inFormate: DateFormate.UTC)
        babysitterModals.dob = dob
        //babysitterModals.genderText = txtSpecifyGender.text
                
        performSave(babysitter: babysitterModals)
    }
    
    //------------------------------------------------------
    
    //MARK: SlImagePickerDelegate
    
    func didSelect(image: UIImage?) {
        
        if let imageData = image?.jpegData(compressionQuality: 0), let compressImage = UIImage(data: imageData) {
            if isImagePickerForProfile {
                self.selectedImage = Toucan.init(image: compressImage).resizeByCropping(PMSettings.profileImageSize).maskWithRoundedRect(cornerRadius: PMSettings.profileImageSize.width/2, borderWidth: PMSettings.profileBorderWidth, borderColor: PMColor.clear).image
            } else {
                self.selectedImageId = Toucan.init(image: compressImage).image
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: EditNameDelegate
    
    func editName(controller: EditNameVC, didChangeName name: String) {
        
        if controller.comingfrom == .name {
            self.babysitterModal?.name = name
            btnName.setTitle(name, for: .normal)
        }
        controller.pop()
    }
    
    func editName(controller: EditNameVC, didChangeDate date: Date) {
        
        let dob = DateTimeManager.shared.stringFrom(date: date, inFormate: DateFormate.UTC)
        babysitterModal?.dob = dob
        dateDOB = babysitterModal?.getDOB()       
        btnDob.setTitle(babysitterModal?.displayDOB(), for: .normal)
        controller.pop()
    }
    
    func editName(controller: EditNameVC, didChangeGender gender: String) {
        
        let value = babysitterModal?.getGenderValueToSet(from: gender)
        babysitterModal?.gender = value
        btnGender.setTitle(gender, for: .normal)
        controller.pop()
    }
    
    func editName(controller: EditNameVC, didChangeRelationship name: String) {
                
        controller.pop()
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                
        if self.babysitterModal == nil {
            btnSave.isEnabled = false
            if self.babysitterModal == nil {
                FirebaseManager.shared.find(babysitterFrom: PreferenceManager.shared.userId ?? String()) { dict in
                    self.babysitterModal = BabysitterModal(fromDictionary: dict)
                    self.btnSave.isEnabled = true
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        imgPlaceholder.cornerRadius = imgPlaceholder.bounds.height/2
        imgProfile.cornerRadius = imgPlaceholder.bounds.height/2
    }
    
    //------------------------------------------------------
}

