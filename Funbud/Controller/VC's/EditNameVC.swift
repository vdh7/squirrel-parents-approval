//
//  EditNameVC.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 9/16/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

protocol EditNameDelegate {
    
    func editName(controller: EditNameVC, didChangeName name: String)
    func editName(controller: EditNameVC, didChangeDate date: Date)
    func editName(controller: EditNameVC, didChangeGender gender: String)
    func editName(controller: EditNameVC, didChangeRelationship name: String)
}

class EditNameVC : BaseVC, UITextFieldDelegate {
    
    @IBOutlet weak var lblTitle: PoppinsRegularLabel!
    @IBOutlet weak var txtName: ATCTextField!
    @IBOutlet weak var txtDob: FGBirthDateTextField!
    @IBOutlet weak var txtGender: SLGenderTextField!
    @IBOutlet weak var txtRelationShip: SLRelationshipTextField!
    @IBOutlet weak var btnSave: PoppinsBoldButton!
        
    var role: String?
    var bbModal: BabysitterModal?
    
    var delegate: EditNameDelegate?
    var comingfrom: EditProfileType = .none
   
    private var name: String = String()
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        txtName?.delegate = self
        
        if role == RoleType.parent.rawValue {
            txtName?.text = parentModal?.name
            txtRelationShip.text = parentModal?.getGender()            
            txtRelationShip.pvGender.selectRow(Int(parentModal?.gender ?? "0") ?? .zero, inComponent: .zero, animated: false)
        } else if role == RoleType.babysitter.rawValue {
            txtName?.text = babysitterModal?.name
        }
        
        txtDob?.delegate = self
        txtDob.text = bbModal?.displayDOB()
        txtDob.dpDate.date = bbModal?.getDOB() ?? Date()
        
        if let displayGender = bbModal?.displayGender() {
            txtGender.text = displayGender
            txtGender.selectedOption = GenderType(rawValue: displayGender)?.rawValue
        }
        
        if role == RoleType.parent.rawValue {
            btnSave.backgroundColor = PMColor.themeOrange
        } else if role == RoleType.babysitter.rawValue {
            btnSave.backgroundColor = PMColor.themeGreen
        }
    }
    
    func validate() -> Bool {
        
        if role == RoleType.parent.rawValue {
            if ValidationManager.shared.isEmpty(text: txtName.text) == true {
                DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.enterParentName) {
                }
                return false
            }
        } else if role == RoleType.babysitter.rawValue {
            if ValidationManager.shared.isEmpty(text: txtName.text) == true {
                DisplayAlertManager.shared.displayAlert(target: self, animated: true, message: LocalizableConstants.ValidationMessage.BS_name) {
                }
                return false
            }
        }
    
        return true
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func btnBackTap(_ sender: Any) {
        self.pop()
    }
    
    @IBAction func btnSaveTap(_ sender: Any) {
        
        if validate() == false {
            return
        }
        
        self.view.endEditing(true)
        
        if comingfrom == .name {
            delegate?.editName(controller: self, didChangeName: name)
        } else if comingfrom == .dob {
            delegate?.editName(controller: self, didChangeDate: txtDob.dpDate.date)
        } else if comingfrom == .gender {
            delegate?.editName(controller: self, didChangeGender: txtGender.text ?? String())
        } else if comingfrom == .relationship {
            delegate?.editName(controller: self, didChangeRelationship: String(txtRelationShip.pvGender.selectedRow(inComponent: .zero)))
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtName {
            let fullString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
            if comingfrom == .name {
                name = fullString ?? String()
            }
        }
        return true
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtName.isHidden = true
        self.txtDob.isHidden = true
        self.txtGender.isHidden = true
        self.txtRelationShip.isHidden = true
        
        if comingfrom == .name {
            self.txtName.isHidden = false
            self.lblTitle.text = "Edit name"
        } else if comingfrom == .dob {
            self.txtDob.isHidden = false
            self.lblTitle.text = "Edit Date of Birth"
        } else if comingfrom == .gender {
            self.txtGender.isHidden = false
            self.lblTitle.text = "Edit Gender"
        } else if comingfrom == .relationship {
            self.txtRelationShip.isHidden = false
            self.lblTitle.text = "Edit RelationShip"
        }
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}

