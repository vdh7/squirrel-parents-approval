//
//  RequestGroupChatVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 21/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class RequestGroupChatVC : BaseVC,UITextViewDelegate {
    
    //------------------------------------------------------
    
    @IBOutlet weak var lblDep: PoppinsBoldLabel!
    @IBOutlet weak var lblKid2: FredokaOneRegularLabel!
    @IBOutlet weak var lblKid1: FredokaOneRegularLabel!
    @IBOutlet weak var avatarImg2: UIImageView!
    @IBOutlet weak var avatarImg1: UIImageView!
    @IBOutlet weak var txtSendMsg: UITextView!
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    @IBAction func btnSendTap(_ sender: Any) {
    }
    @IBAction func btnCrossTap(_ sender: Any) {
        self.pop()
    }
    @IBAction func btnDismissTap(_ sender: Any) {
        self.pop()
    }
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        txtSendMsg.delegate = self
        txtSendMsg.textContainerInset = UIEdgeInsets(top: 20, left: 25, bottom: 11, right: 25)
    }
    
    //------------------------------------------------------
}
