//
//  OtpVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 26/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import OTPFieldView
import Firebase
enum DisplayType: Int {
    case circular
    case roundedCorner
    case square
    case diamond
    case underlinedBottom
}

class OtpVC : BaseVC, UITextFieldDelegate, OTPFieldViewDelegate {
    
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var lblDispalynumber: UILabel!
    @IBOutlet weak var otpfileds: OTPFieldView!
    @IBOutlet weak var confirmButton: PoppinsBoldButton!
    @IBOutlet weak var lblResendTimer: PoppinsRegularLabel!
    
    var phoneNumber: String?
    var timer : Timer?
    var timeDuration : Int = 15
    var otpString: String?
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }

    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        confirmButton.isEnabled = false
        resendButton.isEnabled = false
    }
    
    func setupOtpView() {
        
        self.otpfileds.delegate = self
        self.otpfileds.initializeUI()
        self.otpfileds.resignFirstResponder()
    }
    
    func sheduleTimer() {
        self.timeDuration = 15
        self.timer?.invalidate()
        self.timer = nil
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updareTimer(timer:)), userInfo: nil, repeats: true)
    }
    
    func performVerification() {
        
        guard let otpString = self.otpString else {
            return
        }
        
        let verificationId = PreferenceManager.shared.verificationId ?? String()
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationId,
            verificationCode: otpString)
        
        self.view.endEditing(true)
        
        LoadingManager.shared.showLoading()
        
        Auth.auth().signIn(with: credential) { (authData, error) in
            
            PreferenceManager.shared.phone = self.phoneNumber
            PreferenceManager.shared.userId = authData?.user.uid
            LoadingManager.shared.hideLoading()
            
            if let error = error {
                delay {
                    DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
                }
                return
            }
            NavigationManager.shared.setupSigninUserRootController()                        
        }
    }
    
    //------------------------------------------------------
    
    //MARK: OTPFieldViewDelegate
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        if hasEntered == true {
            confirmButton.isEnabled = true
        } else {
            confirmButton.isEnabled = false
            otpString = nil
        }
        return false
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        self.otpString = otpString
    }
    
    //-----------------------------
 
    // MARK:- Action
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.pop()
    }
    
    @IBAction func ResendBtnClicked(_ sender: UIButton) {
        
        if let phoneNumber = phoneNumber, resendButton.isEnabled == true {
            
            LoadingManager.shared.showLoading()
            
            FirebaseManager.shared.signIn(withPhone: phoneNumber) { (verificationID: String?, error: Error?) in
                
                LoadingManager.shared.hideLoading()
            
                delay {
                    if let error = error {
                        DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
                        return
                    }
                    
                    self.sheduleTimer()
                }
            }
        }
    }
    
    @IBAction func confirmBtnClicked(_ sender: PoppinsBoldButton) {
        
        performVerification()
    }
    
    //------------------------------------------------------
    
    //MARK: Selector
    
    @objc func updareTimer(timer : Timer) {
        
        if timeDuration == 0 {
            resendButton.isEnabled = true;
            lblResendTimer.isHidden = true
            timer.invalidate()
        } else {
            lblResendTimer.isHidden = false
        }
        timeDuration = timeDuration - 1
        lblResendTimer.text = String(format: LocalizableConstants.Controller.notEarlierThanSeconds.localized(), timeDuration)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupOtpView()
        setup()
        lblDispalynumber.attributedText = setLoginTextView()
        sheduleTimer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
    
    func setLoginTextView()->NSMutableAttributedString{
        let str = "We sent a code via SMS on your number \(phoneNumber ?? String()). Please enter it below"
        let attributedString = NSMutableAttributedString(string: str)
        let foundRange = attributedString.mutableString.range(of: phoneNumber ?? String())
        attributedString.addAttributes([NSMutableAttributedString.Key.font:PMFont.sfpoppinsBold(size: 18),NSMutableAttributedString.Key.foregroundColor:PMColor.darkblack], range: foundRange)
        //attributedString.addAttribute(NSAttributedString.Key.link, value: Request.Method.terms, range: foundRange)
        
        return attributedString
    }
}
