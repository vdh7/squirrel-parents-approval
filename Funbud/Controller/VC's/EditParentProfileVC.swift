//
//  EditParentProfileVC.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 9/16/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage
import Toucan

class EditParentProfileVC : BaseVC, ImagePickerDelegate, EditNameDelegate {
    
    @IBOutlet weak var btnDOB: PoppinsRegularButton!
    @IBOutlet weak var imgPlaceholder: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnEdit: PoppinsRegularButton!
    @IBOutlet weak var btnName: PoppinsRegularButton!
    @IBOutlet weak var btnRelationship: PoppinsRegularButton!
    @IBOutlet weak var btnSave: PoppinsBoldButton!
    
    @IBOutlet weak var stackViewName: UIStackView!
    @IBOutlet weak var viewGraylineName: UIView!
    @IBOutlet weak var stackViewDOB: UIStackView!
    @IBOutlet weak var viewGraylineDOB: UIView!
    @IBOutlet weak var stackViewRelationship: UIStackView!
    @IBOutlet weak var viewGraylineRelationship: UIView!
    //@IBOutlet weak var stackViewGender: UIStackView!
    //@IBOutlet weak var stackViewID: UIStackView!
    @IBOutlet weak var btnID: PoppinsRegularButton!
    
    var imagePickerVC: SLImagePickerVC?
    var isImagePickerForProfile: Bool = false
    
    var selectedImage: UIImage? {
        didSet {
            if selectedImage != nil {
                imgProfile.image = selectedImage
            }
        }
    }
    
    var selectedImageId: UIImage? {
      
        didSet {
            
            if selectedImageId != nil {
                
                let fileName = String(format: "%@.jpeg", "ID")
                btnID.setTitle(fileName, for: .normal)
                
            } else {
                
                btnID.setTitle(nil, for: .normal)
            }
        }
    }
    
    override var parentModal: ParentsModal? {
        
        didSet {
            
            guard parentModal != nil else {
                return
            }
            
            if let imageURL = parentModal?.imgsource {
                imgProfile.sd_setIndicatorStyle(UIActivityIndicatorView.Style.medium)
                imgProfile.sd_addActivityIndicator()
                imgProfile.sd_showActivityIndicatorView()
                imgProfile.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                    self.imgProfile.image = img
                    self.imgProfile.sd_removeActivityIndicator()
                }
            }
            
            btnName.setTitle(parentModal?.name, for: .normal)
            btnDOB.setTitle(nil, for: .normal)
            
            btnRelationship.setTitle(parentModal?.getGender(), for: .normal)
            
            if isParentProfile {
                stackViewName.isHidden = false
                stackViewDOB.isHidden = true
                stackViewRelationship.isHidden = false
                //stackViewGender.isHidden = true
                //stackViewID.isHidden = true
            } else {
                stackViewName.isHidden = false
                stackViewDOB.isHidden = false
                stackViewRelationship.isHidden = true
                //stackViewGender.isHidden = false
                //stackViewID.isHidden = false
            }
            
            stackViewName.isHidden = stackViewName.isHidden
            viewGraylineDOB.isHidden = stackViewDOB.isHidden
            stackViewRelationship.isHidden = stackViewRelationship.isHidden
            
            if ValidationManager.shared.isEmpty(text: parentModal?.imageId) == false {
                let fileName = String(format: "%@.jpeg", "ID")
                btnID.setTitle(fileName, for: .normal)
            } else {
                btnID.setTitle(nil, for: .normal)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        imgPlaceholder.layoutIfNeeded()
        imgPlaceholder.cornerRadius = imgPlaceholder.bounds.height/2
        imgPlaceholder.clipsToBounds = true
        imgProfile.layoutIfNeeded()
        imgProfile.cornerRadius = imgProfile.bounds.height/2
        imgProfile.clipsToBounds = true
        imagePickerVC = SLImagePickerVC(presentationController: self, delegate: self)
        btnName.titleLabel?.numberOfLines = 2
        btnName.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    func performSave(parent parentModal: ParentsModal) {
        
        LoadingManager.shared.showLoading()
        
        delay {
            
            let firebaseId: String = PreferenceManager.shared.userId ?? String()
            let fileName = String(format: "%@.jpeg", firebaseId)
            
            if self.selectedImage != nil {
                
                parentModal.userid = firebaseId
                
                FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
                
                FirebaseManager.shared.save(image: firebaseId, fileName: fileName, imageData: self.selectedImage?.pngData() ?? Data(), completion: { (imageURL: URL?, error: Error?) in
                    
                    if let error = error {
                        LoadingManager.shared.hideLoading()
                        delay {
                            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message:
                                                                        error.localizedDescription) {
                            }
                        }
                        
                    } else if self.selectedImageId != nil {
                        
                        FirebaseManager.shared.save(imageId: firebaseId, fileName: fileName, imageData: self.selectedImageId?.pngData() ?? Data(), completion: { (imageIdURL: URL?, error: Error?) in
                            
                            if let error = error {
                                LoadingManager.shared.hideLoading()
                                delay {
                                    DisplayAlertManager.shared.displayAlert(target: self, animated: true, message:
                                                                                error.localizedDescription) {
                                    }
                                }
                            } else {
                                 
                                parentModal.imgsource = imageURL?.absoluteString ?? String()
                                parentModal.imageId = imageIdURL?.absoluteString ?? String()
                                FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
                                LoadingManager.shared.hideLoading()
                                
                                delay {
                                    
                                    DisplayAlertManager.shared.displayAlert(message: "You have save your changes successfully.") {
                                        NavigationManager.shared.sideMenuController.showLeftView()
                                    }
                                }
                            }
                        })
                        
                    } else if let stringImageURL = imageURL?.absoluteString {
                        
                        parentModal.imgsource = stringImageURL
                        FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
                        LoadingManager.shared.hideLoading()
                        
                        delay {
                            
                            //self.pop()
                            DisplayAlertManager.shared.displayAlert(message: "You have save your changes successfully.") {
                                NavigationManager.shared.sideMenuController.showLeftView()
                            }
                        }
                    }
                })
                
            } else if self.selectedImageId != nil {
                
                FirebaseManager.shared.save(imageId: firebaseId, fileName: fileName, imageData: self.selectedImageId?.pngData() ?? Data(), completion: { (imageIdURL: URL?, error: Error?) in
                    
                    if let error = error {
                        LoadingManager.shared.hideLoading()
                        delay {
                            DisplayAlertManager.shared.displayAlert(target: self, animated: true, message:
                                                                        error.localizedDescription) {
                            }
                        }
                    } else {
                                                
                        parentModal.imageId = imageIdURL?.absoluteString ?? String()
                        FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
                        LoadingManager.shared.hideLoading()
                        
                        delay {
                            
                            self.pop()
                        }
                    }
                })
                
            } else {
                
                FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
                LoadingManager.shared.hideLoading()
                
                delay {
                    
                    parentModal.userid = firebaseId
                    
                    FirebaseManager.shared.save(parent: parentModal, of: firebaseId)
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        
                        //self.pop()
                        DisplayAlertManager.shared.displayAlert(message: "You have save your changes successfully.") {
                            NavigationManager.shared.sideMenuController.showLeftView()
                        }
                    }
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func btnBackTap(_ sender: Any) {
        //self.pop()
        NavigationManager.shared.sideMenuController.showLeftView()
    }
    
    @IBAction func btnEditTap(_ sender: PoppinsRegularButton) {
        
        isImagePickerForProfile = true
        self.imagePickerVC?.present(from: sender)
    }
    
    @IBAction func btnDobTap(_ sender: Any) {
        
        /*let controller = NavigationManager.shared.editNameVC
        controller.comingfrom = .dob
        push(controller: controller)*/
    }
    
    @IBAction func btnNameTap(_ sender: Any) {
        let controller = NavigationManager.shared.editNameVC
        controller.comingfrom = .name
        controller.role = self.parentModal?.role
        controller.delegate = self
        push(controller: controller)
    }
    
    @IBAction func btnRelationshipTap(_ sender: Any) {
        
        let controller = NavigationManager.shared.editNameVC
        controller.delegate = self
        controller.comingfrom = .relationship
        controller.role = self.parentModal?.role
        push(controller: controller)
    }
    
    @IBAction func btnSaveTap(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let parentModals = parentModal ?? ParentsModal(fromDictionary: [:])
        parentModals.name = btnName.titleLabel?.text
        
        performSave(parent: parentModals)
    }
    
    @IBAction func btnIDTap(_ sender: UIButton) {
        
        isImagePickerForProfile = false
        self.imagePickerVC?.present(from: sender)
    }
    
    //------------------------------------------------------
    
    //MARK: SlImagePickerDelegate
    
    func didSelect(image: UIImage?) {
        
        /*if let imageData = image?.jpegData(compressionQuality: 0), let compressImage = UIImage(data: imageData) {
            self.selectedImage = Toucan.init(image: compressImage).resizeByCropping(PMSettings.profileImageSize).maskWithRoundedRect(cornerRadius: PMSettings.profileImageSize.width/2, borderWidth: PMSettings.profileBorderWidth, borderColor: PMColor.clear).image
        }*/
        
        if let imageData = image?.jpegData(compressionQuality: 0), let compressImage = UIImage(data: imageData) {
            if isImagePickerForProfile {
                self.selectedImage = Toucan.init(image: compressImage).resizeByCropping(PMSettings.profileImageSize).maskWithRoundedRect(cornerRadius: PMSettings.profileImageSize.width/2, borderWidth: PMSettings.profileBorderWidth, borderColor: PMColor.clear).image
            } else {
                self.selectedImageId = Toucan.init(image: compressImage).image
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: EditNameDelegate
    
    func editName(controller: EditNameVC, didChangeName name: String) {
        
        self.parentModal?.name = name
        btnName.setTitle(name, for: .normal)
        
        controller.pop()
    }
    
    func editName(controller: EditNameVC, didChangeDate date: Date) {
        //TODO: Nothing to do
    }
    
    func editName(controller: EditNameVC, didChangeGender gender: String) {
        //TODO: Nothing to do
    }
    
    func editName(controller: EditNameVC, didChangeRelationship name: String) {
        
        self.parentModal?.gender = name
        btnRelationship.setTitle(parentModal?.getGender(), for: .normal)                
        controller.pop()
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.parentModal == nil {
            btnSave.isEnabled = false
            FirebaseManager.shared.find(parentFrom: PreferenceManager.shared.userId ?? String()) { dict in
                self.parentModal = ParentsModal(fromDictionary: dict)
                self.btnSave.isEnabled = true
            }
        }
    }
    
    //------------------------------------------------------
}
