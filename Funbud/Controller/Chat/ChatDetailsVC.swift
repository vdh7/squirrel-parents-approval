//
//  ChatDetailsVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 22/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation

class ChatDetailsVC : BaseVC,UITableViewDelegate,UITableViewDataSource {
    
    //------------------------------------------------------
    @IBOutlet weak var chatDetailsTable: UITableView!
    var bs_profile = ["bs_profile1","bs_profile2","bs_profile3","bs_profile1","bs_profile2"]
    var bs_profile_title = ["Jenny Wilson","Leslie Alexander","Jane Cooper","Brooklyn Simmons","Jenny Wilson"]
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    //MARK: Custom
    
    func setup() {
        
        chatDetailsTable.delegate = self
        chatDetailsTable.dataSource = self
        chatDetailsTable.register(UINib(nibName: String(describing: ChatDetailsTVC.self), bundle: nil), forCellReuseIdentifier: String(describing: ChatDetailsTVC.self))
    }
        //------------------------------------------------------
   
    //MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bs_profile.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            if let cell = chatDetailsTable.dequeueReusableCell(withIdentifier: String(describing: ChatDetailsTVC.self), for: indexPath) as? ChatDetailsTVC {
                cell.selectionStyle = .none
                
                return cell
    }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    @IBAction func backBtnClicked(_ sender: Any) {
        self.pop()
    }
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
