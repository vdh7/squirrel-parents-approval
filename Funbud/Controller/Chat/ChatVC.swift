//
//  ChatVC.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 10/8/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import MessageKit
import Kingfisher
import InputBarAccessoryView
import IQKeyboardManagerSwift
import Firebase
import SDWebImage
import FittedSheets

class ChatVC: ChatViewController, MessagesDisplayDelegate, MessagesLayoutDelegate, InputBarAccessoryViewDelegate {
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblPlayDateName: UILabel!
    @IBOutlet weak var lblKids: UILabel!
    @IBOutlet weak var lblKidsCount: UILabel!
    @IBOutlet weak var imgViewAvatar: UIImageView!
    
    @IBOutlet weak var lblNoRecordsFound: PMSFProDisplayRegularLabel!
    
    var conversationModal: ConversationModal?
    var imgAvatar: UIImage?
    var messageSenderUser = MessageuserModal(fromDictionary: [:])
    var isParentProfile: Bool = false
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Custom
    
    func setupUI() {
        
        self.view.backgroundColor = PMColor.white
        messagesCollectionView.backgroundColor = UIColor.clear
        
        refreshControl.addTarget(self, action: #selector(loadMoreMessages), for: .valueChanged)
        
        configureMessageInputBar()
        lblNoRecordsFound.isHidden = true
        
        messageInputBar.inputTextView.placeholder = "Send a message..."
    }
    
    func setupData() {
        
        imgViewAvatar.image = nil
        lblPlayDateName.text = "Loading..."
        if let conversationModal = conversationModal, conversationModal.name != nil {
            /*if conversationModal.isForBabysitter == "1" {
                if self.isParentProfile {
                    self.lblPlayDateName.text = conversationModal.name
                } else {
                    self.lblPlayDateName.text = conversationModal.username
                }
            } else {
                lblPlayDateName.text = conversationModal.name
            }*/
            setAvatar(conversationModal)
        } else {
            imgViewAvatar.image = imgAvatar
        }
        //lblKids.text = conversationModal?.kidsName
        //lblKids.text = conversationModal?.displayChatDetailsKidsName()
        
        /*let kidsCount = conversationModal?.kidsList.count ?? .zero
        lblKidsCount.text = String( kidsCount )
        if kidsCount > 1 {
            lblKidsCount.isHidden = false
        } else {
            lblKidsCount.isHidden = true
        }*/
        
        lblKids.text = conversationModal?.displayChatDetailsKidsName()
        lblKidsCount.isHidden = true
        if let (isHidden, stringToDisplayKidsCount) = conversationModal?.displayKidsCount() {
            //lblKidsCount.isHidden = isHidden
            debugLog(object: isHidden)
            lblKidsCount.text = stringToDisplayKidsCount
        }
    }
    
    func updateUI() {
        
        lblNoRecordsFound.isHidden = messageList.count != .zero
        self.messagesCollectionView.reloadData()
    }
    
    func configureMessageInputBar() {
        messageInputBar.delegate = self
        messageInputBar.inputTextView.tintColor = PMColor.themeOrange
        messageInputBar.sendButton.setTitleColor(PMColor.themeOrange, for: .normal)
        messageInputBar.sendButton.setTitleColor(
            PMColor.themeOrange.withAlphaComponent(0.3), for: .highlighted)
    }
    
    func setupDisplayName(_ conversation: ConversationModal) {
        
        if conversation.isForBabysitter == "1" {
            if self.isParentProfile {
                self.lblPlayDateName.text = conversation.username
                self.lblKids.text = conversation.name
            } else {
                self.lblPlayDateName.text = conversation.username
                self.lblKids.text = conversation.name
            }
        } else {
            self.lblPlayDateName.text = conversation.name
            self.lblKids.text = conversation.username
        }
    }
    
    fileprivate func setAvatar(_ conversation: ConversationModal) {
        
        //if conversation.isForBabysitter == "1" && isParentProfile == true {
        if conversation.isForBabysitter == "1" {
            
            let bbsitterId = conversation.access?.first(where: { userId in
                return conversation.owner != userId
            })
            
            if let bbsitterId = bbsitterId, isParentProfile == true {
                FirebaseManager.shared.findDeviceToken(of: bbsitterId, roleType: RoleType.babysitter) { baseModal in
                    
                    conversation.username = baseModal.name
                    self.setupDisplayName(conversation)
                    if let imageURL = baseModal.imgsource {
                        self.imgViewAvatar.sd_setIndicatorStyle(UIActivityIndicatorView.Style.medium)
                        self.imgViewAvatar.sd_addActivityIndicator()
                        self.imgViewAvatar.sd_showActivityIndicatorView()
                        self.imgViewAvatar.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                            self.imgViewAvatar.image = img
                            self.imgViewAvatar.sd_removeActivityIndicator()
                        }
                    } else {
                        self.imgViewAvatar.image = nil
                    }
                }
            } else if isParentProfile == false, let parentId = conversation.owner {
                
                FirebaseManager.shared.findDeviceToken(of: parentId, roleType: RoleType.parent) { parentModal in
                    //conversation.username = parentModal.name
                    self.setupDisplayName(conversation)
                    if let imageURL = parentModal.imgsource {
                        self.imgViewAvatar.sd_setIndicatorStyle(UIActivityIndicatorView.Style.medium)
                        self.imgViewAvatar.sd_addActivityIndicator()
                        self.imgViewAvatar.sd_showActivityIndicatorView()
                        self.imgViewAvatar.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                            self.imgViewAvatar.image = img
                            self.imgViewAvatar.sd_removeActivityIndicator()
                        }
                    } else {
                        self.imgViewAvatar.image = nil
                    }
                }
            } else {
                imgViewAvatar.image = nil
            }
            
        } else {
            
            /*let parentId = conversation.access?.first(where: { userId in
                return conversation.owner != userId
            })
            
            if let parentId = parentId {
                FirebaseManager.shared.findDeviceToken(of: parentId, roleType: RoleType.parent) { parentModal in
                    conversation.username = parentModal.name
                    self.setupDisplayName(conversation)
                    if let imageURL = parentModal.imgsource {
                        self.imgViewAvatar.sd_setIndicatorStyle(UIActivityIndicatorView.Style.medium)
                        self.imgViewAvatar.sd_addActivityIndicator()
                        self.imgViewAvatar.sd_showActivityIndicatorView()
                        self.imgViewAvatar.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                            self.imgViewAvatar.image = img
                            self.imgViewAvatar.sd_removeActivityIndicator()
                        }
                    } else {
                        self.imgViewAvatar.image = nil
                    }
                }
            }*/
            
            self.setupDisplayName(conversation)
            if let first = conversation.kidsList.first {
                FirebaseManager.shared.findKid(fromID: first) { kidsModal in
                    let avatarIndex = Int(kidsModal?.avatar ?? String()) ?? .zero
                    let img = PMImageName.avatars
                    self.imgViewAvatar.image = UIImage(named: img[avatarIndex])
                }
            } else {
                imgViewAvatar.image = nil
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func btnBackTap(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnImgTap(_ sender: Any) {
        
        if conversationModal?.isForBabysitter == "1" {
            
            let bbsitterId = conversationModal?.access?.first(where: { userId in
                return conversationModal?.owner != userId
            })
            if let bbsitterId = bbsitterId, isParentProfile == true {
               
                FirebaseManager.shared.findDeviceToken(of: bbsitterId, roleType: RoleType.babysitter) { baseModal in
                    
                    let controller = NavigationManager.shared.babySitterProfileVC
                    controller.profileModal = baseModal as? BabysitterModal
                    var sizes = [SheetSize]()
                    sizes.append(.fixed(UIScreen.main.bounds.size.height * 0.5))
                    sizes.append(.marginFromTop(60))
                    let sheetController = SheetViewController(controller: controller, sizes: sizes)
                    self.present(sheetController, animated: true, completion: nil)
                }
                
            } else if isParentProfile == false, let parentId = conversationModal?.owner {
                
                FirebaseManager.shared.findDeviceToken(of: parentId, roleType: RoleType.parent) { baseModal in
                    //conversation.username = parentModal.name
                    let controller = NavigationManager.shared.kidsProfileVC
                    controller.profileModal = baseModal as? ParentsModal
                    var sizes = [SheetSize]()
                    sizes.append(.fixed(UIScreen.main.bounds.size.height * 0.5))
                    sizes.append(.marginFromTop(60))
                    let sheetController = SheetViewController(controller: controller, sizes: sizes)
                    self.present(sheetController, animated: true, completion: nil)
                }
            }
            
        } else {
            
            let otherUserId = conversationModal?.access?.first(where: { userId in
                return messageSenderUser.id != userId
            })
            if let otherUserId = otherUserId, isParentProfile == true {
               
                FirebaseManager.shared.findDeviceToken(of: otherUserId, roleType: RoleType.parent) { baseModal in
                    //conversation.username = parentModal.name
                    let controller = NavigationManager.shared.kidsProfileVC
                    controller.profileModal = baseModal as? ParentsModal
                    var sizes = [SheetSize]()
                    sizes.append(.fixed(UIScreen.main.bounds.size.height * 0.5))
                    sizes.append(.marginFromTop(60))
                    let sheetController = SheetViewController(controller: controller, sizes: sizes)
                    self.present(sheetController, animated: true, completion: nil)
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: MessagesDisplayDelegate
    
    // MARK: - Text Messages
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .black : .black
    }
    
    func detectorAttributes(for detector: DetectorType, and message: MessageType, at indexPath: IndexPath) -> [NSAttributedString.Key: Any] {
        switch detector {
        case .hashtag, .mention: return [.foregroundColor: UIColor.blue]
        default: return MessageLabel.defaultAttributes
        }
    }
    
    func enabledDetectors(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> [DetectorType] {
        return [.url, .address, .phoneNumber, .date, .transitInformation, .mention, .hashtag]
    }
    
    // MARK: - All Messages
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? PMColor.clear : PMColor.clear
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        
        //let tail: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        if isFromCurrentSender(message: message) {
            return .bubbleTailOutline(PMColor.themeOrange, MessageStyle.TailCorner.bottomRight, MessageStyle.TailStyle.pointedEdge)
        } else {
            return .bubbleTailOutline(.lightGray.withAlphaComponent(0.5), MessageStyle.TailCorner.bottomLeft, MessageStyle.TailStyle.pointedEdge)
        }
        
        /*let custom = MessageStyle.custom { (messageView: MessageContainerView) in
         if self.isFromCurrentSender(message: message) {
         messageView.image = SCImageName.messageSentText
         } else {
         messageView.image = SCImageName.messageReceivedText
         }
         messageView.backgroundColor = UIColor.clear
         }
         return custom*/
    }
    
    /*func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        
        //let avatar = SampleData.shared.getAvatarFor(sender: message.sender)
        //avatarView.set(avatar: avatar)
    }*/
    
    func configureMediaMessageImageView(_ imageView: UIImageView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        if case MessageKind.photo(let media) = message.kind, let imageURL = media.url {
            imageView.kf.setImage(with: imageURL)
        } else {
            imageView.kf.cancelDownloadTask()
        }
    }
    
    // MARK: - Location Messages
    
    func annotationViewForLocation(message: MessageType, at indexPath: IndexPath, in messageCollectionView: MessagesCollectionView) -> MKAnnotationView? {
        let annotationView = MKAnnotationView(annotation: nil, reuseIdentifier: nil)
        let pinImage = #imageLiteral(resourceName: "ic_map_marker")
        annotationView.image = pinImage
        annotationView.centerOffset = CGPoint(x: 0, y: -pinImage.size.height / 2)
        return annotationView
    }
    
    func animationBlockForLocation(message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> ((UIImageView) -> Void)? {
        return { view in
            view.layer.transform = CATransform3DMakeScale(2, 2, 2)
            UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [], animations: {
                view.layer.transform = CATransform3DIdentity
            }, completion: nil)
        }
    }
    
    func snapshotOptionsForLocation(message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> LocationMessageSnapshotOptions {
        
        return LocationMessageSnapshotOptions(showsBuildings: true, showsPointsOfInterest: true, span: MKCoordinateSpan(latitudeDelta: 10, longitudeDelta: 10))
    }
    
    // MARK: - Audio Messages
    
    func audioTintColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .darkGray : .darkGray
    }
    
    func configureAudioCell(_ cell: AudioMessageCell, message: MessageType) {
        audioController.configureAudioCell(cell, message: message) // this is needed especily when the cell is reconfigure while is playing sound
    }
    
    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        
        if messageList.count > 0 && indexPath.section > 0 {
            let previousDate = messageList[indexPath.section - 1].sentDate
            let order = Calendar.current.compare(message.sentDate, to: previousDate, toGranularity: .day)
            switch order {
            case .orderedDescending, .orderedAscending:
                return 18
            case .orderedSame:
                return 0
            }
        } else {
            return 0
        }
    }
    
    func cellBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 17
    }
    
    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 20
    }
    
    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 16
    }
    
    //------------------------------------------------------
    
    //MARK: InputBarAccessoryViewDelegate
    
    func inputBar(_ inputBar: InputBarAccessoryView, textViewTextDidChangeTo text: String) {
    }
    
    @objc
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        processInputBar(messageInputBar)
    }
    
    func processInputBar(_ inputBar: InputBarAccessoryView) {
        // Here we can parse for which substrings were autocompleted
        let attributedText = inputBar.inputTextView.attributedText!
        let range = NSRange(location: 0, length: attributedText.length)
        attributedText.enumerateAttribute(.autocompleted, in: range, options: []) { (_, range, _) in
            let substring = attributedText.attributedSubstring(from: range)
            let context = substring.attribute(.autocompletedContext, at: 0, effectiveRange: nil)
            print("Autocompleted: `", substring, "` with context: ", context ?? [])
        }
        
        let components = inputBar.inputTextView.components
        inputBar.inputTextView.text = String()
        inputBar.invalidatePlugins()
        inputBar.sendButton.startAnimating()
        inputBar.inputTextView.placeholder = "Sending..."
        
        // Resign first responder for iPad split view
        //inputBar.inputTextView.resignFirstResponder()
        DispatchQueue.main.async { [weak self] in
            for component in components {
                if let stringMessage = component as? String {
                    let message = MessageModal(fromDictionary: [:])
                    message.pMessage = stringMessage
                    self?.insertMessage(message, completion: { (error: Error?) in
                        if let error = error {
                            DisplayAlertManager.shared.displayAlert(message: error.localizedDescription, handlerOK: nil)
                        } else {
                            inputBar.sendButton.stopAnimating()
                            inputBar.inputTextView.placeholder = "Send a message..."
                            self?.messagesCollectionView.scrollToLastItem(animated: true)
                        }
                    })
                }
            }
        }
    }
    
    func insertMessage(_ message: MessageModal, completion: @escaping(_ error: Error?)->Void) {
        
        message.pMessageuser = messageSenderUser
        FirebaseManager.shared.sendMessage(toConversation: conversationModal?.conversationId ?? String(), withTitle: lblKids.text ?? conversationModal?.name, message: message, completion: completion)
    }
    
    /*private func insertMessages(_ data: [Any]) {
        for component in data {
            if let stringMessage = component as? String {
                //MockMessage(text: str, user: user, messageId: UUID().uuidString, date: Date())
                let message = MessageModal(fromDictionary: [:])
                message.message = stringMessage
                insertMessage(message)
            } /*else if let img = component as? UIImage {
                //let message = MockMessage(image: img, user: user, messageId: UUID().uuidString, date: Date())
                let message = MessageModal(fromDictionary: [:])
                insertMessage(message)
            }*/
        }
    }*/
    
    //------------------------------------------------------
    
    //MARK: Override
    
    override func configureMessageCollectionView() {
        super.configureMessageCollectionView()
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
    }

    override func currentSender() -> SenderType {
        return messageSenderUser
    }
    
    override func loadMoreMessages() {
        /*FirebaseManager.shared.getMoreMessagesFrom(for: conversationModal?.conversationId ?? String(), lastDocumentId: messageList.first?.messageId ?? String()) { (moreMessages: [MessageModal]) in
            self.messageList.insert(contentsOf: moreMessages, at: .zero)
            self.updateUI()
            self.refreshControl.endRefreshing()
        }*/
        self.refreshControl.endRefreshing()
    }
    
    //------------------------------------------------------
    
    //MARK: Selector
    
    @objc func backBtn(sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblKids.text = nil
        self.lblPlayDateName.text = nil
        imgViewAvatar.image = nil
        
        let parentModal = FirebaseManager.shared.parentModal
        let babysitterModal = FirebaseManager.shared.babysitterModal
        
        if (parentModal?.role == RoleType.parent.rawValue || babysitterModal?.role == RoleType.parent.rawValue) && parentModal != nil {
            messageSenderUser.id = parentModal?.userid
            messageSenderUser.name = parentModal?.name
            messageSenderUser.avatar = parentModal?.imgsource
        } else if (parentModal?.role == RoleType.babysitter.rawValue || babysitterModal?.role == RoleType.babysitter.rawValue) && babysitterModal != nil {
            messageSenderUser.id = babysitterModal?.userid
            messageSenderUser.name = babysitterModal?.name
            messageSenderUser.avatar = babysitterModal?.imgsource
        }
        
        if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
            
            layout.setMessageIncomingAvatarSize(.zero)
            layout.setMessageOutgoingAvatarSize(.zero)
            
            layout.setMessageIncomingMessageBottomLabelAlignment(LabelAlignment(textAlignment: .left, textInsets: .init(top: 5, left: 2, bottom: .zero, right: .zero)))
            layout.setMessageOutgoingMessageBottomLabelAlignment(LabelAlignment(textAlignment: .right, textInsets: .init(top: 5, left: .zero, bottom: .zero, right: 2)))
        }
        
        setupUI()
        
        FirebaseManager.shared.getRecentMessages(forConversation: conversationModal?.conversationId ?? String()) { (messages: [MessageModal]) in
            self.messageList = messages
            self.updateUI()
            self.messagesCollectionView.scrollToLastItem(animated: true)
        }
        
        let collectionRequest = FirebaseManager.shared.firestore.collection(FirebaseManager.shared.nodeConversations).document(conversationModal?.conversationId ?? String()).collection(FirebaseManager.shared.nodeMessages)
        collectionRequest.addSnapshotListener { (snapshot: QuerySnapshot?, error: Error?) in
            FirebaseManager.shared.getRecentMessages(forConversation: self.conversationModal?.conversationId ?? String()) { (messages: [MessageModal]) in
                self.messageList = messages
                self.updateUI()
                self.messagesCollectionView.scrollToLastItem(animated: true)
                
                //update last message
                if let last = messages.last {
                    FirebaseManager.shared.update(lastMessage: last, toConversation: self.conversationModal?.conversationId ?? String()) {
                        //Refresh list if needed.
                    }
                }
            }
        }
        
        delay {
            self.setupData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureMessageCollectionView()
        
        IQKeyboardManager.shared.enable = false
        
        if messageList.count == 0 {
            lblNoRecordsFound.isHidden = false
            lblNoRecordsFound.bringSubviewToFront(messagesCollectionView)
        } else {
            lblNoRecordsFound.isHidden = true
        }
        
        //navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backBtn(sender:)))
        //messagesCollectionView.contentInset.top = viewHeader.frame.origin.y + viewHeader.bounds.height
        
        /*for constraint in messagesCollectionView.constraints {
            messagesCollectionView.removeConstraint(constraint)
        }*/
        
        messagesCollectionView.translatesAutoresizingMaskIntoConstraints = false
        messagesCollectionView.bringSubviewToFront(self.viewHeader)
        
        viewHeader.layoutIfNeeded()
        let window = AppDelegate.shared.window
        let topPadding = (window?.safeAreaInsets.top ?? .zero) + viewHeader.bounds.height
        messagesCollectionView.contentInset.top = .zero
        let top = messagesCollectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: topPadding * 2)
        let bottom = messagesCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        let leading = messagesCollectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor)
        let trailing = messagesCollectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        NSLayoutConstraint.activate([top, bottom, trailing, leading])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        IQKeyboardManager.shared.enable = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //------------------------------------------------------
}
