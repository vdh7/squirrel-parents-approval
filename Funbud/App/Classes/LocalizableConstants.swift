//
//  LocalizableConstant.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 25/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

struct LocalizableConstants {
    
    struct SuccessMessage {
        
        static let user_rating_success = "user_rating_success"
    }
    
    struct Error {
        static let locationServiceDisable = "location_service_disable"
        static let failed_to_signout = "failed_to_signout"
        static let conversation_not_available = "conversation_not_available"
        static let admin_reject_bs_request = "admin_reject_bs_request"
        static let your_profile_blocked = "your_profile_blocked"
        static let admin_reject_parent_request = "admin_reject_parent_request"
    }
    
    struct ValidationMessage {
        
        //common
        static let noNetworkConnection = "no_network_connection"
        
        //signing in with phone
        static let enterPhoneNumber = "enter_phone_number"
        static let enterValidPhoneNumber = "enter_valid_phone_number"
        static let selectParentProfileImage = "select_parent_profile_image"
        static let enterParentName = "enter_parent_name"
        static let specficRelation = "specficRelation"
        static let location = "location"
        static let BS_name = "Enter_Full_Name"
        static let BS_DOB = "D_O_B"
        
        //add kid
        static let emptyKidName = "enter_kid_name"
        static let emptyKidDOB = "select_kid_dob"
        static let emptyKidSpecifyGender = "select_specify_gender"
        static let emptyKidAvatar = "select_kid_avatar"
        static let emptyKidDescription = "enter_kid_description"
        
        //location enabled
        static let noAddressFound = "no_address_found"
        
        //babysitter
        static let selectBabysitterProfileImage = "select_babysitter_profile_image"
        static let selectBabysitterIdImage = "select_babysitter_id_image"
        static let emptyHourlyRate = "enter_hourly_rate"
        static let emptyBabysitterFrom = "enter_bs_from"
        static let emptyBabysitterTo = "enter_bs_to"
        
        // createPlaydates
        static let title = "title_Pd"
        static let location_pd = "location_pd"
        static let date = "date_pd"
        static let time = "time_pd"
        static let selectKidToPlaydate = "select_kid_to_playdate"
        
        //rating
        static let emptyUserRating = "empty_user_rating"
        static let alredyGivenRating = "already_given_rating"
        static let not_allow_to_rate_babysitter = "not_allow_to_rate_babysitter"
        
        //select kid
        static let selectKidForRequest = "select_kid_for_request"
    }
    
    struct Controller {
        
        // Phone number vc
        static let termsAndConditions = "terms_condition"
        static let privacy = "Privacy Policy"
        static let terms = "Terms and Conditions"
        static let placeholderPhoneNumber = "placeholder_phone_number"
        
        //otp
        static let displayNumber = "display_number"
        
        //landing
        static let goToConversation = "go_to_conversation"
        
        //menu
        static let confirmationSignout = "confirmation_signout"
        
        //my kids
        static let confirmationKidDelete = "confirmation_Kid_delete"
        static let addKid = "add_kid"
        static let editKid = "edit_kid"
        
        static let next = "next"
        static let save = "save"
        
        static let editPlayDate = "edit_playdate"
        static let deletePlayDate = "delete_playdate"
                
        static let confirmationDeletePlaydate = "confirmation_delete_playdate"
        
        static let titleEditPlaydate = "title_edit_playdate"
        static let titleCreatePlaydate = "title_create_playdate"
        static let post = "post"
        
        //requests
        static let requestSent = "request_sent"
        static let requestAlreadySent = "request_already_sent"
        static let requestOwnCancel = "request_own_cancel"
        static let requestAlreadyApproved = "request_already_approved"
        static let requestRejected = "request_rejected"
        static let admitedSuccessfully = "admited_successfully"
        static let babysitterRequestSent = "babysitter_request_sent"
        static let requestRejectedSuccessfully = "request_rejected_successfully"
        static let isRequestingForBabysitter = "is_requesting_babysitter"
        
        //otp
        static let notEarlierThanSeconds = "not_earlier_than_seconds"
    }
}
