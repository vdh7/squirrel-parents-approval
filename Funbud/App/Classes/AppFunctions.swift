//
//  AppFunctions.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 25/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation
import UIKit
import AssistantKit
import CoreLocation
import MapKit
import Contacts
import SafariServices

/// This function will retutn font size according to device.
///
/// - Parameter fontDefaultSize: <#fontDefaultSize description#>
/// - Returns: <#return value description#>
func getDynamicFontSize(fontDefaultSize: CGFloat ) -> CGFloat {
    
    var fontSize : CGFloat = 0.0
    let device = Device.version
       
    if device == Version.phone4 || device == Version.phone5 {
        fontSize = fontDefaultSize
    } else if device == Version.phone6 || device == Version.phone7 || device == Version.phone6S || device == Version.phone8 {
        fontSize = fontDefaultSize
    } else if device == Version.phone7Plus || device == Version.phone8Plus {
        fontSize = fontDefaultSize + PMFont.increaseSize
    } else if device == Version.phoneX || device == Version.phoneXR || device == Version.phoneXS || device == Version.phoneXSMax {
        fontSize = fontDefaultSize + PMFont.increaseSize
    } else
    //7.9 inches
    if device == Version.padMini || device == Version.padMini2 || device == Version.padMini3 || device == Version.padMini4 {
        fontSize = fontDefaultSize - PMFont.reduceSize
    //9.7 inches
    } else if device == Version.padAir || device == Version.padAir2  || device == Version.pad1 || device == Version.pad2 || device == Version.pad3 || device == Version.pad4 {
        //fontSize = fontDefaultSize + PMFont.increaseSize
        fontSize = fontDefaultSize
    //10.5 and later
    } else if device == Version.padPro {
        fontSize = fontDefaultSize + PMFont.increaseSize
    } else {
        fontSize = fontDefaultSize
    }
    
    return fontSize
}

/// Delay in execuation of statement
/// - Parameters:
///   - delay: <#delay description#>
///   - closure: <#closure description#>
func delay(_ delay: Double = 0.4, closure:@escaping ()->()) {
    DispatchQueue.main.async {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
}

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}


func getEmptyView() -> UIView {
    let view = UIView(frame: CGRect.zero)
    view.backgroundColor = UIColor.clear
    return view
}

func localized(code: Int) -> String {
    
    let codeKey = String(format: "error_%d", code)
    let localizedMessage = codeKey.localized()
    return localizedMessage
}

func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    
    let size = image.size

    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height

    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
    }

    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    return newImage!
}

func getDistanceInKm(from coordinate0: CLLocationCoordinate2D?, coordinate1: CLLocationCoordinate2D?) -> String {
    let location1 = CLLocation(latitude: coordinate0?.latitude ?? .zero, longitude: coordinate0?.latitude ?? .zero)
    let location2 = CLLocation(latitude: coordinate1?.latitude ?? .zero, longitude: coordinate1?.latitude ?? .zero)
    let distance = location1.distance(from: location2)
    let km = Measurement(value: distance, unit: UnitLength.meters).converted(to: UnitLength.miles)
    let formatter = MeasurementFormatter()
    formatter.unitOptions = .providedUnit
    formatter.unitStyle = .short
    return formatter.string(from: km)
}

func countBytes(ofData imgData: Data?) -> String {
    let bcf = ByteCountFormatter()
    bcf.allowedUnits = [.useKB]
    bcf.countStyle = .file
    let bytes = imgData?.count ?? .zero
    return bcf.string(fromByteCount: Int64(bytes))
}

func calculateAge(dob : String, format: String = DateFormate.UTC) -> (year :Int, month : Int, day : Int) {
    let df = DateFormatter()
    df.dateFormat = format
    let date = df.date(from: dob)
    guard let val = date else{
        return (0, 0, 0)
    }
    var years = 0
    var months = 0
    var days = 0
    
    let cal = Calendar.current
    years = cal.component(.year, from: Date()) -  cal.component(.year, from: val)
    
    let currMonth = cal.component(.month, from: Date())
    let birthMonth = cal.component(.month, from: val)
    
    //get difference between current month and birthMonth
    months = currMonth - birthMonth
    //if month difference is in negative then reduce years by one and calculate the number of months.
    if months < 0
    {
        years = years - 1
        months = 12 - birthMonth + currMonth
        if cal.component(.day, from: Date()) < cal.component(.day, from: val){
            months = months - 1
        }
    } else if months == 0 && cal.component(.day, from: Date()) < cal.component(.day, from: val)
    {
        years = years - 1
        months = 11
    }
    
    //Calculate the days
    if cal.component(.day, from: Date()) > cal.component(.day, from: val){
        days = cal.component(.day, from: Date()) - cal.component(.day, from: val)
    }
    else if cal.component(.day, from: Date()) < cal.component(.day, from: val)
    {
        let today = cal.component(.day, from: Date())
        let date = cal.date(byAdding: .month, value: -1, to: Date())
        
        days = date!.daysInMonth - cal.component(.day, from: val) + today
    } else
    {
        days = 0
        if months == 12
        {
            years = years + 1
            months = 0
        }
    }    
    return (years, months, days)
}

func covert(distanceInMeter: Double, to unit: UnitLength = .kilometers) -> String {
    let km = Measurement(value: distanceInMeter, unit: UnitLength.meters).converted(to: unit)
    let formatter = MeasurementFormatter()
    formatter.unitOptions = .providedUnit
    formatter.unitStyle = .short
    return formatter.string(from: km)
}

public func debugLog(object: Any, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
  #if DEBUG
    let className = (fileName as NSString).lastPathComponent
    print("<\(className)> \(functionName) [#\(lineNumber)]| \(object)\n")
  #endif
}

func openSafariViewController(urlString : String){
    
    if urlString.isValidURL == true, let url =  URL(string: urlString) {
        var websiteURL = url
        if urlString.hasPrefix("https://") == false && urlString.hasPrefix("http://") == false {
            websiteURL = URL(string: "https://\(urlString)") ?? url
        }
        let safariVC = SFSafariViewController(url: websiteURL)
        UIApplication.topViewController()?.present(safariVC, animated: false)
    }
}

func getDayName(fromDay day: Int) -> String {
    switch day {
    case 1:
        return "Monday"
    case 2:
        return "Tuesday"
    case 3:
        return "Wednesday"
    case 4:
        return "Thursday"
    case 5:
        return "Friday"
    case 6:
        return "Saturday"
    case 7:
        return "Sunday"
    default:
        return "N/A"
    }
}

func getDynamicTitles() -> [String] {
    
    return ["Meet the neighbourhood’s newest FunBud!",
    "Meet the neighbourhood’s coolest FunBud!",
    "This FunBud can’t wait to meet you!",
    "Pop up and say hi!",
    "Welcome the Funnest and Buddest FunBud ever!",
    "The more FunBuds the merrier!",
    "1 + 1 = 2 FunBuds!",
    "How do you call a buddy who’s so much fun? A FunBud!"]
}
