//
//  AppConstants.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 25/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

let kAppName : String = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ?? String()
let kAppBundleIdentifier : String = Bundle.main.bundleIdentifier ?? String()

//-----------------------------

// MARK:-- Enum

enum DeviceType: String {
    case iOS = "iOS"
    case android = "android"
}

enum RoleType: String {
    case none = "0"
    case parent = "1"
    case babysitter = "2"
}

enum GenderType: String, CaseIterable {
    
    case male = "Male"
    case female = "Female"
    case other = "Other"
}

enum ParentsType: String, CaseIterable {
    
    case mother = "Mother"
    case father = "Father"
    case other = "Other"
}

enum EditProfileType: String, CaseIterable {
    
    case name = "name"
    case dob = "dob"
    case relationship = "relationship"
    case gender = "gender"
    case none = "others"
}

enum DaysType: String, CaseIterable {
    
    case all = "All"
    case monday = "Monday"
    case tuesday = "Tuesday"
    case wednesday = "Wednesday"
    case thursday = "Thursday"
    case friday = "Friday"
    case saturday = "Saturday"
    case sunday = "Sunday"
}

enum TimeSlotTypes: String, CaseIterable {
    
    case all = "All"
    case T_01 = "01:00 AM"
    case T_02 = "02:00 AM"
    case T_03 = "03:00 AM"
    case T_04 = "04:00 AM"
    case T_05 = "05:00 AM"
    case T_06 = "06:00 AM"
    case T_07 = "07:00 AM"
    case T_08 = "08:00 AM"
    case T_09 = "09:00 AM"
    case T_10 = "10:00 AM"
    case T_11 = "11:00 AM"
    case T_12 = "12:00 PM"
    case T_13 = "1:00 PM"
    case T_14 = "2:00 PM"
    case T_15 = "3:00 PM"
    case T_16 = "4:00 PM"
    case T_17 = "5:00 PM"
    case T_18 = "6:00 PM"
    case T_19 = "7:00 PM"
    case T_20 = "8:00 PM"
    case T_21 = "9:00 PM"
    case T_22 = "10:00 PM"
    case T_23 = "11:00 PM"
    case T_24 = "12:00 AM"
}

public enum FCMNotificationType: Int {
    
    case none
    case playdateCreated
    case playdateGroupRequestSend
    case playdateGroupRequestAccepted
    case playdateGroupRequestRejected
    case playdateNonGroupRequestSend
    case playdateNonGroupRequestAccepted
    case playdateNonGroupRequestRejected
    case babysitterRequestSend
    case babysitterRequestAccepted
    case babysitterRequestRejected
    case message
    case babysitterProfileApproved
    case parents
    case babysitter
}

//-----------------------------

// MARK:-- General

let emptyJsonString = "{}"

struct PMSettings {
    
    static let cornerRadius: CGFloat = 5
    static let borderWidth: CGFloat = 1
    static let shadowOpacity: Float = 0.4
    static let tableViewMargin: CGFloat = 50
    
    static let nameLimit = 20
    static let emailLimit = 70
    static let passwordLimit = 20
    
    static let footerMargin: CGFloat = 50
    
    static let profileImageSize = CGSize.init(width: 400, height: 400)
    static let profileBorderWidth: CGFloat = 4
}

struct PMColor {
    
    static let white = UIColor.white
    static let black = UIColor.black
    static let darkGray = UIColor.darkGray
    static let clear = UIColor.clear
    static let themeOrange = UIColor(named: "ParentFlow") ?? .systemOrange
    static let themeGreen = UIColor(named: "BabySitter") ?? PMColor.green
    static let darkblack = #colorLiteral(red: 0.04210930318, green: 0.0574330166, blue: 0.2074161172, alpha: 1)
    static let green = #colorLiteral(red: 0.06128483266, green: 0.695394814, blue: 0.004404011648, alpha: 1)
    static let segmentSelected = #colorLiteral(red: 0.195715189, green: 0.2128813267, blue: 0.3288620412, alpha: 1)
    static let segmentUnselect = #colorLiteral(red: 0.195715189, green: 0.2128813267, blue: 0.3288620412, alpha: 1)
    
}
struct PMFont {
    static let defaultRegularFontSize: CGFloat = 20.0
    static let zero: CGFloat = 0.0
    static let reduceSize: CGFloat = 3.0
    static let increaseSize : CGFloat = 2.0
    //"family: SF Pro Display"
    static func sfProDisplayRegular(size: CGFloat?) -> UIFont {
        return UIFont(name: "SFProDisplay-Regular", size: size ?? defaultRegularFontSize)!
    }
    static func sfProDisplayBold(size: CGFloat?) -> UIFont {
        return UIFont(name: "SFProDisplay-Bold", size: size ?? defaultRegularFontSize)!
    }
    static func sfpoppinslight(size: CGFloat?) -> UIFont {
        return UIFont(name: "Poppins-Light", size: size ?? defaultRegularFontSize)!
    }
    static func sfpoppinsmedium(size: CGFloat?) -> UIFont {
        return UIFont(name: "Poppins-Medium", size: size ?? defaultRegularFontSize)!
    }
    static func sfpoppinsRegular(size: CGFloat?) -> UIFont {
        return UIFont(name: "Poppins-Regular", size: size ?? defaultRegularFontSize)!
    }
    static func sfpoppinsSemiBold(size: CGFloat?) -> UIFont {
        return UIFont(name: "Poppins-SemiBold", size: size ?? defaultRegularFontSize)!
    }
    static func sfpoppinsBold(size: CGFloat?) -> UIFont {
        return UIFont(name: "Poppins-Bold", size: size ?? defaultRegularFontSize)!
    }
    static func sfpoppinsExtraBold(size: CGFloat?) -> UIFont {
        return UIFont(name: "Poppins-ExtraBold", size: size ?? defaultRegularFontSize)!
    }
    static func sfpoppinsThin(size: CGFloat?) -> UIFont {
        return UIFont(name: "Poppins-Thin", size: size ?? defaultRegularFontSize)!
    }
    static func sfFredokaRegular(size: CGFloat?) -> UIFont {
        return UIFont(name: "FredokaOne-Regular", size: size ?? defaultRegularFontSize)!
    }
    //    static func sfLatoRegular(size: CGFloat?) -> UIFont {
    //        return UIFont(name: "Lato-Regular", size: size ?? defaultRegularFontSize)!
    //    }
}

struct PMImageName {
    
    static let iconDropDown = "icon_dropdown"
    static var avatars = ["AB1","AG1","AB2","AG2","AB3","AG3","AB4","AG4","AB5","AG5","AB6","AG6","AB7","AG7","AB8","AG8","AB9","AG9","AB10","AG10","AB11","AG11","AB12","AG12","AB13","AG13","AB14","AG14","AB15","AG15","AB16","AG16","AB17","AG17","AG18","AG19","AG20","AG21"]
    
    static let bottom_menu_home = "Home"
    static let bottom_menu_home_active = "HomeActive"
    static let bottom_menu_playdate = "PlayDate"
    static let bottom_menu_chat = "Chat"
    static let bottom_menu_chat_active = "ChatActive"
    static let menu_logo_parents = "menu_squireel"
    static let menu_logo_babysitter = "menu_logo_babysiters"
    static let logo_squirrel = "Logo"
    static let placeholder = "placeholder"
}

struct PMScreenName {
    
    static let subscribed = "subscribed"
    static let home = "home"
    static let settings = "settings"
}

struct SlColors {
    
    static let S1 = UIColor(named: "S1")
    static let S2 = UIColor(named: "S2")
    static let S3 = UIColor(named: "S3")
    static let S4 = UIColor(named: "S4")
    static let S5 = UIColor(named: "S5")
    static let S6 = UIColor(named: "S6")
    static let S7 = UIColor(named: "S7")
}

struct PMNotification {
    
    static let refreshMyKids = "refreshMyKids"
    static let refreshPlayDates = "refreshPlayDates"
}
