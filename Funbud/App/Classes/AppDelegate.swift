//
//  AppDelegate.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 25/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleSignIn
import FBSDKCoreKit
import Firebase
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    private lazy var pushNotificationOption: UNNotificationPresentationOptions = {
        if #available(iOS 14.0, *) {
            return [.alert, .banner, .sound]
        } else {
            return [.alert, .sound]
        }
    }()
    
    private lazy var pushNotificationOptionEmpty: UNNotificationPresentationOptions = {
        return []
    }()
    
    var pendingPayload: [AnyHashable: Any]?
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    /// keyboard configutation
    private func configureKeboard() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.toolbarTintColor = PMColor.black
    }
    
    /// to get custom added font names
    private func getCustomFontDetails() {
#if DEBUG
        for family in UIFont.familyNames {
            let sName: String = family as String
            debugPrint("family: \(sName)")
            for name in UIFont.fontNames(forFamilyName: sName) {
                debugPrint("name: \(name as String)")
            }
        }
#endif
    }
    
    public func configureNavigationBar() {
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = PMColor.white
            appearance.titleTextAttributes = [.foregroundColor: PMColor.themeOrange]
            appearance.largeTitleTextAttributes = [.foregroundColor: PMColor.themeOrange]
            
            UINavigationBar.appearance().tintColor = PMColor.themeOrange
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().compactAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        } else {
            UINavigationBar.appearance().tintColor = PMColor.themeOrange
            UINavigationBar.appearance().barTintColor = PMColor.themeOrange
            UINavigationBar.appearance().isTranslucent = false
        }
    }
    
    func registerRemoteNotificaton(_ application: UIApplication) {
        
        if #available(iOS 10.0, *) {
            
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }
    
    //------------------------------------------------------
    
    //MARK: - UNUserNotificationCenterDelegate
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        #if DEBUG
        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.sandbox)//FIXME: Development
        #else        
        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)
        #endif
        
        Messaging.messaging().apnsToken = deviceToken
        PreferenceManager.shared.deviceToken = deviceToken.hexString
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        debugPrint("didFailToRegisterForRemoteNotificationsWithError: \(error.localizedDescription)")
        
        #if !targetEnvironment(simulator)
        DisplayAlertManager.shared.displayAlert(animated: true, message: error.localizedDescription, handlerOK: nil)
        #endif
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let data = notification.request.content.userInfo
        debugLog(object: data)
        
        guard FirebaseManager.shared.isSignedIn() else {
            completionHandler(pushNotificationOptionEmpty)
            return
        }
        
        //let keyData = "data"
        let keyType = "type"
                
        if let messageType = data[keyType] as? String {
            let fcmNotificationType: FCMNotificationType = FCMNotificationType(rawValue: Int(messageType) ?? .zero) ?? .none
            switch fcmNotificationType {
            case .message:
                if UIApplication.topViewController() is ChatVC {
                    completionHandler(pushNotificationOptionEmpty)
                    return
                }
            case .none:
                break
            case .playdateCreated:
                break
            case .playdateGroupRequestSend:
                break
            case .playdateGroupRequestAccepted:
                break
            case .playdateGroupRequestRejected:
                break
            case .playdateNonGroupRequestSend:
                break
            case .playdateNonGroupRequestAccepted:
                break
            case .playdateNonGroupRequestRejected:
                break
            case .babysitterRequestSend:
                break
            case .babysitterRequestAccepted:
                break
            case .babysitterRequestRejected:
                break
            case .babysitterProfileApproved:                
                break
            case .parents:
                break
            case .babysitter:
                break
            }
        }
        completionHandler(pushNotificationOption)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
                
        debugLog(object: response.notification.request.content.userInfo)
        
        pendingPayload = response.notification.request.content.userInfo
        NavigationManager.shared.handleRemote(data: response.notification.request.content.userInfo)
        
        /*if UIApplication.shared.applicationState == .inactive {
            pendingPayload = response.notification.request.content.userInfo
        } else {
            NavigationManager.shared.handleRemote(data: response.notification.request.content.userInfo)
        }*/
        completionHandler()
    }
    
    //------------------------------------------------------
    
    //MARK: MessagingDelegate
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        debugLog(object: fcmToken)
        PreferenceManager.shared.fcmToken = fcmToken
        
        if FirebaseManager.shared.isSignedIn() {
            FirebaseManager.shared.find(parentFrom: PreferenceManager.shared.userId ?? String()) { dict in
                let baseModal = BaseModal(fromDictionary: dict)
                if baseModal.role == RoleType.parent.rawValue {
                    let parentModal = ParentsModal(fromDictionary: dict)
                    FirebaseManager.shared.save(parent: parentModal, of: parentModal.userid)
                } else if baseModal.role == RoleType.babysitter.rawValue {
                    let babysitterModal = BabysitterModal(fromDictionary: dict)
                    FirebaseManager.shared.save(babysitter: babysitterModal, of: babysitterModal.userid)
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIApplicationDelegate
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        configureKeboard()
        getCustomFontDetails()
        configureNavigationBar()
        
        //RealmManager.shared.save(channelDownload: false)
        window?.tintColor = PMColor.black
        
        FirebaseManager.configure()
        
        Messaging.messaging().delegate = self
        
        let remoteNotif = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: Any]
            if remoteNotif != nil {
                pendingPayload = remoteNotif
            }
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        
        var result: Bool = false
        if GIDSignIn.sharedInstance.handle(url) {
            result = true
        }
        if ApplicationDelegate.shared.application(application, open: url, options: options) {
            result = true
        }
        return result
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        
        return .portrait
    }
    
    //------------------------------------------------------
}

