//
//  BaseVC.swift
//  Renov8
//
//  Created by Dharmesh Avaiya on 1/1/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import Firebase

class BaseVC : UIViewController, UIGestureRecognizerDelegate {
    
    var currentUser: User? {
        return FirebaseManager.shared.currentUser()
    }
    
    var parentModal: ParentsModal?
    var babysitterModal: BabysitterModal?
    
    var isParentProfile: Bool {
        return parentModal?.role == RoleType.parent.rawValue || babysitterModal?.role == RoleType.parent.rawValue
    }
    
    var isNotificationEnabled: Bool {
        if parentModal?.role == RoleType.parent.rawValue {
            return parentModal?.isNotificationEnabled == "1"
        } else if babysitterModal?.role == RoleType.parent.rawValue {
            return babysitterModal?.isNotificationEnabled == "1"
        }
        return false
    }
        
    var isLocationEnabled: Bool {
        if parentModal?.role == RoleType.parent.rawValue {
            return parentModal?.isLocationEnable == "1"
        } else if babysitterModal?.role == RoleType.parent.rawValue {
            return babysitterModal?.isLocationEnable == "1"
        }
        return false
    }
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //------------------------------------------------------
    
    deinit { //same like dealloc in ObjectiveC
        
        parentModal = nil
        babysitterModal = nil
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    public func push(controller: UIViewController, animated: Bool = true) {
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(controller, animated: animated)
        }
    }
    
    public func pushWithHandler(controller: UIViewController, animated: Bool = true , complition : @escaping () -> Void  ) {
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(controller, animated: animated)
        }
    }
    
    public func pop(animated: Bool = true) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: animated)
        }
    }
    
    public func pushWithoutAnimate(controller: UIViewController, animated: Bool = false) {
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(controller, animated: animated)
        }
    }
    
    public func popWithoutAnimate(animated: Bool = false) {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: animated)
        }
    }
    
    // pop back n viewcontroller
    public func popBack(_ nb: Int) {
        DispatchQueue.main.async {
            if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
                guard viewControllers.count < nb else {
                    self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: false)
                    return
                }
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func handleError(code: Int?) {
        
        if code == 105 {
            
            let message = localized(code: code ?? 0)
            DisplayAlertManager.shared.displayAlert(animated: true, message: message, handlerOK: {
                //NavigationManager.shared.setupSingInOption()
            })
            
        } else {
            
            let message = localized(code: code ?? 0)
            DisplayAlertManager.shared.displayAlert(animated: true, message: message, handlerOK: nil)
        }
    }
    
    func refreshUserDataIfNeeded() {
        
        if FirebaseManager.shared.isSignedIn() {
            FirebaseManager.shared.find(parentFrom: PreferenceManager.shared.userId ?? String()) { dict in
                let baseModal = BaseModal(fromDictionary: dict)
                if baseModal.role == RoleType.parent.rawValue {
                    let parentModal = ParentsModal(fromDictionary: dict)
                    FirebaseManager.shared.parentModal = parentModal
                    self.parentModal = FirebaseManager.shared.parentModal
                } else if baseModal.role == RoleType.babysitter.rawValue {
                    let babysitterModal = BabysitterModal(fromDictionary: dict)
                    FirebaseManager.shared.babysitterModal = babysitterModal
                    self.babysitterModal = FirebaseManager.shared.babysitterModal
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .default
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        parentModal = FirebaseManager.shared.parentModal
        babysitterModal = FirebaseManager.shared.babysitterModal
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        refreshUserDataIfNeeded()
    }
    
    //------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //------------------------------------------------------
}
