//
//  SegmentView.swift
//  SessionControl
//
//  Created by Dharmesh Avaiya on 3/31/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

protocol SegmentViewDelegate {
    
    func segment(view: SegmentView, didChange flag: Bool)
}

class SegmentView: UIView {
    
    
    @IBOutlet weak var separater: UIView!
    @IBOutlet weak var btn: PoppinsSemiBoldButton!
    
    var isSelected: Bool = false {
        didSet {
            if isSelected {
               
                btn.setTitleColor(PMColor.segmentSelected, for: UIControl.State.normal)
                separater.backgroundColor = #colorLiteral(red: 0.9443284869, green: 0.4229181409, blue: 0.1275268793, alpha: 1)
            } else {
               
                btn.setTitleColor(PMColor.segmentUnselect, for: UIControl.State.normal)
                separater.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
        }
    }
    
    var delegate: SegmentViewDelegate?
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    
    @IBAction func btnTap(_ sender: Any) {
        if isSelected {
            return
        }
        isSelected.toggle()
        delegate?.segment(view: self, didChange: true)
    }
    
    //------------------------------------------------------
}
