//
//  PMLabel.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 25/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit

class PMBaseLabel: UILabel {

    private var fontDefaultSize: CGFloat {
        return font.pointSize
    }
    
    public var fontSize: CGFloat = 0
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
       fontSize = getDynamicFontSize(fontDefaultSize: fontDefaultSize)
    }
}

class PMSFProDisplayRegularLabel: PMBaseLabel {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.font = PMFont.sfProDisplayRegular(size: self.fontSize)
    }
}

class PMSFProDisplayBoldLabel: PMBaseLabel {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.font = PMFont.sfProDisplayBold(size: self.fontSize)
    }
}
    
    class PoppinsLightLabel: PMBaseLabel {

        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            
            self.font = PMFont.sfpoppinslight(size: self.fontSize)
        }
    }
    
    class PoppinsmediumLabel: PMBaseLabel {

        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            
            self.font = PMFont.sfpoppinsmedium(size: self.fontSize)
        }
    }
    
    class PoppinsRegularLabel: PMBaseLabel {

        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            
            self.font = PMFont.sfpoppinsRegular(size: self.fontSize)
        }
    }
    
    class PoppinsSemiBoldLabel: PMBaseLabel {

        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            
            self.font = PMFont.sfpoppinsSemiBold(size: self.fontSize)
        }
    }
    class PoppinsBoldLabel: PMBaseLabel {
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            self.font = PMFont.sfpoppinsBold(size: self.fontSize)
        }
    }
    class FredokaOneRegularLabel: PMBaseLabel {
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            self.font = PMFont.sfFredokaRegular(size: self.fontSize)
        }
    }
//class LatoRegular: PMBaseLabel {
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//
//        self.font = PMFont.sfLatoRegular(size: self.fontSize)
//    }
//}
class PoppinsExtraBoldLabel: PMBaseLabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.font = PMFont.sfpoppinsExtraBold(size: self.fontSize)
    }
}
 

