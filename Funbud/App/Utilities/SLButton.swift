//
//  SLButton.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 25/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation
import UIKit

class SLBaseButton: UIButton {

    var fontDefaultSize : CGFloat {
        return self.titleLabel?.font.pointSize ?? 0.0
    }
    var fontSize : CGFloat = 0.0
    
    /// common lable layout
    /// - Parameter aDecoder: <#aDecoder description#>
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fontSize = getDynamicFontSize(fontDefaultSize: fontDefaultSize)
    }
}

class SLSFProDisplayRegularButton: SLBaseButton {

    /// common lable layout
    ///
    /// - Parameter aDecoder: <#aDecoder description#>
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.titleLabel?.font = PMFont.sfProDisplayRegular(size: self.fontSize)
        self.imageView?.contentMode = .scaleAspectFit
    }
}

class SLSFProDisplayBoldButton: SLBaseButton {

    /// common lable layout
    ///
    /// - Parameter aDecoder: <#aDecoder description#>
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.titleLabel?.font = PMFont.sfProDisplayBold(size: self.fontSize)
        self.imageView?.contentMode = .scaleAspectFit
    }
}

class PoppinsLightButton: SLBaseButton {

    /// common lable layout
    ///
    /// - Parameter aDecoder: <#aDecoder description#>
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.titleLabel?.font = PMFont.sfpoppinslight(size: self.fontSize)
        self.imageView?.contentMode = .scaleAspectFit
    }
}

class PoppinsmediumButton: SLBaseButton {
    /// common lable layout
    ///
    /// - Parameter aDecoder: <#aDecoder description#>
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.titleLabel?.font = PMFont.sfpoppinsmedium(size: self.fontSize)
        self.imageView?.contentMode = .scaleAspectFit
    }
}

class PoppinsRegularButton: SLBaseButton {

    /// common lable layout
    ///
    /// - Parameter aDecoder: <#aDecoder description#>
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.titleLabel?.font = PMFont.sfpoppinsRegular(size: self.fontSize)
        self.imageView?.contentMode = .scaleAspectFit
    }
}

class PoppinsSemiBoldButton: SLBaseButton {

    /// common lable layout
    ///
    /// - Parameter aDecoder: <#aDecoder description#>
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.titleLabel?.font = PMFont.sfpoppinsSemiBold(size: self.fontSize)
        self.imageView?.contentMode = .scaleAspectFit
    }
}

class PoppinsBoldButton: SLBaseButton {

    /// common lable layout
    ///
    /// - Parameter aDecoder: <#aDecoder description#>
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.titleLabel?.font = PMFont.sfpoppinsBold(size: self.fontSize)
        self.imageView?.contentMode = .scaleAspectFit
    }
}

class FredokaOneRegularButton: SLBaseButton {

    /// common lable layout
    ///
    /// - Parameter aDecoder: <#aDecoder description#>
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.titleLabel?.font = PMFont.sfFredokaRegular(size: self.fontSize)
        self.imageView?.contentMode = .scaleAspectFit
    }
}

//class LatoRegularButton: SLBaseButton {
//
//    /// common lable layout
//    ///
//    /// - Parameter aDecoder: <#aDecoder description#>
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        
//        self.titleLabel?.font = PMFont.sfLatoRegular(size: self.fontSize)
//        self.imageView?.contentMode = .scaleAspectFit
//    }
//}

class PoppinsExtraBoldButton: SLBaseButton {

    /// common lable layout
    ///
    /// - Parameter aDecoder: <#aDecoder description#>
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.titleLabel?.font = PMFont.sfpoppinsExtraBold(size: self.fontSize)
        self.imageView?.contentMode = .scaleAspectFit
    }
}






