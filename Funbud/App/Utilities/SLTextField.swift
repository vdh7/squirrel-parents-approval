//
//  SLTextField.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 25/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import Foundation
import UIKit
import FlagPhoneNumber

class SLBaseTextField: UITextField {
    
    var fontDefaultSize : CGFloat {
        return font?.pointSize ?? 0.0
    }
    
    public var fontSize : CGFloat = 0.0
    
    public var padding: Double = 13
    
    private var leftEmptyView: UIView {
        return UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: padding, height: Double.zero)))
    }
    
    private var leftButton : UIButton {
        return UIButton(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: padding, height: Double.zero)))
    }
    
    /// common text field layout for inputs
    ///
    /// - Parameter aDecoder: <#aDecoder description#>
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       fontSize = getDynamicFontSize(fontDefaultSize: fontDefaultSize)
    }
    private var rightEmptyViewForButton : UIView {
        return leftButton
    }
    
    private var rightEmptyView: UIView {
        return leftEmptyView
    }
}


class SLSFProDisplayRegularTextField: SLBaseTextField {

    /// common text field layout for inputs
    ///
    /// - Parameter aDecoder: aDecoder description
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.font = PMFont.sfProDisplayRegular(size: fontSize)
    }
}

class SLSFProDisplayBoldTextField: SLBaseTextField {
    /// common text field layout for inputs
    ///
    /// - Parameter aDecoder: aDecoder description
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.font = PMFont.sfProDisplayBold(size: fontSize)
    }
}

class SLGenderTextField: SLBaseTextField, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var leftUserView: UIView {
        let imgView = UIImageView(image: UIImage(named: ""))
        imgView.contentMode = .scaleAspectFit
        return imgView
    }
    
    let paddings = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 5)
    private static let height: CGFloat = 20
    private static let crossButtonSize = CGSize(width: height, height: height)
    private let crossButtonView = UIButton(frame: CGRect(origin: CGPoint.zero, size: crossButtonSize))
    
    let pvGender = UIPickerView()
    //let pvOptions: [String] = ["Agender", "Bigender", "Cisgender", "Gender Expression", "Gender Fluid", "Genderqueer", "Intersex", "Gender Variant", "Mx.", "Third Gender", "Transgender", "Two-Spirit", "Ze / Hir"]
    let pvOptions: [String] = [GenderType.male.rawValue, GenderType.female.rawValue, GenderType.other.rawValue]
    var selectedOption: String? {
        didSet {
            self.text = selectedOption
            if let selectedOption = selectedOption, let index = pvOptions.firstIndex(of: selectedOption)  {
                pvGender.selectRow(index, inComponent: .zero, animated: false)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        leftView = leftUserView
        self.keyboardType = .default
        self.autocorrectionType = .no
        self.autocapitalizationType = .words
        
        
        pvGender.dataSource = self
        pvGender.delegate = self
        inputView = pvGender
        
        crossButtonView.contentMode = .center
        crossButtonView.setImage(UIImage(named: ""), for: UIControl.State.normal)
        
        if let selectedOption = selectedOption {
            if let index = pvOptions.firstIndex(of: selectedOption) {
                pvGender.selectedRow(inComponent: index)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Override
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(origin: CGPoint(x: self.bounds.width - CGFloat(padding * 4), y: CGFloat(padding * 1.6)), size: CGSize(width: CGFloat(padding) * 2.0, height: bounds.height -  CGFloat(padding * 3.2)))
        
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if selectedOption == nil {
            selectedOption = pvOptions.first
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pvOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pvOptions[row]
    }
    
    //------------------------------------------------------
    
    //MARK: UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedOption = pvOptions[row]
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    /// common text field layout for inputs
    ///
    /// - Parameter aDecoder: aDecoder description
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.delegate = self
    }
}


class SLRelationshipTextField: SLBaseTextField, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var leftUserView: UIView {
        let imgView = UIImageView(image: UIImage(named: ""))
        imgView.contentMode = .scaleAspectFit
        return imgView
    }
    let paddings = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 5)
    private static let height: CGFloat = 20
    private static let crossButtonSize = CGSize(width: height, height: height)
    private let crossButtonView = UIButton(frame: CGRect(origin: CGPoint.zero, size: crossButtonSize))
    
    let pvGender = UIPickerView()
    let pvOptions: [String] = ParentsType.allCases.map( { $0.rawValue })
    var selectedOption: String? {
        didSet {
            self.text = selectedOption
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        leftView = leftUserView
        self.keyboardType = .default
        self.autocorrectionType = .no
        self.autocapitalizationType = .words
        
        
        pvGender.dataSource = self
        pvGender.delegate = self
        inputView = pvGender
        
        crossButtonView.contentMode = .center
        crossButtonView.setImage(UIImage(named: ""), for: UIControl.State.normal)
    }
    
    //------------------------------------------------------
    
    //MARK: Override
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(origin: CGPoint(x: self.bounds.width - CGFloat(padding * 4), y: CGFloat(padding * 1.6)), size: CGSize(width: CGFloat(padding) * 2, height: bounds.height -  CGFloat(padding * 3.2)))
        
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if selectedOption == nil {
            selectedOption = pvOptions.first
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pvOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pvOptions[row]
    }
    
    //------------------------------------------------------
    
    //MARK: UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedOption = pvOptions[row]
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    /// common text field layout for inputs
    ///
    /// - Parameter aDecoder: aDecoder description
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.delegate = self
    }
}

class FGBirthDateTextField: SLBaseTextField, UITextFieldDelegate {
    
    var rightUserView: UIView {
        let imgView = UIImageView(image: UIImage(named: ""))
        imgView.contentMode = .scaleAspectFit
        return imgView
    }
    let paddings = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 5)
    private static let height: CGFloat = 20
    private static let crossButtonSize = CGSize(width: height, height: height)
    private let crossButtonView = UIButton(frame: CGRect(origin: CGPoint.zero, size: crossButtonSize))
    
    let dpDate = UIDatePicker()
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        rightView = rightUserView
        
        self.keyboardType = .default
        self.autocorrectionType = .no
        self.autocapitalizationType = .words
        dpDate.datePickerMode = .date
        dpDate.maximumDate = Date()
        //dpDate.minimumDate = DateTimeManager.shared.date(byAdding: -110)
        dpDate.minimumDate = DateTimeManager.shared.date(byAddingYear: -110)
        if #available(iOS 13.4, *) {
            dpDate.preferredDatePickerStyle = .wheels
            dpDate.backgroundColor = .white
            dpDate.setValue(UIColor.black, forKeyPath: "textColor")
            dpDate.setValue(false, forKeyPath: "highlightsToday")
            
        } else {
            // Fallback on earlier versions
        }
        inputView = dpDate
        dpDate.setDate(Date(), animated: false)
        crossButtonView.contentMode = .center
        crossButtonView.setImage(UIImage(named: ""), for: UIControl.State.normal)
        
    }
    
    //------------------------------------------------------
    
    //MARK: Override
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(origin: CGPoint(x: self.bounds.width - CGFloat(padding * 4), y: CGFloat(padding * 1.6)), size: CGSize(width: CGFloat(padding) * 2.5, height: bounds.height -  CGFloat(padding * 3.2)))
        
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.text = DateTimeManager.shared.stringFrom(date: dpDate.date, inFormate: DateFormate.MMM_DD_COM_yyyy)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.text = DateTimeManager.shared.stringFrom(date: dpDate.date, inFormate: DateFormate.MMM_DD_COM_yyyy)
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    /// common text field layout for inputs
    ///
    /// - Parameter aDecoder: aDecoder description
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.delegate = self
    }
}

class FGDateTextField: SLBaseTextField, UITextFieldDelegate {
    
    var rightUserView: UIView {
        let imgView = UIImageView(image:UIImage(named: ""))
        imgView.contentMode = .scaleAspectFit
        return imgView
    }
    
    let paddings = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    private static let height: CGFloat = 20
    private static let crossButtonSize = CGSize(width: height, height: height)
    private let crossButtonView = UIButton(frame: CGRect(origin: CGPoint.zero, size: crossButtonSize))
    let dpDate = UIDatePicker()
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        rightView = rightUserView
        
        self.keyboardType = .default
        self.autocorrectionType = .no
        self.autocapitalizationType = .words
        dpDate.datePickerMode = .date
        //dpDate.maximumDate = Date()
        //dpDate.minimumDate = DateTimeManager.shared.date(byAdding: -110)
        if #available(iOS 13.4, *) {
            dpDate.preferredDatePickerStyle = .wheels
            dpDate.backgroundColor = .white
            //dpDate.setValue(UIColor.black, forKeyPath: "textColor")
            dpDate.setValue(false, forKeyPath: "highlightsToday")
            
        } else {
            // Fallback on earlier versions
        }
        inputView = dpDate
        dpDate.setDate(Date(), animated: false)
        crossButtonView.contentMode = .center
        crossButtonView.setImage(UIImage(named: ""), for: UIControl.State.normal)
        
        tintColor = UIColor.clear        
    }
    
    //------------------------------------------------------
    
    //MARK: Override
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(origin: CGPoint(x: self.bounds.width - CGFloat(padding * 4), y: CGFloat(padding * 1.6)), size: CGSize(width: CGFloat(padding) * 4, height: frame.height -  CGFloat(padding * 3.2)))
        
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.text = DateTimeManager.shared.stringFrom(date: dpDate.date, inFormate: DateFormate.MMM_DD_COM_yyyy)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.text = DateTimeManager.shared.stringFrom(date: dpDate.date, inFormate: DateFormate.MMM_DD_COM_yyyy)
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    /// common text field layout for inputs
    ///
    /// - Parameter aDecoder: aDecoder description
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.delegate = self
    }
}

class FGTimeTextField: SLBaseTextField, UITextFieldDelegate {
    
    var rightUserView: UIView {
        let imgView = UIImageView(image:UIImage(named: ""))
        imgView.contentMode = .scaleAspectFit
        return imgView
    }
    let paddings = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    private static let height: CGFloat = 20
    private static let crossButtonSize = CGSize(width: height, height: height)
    private let crossButtonView = UIButton(frame: CGRect(origin: CGPoint.zero, size: crossButtonSize))
    
    let dpDate = UIDatePicker()
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        rightView = rightUserView
        
        self.keyboardType = .default
        self.autocorrectionType = .no
        self.autocapitalizationType = .words
        dpDate.datePickerMode = .time
        //dpDate.maximumDate = Date()
        //dpDate.minimumDate = DateTimeManager.shared.date(byAdding: -110)
        dpDate.minimumDate = DateTimeManager.shared.date(byAddingYear: -110)
        dpDate.maximumDate = DateTimeManager.shared.date(byAddingHours: 168, toDate: Date())
        if #available(iOS 13.4, *) {
            dpDate.preferredDatePickerStyle = .wheels
            dpDate.backgroundColor = .white
            dpDate.setValue(UIColor.black, forKeyPath: "textColor")
            dpDate.setValue(false, forKeyPath: "highlightsToday")
            
        } else {
            // Fallback on earlier versions
        }
        inputView = dpDate
        dpDate.setDate(Date(), animated: false)
        crossButtonView.contentMode = .center
        crossButtonView.setImage(UIImage(named: ""), for: UIControl.State.normal)
        
        tintColor = UIColor.clear
    }
    
    //------------------------------------------------------
    
    //MARK: Override
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(origin: CGPoint(x: self.bounds.width - CGFloat(padding * 4), y: CGFloat(padding * 1.6)), size: CGSize(width: CGFloat(padding) * 4, height: bounds.height -  CGFloat(padding * 3.2)))
        
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.text = DateTimeManager.shared.stringFrom(date: dpDate.date, inFormate: TimeFormate.HH_MM)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.text = DateTimeManager.shared.stringFrom(date: dpDate.date, inFormate: TimeFormate.HH_MM)
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    /// common text field layout for inputs
    ///
    /// - Parameter aDecoder: aDecoder description
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.delegate = self
    }
}

class ATCTextField: UITextField {
    let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 5)
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

class ATCLocationTextField: UITextField {
    let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 40)
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}


// simply have this in any Swift file, say, Handy.swift

private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
               return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.prefix(maxLength).decomposedStringWithCanonicalMapping
    }
}

@IBDesignable class UITextViewFixed: UITextView {
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    func setup() {
        textContainerInset = UIEdgeInsets.zero
        textContainer.lineFragmentPadding = 0
    }
}

class SQPhoneTextField: FPNTextField {
    /*let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 5)
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }*/

}

protocol FGDayTextFieldDelegate {
    
    func fgDay(textField: FGDayTextField, didEndEditing value: DaysType?)
}

class FGDayTextField: SLBaseTextField, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var rightUserView: UIView {
        let imgView = UIImageView(image:UIImage(named: ""))
        imgView.contentMode = .scaleAspectFit
        return imgView
    }
    
    
    let paddings = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    private static let height: CGFloat = 20
    private static let crossButtonSize = CGSize(width: height, height: height)
    private let crossButtonView = UIButton(frame: CGRect(origin: CGPoint.zero, size: crossButtonSize))
    
    var days: [DaysType] = DaysType.allCases
    let pickerView = UIPickerView()
    var selectedDay: DaysType?
    
    var daysDelegate: FGDayTextFieldDelegate?
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        rightView = rightUserView
        
        self.keyboardType = .default
        self.autocorrectionType = .no
        self.autocapitalizationType = .words

        inputView = pickerView
        pickerView.dataSource = self
        pickerView.delegate = self
        crossButtonView.contentMode = .center
        crossButtonView.setImage(UIImage(named: ""), for: UIControl.State.normal)
        
        tintColor = .clear
        
    }
    
    //------------------------------------------------------
    
    //MARK: Override
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(origin: CGPoint(x: self.bounds.width - CGFloat(padding * 4), y: CGFloat(padding * 1.6)), size: CGSize(width: CGFloat(padding) * 4, height: frame.height -  CGFloat(padding * 3.2)))
        
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if selectedDay == nil {
            selectedDay =  days.first
        }
        textField.text = selectedDay?.rawValue
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        daysDelegate?.fgDay(textField: self, didEndEditing: selectedDay)
    }
    
    //------------------------------------------------------
    
    //MARK: UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return days.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return days[row].rawValue
    }
    
    //------------------------------------------------------
    
    //MARK: UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selectedDay = days[row]
        self.text = selectedDay?.rawValue
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    /// common text field layout for inputs
    ///
    /// - Parameter aDecoder: aDecoder description
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.delegate = self
    }
}

protocol FGTimeSlotTextFieldDelegate {
    
    func fgTimeslot(textField: FGTimeSlotTextField, didEndEditing from: TimeSlotTypes?, to: TimeSlotTypes?)
    func fgTimeslot(textField: FGTimeSlotTextField, didRightViewTap from: TimeSlotTypes?, to: TimeSlotTypes?)
}

class FGTimeSlotTextField: SLBaseTextField, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    let paddings = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    let height: CGFloat = 20
    
    var rightUserView: UIView {
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: "BSCross"), for: .normal)
        btn.contentMode = .scaleAspectFit
        btn.addTarget(self, action: #selector(rightViewTap), for: .touchUpInside)
        btn.frame = CGRect(origin: .zero, size: CGSize(width: height * 2, height: height))
        return btn
    }
    
    var timeslots: [TimeSlotTypes] = TimeSlotTypes.allCases
    let pickerView = UIPickerView()
    
    var selectedFrom: TimeSlotTypes? {
        didSet {
            if selectedFrom != nil {
                rightView?.isHidden = false
            } else {
                rightView?.isHidden = true
            }
        }
    }
    var selectedTo: TimeSlotTypes? {
        didSet {
            if selectedTo != nil {
                rightView?.isHidden = false
            } else {
                rightView?.isHidden = true
            }
        }
    }
    
    var timeslotDelegate: FGTimeSlotTextFieldDelegate?
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        rightView = rightUserView
        
        self.keyboardType = .default
        self.autocorrectionType = .no
        self.autocapitalizationType = .words
        
        inputView = pickerView
        pickerView.dataSource = self
        pickerView.delegate = self
        rightViewMode = .unlessEditing
        
        tintColor = .clear
    }
    
    //------------------------------------------------------
    
    //MARK: Override
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        let width =  rightUserView.bounds.width
        return CGRect(origin: CGPoint(x:self.bounds.width - width + padding + 5, y:.zero), size: CGSize(width: width, height: frame.height))
        
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    //------------------------------------------------------
    
    //MARK: UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if selectedFrom == nil {
            selectedFrom =  timeslots.first
        }
        if selectedTo == nil {
            selectedTo =  timeslots.first
        }
        
        var stringToDisplay = selectedFrom?.rawValue ?? String()
        if selectedTo != nil {
            stringToDisplay = stringToDisplay.appending("- \(selectedTo?.rawValue ?? String())")
        }
        textField.text = stringToDisplay
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        var stringToDisplay = selectedFrom?.rawValue ?? String()
        if selectedTo != nil {
            stringToDisplay = stringToDisplay.appending("- \(selectedTo?.rawValue ?? String())")
        }
        textField.text = stringToDisplay
        
        timeslotDelegate?.fgTimeslot(textField: self, didEndEditing: selectedFrom, to: selectedTo)
    }
    
    //------------------------------------------------------
    
    //MARK: UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return timeslots.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return timeslots[row].rawValue
    }
    
    //------------------------------------------------------
    
    //MARK: UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if component == .zero {
            selectedFrom = timeslots[row]
        } else {
            selectedTo = timeslots[row]
        }
        //self.text = selectedFrom?.rawValue
        var stringToDisplay = selectedFrom?.rawValue ?? String()
        if selectedTo != nil {
            stringToDisplay = stringToDisplay.appending("- \(selectedTo?.rawValue ?? String())")
        }
        self.text = stringToDisplay
    }
    
    //------------------------------------------------------
    
    //MARK: Selector
    
    @objc func rightViewTap(_ sender: Any) {
        
        timeslotDelegate?.fgTimeslot(textField: self, didRightViewTap: selectedFrom, to: selectedTo)
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    /// common text field layout for inputs
    ///
    /// - Parameter aDecoder: aDecoder description
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.delegate = self
    }
}

protocol FGCalendarTextFieldDelegate {
    
    func fgDay(textField: FGCalendarTextField, didEndEditing value: DaysType?)
    func fgDay(textField: FGCalendarTextField, didRightViewTap value: DaysType?)
}

class FGCalendarTextField: SLBaseTextField {
    
    let paddings = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    let height: CGFloat = 20
    
    var rightUserView: UIView {
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: "BSCross"), for: .normal)
        btn.contentMode = .scaleAspectFit
        btn.addTarget(self, action: #selector(rightViewTap), for: .touchUpInside)
        btn.frame = CGRect(origin: .zero, size: CGSize(width: height * 2, height: height))
        return btn
    }
    
    var days: [DaysType] = DaysType.allCases
    let pickerView = UIPickerView()
    var selectedDay: DaysType? {
        didSet {
            if selectedDay != nil {
                rightView?.isHidden = false
            } else {
                rightView?.isHidden = true
            }
        }
    }
    
    var daysDelegate: FGCalendarTextFieldDelegate?
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        rightView = rightUserView
        
        self.keyboardType = .default
        self.autocorrectionType = .no
        self.autocapitalizationType = .words
        rightViewMode = .unlessEditing
        
        tintColor = .clear
    }
    
    //------------------------------------------------------
    
    //MARK: Override
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        let width =  rightUserView.bounds.width
        return CGRect(origin: CGPoint(x:self.bounds.width - width + padding + 5, y:.zero), size: CGSize(width: width, height: frame.height))
        
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: paddings)
    }
    
    //------------------------------------------------------
    
    //MARK: Selector
    
    @objc func rightViewTap(_ sender: Any) {
        daysDelegate?.fgDay(textField: self, didRightViewTap: selectedDay)
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    /// common text field layout for inputs
    ///
    /// - Parameter aDecoder: aDecoder description
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    //------------------------------------------------------
}
