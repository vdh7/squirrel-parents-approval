//
//  FirebaseManager.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 9/1/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class FirebaseManager: NSObject {
    
    let firestore = Firestore.firestore()
    let storage = Storage.storage()
    
    let nodeSession: String = "Session"
    let nodeParents: String = "Parents"
    let nodeKids: String = "Kids"
    let nodeLists: String = "Lists"
    let nodeEvents: String = "Events"
    let nodeConversations: String = "Conversations"
    let nodeUserImages: String = "user_images"
    let nodeUserImageIds: String = "user_image_ids"
    let nodeBabysitters: String = "Babysitters"
    let nodeRequests: String = "Requests"
    let nodeMessages: String = "messages"
    let nodeParentsRating: String = "ParentsRating"
    
    var parentModal: ParentsModal?
    var babysitterModal: BabysitterModal?
    
    private var isParentProfile: Bool {
        return parentModal?.role == RoleType.parent.rawValue || babysitterModal?.role == RoleType.parent.rawValue
    }
    
    //------------------------------------------------------
    
    //MARK: Shared
    
    static let shared = FirebaseManager()
    
    //------------------------------------------------------
    
    //MARK: Configure
    
    public static func configure() {
        
        FirebaseApp.configure()
        
        //Firestore offline support
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = true
        settings.cacheSizeBytes = FirestoreCacheSizeUnlimited
        Firestore.firestore().settings = settings
        
        //Database offline support
        //Database.database().isPersistenceEnabled = true
        debugLog(object: FieldValue.serverTimestamp())
    }
    
    public func getServerTimestamp(completion: @escaping(_ timestamp: Timestamp)->Void) {
        let key = "serverTimestamp"
        let saveSession = firestore.collection(key).document(nodeSession)
        saveSession.setData([key: FieldValue.serverTimestamp()]) { error in
            let receivedSesssion = self.firestore.collection(key).document(self.nodeSession)
            receivedSesssion.getDocument { snapshot, error in
                if let timestamp = snapshot?.data()?[key] as? Timestamp {
                    completion(timestamp)
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: SignIn / Signout
    
    public func isSignedIn() -> Bool {
        //return Auth.auth().currentUser != nil
        return PreferenceManager.shared.userId != nil || PreferenceManager.shared.userId?.isEmpty == false
    }
    
    public func currentUser() -> User? {
        return Auth.auth().currentUser
    }
    
    public func signIn(withUsername username: String, passowrd: String, completion: @escaping(_ error: Error?)->()) {
        
        Auth.auth().signIn(withEmail: username, password: passowrd) { (authData: AuthDataResult?, error: Error?) in
            
            PreferenceManager.shared.userId = authData?.user.uid
            completion(error)
        }
    }
    
    public func signIn(withPhone number: String, completion: @escaping(_ verificationID: String?,_ error: Error?)->()) {
        PhoneAuthProvider.provider().verifyPhoneNumber((number), uiDelegate: nil) { (verificationID, error) in
            completion(verificationID, error)
        }
    }
    
    public func signIn(withCredential credential: AuthCredential, completion: @escaping(_ error: Error?)->()) {
        
        Auth.auth().signIn(with: credential) { (authData: AuthDataResult?, error: Error?) in
            PreferenceManager.shared.userId = authData?.user.uid
            completion(error)
        }
    }
    
    public func signOut() throws {
        
        FirebaseManager.shared.unsubscribeEvents()
        debugLog(object: Auth.auth().currentUser as Any)
        FirebaseManager.shared.parentModal = nil
        FirebaseManager.shared.babysitterModal = nil
        PreferenceManager.shared.userId = nil
        return try Auth.auth().signOut()
    }
    
    public func resetUser(completion: @escaping(_ flag: Bool)->Void) {
        let listnerHandler = Auth.auth().addStateDidChangeListener { auth, user in
            if let user = user {
                debugLog(object: user)
                completion(false)
            } else {
                completion(true)
            }
        }
        Auth.auth().removeStateDidChangeListener(listnerHandler)
    }
    
    //------------------------------------------------------
    
    //MARK: Reset Password
    
    public func sendPasswordReset(withEmail email: String, completion: @escaping(_ error: Error?)->()) {
        
        Auth.auth().sendPasswordReset(withEmail: email) { (error: Error?) in
            completion(error)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Email Verification
    
    public func sendEmailVerification(completion: @escaping(_ error: Error?)->()) {
        
        Auth.auth().currentUser?.sendEmailVerification(completion: { (error: Error?) in
            completion(error)
        })
    }
    
    //------------------------------------------------------
    
    //MARK: Image Processing
    
    public func save(image firebaseId: String, fileName: String, imageData: Data, completion: @escaping(_ imageURL: URL?, _ error: Error?)->()) {
        
        let storageRef = storage.reference(withPath: nodeUserImages).child(firebaseId).child(fileName)
        _ = storageRef.putData(imageData, metadata: nil) { (metaData: StorageMetadata?, error: Error?) in
            if error == nil {
                storageRef.downloadURL { (imageURL: URL?, error: Error?) in
                    completion(imageURL, error)
                }
            } else {
                completion(nil, error)
            }
        }
    }
    
    public func save(imageId firebaseId: String, fileName: String, imageData: Data, completion: @escaping(_ imageURL: URL?, _ error: Error?)->()) {
        
        let storageRef = storage.reference(withPath: nodeUserImageIds).child(firebaseId).child(fileName)
        _ = storageRef.putData(imageData, metadata: nil) { (metaData: StorageMetadata?, error: Error?) in
            if error == nil {
                storageRef.downloadURL { (imageURL: URL?, error: Error?) in
                    completion(imageURL, error)
                }
            } else {
                completion(nil, error)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Parents
    
    func find(parentFrom firebaseId: String, completion: @escaping(_ dictionary: [String: Any]) -> Void) {
        
        let documentParent = firestore.collection(nodeParents).document(firebaseId)
        documentParent.getDocument { (documentSnapshop: DocumentSnapshot?, error:Error?) in
            let result = documentSnapshop?.data() ?? [:]
            if result.count == .zero {
                self.find(babysitterFrom: firebaseId) { dictResult in
                    completion(dictResult)
                }
            } else {
                completion(result)
            }
        }
    }
    
    func save(parent: ParentsModal, of firebaseId: String) {
        
        let documentParent = firestore.collection(nodeParents).document(firebaseId)
        parent.deviceToken = PreferenceManager.shared.fcmToken
        parent.phone = PreferenceManager.shared.phone
        
        var valueToStore = parent.toDictionary()
        valueToStore["updatedAt"] = FieldValue.serverTimestamp()
        documentParent.setData(valueToStore, merge: true)
    }
    
    func findDeviceToken(of userId: String, roleType: RoleType, completion:@escaping(_ baseModal: BaseModal) -> Void) {
        if roleType == .parent {
            let documentParent = firestore.collection(nodeParents).document(userId)
            documentParent.getDocument { (documentSnapshop: DocumentSnapshot?, error:Error?) in
                let result = documentSnapshop?.data() ?? [:]
                let parentObj = ParentsModal(fromDictionary: result)
                completion(parentObj)
            }
        } else if roleType == .babysitter {
            let documentBbsitter = firestore.collection(nodeBabysitters).document(userId)
            documentBbsitter.getDocument { (documentSnapshop: DocumentSnapshot?, error:Error?) in
                let result = documentSnapshop?.data() ?? [:]
                let bbsitterObj = BabysitterModal(fromDictionary: result)
                completion(bbsitterObj)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Kids
    
    func save(kid: KidsModal) -> String {
            
        var firebaseID = kid.firebaseID ?? String()
        //let parentID = kid.parentID ?? String()
        if ValidationManager.shared.isEmpty(text: firebaseID) {
            //firebaseID = firestore.collection(nodeKids).document(parentID).collection(nodeLists).document().documentID
            firebaseID = firestore.collection(nodeKids).document().documentID
        }
        kid.firebaseID = firebaseID
        var valueToStore = kid.toDictionary()
        valueToStore["updatedAt"] = FieldValue.serverTimestamp()
        //let document = firestore.collection(nodeKids).document(parentID).collection(nodeLists).document(firebaseID)
        let document = firestore.collection(nodeKids).document(firebaseID)
        document.setData(valueToStore, merge: true)
        return firebaseID
    }
    
    func update(kidIDs: [String], to parentID: String) {
        find(parentFrom: parentID) { dictionary in
            let parent = ParentsModal(fromDictionary: dictionary)
            parent.kidsList = kidIDs
            self.save(parent: parent, of: parentID)
        }
    }
    
    func find(kidsFrom parentId: String, completion:@escaping(_ kids: [KidsModal])->Void) {
        //let collectionKids = firestore.collection(nodeKids).document(parentId).collection(nodeLists)
            
        let q = firestore.collection(nodeKids)
            .whereField("parentID", isEqualTo: parentId)
            .order(by: "updatedAt", descending: false)
        
        q.getDocuments { (query: QuerySnapshot?, error:Error?) in
                var result: [KidsModal] = []
                for doc in query?.documents ?? [] {
                    let kid = KidsModal(fromDictionary: doc.data())
                    result.append(kid)
                }
                completion(result)
            }
    }
    
    func delete(kid: KidsModal, completion: @escaping(_ error: Error?)->Void) {
        let firebaseID = kid.firebaseID ?? String()
        let parentID = kid.parentID ?? String()
        //let document = firestore.collection(nodeKids).document(parentID).collection(nodeLists).document(firebaseID)
        find(parentFrom: parentID) { dictionary in
            
            //1. remove from parent
            let parent = ParentsModal(fromDictionary: dictionary)
            parent.kidsList.removeAll { kidsId in
                return kidsId == firebaseID
            }
            self.save(parent: parent, of: parentID)
            
            //2. remove from kids collection
            let document = self.firestore.collection(self.nodeKids).document(firebaseID)
            document.delete { error in
                completion(error)
            }
            
            //3. remove auto generate playdate from the events
            self.findAutogeneratedPlaydate(ofChildId: firebaseID) { (autogeneratedPlaydates: [PlayDatesModal]) in
                autogeneratedPlaydates.forEach { pd in
                    self.deleteBy(playdateId: pd.id) { error in
                    }
                }
            }
        }
    }
    
    func findKid(fromID firebaseID: String, completion: @escaping(_ kidsModal: KidsModal?) -> Void) {
        
        //iterate parents
        debugLog(object: firebaseID)
        let q = firestore.collection(nodeKids)
            .whereField("firebaseID", isEqualTo: firebaseID)
        
        q.getDocuments { snapshot, error in
            
            if let error = error {
                debugLog(object: error)
            }
            
            //check if parents exist
            if snapshot?.documents.count == .zero {
                completion(nil)
                return
            }
            
            if let dic = snapshot?.documents.first?.data() {
                let kidsModal = KidsModal(fromDictionary: dic)
                completion(kidsModal)
            } else {
                completion(nil)
            }
        }
        
        /*self.firestore.collection(nodeKids).getDocuments { qSnapShot, docError in

            //check if parents exist
            if qSnapShot?.documents.count == .zero {
                completion(nil)
            }
            
            //check parents
            for parent in qSnapShot?.documents ?? [] {
                let q = self.firestore.collection(self.nodeKids).document(parent.documentID).collection(self.nodeLists).whereField(self.nodeLists, isEqualTo: firebaseID)
                q.getDocuments { snapshot, error in
                    if let error = error {
                        debugPrint(error)
                    }
                    if snapshot?.documents.first != nil {
                        if let dic = snapshot?.documents.first?.data() {
                            let kidsModal = KidsModal(fromDictionary: dic)
                            completion(kidsModal)
                        }
                    } else {
                        completion(nil)
                    }
                }
            }
        }*/
    }
    
    func findPlaydateModal(byID playdateID: String, completion: @escaping(_ playdateModal: PlayDatesModal?)->Void) {
        
        let documentEvents = firestore.collection(nodeEvents).document(playdateID)
        documentEvents.getDocument { documentSnapshot, error in
            if let dict = documentSnapshot?.data() {
                completion(PlayDatesModal(fromDictionary: dict))
            } else {
                completion(nil)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Babysitter
    
    func find(babysitterFrom firebaseId: String, completion: @escaping(_ dictionary: [String: Any]) -> Void) {
        
        let documentParent = firestore.collection(nodeBabysitters).document(firebaseId)
        documentParent.getDocument { (documentSnapshop: DocumentSnapshot?, error:Error?) in
            let result = documentSnapshop?.data() ?? [:]
            completion(result)
        }
    }
    
    func save(babysitter: BabysitterModal, of firebaseId: String) {
        
        let documentParent = firestore.collection(nodeBabysitters).document(firebaseId)
        babysitter.deviceToken = PreferenceManager.shared.fcmToken
        babysitter.phone = PreferenceManager.shared.phone
        
        var valueToStore = babysitter.toDictionary()
        valueToStore["updatedAt"] = FieldValue.serverTimestamp()
        documentParent.setData(valueToStore, merge: true)
    }
    
    func findBabysitters(_ withDay: String? = nil, fromTimeSlot: String? = nil, toTimeSlot: String? = nil, completion: @escaping(_ babysitterModals: [BabysitterModal]) -> Void) {
                
        func isTimeslotMatch(_ arg0: DaysModal, _ fromTimeSlot: String? = nil, _ toTimeSlot: String? = nil) -> Bool? {
            
            /*if (TimeSlotTypes.all.rawValue == fromTimeSlot) && (TimeSlotTypes.all.rawValue == toTimeSlot) {
                return true
            } else if (TimeSlotTypes.all.rawValue == fromTimeSlot) && (arg0.toDate(toTimeSlot) <= arg0.actualToDate()) {
                return true
            } else if (arg0.actualFromDate() <= arg0.fromDate(fromTimeSlot)) && (TimeSlotTypes.all.rawValue == toTimeSlot) {
                return true
            } else if (arg0.actualFromDate() <= arg0.fromDate(fromTimeSlot)) && (arg0.toDate(toTimeSlot) <= arg0.actualToDate()) {
                return true
            }*/
            
            if (TimeSlotTypes.all.rawValue == fromTimeSlot) && (TimeSlotTypes.all.rawValue == toTimeSlot) {
                return true
            } else if (TimeSlotTypes.all.rawValue == fromTimeSlot) && (arg0.toDate(toTimeSlot).compareTimeOnly(to: arg0.actualToDate()) == .orderedDescending) {
                return true
            } else if (arg0.actualFromDate().compareTimeOnly(to: arg0.fromDate(fromTimeSlot)) == .orderedAscending) && (TimeSlotTypes.all.rawValue == toTimeSlot) {
                return true
            } else if (arg0.actualFromDate().compareTimeOnly(to: arg0.fromDate(fromTimeSlot)) == .orderedAscending) || (arg0.toDate(toTimeSlot).compareTimeOnly(to: arg0.actualToDate()) == .orderedAscending) {
                return true
            }                        
            return nil
        }
                
        let ref = firestore.collection(nodeBabysitters)
        var documentEvents: Query!
        /*if withDay != nil {
         let index = ( Int( withDay ?? "1" ) ?? 1 )  - 1
         documentEvents = ref.whereField("days.\(index).day", isEqualTo: withDay!)
         } else {
         documentEvents = ref
         }*/
        documentEvents = ref
        documentEvents.getDocuments { (query: QuerySnapshot?, error:Error?) in
            debugPrint(String(describing: error))
            var bbs: [BabysitterModal] = []
            for document in query?.documents ?? [] {
                let result = document.data()
                let object = BabysitterModal(fromDictionary: result)
                debugLog(object: object.name ?? String())
                if withDay != nil && (fromTimeSlot == nil || toTimeSlot == nil) {
                    if object.days.contains(where: { (arg0: DaysModal) in
                        if arg0.day == withDay && (arg0.isAvailable == "1" || object.isAlwaysAvailable == "1") {
                            return true
                        }
                        return false
                    }) {
                        bbs.append(object)
                    }
                } else if withDay == nil && (fromTimeSlot != nil || toTimeSlot != nil) {
                    if object.days.contains(where: { (arg0: DaysModal) in
                        if object.isAlwaysAvailable == "1" {
                            if let result = isTimeslotMatch(arg0, fromTimeSlot, toTimeSlot) {
                                return true
                            }
                        }
                        if arg0.isAvailable == "1" {
                            if let result = isTimeslotMatch(arg0, fromTimeSlot, toTimeSlot) {
                                return true
                            }
                        }
                        return false
                    }) {
                        if !PreferenceManager.shared.blockedBabysitter.contains(object.userid) {
                            bbs.append(object)
                        }                        
                    }
                } else if withDay != nil && (fromTimeSlot != nil || toTimeSlot != nil) {
                    if object.days.contains(where: { (arg0: DaysModal) in
                        if object.isAlwaysAvailable == "1" {
                            if let result = isTimeslotMatch(arg0, fromTimeSlot, toTimeSlot) {
                                return true
                            }
                        }
                        
                        if arg0.isAvailable == "1" && arg0.day == withDay {
                            if let result = isTimeslotMatch(arg0, fromTimeSlot, toTimeSlot) {
                                return true
                            }
                        }
                        return false
                        
                    }) {
                        if !PreferenceManager.shared.blockedBabysitter.contains(object.userid) {
                            bbs.append(object)
                        }
                    }
                } else {
                    if !PreferenceManager.shared.blockedBabysitter.contains(object.userid) {
                        bbs.append(object)
                    }                    
                }
            }
            bbs.sort { arg0, arg1 in
                let doubleLattitude0 = Double( arg0.lattitude ?? String() ) ?? .zero
                let doubleLongitude0 = Double( arg0.longitude ?? String() ) ?? .zero
                let distArg0 = LocationManager.shared.getDistanceFrom(lattitude: doubleLattitude0, longitude: doubleLongitude0)
                
                let doubleLattitude1 = Double( arg1.lattitude ?? String() ) ?? .zero
                let doubleLongitude1 = Double( arg1.longitude ?? String() ) ?? .zero
                let distArg1 = LocationManager.shared.getDistanceFrom(lattitude: doubleLattitude1, longitude: doubleLongitude1)
                
                return distArg1 > distArg0
            }
            completion(bbs)
        }
    }
    
    func saveUser(rating babysitterId: String, parentId: String, parentRating: String, comment: String?) {
        
        let documentRatingBB = firestore.collection(nodeParentsRating).document(parentId)
        documentRatingBB.setData([babysitterId: ["rating": parentRating, "comment": comment ?? String()]], merge: true)
    }
    
    func checkIfRatingAlreadyGiven(with babysitterId: String, parentId: String, completion: @escaping(_ isExist: Bool)->Void) {
        let documentRatingBB = firestore.collection(nodeParentsRating).document(parentId)
        documentRatingBB.getDocument { bbdoc, bbError in
            if let dict = bbdoc?.data() {
                if dict[babysitterId] != nil {
                    completion(true)
                } else {
                    completion(false)
                }
            } else {
                completion(false)
            }
        }
    }
    
    func isBabysitterConnect(withUser userId: String, completion: @escaping(_ latestConversationObj: ConversationModal?)->Void) {
        let reqConversation = firestore.collection(nodeConversations)
            .whereField("access", arrayContains: userId)
            .order(by: "updatedAt", descending: true)
        reqConversation.getDocuments { result, error in
            if let dict = result?.documents.first?.data() {
                let updatedConversation = ConversationModal(fromDictionary: dict)
                completion(updatedConversation)
            } else {
                completion(nil)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Playdates
    
    func findPlaydates(completion: @escaping(_ playDates: [PlayDatesModal]) -> Void) {
        
        getServerTimestamp { timestamp in
            
            let documentEvents = self.firestore.collection(self.nodeEvents)
            documentEvents.getDocuments { (query: QuerySnapshot?, error:Error?) in
                var playdates: [PlayDatesModal] = []
                for document in query?.documents ?? [] {
                    let result = document.data()
                    let objModal = PlayDatesModal(fromDictionary: result)
                    if let timeInterval = objModal.getPlaydateInTimestamp() {
                        if TimeInterval(timestamp.seconds) > timeInterval {
                            self.delete(playdateModal: objModal) { error in
                                debugLog(object: error as Any)
                            }        
                        } else {
                            
                            if objModal.parentID == self.currentUser()?.uid, objModal.isActive == "false" {
                                self.activate(playdateById: objModal.id) { error in
                                    debugLog(object: error as Any)
                                }
                            } else if objModal.isActive == "false" {
                                continue
                            }
                            
                            if !PreferenceManager.shared.blockedPlaydates.contains(objModal.id) && objModal.parentID != self.currentUser()?.uid {
                                if self.parentModal?.isLocationEnable == "1" {
                                    let distArg0 = self.calculateDistance(objModal)
                                    let maxMeter: Double = 25000 //25KM
                                    if distArg0 < maxMeter {
                                        playdates.append(objModal)
                                    }
                                } else {
                                    playdates.append(objModal)
                                }
                            }
                        }
                    } else {
                        
                        if objModal.isActive == "false" {
                            continue
                        }
                        
                        if objModal.id != nil, !PreferenceManager.shared.blockedPlaydates.contains(objModal.id) && objModal.parentID != self.currentUser()?.uid {
                            if self.parentModal?.isLocationEnable == "1" {
                                let distArg0 = self.calculateDistance(objModal)
                                let maxMeter: Double = 25000 //25KM
                                if distArg0 < maxMeter {
                                    playdates.append(objModal)
                                }
                            } else {
                                playdates.append(objModal)
                            }
                        }
                    }
                }
                
                playdates.sort { arg0, arg1 in
                    if let date1 = DateTimeManager.shared.dateFrom(stringDate: arg0.date, inFormate: DateFormate.UTC), let date2 = DateTimeManager.shared.dateFrom(stringDate: arg1.date, inFormate: DateFormate.UTC) {
                        return date1.compare(date2) == .orderedAscending
                    }
                    return false
                }
                
                playdates.sort { arg0, arg1 in
                    let distArg0 = self.calculateDistance(arg0)
                    let distArg1 = self.calculateDistance(arg1)
                    let maxMeter: Double = 15000 //15KM
                    if distArg0 < maxMeter, distArg1 < maxMeter {
                        return distArg1 > distArg0
                    }
                    return false
                }
                completion(playdates)
            }
        }
    }
    
    func calculateDistance(_ arg0: PlayDatesModal) -> Double {
        let doubleLattitude0 = Double( arg0.lattitude ?? String() ) ?? .zero
        let doubleLongitude0 = Double( arg0.longitude ?? String() ) ?? .zero
        return LocationManager.shared.getDistanceFrom(lattitude: doubleLattitude0, longitude: doubleLongitude0)
    }
    
    func findMyPlaydates(completion: @escaping(_ playDates: [PlayDatesModal]) -> Void) {
        
        let documentEvents = firestore.collection(nodeEvents).whereField("parentID", isEqualTo: currentUser()?.uid ?? String())
        documentEvents.getDocuments { (query: QuerySnapshot?, error:Error?) in
            var playdates: [PlayDatesModal] = []
            for document in query?.documents ?? [] {
                let result = document.data()
                playdates.append(PlayDatesModal(fromDictionary: result))
            }
            
            playdates.sort { arg0, arg1 in
                if let date1 = DateTimeManager.shared.dateFrom(stringDate: arg0.date, inFormate: DateFormate.UTC), let date2 = DateTimeManager.shared.dateFrom(stringDate: arg1.date, inFormate: DateFormate.UTC) {
                    return date1.compare(date2) == .orderedAscending
                }
                return false
            }
                            
            playdates.sort { arg0, arg1 in
                let distArg0 = self.calculateDistance(arg0)
                let distArg1 = self.calculateDistance(arg1)
                let maxMeter: Double = 15000
                if distArg0 < maxMeter, distArg1 < maxMeter {
                    return distArg1 > distArg0
                }
                return false
            }
            completion(playdates)
        }
    }
    
    func save(playDatesOf playDateModal: PlayDatesModal) {
        
        var valueToStore = playDateModal.toDictionary()
        var documentEvents: DocumentReference!
        if playDateModal.id == nil || playDateModal.id?.isEmpty == true {
            documentEvents = firestore.collection(nodeEvents).document()
            valueToStore["createdAt"] = FieldValue.serverTimestamp()
            valueToStore["id"] = documentEvents.documentID
            
            //create conversation as well.
            let documentConversation = firestore.collection(nodeConversations).document(documentEvents.documentID)
            documentConversation.getDocument { snapshop, error in
         
                /*let oldConversationModal = ConversationModal(fromDictionary: snapshop?.data() ?? [:])
                
                let initiateConversation = ConversationModal(fromDictionary: [:])
                initiateConversation.conversationId = documentConversation.documentID
                initiateConversation.lastMsg = "Welcome!!"
                initiateConversation.name = playDateModal.title
                initiateConversation.owner = playDateModal.parentID
                
                if oldConversationModal.access?.contains(playDateModal.parentID) == false || oldConversationModal.access == nil {
                    initiateConversation.access = [playDateModal.parentID]
                }
                //initiateConversation.avatar = playDateModal.childList.first?.avatar ?? "0"
                //initiateConversation.personsInvolved = String( playDateModal.childList.count )
                initiateConversation.kidsList = playDateModal.childList
                
                var conValueToStore = initiateConversation.toDictionary()
                conValueToStore["createdAt"] = FieldValue.serverTimestamp()
                conValueToStore["updatedAt"] = FieldValue.serverTimestamp()
                documentConversation.setData(conValueToStore, merge: true)*/
                
                self.create(conversationFromDic: snapshop?.data() ?? [:], conversationId: documentConversation.documentID, title: playDateModal.title, ownerId: playDateModal.parentID, kidsList: playDateModal.childList, isGroupEvent: playDateModal.groupEvent) { conversationObj in
                }
            }
                     
        } else {
            
            documentEvents = firestore.collection(nodeEvents).document(playDateModal.id)
            valueToStore["updatedAt"] = FieldValue.serverTimestamp()
        }
        documentEvents.setData(valueToStore, merge: true)
    }
    
    func create(conversationFromDic dict: [String: Any], conversationId: String, title: String, ownerId: String, kidsList: [String], isGroupEvent: Bool = false, completion: @escaping(_ conversationObj: ConversationModal?)->Void ) {
        
        let oldConversationModal = ConversationModal(fromDictionary: dict)
        
        let initiateConversation = ConversationModal(fromDictionary: [:])
        initiateConversation.conversationId = conversationId
        initiateConversation.lastMsg = "Welcome!!"
        initiateConversation.name = title
        initiateConversation.owner = ownerId
        initiateConversation.groupEvent = isGroupEvent == true ? "1" : "0"
        
        if oldConversationModal.access?.contains(ownerId) == false || oldConversationModal.access == nil {
            initiateConversation.access = [ownerId]
        }
        
        if oldConversationModal.deviceTokens.contains(ownerId) == false || oldConversationModal.deviceTokens.count == .zero {
            if let deviceToken = self.parentModal?.deviceToken {
                initiateConversation.deviceTokens.append(deviceToken)
            }
            if let deviceToken = self.babysitterModal?.deviceToken {
                initiateConversation.deviceTokens.append(deviceToken)
            }
        }
        
        //initiateConversation.avatar = playDateModal.childList.first?.avatar ?? "0"
        //initiateConversation.personsInvolved = String( playDateModal.childList.count )
        initiateConversation.kidsList = kidsList
        
        var conValueToStore = initiateConversation.toDictionary()
        conValueToStore["createdAt"] = FieldValue.serverTimestamp()
        conValueToStore["updatedAt"] = FieldValue.serverTimestamp()
        
        let documentConversation = firestore.collection(nodeConversations).document(conversationId)
        documentConversation.setData(conValueToStore, merge: true) { pError in
            completion(initiateConversation)
        }
    }
    
    func delete(playdateModal: PlayDatesModal, completion: @escaping(_ error: Error?) -> Void) {
        deleteBy(playdateId: playdateModal.id, completion: completion)
    }
    
    func deleteBy(playdateId: String?, completion: @escaping(_ error: Error?) -> Void) {
        
        if let playdateID = playdateId {
            //1. delete conversation
            delete(conversation: playdateID) {
                //3. delete actual play date event
                let documentEvents = self.firestore.collection(self.nodeEvents).document(playdateID)
                documentEvents.delete(completion: completion)
            }
        } else {
            completion(nil)
        }
    }
    
    func activate(playdateById playdateID: String?, completion: @escaping(_ error: Error?) -> Void) {
        if let playdateID = playdateID {
            let documentEvents = self.firestore.collection(self.nodeEvents).document(playdateID)
            documentEvents.updateData(["isActive":"true"], completion: completion)
        }
    }
    
    func subscribeEvents() {
        Messaging.messaging().subscribe(toTopic: nodeEvents) { error in
            debugLog(object: error as Any)
        }
    }
    
    func subscribeParents() {
        Messaging.messaging().subscribe(toTopic: nodeParents) { error in
            debugLog(object: error as Any)
        }
    }
    
    func subscribeBabysitter() {
        Messaging.messaging().subscribe(toTopic: nodeBabysitters) { error in
            debugLog(object: error as Any)
        }
    }
    
    func unsubscribeEvents() {
        Messaging.messaging().unsubscribe(fromTopic: nodeEvents)
    }
    
    func unsubscribeParents() {
        Messaging.messaging().unsubscribe(fromTopic: nodeParents)
    }
    
    func unsubscribeBabysitter() {
        Messaging.messaging().unsubscribe(fromTopic: nodeBabysitters)
    }
    
    func findAutogeneratedPlaydate(ofChildId childId: String, completion: @escaping(_ autogeneratedPlaydates: [PlayDatesModal])->Void) {
        
        let documentEvents = firestore.collection(nodeEvents).whereField("createdOnChildId", isEqualTo: childId)
        documentEvents.getDocuments { querySnapshot, queryError in
            var playdates: [PlayDatesModal] = []
            querySnapshot?.documents.forEach({ documentSnapshop in
                let dict = documentSnapshop.data()
                let pd = PlayDatesModal(fromDictionary: dict)
                playdates.append(pd)
            })
            completion(playdates)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Requests
        
    func sendRequest(playdateId: String, playdateParentId: String, playdateName: String, userId: String, kids: [String], status: String = "0") {
        
        let documentRequest = firestore.collection(nodeRequests).document()
        let request = RequestModal(fromDictionary: [:])
        if playdateId.isEmpty == true {
            request.playdateId = documentRequest.documentID
        } else {
            request.playdateId = playdateId
        }
        request.playdateName = playdateName
        request.playdateCreatedParentId = playdateParentId
        request.id = documentRequest.documentID
        request.requestParentId = userId
        request.kids = kids
        request.status = status // 0. Initiated, 1. Approved 2. Rejected 3. Own cancel
        
        var valueToStore = request.toDictionary()
        valueToStore["createdAt"] = FieldValue.serverTimestamp()
        documentRequest.setData(valueToStore, merge: true)
        
        FirebaseManager.shared.findDeviceToken(of: playdateParentId, roleType: RoleType.parent) { baseModal in
            if baseModal.isNotificationEnabled == "1" {
                RequestManager.shared.fcmSend(withType: FCMNotificationType.playdateGroupRequestSend, toDeviceToken: baseModal.deviceToken, withTitle: "Request to join", withSubTitle: playdateName, withData: request.toDictionary())
            }
        }
    }
    
    func sendBabysitterRequest(playdateId: String, playdateParentId: String, playdateName: String, userId: String, kids: [String], status: String = "0") {
    
        let documentRequest = firestore.collection(nodeRequests).document()
        let request = RequestModal(fromDictionary: [:])
        if playdateId.isEmpty == true {
            request.playdateId = documentRequest.documentID
        } else {
            request.playdateId = playdateId
        }
        request.isForBabysitter = "1"
        request.playdateName = playdateName
        request.playdateCreatedParentId = playdateParentId
        request.id = documentRequest.documentID
        request.requestParentId = userId
        request.kids = kids
        request.status = status // 0. Initiated, 1. Approved 2. Rejected 3. Own cancel
        
        var valueToStore = request.toDictionary()
        valueToStore["createdAt"] = FieldValue.serverTimestamp()
        documentRequest.setData(valueToStore, merge: true)
        
        FirebaseManager.shared.findDeviceToken(of: playdateParentId, roleType: RoleType.babysitter) { baseModal in
            if baseModal.isNotificationEnabled == "1" {
                RequestManager.shared.fcmSend(withType: FCMNotificationType.babysitterRequestSend, toDeviceToken: baseModal.deviceToken, withTitle: "Requesting for a babysitter", withSubTitle: playdateName, withData: request.toDictionary())
            }
        }
        
        //create conversation as well.
        let documentConversation = firestore.collection(nodeConversations).document(documentRequest.documentID)
        documentConversation.getDocument { snapshop, error in
     
            let oldConversationModal = ConversationModal(fromDictionary: snapshop?.data() ?? [:])
            
            let initiateConversation = ConversationModal(fromDictionary: [:])
            initiateConversation.conversationId = documentConversation.documentID
            initiateConversation.lastMsg = "Welcome!!"
            initiateConversation.name = playdateName
            initiateConversation.owner = userId
            initiateConversation.isForBabysitter = "1"
            
            if oldConversationModal.access?.contains(userId) == false || oldConversationModal.access == nil {
                initiateConversation.access = [userId]
            }
            initiateConversation.kidsList = kids
            
            var conValueToStore = initiateConversation.toDictionary()
            conValueToStore["createdAt"] = FieldValue.serverTimestamp()
            conValueToStore["updatedAt"] = FieldValue.serverTimestamp()
            documentConversation.setData(conValueToStore, merge: true)
        }
    }
    
    func sendNonGroupRequest(playdateId: String, playdateParentId: String, playdateName: String, userId: String, kids: [String], status: String = "0") {
    
        let documentRequest = firestore.collection(nodeRequests).document()
        let request = RequestModal(fromDictionary: [:])
        if playdateId.isEmpty == true {
            request.playdateId = documentRequest.documentID
        } else {
            request.playdateId = playdateId
        }
        request.isForBabysitter = "0"
        request.playdateName = playdateName
        request.playdateCreatedParentId = playdateParentId
        request.id = documentRequest.documentID
        request.requestParentId = userId
        request.kids = kids
        request.status = status // 0. Initiated, 1. Approved 2. Rejected 3. Own cancel
        
        var valueToStore = request.toDictionary()
        valueToStore["createdAt"] = FieldValue.serverTimestamp()
        documentRequest.setData(valueToStore, merge: true)
        
        FirebaseManager.shared.findDeviceToken(of: playdateParentId, roleType: RoleType.parent) { baseModal in
            if baseModal.isNotificationEnabled == "1" {
                RequestManager.shared.fcmSend(withType: FCMNotificationType.playdateNonGroupRequestSend, toDeviceToken: baseModal.deviceToken, withTitle: "Request to join", withSubTitle: playdateName, withData: request.toDictionary())
            }
        }
        
        //create conversation as well.
        let documentConversation = firestore.collection(nodeConversations).document(documentRequest.documentID)
        documentConversation.getDocument { snapshop, error in
     
            let oldConversationModal = ConversationModal(fromDictionary: snapshop?.data() ?? [:])
            
            let initiateConversation = ConversationModal(fromDictionary: [:])
            initiateConversation.conversationId = documentConversation.documentID
            initiateConversation.lastMsg = "Welcome!!"
            initiateConversation.name = playdateName
            initiateConversation.owner = userId
            
            if oldConversationModal.access?.contains(userId) == false || oldConversationModal.access == nil {
                initiateConversation.access = [userId]
            }
            initiateConversation.kidsList = kids
            
            var conValueToStore = initiateConversation.toDictionary()
            conValueToStore["createdAt"] = FieldValue.serverTimestamp()
            conValueToStore["updatedAt"] = FieldValue.serverTimestamp()
            documentConversation.setData(conValueToStore, merge: true)
        }
    }
    
    func find(requestModalFrom playdateId: String, playdateParentId: String, userId: String, completion: @escaping(_ requestModal: RequestModal?) ->Void ) {
        
        let collectionRequest = firestore.collection(nodeRequests)
            .whereField("playdate_id", isEqualTo: playdateId)
            .whereField("playdate_parent_id", isEqualTo: playdateParentId)
            .whereField("request_parent_id", isEqualTo: userId)
        collectionRequest.getDocuments { query, error in
            if let doc = query?.documents.first?.data() {
                let modal = RequestModal(fromDictionary: doc)
                completion(modal)
            } else {
                completion(nil)
            }
        }
    }
    
    func findBabysitter(requestModalFrom playdateParentId: String, userId: String, completion: @escaping(_ requestModal: RequestModal?) ->Void ) {
        
        let collectionRequest = firestore.collection(nodeRequests)
            .whereField("playdate_parent_id", isEqualTo: playdateParentId)
            .whereField("request_parent_id", isEqualTo: userId)
        collectionRequest.getDocuments { query, error in
            if let doc = query?.documents.first?.data() {
                let modal = RequestModal(fromDictionary: doc)
                completion(modal)
            } else {
                completion(nil)
            }
        }
    }
    
    func checkIfExist(playdateId: String, playdateParentId: String, userId: String, completion: @escaping(_ isExist: Bool)->Void )  {
        
        let collectionRequest = firestore.collection(nodeRequests)
            .whereField("playdate_id", isEqualTo: playdateId)
            .whereField("playdate_parent_id", isEqualTo: playdateParentId)
            .whereField("request_parent_id", isEqualTo: userId)
        collectionRequest.getDocuments { query, error in
            if query?.documents.count == .zero {
                completion(false)
            } else {
                completion(true)
            }
        }
    }
    
    func checkIfExist(request: RequestModal, completion: @escaping(_ isExist: Bool)->Void ) {
        let documentRequest = firestore.collection(nodeRequests).document(request.id)
        documentRequest.getDocument { documentSnapshop, error in
            let result = documentSnapshop?.data() ?? [:]
            if result.count == .zero {
                completion(false)
            } else {
                completion(true)
            }
        }
    }
    
    func getAllMy(request userId: String, completion: @escaping(_ result: [RequestModal]) ->Void) {
        let collectionRequest = firestore.collection(nodeRequests)
            .whereField("playdate_parent_id", isEqualTo: userId)
            .order(by: "createdAt", descending: true)
            //.order(by: "updatedAt", descending: true)
        
        collectionRequest.getDocuments { query, error in
            var requests: [RequestModal] = []
            for document in query?.documents ?? [] {
                let modal = RequestModal(fromDictionary: document.data())
                if modal.status == "0" || modal.status.isEmpty == true {
                    requests.append(modal)
                }
            }
            completion(requests)
        }
    }
    
    func admit(requestModal: RequestModal, completion: @escaping()->Void) {
        
        let documentRequest = firestore.collection(nodeRequests).document(requestModal.id)
        requestModal.status = "1"
        let valueToSave = requestModal.toDictionary()
        documentRequest.setData(valueToSave, merge: true)
        
        if requestModal.isForBabysitter == "1" {
            
            FirebaseManager.shared.findDeviceToken(of: requestModal.requestParentId, roleType: RoleType.parent) { baseModal in
                if baseModal.isNotificationEnabled == "1" {
                    RequestManager.shared.fcmSend(withType: FCMNotificationType.babysitterRequestAccepted, toDeviceToken: baseModal.deviceToken, withTitle: "Your request for a babysitter is accepted", withSubTitle: baseModal.name, withData: requestModal.toDictionary())
                }
            }
            
            //provide access to see the conversation
            self.admit(access: requestModal.playdateCreatedParentId, forConversation: requestModal.id, userRole: RoleType.babysitter, existingObj: nil) {updatedObj in
                //safer side
                self.admit(access: requestModal.requestParentId, forConversation: requestModal.id, userRole: RoleType.parent, existingObj: updatedObj) {updatedObj in
                    completion()
                }
            }
            
        } else {
            
            let documentPlaydate = firestore.collection(nodeEvents).document(requestModal.playdateId)
            documentPlaydate.getDocument { data, error in
                
                if data?.exists == true {
                    
                    if let dict = data?.data() {
                        let playdateModal = PlayDatesModal(fromDictionary: dict)
                        var kids = playdateModal.childList
                        /*let kidsIDs = requestModal.kids.compactMap { arg0 in
                            return arg0.firebaseID
                        }*/
                        kids?.append(contentsOf: requestModal.kids ?? [])
                        playdateModal.childList = kids
                        
                        let valueToSave = playdateModal.toDictionary()
                        documentPlaydate.setData(valueToSave, merge: true)
                        
                        let conversation = self.firestore.collection(self.nodeConversations).document(playdateModal.id)
                        conversation.setData(["kidsList" : kids ?? []], merge: true)
                        
                        FirebaseManager.shared.findDeviceToken(of: requestModal.requestParentId, roleType: RoleType.parent) { baseModal in
                            if baseModal.isNotificationEnabled == "1" {
                                RequestManager.shared.fcmSend(withType: FCMNotificationType.playdateGroupRequestAccepted, toDeviceToken: baseModal.deviceToken, withTitle: "Your request for playdate is accepted", withSubTitle: baseModal.name, withData: requestModal.toDictionary())
                            }
                        }
                        
                        //provide access to see the conversation
                        self.admit(access: requestModal.requestParentId, forConversation: playdateModal.id, userRole: RoleType.parent, existingObj: nil) {updatedObj in
                            completion()
                        }
                    }
                    
                } else {
                    
                    FirebaseManager.shared.findDeviceToken(of: requestModal.requestParentId, roleType: RoleType.parent) { baseModal in
                        if baseModal.isNotificationEnabled == "1" {
                            RequestManager.shared.fcmSend(withType: FCMNotificationType.playdateNonGroupRequestAccepted, toDeviceToken: baseModal.deviceToken, withTitle: "Your request for playdate is accepted", withSubTitle: baseModal.name, withData: requestModal.toDictionary())
                        }
                    }
                    
                    //provide access to see the conversation
                    self.admit(access: requestModal.playdateCreatedParentId, forConversation: requestModal.id, userRole: RoleType.parent, existingObj: nil) { updatedObj in
                        
                        //safer side
                        self.admit(access: requestModal.requestParentId, forConversation: requestModal.id, userRole: RoleType.parent, existingObj: updatedObj) { updatedObj in
                            completion()
                        }
                    }
                }
            }
        }
    }
    
    func reject(requestModal: RequestModal, completion: @escaping()->Void) {
        
        let documentRequest = firestore.collection(nodeRequests).document(requestModal.id)
        requestModal.status = "2"
        let valueToSave = requestModal.toDictionary()
        documentRequest.setData(valueToSave, merge: true)
        completion()
    }
    
    //------------------------------------------------------
    
    //MARK: Conversations
    
    func getAllMy(conversations userId: String, completion: @escaping(_ result: [ConversationModal]) ->Void) {
        let collectionRequest = firestore.collection(nodeConversations)
            .whereField("access", arrayContains: userId)
            .whereField("isForBabysitter", isEqualTo: "0")
            //.order(by: "createdAt", descending: true)
            .order(by: "updatedAt", descending: true)
        
        collectionRequest.getDocuments { query, error in
            var conversations: [ConversationModal] = []
            for document in query?.documents ?? [] {
                let modal = ConversationModal(fromDictionary: document.data())
                if (modal.access?.count ?? .zero) > 1 {
                    conversations.append(modal)
                }
                                
                let pDeviceToken = self.parentModal?.deviceToken
                let bsDeviceToken = self.babysitterModal?.deviceToken
                
                let isExist = modal.deviceTokens.contains { arg0 in
                    if ValidationManager.shared.isEmpty(text: pDeviceToken) == false {
                        return arg0 == pDeviceToken
                    }
                    if ValidationManager.shared.isEmpty(text: bsDeviceToken) == false {
                        return arg0 == bsDeviceToken
                    }
                    return false
                }
                
                if isExist == false {
                    if ValidationManager.shared.isEmpty(text: pDeviceToken) == false {
                        modal.deviceTokens.append(pDeviceToken!)
                    }
                    if ValidationManager.shared.isEmpty(text: bsDeviceToken) == false {
                        modal.deviceTokens.append(bsDeviceToken!)
                    }
                    
                    FirebaseManager.shared.save(conversation: modal) { error in
                    }
                }
            }
            completion(conversations)
        }
    }
    
    func getAllwithBabysitter(conversations userId: String, completion: @escaping(_ result: [ConversationModal]) ->Void) {
        let collectionRequest = firestore.collection(nodeConversations)
            .whereField("access", arrayContains: userId)
            .whereField("isForBabysitter", isEqualTo: "1")
            //.order(by: "createdAt", descending: true)
            .order(by: "updatedAt", descending: true)
        
        collectionRequest.getDocuments { query, error in
            var conversations: [ConversationModal] = []
            for document in query?.documents ?? [] {
                let modal = ConversationModal(fromDictionary: document.data())
                if (modal.access?.count ?? .zero) > 1 {
                    conversations.append(modal)
                }
            }
            completion(conversations)
        }
    }
    
    func admit(access toUserId: String, forConversation conversationId: String, userRole: RoleType, existingObj: ConversationModal?, completion: @escaping(_ updatedObj: ConversationModal? )->Void) {
        
        let document = firestore.collection(nodeConversations).document(conversationId)
        document.getDocument { documentData, error in
            if let dic = documentData?.data() {
                var modal: ConversationModal?
                if existingObj == nil {
                    modal = ConversationModal(fromDictionary: dic)
                } else {
                    modal = existingObj
                }
                if modal?.access?.contains(toUserId) == false || modal?.access == nil {
                    modal?.access?.append(toUserId)
                }
                
                var valueToStore = modal?.toDictionary() ?? [:]
                valueToStore["updatedAt"] = FieldValue.serverTimestamp()
                document.setData(valueToStore, merge: true)
                
                self.findDeviceToken(of: toUserId, roleType: userRole) { baseModal in
                    if let token = baseModal.deviceToken {
                        if modal?.deviceTokens.contains(token) == false || modal?.deviceTokens == nil {
                            modal?.deviceTokens.append(token)
                        }
                    }
                    valueToStore = modal?.toDictionary() ?? [:]
                    valueToStore["updatedAt"] = FieldValue.serverTimestamp()
                    document.setData(valueToStore, merge: true)
                    completion(modal)
                }
            } else {
                completion(nil)
            }
        }
    }
    
    func delete(conversation conversationId: String, completion: @escaping()->Void) {
        
        let batchDelete = self.firestore.batch()
        
        //1. delete all pending requests
        let qFindRequest = firestore.collection(nodeRequests).whereField("playdate_id", isEqualTo: conversationId)
        qFindRequest.getDocuments { playdateDocument, err in
            for doc in playdateDocument?.documents ?? [] {
                let path = self.firestore.collection(self.nodeRequests).document(doc.documentID)
                //path.delete()
                batchDelete.deleteDocument(path)
            }
                               
            //make empty field first
            let collectionMessages = self.firestore.collection(self.nodeConversations).document(conversationId).collection(self.nodeMessages)
            collectionMessages.getDocuments { qSnapshot, mError in
                qSnapshot?.documents.forEach({ arg0 in
                    let query =  self.firestore.collection(self.nodeConversations).document(conversationId).collection(self.nodeMessages).document(arg0.documentID)
                    batchDelete.deleteDocument(query)
                })
                
                //2. delete conversation & messages
                let documentConversation = self.firestore.collection(self.nodeConversations).document(conversationId)
                batchDelete.deleteDocument(documentConversation)
                        
                batchDelete.commit { _ in
                    completion()
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Messages
    
    func sendMessage(toConversation conversationId: String, withTitle title: String? = nil, message: MessageModal, completion: @escaping(_ error: Error?)->Void) {
        
        let collectionRequest = firestore.collection(nodeConversations).document(conversationId).collection(nodeMessages)   
        let document = collectionRequest.document()
        message.pMessageId = document.documentID
        message.pOwner = conversationId
        message.pIsread = false
        var valueToStore = message.toDictionary()
        valueToStore["date"] = FieldValue.serverTimestamp()
        document.setData(valueToStore) { error in
            completion(error)
        }
        
        FirebaseManager.shared.findConversation(fromConversationId: conversationId) { latestConversationObj in
            if latestConversationObj.deviceTokens.count > .zero {
                var devicesList: [String] = latestConversationObj.deviceTokens
                if let removedToken = self.parentModal?.deviceToken {
                    devicesList.removeAll { arg0 in
                        return arg0 == removedToken
                    }
                }
                if let removedToken = self.babysitterModal?.deviceToken {
                    devicesList.removeAll { arg0 in
                        return arg0 == removedToken
                    }
                }
                
                if title != nil || latestConversationObj.kidsName != nil {
                    RequestManager.shared.fcmSend(withType: .message, toDeviceTokens: devicesList, withTitle: title ?? latestConversationObj.kidsName ?? String(), withSubTitle: message.pMessage, withData: message.toDictionary())
                }
            }
        }
    }
    
    func getRecentMessages(forConversation conversationId: String, completion: @escaping(_ messages: [MessageModal])->Void) {
        
        let collectionRequest = firestore.collection(nodeConversations).document(conversationId).collection(nodeMessages).order(by: "date", descending: true)
            .limit(to: 10)
        collectionRequest.getDocuments { (snapshot: QuerySnapshot?, error: Error?) in
            var messages: [MessageModal] = []
            for document in snapshot?.documents ?? [] {
                let dict = document.data()
                messages.insert(MessageModal(fromDictionary: dict), at: .zero)
            }
            completion(messages)
        }
    }
    
    func getMoreMessagesFrom(for conversationId: String, lastDocumentId documentId: String, completion: @escaping(_ messages: [MessageModal])->Void)  {
        let lastDocument = firestore.collection(nodeConversations).document(conversationId).collection(nodeMessages).document(documentId)
        lastDocument.getDocument { documentSnapshot, error in
            if documentSnapshot != nil {
                let collectionRequest = self.firestore.collection(self.nodeConversations).document(conversationId).collection(self.nodeMessages).order(by: "date", descending: true)
                    .start(afterDocument: documentSnapshot!)
                    .limit(to: 10)
                collectionRequest.getDocuments { (snapshot: QuerySnapshot?, error: Error?) in
                    var messages: [MessageModal] = []
                    for document in snapshot?.documents ?? [] {
                        let dict = document.data()
                        messages.insert(MessageModal(fromDictionary: dict), at: .zero)
                    }
                    completion(messages)
                }
            } else {
                completion([])
            }
        }
    }
    
    func update(lastMessage message: MessageModal, toConversation conversationId: String, completion: @escaping()->Void) {
        let document = firestore.collection(nodeConversations).document(conversationId)
        document.getDocument { (snapshot: DocumentSnapshot?, error: Error?) in
            if let dict = snapshot?.data() {
                let modal = ConversationModal(fromDictionary: dict)
                modal.lastMsg = message.pMessage
                //modal.updatedAt = message.pDate
                var valueToStore = modal.toDictionary()
                valueToStore["updatedAt"] = message.pDate
                document.setData(valueToStore, merge: true)
                completion()
            } else {
                completion()
            }
        }
    }
    
    func findConversation(fromConversationId conversationId: String, completion: @escaping(_ latestConversationObj: ConversationModal)->Void) {
        let reqConversation = firestore.collection(nodeConversations).document(conversationId)
        reqConversation.getDocument { documentSnapshot, error in
            if let dict = documentSnapshot?.data() {
                let updatedConversation = ConversationModal(fromDictionary: dict)
                completion(updatedConversation)
            }
        }
    }
    
    func listenEvent(ofConversationId conversationId: String, completion: @escaping(_ latestConversationObj: ConversationModal)->Void) {
        let reqConversation = firestore.collection(nodeConversations).document(conversationId)
        reqConversation.addSnapshotListener { snapshot, error in
            if let dict = snapshot?.data() {
                let updatedConversation = ConversationModal(fromDictionary: dict)
                completion(updatedConversation)
            }
        }
    }
    
    func subscribe(conversation conversationId: String) {
        /*Messaging.messaging().subscribe(toTopic: "\(nodeConversations)/\(conversationId)") { error in
            debugLog(object: error as Any)
        }*/
        
        Messaging.messaging().subscribe(toTopic: nodeConversations) { error in
            debugLog(object: error as Any)
        }
    }
    
    func unsubscribe(conversation conversationId: String) {
        
        /*Messaging.messaging().unsubscribe(fromTopic: "\(nodeConversations)/\(conversationId)") { error in
            debugLog(object: error as Any)
        }*/
        
        Messaging.messaging().unsubscribe(fromTopic: nodeConversations) { error in
            debugLog(object: error as Any)
        }
    }
    
    func listenMyRequests(_ userId: String, completion: @escaping(_ result: [RequestModal])->Void) {
        
        let collectionRequest = firestore.collection(nodeRequests)
            .whereField("playdate_parent_id", isEqualTo: userId)
            .order(by: "createdAt", descending: true)
        collectionRequest.addSnapshotListener { query, error in
            var requests: [RequestModal] = []
            for document in query?.documents ?? [] {
                let modal = RequestModal(fromDictionary: document.data())
                if modal.status == "0" || modal.status.isEmpty == true {
                    requests.append(modal)
                }
            }
            completion(requests)
        }
    }
    
    func findGotoConversation(forConversationId conversationId: String, userId: String, completion: @escaping(_ latestConversationObj: ConversationModal?)->Void) {
        
        let reqConversation = firestore.collection(nodeConversations)
            .whereField("conversationId", isEqualTo: conversationId)
            .whereField("access", arrayContains: userId)
            .order(by: "updatedAt", descending: true)
        reqConversation.getDocuments { result, error in
            if let dict = result?.documents.first?.data() {
                let updatedConversation = ConversationModal(fromDictionary: dict)
                completion(updatedConversation)
            } else {
                completion(nil)
            }
        }
    }
    
    func save(conversation conversationModal: ConversationModal, completion: @escaping(_ error: Error?)->Void) {
        
        let conversation = firestore.collection(nodeConversations).document(conversationModal.conversationId ?? String())
        let valueToStore = conversationModal.toDictionary()
        conversation.setData(valueToStore) { error in
            completion(error)
        }
    }
    
    func listenMe(_ userId: String, ofNode node: String, completion: @escaping(_ result: BaseModal?)->Void) {
        
        let collectionRequest = firestore.collection(node)
            .whereField("userid", isEqualTo: userId)
        collectionRequest.addSnapshotListener { query, error in
            if let document = query?.documents.first {
                let modal = BaseModal(fromDictionary: document.data())
                completion(modal)
            } else {
                completion(nil)
            }
        }
    }
    
    func startListeningIfUserBlocked() {
        
        let userId = isParentProfile ? (parentModal?.userid ?? String()) : (babysitterModal?.userid ?? String())
        let node = isParentProfile ? nodeParents : nodeBabysitters
        FirebaseManager.shared.listenMe(userId, ofNode: node) { result in
           
            if result?.isBlocked == "1" {
                
                let emailAddress = "support@funbud.com"
                let attributedString = NSMutableAttributedString(string: emailAddress)
                attributedString.setAsLink(textToFind: emailAddress, linkURL: "\(emailAddress)")

                let alert = UIAlertController(title: kAppName.localized(), message: LocalizableConstants.Error.your_profile_blocked.localized(), preferredStyle: .alert)
                let textView = UITextView()
                textView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                
                let controller = UIViewController()
                textView.frame = controller.view.frame
                controller.view.addSubview(textView)
                textView.backgroundColor = .clear
                alert.setValue(controller, forKey: "contentViewController")
                
                let height: NSLayoutConstraint = NSLayoutConstraint(item: alert.view ?? UIView(), attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 200)
                alert.view.addConstraint(height)
                
                textView.attributedText = attributedString
                textView.isUserInteractionEnabled = true
                textView.isEditable = false
                textView.textAlignment = .center
                textView.textContainerInset = .init(top: 5, left: 5, bottom: 10, right: 5)
                
                // Set how links should appear: blue and underlined
                textView.linkTextAttributes = [
                    .foregroundColor: UIColor.blue,
                    .underlineStyle: NSUnderlineStyle.single.rawValue
                ]
                
                let okAction = UIAlertAction(title: "Contact", style: .default) {
                    UIAlertAction in
                                        
                    if let url = URL(string: "mailto:\(emailAddress)") {
                      if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url)
                      } else {
                        UIApplication.shared.openURL(url)
                      }
                    }
                    
                    delay {
                        try? FirebaseManager.shared.signOut()
                        PreferenceManager.shared.reset()
                        NavigationManager.shared.setupSignInOption()
                    }
                }
                alert.addAction(okAction)
                
                let cancelAction = UIAlertAction(title: "Log out", style: .default) {
                    UIAlertAction in
                    
                    try? FirebaseManager.shared.signOut()
                    PreferenceManager.shared.reset()
                    NavigationManager.shared.setupSignInOption()
                }
                alert.addAction(cancelAction)
                
                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    //------------------------------------------------------
}
