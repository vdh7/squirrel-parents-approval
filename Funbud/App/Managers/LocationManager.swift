//
//  LocationManager.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 9/10/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

@objc
protocol LocationManagerDelegate {
    
    @objc func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    @objc optional func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading)
    @objc func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
}

class LocationManager : NSObject, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    var delegate : LocationManagerDelegate?
    var coordinate2D : CLLocationCoordinate2D! = CLLocationCoordinate2D()
    var lat: Double = .zero
    var lng: Double = .zero
    
    //------------------------------------------------------
    
    //MARK: Shared
    
    static var shared = LocationManager()
    
    //------------------------------------------------------
    
    //MARK: Memory Management Method
    
    deinit { //same like dealloc in ObjectiveC
        
    }
    
    //------------------------------------------------------
    
    //MARK: Public
    
    func isRequiredToShowPermissionDialogue() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                return true
            case .authorizedAlways, .authorizedWhenInUse:
                return false
            case .restricted:
                return false
            case .denied:
                return false
            @unknown default:
                break
            }
        }
        return false
    }
    
    func isLocationServicesEnabled() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            @unknown default:
                return true
            }
        } else {
            return false
        }
    }
    
    func requestForLocationPermissionWhenInUse() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
    func requestForLocationPermissionAlwaysInUse() {
        locationManager.requestAlwaysAuthorization()
        locationManager.requestLocation()
    }
    
    func startMonitoring() {
        locationManager.startUpdatingLocation()
        if lat != .zero && lng != .zero {
            delegate?.locationManager(locationManager, didUpdateLocations: [CLLocation(latitude: lat, longitude: lng)])
        } else {
            locationManager.requestLocation()
        }
    }
    
    func stopMonitoring() {
        //lat = .zero
        //lng = .zero
        locationManager.stopUpdatingLocation()
    }
    
    func startGeofenceMonitoring(on latitude: Double, longitude: Double, identifier: String) {
               
        stopGeofenceMonitoring(identifier)
        
        let geofenceRegionCenter = CLLocationCoordinate2DMake(latitude, longitude)
        let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: 50, identifier: identifier)
        geofenceRegion.notifyOnEntry = true
        geofenceRegion.notifyOnExit = false
        locationManager.startMonitoring(for: geofenceRegion)
    }
    
    func stopGeofenceMonitoring(_ identifier: String) {
        
        if let region = locationManager.monitoredRegions.first(where: { (region: CLRegion) in
            return region.identifier == identifier
        }) {
            locationManager.stopMonitoring(for: region)
        }
    }
    
    func getDistanceFrom(lattitude: Double, longitude: Double) -> Double {
        return CLLocation(latitude: lat, longitude: lng).distance(from: CLLocation(latitude: lattitude, longitude: longitude))
    }
    
    //------------------------------------------------------
    
    //MARK: CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if (locations.first != nil) {
            coordinate2D = locations.first!.coordinate            
            delegate?.locationManager(manager, didUpdateLocations: locations)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        delegate?.locationManager(manager, didFailWithError: error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        
        delegate?.locationManager?(manager, didUpdateHeading: newHeading)
    }

    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
        debugPrint("didEnterRegion:\(region)")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
        debugPrint("didExitRegion:\(region)")
    }
    
    //------------------------------------------------------
    
    //MARK: Initialisation
    
    override init() {
        super.init()
        
        //locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
    }
    
    //------------------------------------------------------
}
