//
//  NavigationManager.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 25/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import LGSideMenuController

struct PMStoryboard {
    public static let main: String = "Main"
    public static let loader: String = "Loader"
    public static let babysitter: String = "BabySitter"
    public static let locationSearch: String = "LocationSearch"
}
struct PMNavigation {
    public static let signIn: String = "navigationSignIn"
    //Location search
    public static let locationSearch: String = "navigationLocationSearchVC"
    //landing
    public static let landing: String = "navigationLanding"
}

class NavigationManager: NSObject, UITabBarControllerDelegate {
    
    let window = AppDelegate.shared.window
    var tabbarController: ESTabBarController?
    let sideMenuController = LGSideMenuController()
    
    //------------------------------------------------------
    
    //MARK: Storyboards
    
    let mainStoryboard = UIStoryboard(name: PMStoryboard.main, bundle: Bundle.main)
    let loaderStoryboard = UIStoryboard(name: PMStoryboard.loader, bundle: Bundle.main)
    let babySitterStoryboard = UIStoryboard(name: PMStoryboard.babysitter, bundle: Bundle.main)
    let locationSearchStoryboard = UIStoryboard(name: PMStoryboard.locationSearch, bundle: Bundle.main)
    
    //------------------------------------------------------
    
    //MARK: Shared
    
    static let shared = NavigationManager()
    
    override init() {
        super.init()
        
        sideMenuController.leftViewWidth = UIScreen.main.bounds.width
        if #available(iOS 13.0, *) {
            sideMenuController.rootViewStatusBarStyle = .darkContent
        } else {
            sideMenuController.rootViewStatusBarStyle = .default
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UINavigationController
    
    var signInNC: UINavigationController {
        return mainStoryboard.instantiateViewController(withIdentifier: PMNavigation.signIn) as! UINavigationController
    }
    
    var locationSearchNC: UINavigationController {
        return locationSearchStoryboard.instantiateViewController(withIdentifier: PMNavigation.locationSearch) as! UINavigationController
    }
    
    var landingNC: UINavigationController {
        return mainStoryboard.instantiateViewController(withIdentifier: PMNavigation.landing) as! UINavigationController
    }
    
    //------------------------------------------------------
    
    //MARK: UITabbarController
    
    var landingTC: ESTabBarController {
        return customIrregularityStyle(delegate: self)
    }
    
    public func customIrregularityStyle(delegate: UITabBarControllerDelegate?) -> ESTabBarController {
        
        let tabBarController = ESTabBarController()
        tabBarController.delegate = delegate
        //tabBarController.tabBar.shadowImage = UIImage(named: TFImageName.iconTransparent)
        tabBarController.tabBar.shadowColor = UIColor.darkGray
        tabBarController.tabBar.shadowOffset = CGSize.zero
        tabBarController.tabBar.shadowOpacity = 0.7
        tabBarController.tabBar.isTranslucent = true
        tabBarController.tabBar.backgroundColor = UIColor.white
        
        let v1 = landingNC
        v1.setViewControllers([landingVC], animated: false)
        let v2 = UINavigationController()
        let v3 = landingNC
        v3.setViewControllers([chatListVC], animated: false)
        
        tabBarController.shouldHijackHandler = {
            tabbarController, viewController, index in
            if index == 1 {
                return true
            }
            return false
        }
        
        tabBarController.didHijackHandler = {
            [weak tabBarController] tabbarController, viewController, index in
            
            let v2 = self.createPlaydatesVC
            let nc = UINavigationController(rootViewController: v2)
            nc.isNavigationBarHidden = true
            tabBarController?.present(nc, animated: true, completion: nil)
        }
        
        let item1 = ESTabBarItem.init(IrregularityBasicContentView(), title: nil, image: UIImage(named: PMImageName.bottom_menu_home), selectedImage: UIImage(named: PMImageName.bottom_menu_home_active))
        item1.contentView?.renderingMode = .alwaysOriginal
        v1.tabBarItem = item1
        
        let item2 = ESTabBarItem.init(IrregularityBasicContentView(), title: nil, image: UIImage(named: PMImageName.bottom_menu_playdate), selectedImage: UIImage(named: PMImageName.bottom_menu_playdate))
        item2.contentView?.renderingMode = .alwaysOriginal
        v2.tabBarItem = item2
        
        let item3 = ESTabBarItem.init(IrregularityBasicContentView(), title: nil, image: UIImage(named: PMImageName.bottom_menu_chat), selectedImage: UIImage(named: PMImageName.bottom_menu_chat_active))
        item3.contentView?.renderingMode = .alwaysOriginal
        v3.tabBarItem = item3
        
        tabBarController.viewControllers = [v1, v2, v3]
        
        return tabBarController
    }
    
    //------------------------------------------------------
    
    //MARK: RootViewController
    
    func setupSignInOption() {
        
        let controller = signInNC
        AppDelegate.shared.window?.rootViewController = controller
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    func setupRoleSelection() {
        let nc = signInNC
        let loginVC = logInVC
        let pn = phoneNumerVC
        let roleVC = selectRoleVC
        nc.setViewControllers([loginVC, pn, roleVC], animated: true)
        AppDelegate.shared.window?.rootViewController = nc
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    func setupParentProfileSelection() {
        let nc = signInNC
        let loginVC = logInVC
        let pn = phoneNumerVC
        let roleVC = selectRoleVC
        let ppVC = parentProfileVC
        nc.setViewControllers([loginVC, pn, roleVC, ppVC], animated: true)
        AppDelegate.shared.window?.rootViewController = nc
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    func setupAddKidProfileSelection() {
        let nc = signInNC
        let loginVC = logInVC
        let pn = phoneNumerVC
        let roleVC = selectRoleVC
        let ppVC = parentProfileVC
        let addKidVCs = addKidVC
        nc.setViewControllers([loginVC, pn, roleVC, ppVC, addKidVCs], animated: true)
        AppDelegate.shared.window?.rootViewController = nc
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    func setupLocationEnableSelection() {
        let nc = signInNC
        let loginVC = logInVC
        let pn = phoneNumerVC
        let roleVC = selectRoleVC
        let ppVC = parentProfileVC
        let addKidVCs = addKidVC
        let locationEnable = enableLocationVC
        nc.setViewControllers([loginVC, pn, roleVC, ppVC, addKidVCs, locationEnable], animated: true)
        AppDelegate.shared.window?.rootViewController = nc
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    func setupParentPendingForApproval() {
        let nc = signInNC
        let loginVC = logInVC
        let pn = phoneNumerVC
        let roleVC = selectRoleVC
        let ppVC = parentProfileVC
        let addKidVCs = addKidVC
        let locationEnable = enableLocationVC
        let parentPending = parentPendingProfileVC
        nc.setViewControllers([loginVC, pn, roleVC, ppVC, addKidVCs, locationEnable, parentPending], animated: true)
        AppDelegate.shared.window?.rootViewController = nc
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    func setupBabysitterProfile() {
        let nc = signInNC
        let loginVC = logInVC
        let pn = phoneNumerVC
        let roleVC = selectRoleVC
        let bsProfile = bSCreateAccountVC
        nc.setViewControllers([loginVC, pn, roleVC, bsProfile], animated: true)
        AppDelegate.shared.window?.rootViewController = nc
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    func setupBabysitterAvailabilityProfile() {
        let nc = signInNC
        let loginVC = logInVC
        let pn = phoneNumerVC
        let roleVC = selectRoleVC
        let bsProfile = bSCreateAccountVC
        let bsAvailabilityProfile = bSAvailabilityVC
        nc.setViewControllers([loginVC, pn, roleVC, bsProfile, bsAvailabilityProfile], animated: true)
        AppDelegate.shared.window?.rootViewController = nc
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    func setupBabysitterLocationEnableSelection() {
        let nc = signInNC
        let loginVC = logInVC
        let pn = phoneNumerVC
        let roleVC = selectRoleVC
        let bsProfile = bSCreateAccountVC
        let bsAvailabilityProfile = bSAvailabilityVC
        let bsLocationEnable = bSEnableLocationVC
        nc.setViewControllers([loginVC, pn, roleVC, bsProfile, bsAvailabilityProfile, bsLocationEnable], animated: true)
        AppDelegate.shared.window?.rootViewController = nc
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    func setupBabysitterPendingForApproval() {
        let nc = signInNC
        let loginVC = logInVC
        let pn = phoneNumerVC
        let roleVC = selectRoleVC
        let bsProfile = bSCreateAccountVC
        let bsAvailabilityProfile = bSAvailabilityVC
        let bsLocationEnable = bSEnableLocationVC
        let bsPending = gifVC
        
        nc.setViewControllers([loginVC, pn, roleVC, bsProfile, bsAvailabilityProfile, bsLocationEnable, bsPending], animated: true)
        AppDelegate.shared.window?.rootViewController = nc
        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
    func setupLanding() {
        
        PreferenceManager.shared.isAtLocationEnable = false
        
        /*AppDelegate.shared.window?.rootViewController = landingNC
         AppDelegate.shared.window?.makeKeyAndVisible()*/
        
        sideMenuController.hideLeftView()
        tabbarController = landingTC
        sideMenuController.rootViewController = tabbarController
        sideMenuController.leftViewController = menuVC
        let nc = UINavigationController(rootViewController: sideMenuController)
        nc.setNavigationBarHidden(true, animated: false)
        AppDelegate.shared.window?.rootViewController = nc
        AppDelegate.shared.window?.makeKeyAndVisible()
        
        if let pushPayload = AppDelegate.shared.pendingPayload {
            handleRemote(data: pushPayload)
            AppDelegate.shared.pendingPayload = nil
        }
        
        FirebaseManager.shared.startListeningIfUserBlocked()
    }
    
    func setupBabysitterLanding() {
        
        PreferenceManager.shared.isAtLocationEnable = false
        
        let landingsNC = landingNC
        landingsNC.setViewControllers([bSChatVC], animated: false)
        
        sideMenuController.hideLeftView()
        sideMenuController.rootViewController = landingsNC
        sideMenuController.leftViewController = menuVC
        let nc = UINavigationController(rootViewController: sideMenuController)
        nc.setNavigationBarHidden(true, animated: false)
        AppDelegate.shared.window?.rootViewController = nc
        AppDelegate.shared.window?.makeKeyAndVisible()
        
        if let pushPayload = AppDelegate.shared.pendingPayload {
            handleRemote(data: pushPayload)
            AppDelegate.shared.pendingPayload = nil
        }
        
        FirebaseManager.shared.startListeningIfUserBlocked()
    }
    
    func setupSigninUserRootController() {
        
        FirebaseManager.shared.find(parentFrom: PreferenceManager.shared.userId ?? String()) { dict in
            
            let baseModal = BaseModal(fromDictionary: dict)
            
            if baseModal.role == RoleType.parent.rawValue {
                
                let parentModal = ParentsModal(fromDictionary: dict)
                FirebaseManager.shared.parentModal = parentModal
                
                if ValidationManager.shared.isEmpty(text: parentModal.userid) == false {
                    
                    //check if parent profile complete
                    if ValidationManager.shared.isEmpty(text: parentModal.gender) {
                        
                        NavigationManager.shared.setupParentProfileSelection()
                        
                    } else if parentModal.kidsList == nil || parentModal.kidsList.count == .zero {
                        
                        NavigationManager.shared.setupAddKidProfileSelection()
                        
                    } else if ValidationManager.shared.isEmpty(text: parentModal.lattitude) || ValidationManager.shared.isEmpty(text: parentModal.longitude)  {
                        
                        NavigationManager.shared.setupLocationEnableSelection()
                        
                    } else if parentModal.isAdminApproved == "0" || parentModal.isAdminApproved == "2" {
                      
                        NavigationManager.shared.setupParentPendingForApproval()
                        
                    } else {
                        
                        NavigationManager.shared.setupLanding()
                    }
                }
                
            } else if baseModal.role == RoleType.babysitter.rawValue {
                
                let babysitterModal = BabysitterModal(fromDictionary: dict)
                FirebaseManager.shared.babysitterModal = babysitterModal
                
                if ValidationManager.shared.isEmpty(text: babysitterModal.userid) == false {
                    
                    //check if parent profile complete
                    if ValidationManager.shared.isEmpty(text: babysitterModal.gender) {
                        
                        NavigationManager.shared.setupBabysitterProfile()
                        
                    } else if babysitterModal.days.count == .zero {
                        
                        NavigationManager.shared.setupBabysitterAvailabilityProfile()
                        
                    } else if ValidationManager.shared.isEmpty(text: babysitterModal.lattitude) || ValidationManager.shared.isEmpty(text: babysitterModal.longitude)  {
                        
                        NavigationManager.shared.setupBabysitterLocationEnableSelection()
                        
                    } else if babysitterModal.isAdminApproved == "0" || babysitterModal.isAdminApproved == "2" {
                        NavigationManager.shared.setupBabysitterPendingForApproval()
                        
                    } else {
                        NavigationManager.shared.setupBabysitterLanding()
                    }
                }
            } else {
                
                NavigationManager.shared.setupRoleSelection()
            }
        }
    }
    
    func parentProfileSwitchToBabysitter() {
        
        let bsLandingVC = landingBabySitterVC
        if let nc = tabbarController?.viewControllers?.first as? UINavigationController {
            nc.setViewControllers([bsLandingVC], animated: false)
        }
        sideMenuController.hideLeftView()
        sideMenuController.rootViewController = tabbarController
    }
    
    func parentProfileSwitchToPlayDates() {
        
        let landingsVC = landingVC
        if let nc = tabbarController?.viewControllers?.first as? UINavigationController {
            nc.setViewControllers([landingsVC], animated: false)
        }
        sideMenuController.hideLeftView()
        sideMenuController.rootViewController = tabbarController
    }
    
    //------------------------------------------------------
    
    //MARK: Push Notification Redirection
    
    func handleRemote(data: [AnyHashable : Any]) {
        
        guard FirebaseManager.shared.isSignedIn() else { return }
        
        let keyData = "data"
        let keyType = "type"
        
        if let messageType = data[keyType] as? String {
            let fcmNotificationType: FCMNotificationType = FCMNotificationType(rawValue: Int(messageType) ?? .zero) ?? .none
            switch fcmNotificationType {
            case .playdateCreated:
                AppDelegate.shared.pendingPayload = nil
                setupLanding()
                break
            case .playdateGroupRequestSend:
                if let messageString = data[keyData] as? String, let dictionary = messageString.toDictionary() {
                    let messageObj = RequestModal(fromDictionary: dictionary)
                    redirectToPlaydateRequestAdmit(messageObj)
                }
                break
            case .playdateGroupRequestAccepted:
                if let messageString = data[keyData] as? String, let dictionary = messageString.toDictionary() {
                    let messageObj = RequestModal(fromDictionary: dictionary)
                    redirectToPlaydateConversation(messageObj)
                }
                break
            case .playdateGroupRequestRejected:
                //TODO: Nothing to handle at this time
                break
            case .playdateNonGroupRequestSend:
                if let messageString = data[keyData] as? String, let dictionary = messageString.toDictionary() {
                    let messageObj = RequestModal(fromDictionary: dictionary)
                    redirectToPlaydateRequestAdmit(messageObj)
                }
                break
            case .playdateNonGroupRequestAccepted:
                if let messageString = data[keyData] as? String, let dictionary = messageString.toDictionary() {
                    let messageObj = RequestModal(fromDictionary: dictionary)
                    redirectToPlaydateConversation(messageObj)
                }
                break
            case .playdateNonGroupRequestRejected:
                //TODO: Nothing to handle at this time
                break
            case .babysitterRequestSend:
                if let messageString = data[keyData] as? String, let dictionary = messageString.toDictionary() {
                    let messageObj = RequestModal(fromDictionary: dictionary)
                    redirectToAdmitRequest(messageObj)
                }
                break
            case .babysitterRequestAccepted:
                if let messageString = data[keyData] as? String, let dictionary = messageString.toDictionary() {
                    let messageObj = RequestModal(fromDictionary: dictionary)
                    redirectToBabysitterChatConversation(messageObj)
                }
                break
            case .babysitterRequestRejected:
                //TODO: Nothing to handle at this time
                break
            case .message:
                if let messageString = data[keyData] as? String, let dictionary = messageString.toDictionary() {
                    let messageObj = MessageModal(fromDictionary: dictionary)
                    redirectToMessage(messageObj)
                }
                break
            case .babysitterProfileApproved:
                setupSigninUserRootController()
                break
            case .parents:
                break
            case .babysitter:
                break
            case .none:
                break
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Message
    
    func redirectToMessage(_ message: MessageModal) {
        
        if let bottomBarVC = tabbarController {
            bottomBarVC.selectedIndex = 2
            if bottomBarVC.viewControllers?.indices.contains(bottomBarVC.selectedIndex) == true, let nc = bottomBarVC.viewControllers?[bottomBarVC.selectedIndex] as? UINavigationController {
                LoadingManager.shared.showLoading()
                FirebaseManager.shared.findConversation(fromConversationId: message.pOwner) { latestConversationObj in
                    let chatList = self.chatListVC
                    let chatVC = self.chatVC
                    chatVC.conversationModal = latestConversationObj
                    chatVC.isParentProfile = chatList.isParentProfile
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        nc.setViewControllers([chatList, chatVC], animated: true)
                    }
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Admit
    
    func redirectToAdmitRequest(_ request: RequestModal) {
        
        sideMenuController.hideLeftView()
        if let bsChatVC = (sideMenuController.rootViewController as? UINavigationController)?.viewControllers.first as? BSChatVC {
            if request.status == "0" || request.status.isEmpty == true {
                let controller = NavigationManager.shared.admitVC
                controller.requestModal = request
                controller.modalPresentationStyle = .fullScreen
                bsChatVC.present(controller, animated: false, completion: nil)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Playdate Request Admit
    
    func redirectToPlaydateRequestAdmit(_ request: RequestModal) {
        
        if let bottomBarVC = tabbarController {
            bottomBarVC.selectedIndex = 2
            if bottomBarVC.viewControllers?.indices.contains(bottomBarVC.selectedIndex) == true, let nc = bottomBarVC.viewControllers?[bottomBarVC.selectedIndex] as? UINavigationController {
                LoadingManager.shared.showLoading()
                FirebaseManager.shared.findConversation(fromConversationId: request.playdateId) { latestConversationObj in
                    
                    let chatList = self.chatListVC
                    chatList.selectedConversation = latestConversationObj
                    
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        
                        nc.setViewControllers([chatList], animated: true)
                        
                        if request.status == "0" || request.status.isEmpty == true {
                            let controller = NavigationManager.shared.admitVC
                            controller.requestModal = request
                            controller.modalPresentationStyle = .fullScreen
                            chatList.present(controller, animated: false, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    func redirectToPlaydateConversation(_ request: RequestModal) {
        
        if let bottomBarVC = tabbarController {
            bottomBarVC.selectedIndex = 2
            if bottomBarVC.viewControllers?.indices.contains(bottomBarVC.selectedIndex) == true, let nc = bottomBarVC.viewControllers?[bottomBarVC.selectedIndex] as? UINavigationController {
                LoadingManager.shared.showLoading()
                FirebaseManager.shared.findConversation(fromConversationId: request.playdateId) { latestConversationObj in
                    
                    let chatList = self.chatListVC
                    chatList.selectedConversation = latestConversationObj
                    
                    let chatVC = self.chatVC
                    chatVC.conversationModal = latestConversationObj
                    LoadingManager.shared.hideLoading()
                    
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        
                        nc.setViewControllers([chatList, chatVC], animated: true)
                    }
                }
            }
        }
    }
    
    
    //------------------------------------------------------
    
    //MARK: Babysitter Chat Conversation
    
    func redirectToBabysitterChatConversation(_ request: RequestModal) {
        
        if let bottomBarVC = tabbarController {
            bottomBarVC.selectedIndex = 2
            if bottomBarVC.viewControllers?.indices.contains(bottomBarVC.selectedIndex) == true, let nc = bottomBarVC.viewControllers?[bottomBarVC.selectedIndex] as? UINavigationController {
                LoadingManager.shared.showLoading()
                FirebaseManager.shared.findConversation(fromConversationId: request.playdateId) { latestConversationObj in
                    
                    let chatList = self.chatListVC
                    chatList.selectedConversation = latestConversationObj
                    
                    let chatVC = self.chatVC
                    chatVC.conversationModal = latestConversationObj
                    LoadingManager.shared.hideLoading()
                    
                    delay {
                        nc.setViewControllers([chatList, chatVC], animated: true)
                    }
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewControllers
    
    public var loadingIndicatorVC: LoadingIndicatorVC {
        return loaderStoryboard.instantiateViewController(withIdentifier: String(describing: LoadingIndicatorVC.self)) as! LoadingIndicatorVC
    }
    
    public var phoneNumerVC: PhoneNumerVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: PhoneNumerVC.self)) as! PhoneNumerVC
    }
    
    public var logInVC: LogInVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: LogInVC.self)) as! LogInVC
    }
    
    public var otpVC: OtpVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: OtpVC.self)) as! OtpVC
    }
    
    public var selectRoleVC: SelectRoleVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: SelectRoleVC.self)) as! SelectRoleVC
    }
    
    public var parentProfileVC: ParentProfileVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: ParentProfileVC.self)) as! ParentProfileVC
    }
    
    public var addKidVC: AddKidVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: AddKidVC.self)) as! AddKidVC
    }
    
    public var enableLocationVC: EnableLocationVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: EnableLocationVC.self)) as! EnableLocationVC
    }
    
    public var parentPendingProfileVC: ParentPendingProfileVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: ParentPendingProfileVC.self)) as! ParentPendingProfileVC
    }
    
    public var landingVC: LandingVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: LandingVC.self)) as! LandingVC
    }
    
    public var menuVC: MenuVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: MenuVC.self)) as! MenuVC
    }
    
    public var editParentProfileVC: EditParentProfileVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: EditParentProfileVC.self)) as! EditParentProfileVC
    }
    
    public var editNameVC: EditNameVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: EditNameVC.self)) as! EditNameVC
    }
    
    public var settingsVC: SettingsVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: SettingsVC.self)) as! SettingsVC
    }
    
    public var myPlayDateVC: MyPlayDateVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: MyPlayDateVC.self)) as! MyPlayDateVC
    }
    
    public var myKidVC: MyKidVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: MyKidVC.self)) as! MyKidVC
    }
    
    public var editKidVC: EditKidVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: EditKidVC.self)) as! EditKidVC
    }
    
    public var createPlaydatesVC: CreatePlaydatesVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: CreatePlaydatesVC.self)) as! CreatePlaydatesVC
    }
    
    public var myPlaydatesListVC: MyPlaydatesListVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: MyPlaydatesListVC.self)) as! MyPlaydatesListVC
    }
    
    public var kidsProfileVC: KidsProfileVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: KidsProfileVC.self)) as! KidsProfileVC
    }
    
    public var chatListVC: ChatListVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: ChatListVC.self)) as! ChatListVC
    }
    
    public var chatDetailsVC: ChatDetailsVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: ChatDetailsVC.self)) as! ChatDetailsVC
    }
    
    public var deleteVC: DeleteVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: DeleteVC.self)) as! DeleteVC
    }
    
    public var loadingPlayDatesVC: LoadingPlayDatesVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: LoadingPlayDatesVC.self)) as! LoadingPlayDatesVC
    }
    
    public var pDRequestGroupVC: PDRequestGroupVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: PDRequestGroupVC.self)) as! PDRequestGroupVC
    }
    
    public var pDRequestVC: PDRequestVC {
        return mainStoryboard.instantiateViewController(withIdentifier: String(describing: PDRequestVC.self)) as! PDRequestVC
    }
    
    public var bSRatingVC: BSRatingVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: BSRatingVC.self)) as! BSRatingVC
    }
    
    public var bSCreateAccountVC: BSCreateAccountVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: BSCreateAccountVC.self)) as! BSCreateAccountVC
    }
    
    public var bSAvailabilityVC: BSAvailabilityVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: BSAvailabilityVC.self)) as! BSAvailabilityVC
    }
    
    public var bSEnableLocationVC: BSEnableLocationVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: BSEnableLocationVC.self)) as! BSEnableLocationVC
    }
    
    public var gifVC: GifVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: GifVC.self)) as! GifVC
    }
    
    public var editBabysitterProfileVC: EditBabysitterProfileVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: EditBabysitterProfileVC.self)) as! EditBabysitterProfileVC
    }
    
    public var bSPickDateVC: BSPickDateVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: BSPickDateVC.self)) as! BSPickDateVC
    }
    
    public var locationSearchVC: LocationSearchVC {
        return locationSearchStoryboard.instantiateViewController(withIdentifier: String(describing: LocationSearchVC.self)) as! LocationSearchVC
    }
    
    public var bSMenuVC: BSMenuVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: BSMenuVC.self)) as! BSMenuVC
    }
    
    public var landingBabySitterVC: LandingBabySitterVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: LandingBabySitterVC.self)) as! LandingBabySitterVC
    }
    
    public var bSSettingVC: BSSettingVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: BSSettingVC.self)) as! BSSettingVC
    }
    
    public var babySitterProfileVC: BabySitterProfileVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: BabySitterProfileVC.self)) as! BabySitterProfileVC
    }
    
    public var requestGroupChatVC: RequestGroupChatVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: RequestGroupChatVC.self)) as! RequestGroupChatVC
    }
    
    public var bSChatVC: BSChatVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: BSChatVC.self)) as! BSChatVC
    }
    
    public var bSRequestVC: BSRequestVC {
        return babySitterStoryboard.instantiateViewController(withIdentifier: String(describing: BSRequestVC.self)) as! BSRequestVC
    }
    
    public var sendRequestVC: SendRequestVC {
        return SendRequestVC(nibName: String(describing: SendRequestVC.self), bundle: Bundle.main)
    }
    
    public var admitVC: AdmitVC {
        return AdmitVC(nibName: String(describing: AdmitVC.self), bundle: Bundle.main)
    }
    
    public var chatVC: ChatVC {
        return ChatVC(nibName: String(describing: ChatVC.self), bundle: Bundle.main)
    }
    
    /*public var signInOptionsVC: SignInOptionsVC {
     return mainStoryboard.instantiateViewController(withIdentifier: String(describing: SignInOptionsVC.self)) as! SignInOptionsVC
     }*/
    
    public var sendBabysitterRequestVC: SendBabysitterRequestVC {
        return SendBabysitterRequestVC(nibName: String(describing: SendBabysitterRequestVC.self), bundle: Bundle.main)
    }
    
    //------------------------------------------------------
}

