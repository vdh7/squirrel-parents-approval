//
//  PreferenceManager.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 25/08/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import AssistantKit

class PreferenceManager: NSObject {

    public static var shared = PreferenceManager()
    
    private let userDefault = UserDefaults.standard
       
    //------------------------------------------------------
    
    //MARK: Settings
    
    var userBaseURL: String {
        return ""
    }
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    private let keyDeviceToken = "deviceToken"
    private let keyUserId = "userId"
    private let keyVerificationId = "verificationId"
    
    private let keyIsAtRole = "isAtRole"
    private let keyIsAtParentProfile = "isAtParentProfile"
    private let keyIsAtAddKid = "isAtKid"
    private let keyIsAtLocationEnable = "isAtLocationEnable"
    private let keyIsAtBabysitterProfile = "isAtBabysitterProfile"
    private let keyIsAtBabysitterAvailabilityProfile = "isAtBabysitterAvailabilityProfile"
    private let keyIsAtBabysitterLocationEnable = "isAtBabysitterLocationEnable"
    private let keyIsInstalled = "isInstalled"
    
    private let keyFcmToken = "fcmToken"
    private let keyCanceledPlaydates = "canceledPlaydates"
    private let keyCanceledBabysitter = "canceledBabysitter"
    
    private let keyPhone = "phone"
    
    var deviceToken: String? {
        set {
            if newValue != nil {
                userDefault.set(newValue!, forKey: keyDeviceToken)
            } else {
                userDefault.removeObject(forKey: keyDeviceToken)
            }
            userDefault.synchronize()
        }
        get {
            let token = userDefault.string(forKey: keyDeviceToken)
            if token?.isEmpty == true || token == nil {
                return Device.versionCode
            } else {
                return userDefault.string(forKey: keyDeviceToken)
            }            
        }
    }
    
    var fcmToken: String? {
        set {
            if newValue != nil {
                userDefault.set(newValue!, forKey: keyFcmToken)
            } else {
                userDefault.removeObject(forKey: keyFcmToken)
            }
            userDefault.synchronize()
        }
        get {
            let token = userDefault.string(forKey: keyFcmToken)
            if token?.isEmpty == true || token == nil {
                return Device.versionCode
            } else {
                return userDefault.string(forKey: keyFcmToken)
            }
        }
    }

    var userId: String? {
        set {
            if newValue != nil {
                userDefault.set(newValue!, forKey: keyUserId)
            } else {
                userDefault.removeObject(forKey: keyUserId)
            }
            userDefault.synchronize()
        }
        get {            
            return userDefault.string(forKey: keyUserId)
        }
    }
    
    var isInstalled: Bool {
        set {
            userDefault.set(newValue, forKey: keyIsInstalled)
            userDefault.synchronize()
        }
        get {
            return userDefault.bool(forKey: keyIsInstalled)
        }
    }
    
    var verificationId: String? {
        set {
            if newValue != nil {
                userDefault.set(newValue!, forKey: keyVerificationId)
            } else {
                userDefault.removeObject(forKey: keyVerificationId)
            }
            userDefault.synchronize()
        }
        get {
            return userDefault.string(forKey: keyVerificationId)
        }
    }
    
    var isAtRoleSelection: Bool {
        set {
            userDefault.set(newValue, forKey: keyIsAtRole)
            userDefault.synchronize()
        }
        get {
            return userDefault.bool(forKey: keyIsAtRole)
        }
    }
    
    var isAtParentProfileSelection: Bool {
        set {
            if newValue == true {
                isAtBabysitterProfile = false
                isAtBabysitterAvailabilityProfile = false
                isAtBabysitterLocationEnabled = false
                
                isAtRoleSelection = false
                
            }            
            userDefault.set(newValue, forKey: keyIsAtParentProfile)
            userDefault.synchronize()
        }
        get {
            return userDefault.bool(forKey: keyIsAtParentProfile)
        }
    }
    
    var isAtAddKid: Bool {
        set {
            if newValue == true {
                isAtRoleSelection = false
                isAtParentProfileSelection = false
            }
            userDefault.set(newValue, forKey: keyIsAtAddKid)
            userDefault.synchronize()
        }
        get {
            return userDefault.bool(forKey: keyIsAtAddKid)
        }
    }
    
    var isAtLocationEnable: Bool {
        set {
            if newValue == true {
                isAtRoleSelection = false
                isAtParentProfileSelection = false
                isAtAddKid = false
            }
            userDefault.set(newValue, forKey: keyIsAtLocationEnable)
            userDefault.synchronize()
        }
        get {
            return userDefault.bool(forKey: keyIsAtLocationEnable)
        }
    }
    
    var isAtBabysitterProfile: Bool {
        set {
            if newValue == true {
                isAtParentProfileSelection = false
                isAtAddKid = false
                isAtLocationEnable = false
                
                isAtRoleSelection = false
            }
            userDefault.set(newValue, forKey: keyIsAtBabysitterProfile)
            userDefault.synchronize()
        }
        get {
            return userDefault.bool(forKey: keyIsAtBabysitterProfile)
        }
    }
    
    var isAtBabysitterAvailabilityProfile: Bool {
        set {
            if newValue == true {
                isAtRoleSelection = false
                isAtBabysitterProfile = false
            }
            userDefault.set(newValue, forKey: keyIsAtBabysitterAvailabilityProfile)
            userDefault.synchronize()
        }
        get {
            return userDefault.bool(forKey: keyIsAtBabysitterAvailabilityProfile)
        }
    }
    
    var isAtBabysitterLocationEnabled: Bool {
        set {
            if newValue == true {
                isAtRoleSelection = false
                isAtBabysitterProfile = false
                isAtBabysitterAvailabilityProfile = false
            }
            userDefault.set(newValue, forKey: keyIsAtBabysitterLocationEnable)
            userDefault.synchronize()
        }
        get {
            return userDefault.bool(forKey: keyIsAtBabysitterLocationEnable)
        }
    }
    
    var blockedPlaydates: [String] {
        set {
            userDefault.set(newValue, forKey: keyCanceledPlaydates)
            userDefault.synchronize()
        }
        get {
            return userDefault.object(forKey: keyCanceledPlaydates) as? [String] ?? []
        }
    }
    
    var blockedBabysitter: [String] {
        set {
            userDefault.set(newValue, forKey: keyCanceledBabysitter)
            userDefault.synchronize()
        }
        get {
            return userDefault.object(forKey: keyCanceledBabysitter) as? [String] ?? []
        }
    }
    
    func reset() {
        let domain = Bundle.main.bundleIdentifier!
        userDefault.removePersistentDomain(forName: domain)
        userDefault.synchronize()
    }
    
    var phone: String? {
        set {
            if newValue != nil {
                userDefault.set(newValue!, forKey: keyPhone)
            } else {
                userDefault.removeObject(forKey: keyPhone)
            }
            userDefault.synchronize()
        }
        get {
            return userDefault.string(forKey: keyPhone)
        }
    }
    
    //------------------------------------------------------
}
