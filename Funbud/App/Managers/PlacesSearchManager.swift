//
//  PlacesSearchManager.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 05/03/22.
//  Copyright © 2022 dharmesh. All rights reserved.
//

import Foundation
import GooglePlaces

fileprivate let apiKey = "AIzaSyBGcPyVrnH2Se2RpOy-TZHI9ewkX5oKV0s"

class PlacesSearchManager: NSObject, GMSAutocompleteViewControllerDelegate {
    
    let configuration: AutocompleteConfiguration = {
      let fields: [GMSPlaceField] = [
        .name, .placeID, .plusCode, .coordinate, .openingHours, .phoneNumber, .formattedAddress,
        .rating, .userRatingsTotal, .priceLevel, .types, .website, .viewport, .addressComponents,
        .photos, .utcOffsetMinutes, .businessStatus, .iconImageURL, .iconBackgroundColor,
      ]
      return AutocompleteConfiguration(
        autocompleteFilter: GMSAutocompleteFilter(),
        placeFields: GMSPlaceField(rawValue: fields.reduce(0) { $0 | $1.rawValue }))
    }()
    
    //------------------------------------------------------
    
    //MARK: Shared
    
    public static let shared = PlacesSearchManager()
    
    typealias autoCompleteSuccess = (_ ac: GMSAutocompleteViewController, _ place: GMSPlace)->Void
    typealias autoCompleteFailure = (_ ac: GMSAutocompleteViewController, _ error: Error?)->Void
    
    var autoCompletionSuccessBlock: autoCompleteSuccess?
    var autoCompletionErrorBlock: autoCompleteFailure?
    
    //------------------------------------------------------
    
    //MARK: Public
    
    public func getPostalCode(fromPlace place: GMSPlace) -> String {
        
        let postalcodeCmp = place.addressComponents?.first(where: { cmp in
            if cmp.types.contains("postal_code") {
                return true
            }
            return false
        })
        return postalcodeCmp?.name ?? String()
    }
    
    public func getAddressFrom(latitude lat: Double, longitude lon: Double, completion: @escaping( _ address: String?, _ error: Error?)->Void) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
            
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
                
        ceo.reverseGeocodeLocation(loc, completionHandler:
                                    {(placemarks, error) in
            
            if let error = error {
                completion(nil, error)
            } else {
                if let pm = placemarks?.first {
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    debugLog(object: addressString)
                    completion(addressString, nil)
                } else {
                    completion(nil, nil)
                }
            }
        })
    }
    
    public func showAutoComplete(_ nc: UINavigationController?, didAutoCompleteWith successBlock: @escaping autoCompleteSuccess, failureBlock: @escaping autoCompleteFailure) {
        
        /*let viewController = AutocompleteWithSearchViewController.init()
        viewController.autocompleteConfiguration = configuration
        nc?.pushViewController(viewController, animated: true)*/
        
        autoCompletionSuccessBlock = successBlock
        autoCompletionErrorBlock = failureBlock
        
        let autocompleteViewController = GMSAutocompleteViewController()
        autocompleteViewController.delegate = self
        autocompleteViewController.autocompleteFilter = configuration.autocompleteFilter
        autocompleteViewController.placeFields = configuration.placeFields
        nc?.present(autocompleteViewController, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: GMSAutocompleteViewControllerDelegate
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        autoCompletionSuccessBlock?(viewController, place)
    }
    
    func viewController(
        _ viewController: GMSAutocompleteViewController,
        didFailAutocompleteWithError error: Error
    ) {
        autoCompletionErrorBlock?(viewController, error)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        
        autoCompletionErrorBlock?(viewController, nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        
        debugLog(object: "Request autocomplete predictions")
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        
        debugLog(object: "Updated autocomplete predictions.")
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    override init() {
        super.init()
        
        GMSPlacesClient.provideAPIKey(apiKey)
    }
    
    //------------------------------------------------------
}


