//
//  DateTimeManager.swift
//  Funbud
//
//  Created by Dharmesh Avaiya on 6/24/20.
//  Copyright © 2020 dharmesh. All rights reserved.
//

import UIKit

public struct DateFormate {
    
    static let UTC = "yyyy-MM-dd'T'HH:mm:ss"
    static let MMM_DD_COM_yyyy = "dd-MM-YYYY"
    static let dayName = "EEEE, MMM d, yyyy"
    static let HH_MM = "hh:mm a"
    static let MMM_DD_COM_yyyy_HH_MM = "dd-MM-YYYY hh:mm a"
    static let DAY = "dd"
    static let MONTH = "MMM"
    static let HH_MM_24 = "HH:mm"
    static let yyyy_MM_dd = "yyyy-MM-dd"
    static let HH_mm_ss = "HH:mm:ss"
    static let UTC_SPACE = "yyyy-MM-dd HH:mm:ss"
    static let yyyy_MM_dd_HH_mm = "yyyy-MM-dd HH:mm"
}

public struct TimeFormate {
    
    static let HH_mm = "HH:mm"
    static let HH_MM = "hh:mm a"
}

class DateTimeManager: NSObject {
    
    let dateFormatter = DateFormatter()
    
    //------------------------------------------------------
    
    //MARK: Shared
    
    static let shared = DateTimeManager()
    
    //------------------------------------------------------
    
    //MARK: Public
    
    func isToday(_ date: Date) -> Bool {
        return Calendar.current.isDateInToday(date)
    }
    
    func isYesterday(_ date: Date) -> Bool {
        return Calendar.current.isDateInYesterday(date)
    }
    
    func dateFrom(unix: Int) -> Date {
        print(unix)
        return Date(timeIntervalSince1970: TimeInterval(unix))
    }
    
    func dateFrom(unix: Int, inFormate: String) -> String {
        let date = dateFrom(unix: unix)
        dateFormatter.dateFormat = inFormate
        return dateFormatter.string(from: date)
    }
    
    func stringFrom(date: Date, inFormate: String) -> String {
        dateFormatter.dateFormat = inFormate
        return dateFormatter.string(from: date)
    }
    
    func timeFrom(unix: Int, inFormate: String) -> String {
        let date = dateFrom(unix: unix)
        dateFormatter.dateFormat = inFormate
        return dateFormatter.string(from: date)
    }
    
    func stringTimeFrom(date: Date, inFormate: String) -> String {
        dateFormatter.dateFormat = inFormate
        return dateFormatter.string(from: date)
    }
    
    func dateFrom(stringDate: String, inFormate: String) -> Date? {
        dateFormatter.dateFormat = inFormate
        return dateFormatter.date(from: stringDate)
    }
     
    func localToUTC(dateStr: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = Calendar.current
        dateFormatter.timeZone = TimeZone.current
        
        if let date = dateFormatter.date(from: dateStr) {
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            dateFormatter.dateFormat = "H:mm:ss"
        
            return dateFormatter.string(from: date)
        }
        return nil
    }

    func utcToLocal(dateStr: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let date = dateFormatter.date(from: dateStr) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "h:mm a"
        
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    func utcToLocal(dateStr: String, formate: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")        
        if let date = dateFormatter.date(from: dateStr) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = formate
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    func date(byAddingYear year: Int) -> Date? {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        var component = DateComponents()
        component.year = year
        return calendar.date(byAdding: component, to: Date())
    }
    
    func date(byAddingHours hours: Int, toDate: Date) -> Date? {
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        var component = DateComponents()
        component.hour = hours
        return calendar.date(byAdding: component, to: toDate)
    }
    
    func getTodaysHour() -> Int {
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        return hour
    }
    
    func getWeekDayOf(date: Date) -> Int {
        let weekDay = Calendar.current.component(.weekday, from: date) - 1
        if weekDay == .zero {
            return 7
        } else {
            return weekDay
        }        
    }
    
    override init() {
        super.init()
    
        dateFormatter.locale = Locale(identifier: "en_GB")
    }
    
    //------------------------------------------------------
}
