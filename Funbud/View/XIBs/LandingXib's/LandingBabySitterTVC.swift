//
//  LandingBabySitterTVC.swift
//  Funbud
//
//  Created by Vivek Dharmani on 20/09/21.
//  Copyright © 2021 dharmesh. All rights reserved.
//

import UIKit
import SDWebImage

class LandingBabySitterTVC: UITableViewCell {

    @IBOutlet weak var bs_ProfileImg: UIImageView!
    @IBOutlet weak var lbl_bs_Name: PoppinsBoldLabel!
    @IBOutlet weak var lbl_bs_Desp: PoppinsRegularLabel!
    @IBOutlet weak var btnRating: PoppinsBoldButton!
    @IBOutlet weak var lbl_bs_hours: PoppinsSemiBoldLabel!
    @IBOutlet weak var lbl_bs_distance: PoppinsSemiBoldLabel!

    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup(babysitterModal: BabysitterModal) {
        
        if let imageURL = babysitterModal.imgsource {
            bs_ProfileImg.sd_setIndicatorStyle(UIActivityIndicatorView.Style.medium)
            bs_ProfileImg.sd_addActivityIndicator()
            bs_ProfileImg.sd_showActivityIndicatorView()
            bs_ProfileImg.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: [.refreshCached, .avoidAutoSetImage]) { (img: UIImage?, error: Error?, cache: SDImageCacheType, finalURL: URL?) in
                self.bs_ProfileImg.image = img
                self.bs_ProfileImg.sd_removeActivityIndicator()
            }
        } else {
            bs_ProfileImg.image = UIImage(named: PMImageName.placeholder)
        }
        
        lbl_bs_Name.text = babysitterModal.getNameWithAge()
        
        lbl_bs_Desp.text = babysitterModal.location
        
        lbl_bs_hours.text = babysitterModal.displayHours()
        
        lbl_bs_distance.text = babysitterModal.getDistnaceInKm()
        
        btnRating .setTitle(babysitterModal.displayUserRating(), for: .normal)
    }
    
    //------------------------------------------------------
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
            
    //------------------------------------------------------
}
